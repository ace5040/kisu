/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/**
 * @fileOverview The "doclink" plugin provides an editor command that
 *               allows selecting the entire content of editable area.
 *               This plugin also enables a toolbar button for the feature.
 */

( function() {
  var dialogObg;
  var $ = jQuery;
	CKEDITOR.plugins.add( 'doclink', {
    requires: ['dialog'],
		// jscs:disable maximumLineLength
		//lang: 'af,ar,az,bg,bn,bs,ca,cs,cy,da,de,de-ch,el,en,en-au,en-ca,en-gb,eo,es,es-mx,et,eu,fa,fi,fo,fr,fr-ca,gl,gu,he,hi,hr,hu,id,is,it,ja,ka,km,ko,ku,lt,lv,mk,mn,ms,nb,nl,no,oc,pl,pt,pt-br,ro,ru,si,sk,sl,sq,sr,sr-latn,sv,th,tr,tt,ug,uk,vi,zh,zh-cn', // %REMOVE_LINE_CORE%
		// jscs:enable maximumLineLength
		icons: 'doclink', // %REMOVE_LINE_CORE%
		hidpi: true, // %REMOVE_LINE_CORE%
		init: function( editor ) {
      var selectedLink = {};
      dialogObj = CKEDITOR.dialog.add( 'addlinkDialog', function( editor ) {
          return {
              title:          'Додати посилання на документ',
              resizable:      CKEDITOR.DIALOG_RESIZE_BOTH,
              minWidth:       500,
              minHeight:      400,
              contents: [
                  {
                      id:         'tab1',
                      label:      'tab1',
                      title:      'tab1',
                      elements: [
                          {
                              type: 'text',
                              label: 'Пошук документа:',
                              className: 'docsearch',
                              id: 'docsearch'
                          },
                          {
                              type: 'html',
                              html: '<div class="select-wrapper" style="position:relative;width:526px;"><label>Знайдено:</label><select style="width:526px;" class="foundLinks"></select></div>'
                          },
                          {
                              type: 'html',
                              html: '<div class="select-wrapper revisions-ids-wrapper" style="position:relative;width:526px;"><label>Редакція:</label><select style="width:526px;" class="revisionsIds"></select></div>'
                          }

                      ]
                  }
              ],
              onShow: function() {
                var requests = [];
                var links = [];
                $foundLinks = $('.foundLinks');
                $revisionsIds = $('.revisionsIds');

                $revisionsIds.on('change', function(){
                  selectedLink.rev = $revisionsIds.val();
                });

                $foundLinks.on('change', function(){
                  selectedLink = links[$foundLinks.val()];
                  var type = $(".foundLinks option:selected").data('type');

                  if ( type == 'norm' ) {

                    var self = this;
                    var formdata = new FormData();

                    var params = {
                      nid: selectedLink.id
                    };

                    formdata.append('command', 'getNormRevisions');
                    formdata.append('params', JSON.stringify(params));

                    _.each(requests, function(request){
                      request.id.abort();
                    });

                    requests.push(
                      {
                        id: $.ajax({
                          url: '/system/request',
                          data: formdata,
                          processData: false,
                          contentType: false,
                          method: 'POST',
                          success: function ( response ) {
                            if ( response && response.error === 0 ) {
                              $revisionsIds.chosen("destroy");
                              $revisionsIds.html('');
                              $('.revisions-ids-wrapper').css('opacity', 1);
                              var revs = response.data.list;
                              $('<option selected="selected" value="0">Остання</option>').appendTo($revisionsIds);
                              for(i in revs) {
                                var rev = response.data.list[i];
                                $('<option value="' + rev.id + '">(' + rev.id + ') ' + rev.date + '</option>').appendTo($revisionsIds);
                              }
                              $revisionsIds.chosen();
                              $('.revisions-ids-wrapper .chosen-container .chosen-results').css('max-height', '180px');

                              selectedLink.rev = 0;
                            } else {
                              console.info(response.error, response.errorText);
                            }
                            $revisionsIds.trigger('change');
                          }
                        }),
                        command: 'getNormRevisions'
                      }
                    );

                  } else {
                    $('.revisions-ids-wrapper').css('opacity', 0);
                  }

                });

                $('.docsearch input').off('keyup').on('keyup', function(){

                  var searchKey = $('.docsearch input').val();

                  var self = this;
                  var formdata = new FormData();

                  var params = {
                    filters: {
                      key: searchKey
                    }
                  };

                  formdata.append('command', 'getSearchLinks');
                  formdata.append('params', JSON.stringify(params));

                  _.each(requests, function(request){
                    request.id.abort();
                  });

                  requests.push(
                    {
                      id: $.ajax({
                        url: '/system/request',
                        data: formdata,
                        processData: false,
                        contentType: false,
                        method: 'POST',
                        success: function ( response ) {
                          if ( response && response.error === 0 ) {
                            $foundLinks.chosen("destroy");
                            $foundLinks.html('');
                            links = response.data.list;
                            for(i in links) {
                              var link = response.data.list[i];
                              $('<option data-url="' + link.url + '" data-type="' + link.type + '" value="' + i + '">(' + link.id + ') ' + link.typeName + ' - ' + link.title + '</option>').appendTo($foundLinks);
                            }
                            $foundLinks.chosen();
                            selectedLink = links[0];
                          } else {
                            console.info(response.error, response.errorText);
                          }
                          $('.revisions-ids-wrapper').css('opacity', 0);
                          $foundLinks.trigger('change');
                        }
                      }),
                      command: 'getSearchLinks'
                    }
                  );

                });

              },
              onOk: function() {
                  var dialog = this;
                  var html = '';
                  var text = '';
                  var mySelection = editor.getSelection();

                  if (CKEDITOR.env.ie) {
                      mySelection.unlock(true);
                      text = mySelection.getNative().createRange().text;
                  } else {
                      text = mySelection.getSelectedText();
                  }
                  if ( typeof text === 'undefined' || text === null || text.toString().trim().length == 0 ) {
                    text = selectedLink.title;
                  }
                  if ( typeof selectedLink.rev !== 'undefined' && selectedLink.rev != 0 ) {
                    html = '<a target="_blank" href="' + selectedLink.url  +  '/' + selectedLink.rev + '">' + text + '</a>';
                  } else {
                    html = '<a target="_blank" href="' + selectedLink.url + '">' + text + '</a>';
                  }
                  editor.insertHtml(html);

              }
          }
      } );

      editor.addCommand( 'docLink', new CKEDITOR.dialogCommand( 'addlinkDialog' ) );

			editor.ui.addButton && editor.ui.addButton( 'doclink', {
				label: 'Doclink',
				command: 'docLink',
				toolbar: 'selection,10'
			} );
		}
	} );
} )();
