( function() {
	CKEDITOR.plugins.add( 'cleanup', {
    icons: 'cleanup',
    init: function( editor ) {
			editor.addCommand( 'cleanup', { modes: { wysiwyg: 1, source: 1 },
				exec: function( editor ) {
					var editable = editor.editable();

          var html = '';

          var mySelection = editor.getSelection();
          var fragment;
          if (CKEDITOR.env.ie) {
              mySelection.unlock(true);
              fragment = mySelection.getRanges()[0].cloneContents().$;
          } else {
              fragment = mySelection.getRanges()[0].cloneContents().$;
          }

          var div = document.createElement("div");
          div.appendChild(fragment);
          html = div.innerHTML;
					var wholeDocument = false;
					if ( html.length == 0 ) {
						html = editor.getData();
						wholeDocument = true;
					}

          if ( html.length ) {

            jQuery.ajax({
              url: '/cleanup',
              method: 'POST',
              data: {
                html: JSON.stringify(html)
              },
              success: function(data){
                if ( data && data.html && data.html.length ) {
									if ( wholeDocument ) {
										editor.setData(data.html);
									} else {
										editor.insertHtml(data.html);
									}
                }
              }
            });

          }

				},
				canUndo: false
			} );

			editor.ui.addButton && editor.ui.addButton( 'cleanup', {
				label: 'Clean up',
				command: 'cleanup'
			} );
		}
	} );
} )();
