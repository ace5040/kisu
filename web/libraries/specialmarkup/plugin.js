CKEDITOR.plugins.add( 'specialmarkup', {
  requires : ['richcombo'],

  init: function( editor )
  {

    var config = editor.config;
    editor.ui.addRichCombo( 'specialmarkup',
      {
        label : "Виділення", //label displayed in toolbar
        title : 'Виділення',//popup text when hovering over the dropdown
        multiSelect : false,

        //use the same style as the font/style dropdowns
        panel :
        {
           css: [ CKEDITOR.skin.getPath( 'editor' ) ].concat( config.contentsCss ),
        },
        init : function()
        {
          //start group in the dropdown
          //this.startGroup( 'Выделения' );
          //VALUE - The value we get when the row is clicked
          //HTML - html/plain text that should be displayed in the dropdown
          //TEXT - displayed in popup when hovered over the row.
          //this.add( VALUE, HTML, TEXT );
          //add row to the first group
          this.add( 'term', '<div class="style-block style-block-term"><h3>' + config.selection_text_term + '</h3></div>', config.selection_text_term );
          this.add( 'suggestion', '<div class="style-block style-block-suggestion"><h3>' + config.selection_text_suggestion + '</h3></div>', config.selection_text_suggestion );
          this.add( 'file', '<div class="style-block style-block-file"><h3>' + config.selection_text_file + '</h3></div>', config.selection_text_file );
          this.add( 'important', '<div class="style-block style-block-important"><h3>' + config.selection_text_important + '</h3></div>', config.selection_text_important );
          this.add( 'law', '<div class="style-block style-block-law"><h3>' + config.selection_text_law + '</h3></div>', config.selection_text_law );
          this.add( 'quote', '<div class="style-block style-block-quote"><h3>' + config.selection_text_quote + '</h3></div>', config.selection_text_quote );
          //start another group in the dropdown
          //this.startGroup( 'Заголовки' );
          //add row to the second group.
          //this.add( "444","No HTML Here", "666" );

          //we can also set the initial value that the dropdown takes
          //when it is clicked for the first time.
          // Default value on first click
         // this.setValue("444", "No HTML Here");
        },
        //this function is called when a row is clicked
        onClick: function( value ) {
          var html = '';
          var text = '';
          var mySelection = editor.getSelection();

          if (CKEDITOR.env.ie) {
              mySelection.unlock(true);
              text = mySelection.getNative().createRange().text;
          } else {
              text = mySelection.getNative();
          }

          switch ( value ) {
            case 'term':
              html = '<div class="style-block style-block-term"><h3>' + config.selection_text_term + '</h3><p>' + text + '</p></div>';
            break;
            case 'suggestion':
              html = '<div class="style-block style-block-suggestion"><h3>' + config.selection_text_suggestion + '</h3><p>' + text + '</p></div>';
            break;
            case 'file':
              html = '<div class="style-block style-block-file"><h3>' + config.selection_text_file + '</h3><p>' + text + '</p></div>';
            break;
            case 'important':
              html = '<div class="style-block style-block-important"><h3>' + config.selection_text_important + '</h3><p>' + text + '</p></div>';
            break;
            case 'law':
              html = '<div class="style-block style-block-law"><h3>' + config.selection_text_law + '</h3><p>' + text + '</p></div>';
            break;
            case 'quote':
              html = '<div class="style-block style-block-quote"><h3>' + config.selection_text_quote + '</h3><p>' + text + '</p></div>';
            break;
          }
          editor.insertHtml(html);
        },
      });
    // End of richCombo element
  }
});
