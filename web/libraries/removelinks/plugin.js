﻿/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/**
 * @fileOverview The "removelinks" plugin provides an editor command that
 *               allows selecting the entire content of editable area.
 *               This plugin also enables a toolbar button for the feature.
 */

( function() {
	CKEDITOR.plugins.add( 'removelinks', {
		// jscs:disable maximumLineLength
		lang: 'en,ru,uk', // %REMOVE_LINE_CORE%
		// jscs:enable maximumLineLength
		icons: 'removelinks', // %REMOVE_LINE_CORE%
		hidpi: true, // %REMOVE_LINE_CORE%
		init: function( editor ) {
			editor.addCommand( 'removeLinks', { modes: { wysiwyg: 1, source: 1 },
				exec: function( editor ) {

          var data = editor.getData();

          data = data.replace( /<\/*a((>)|( [^>]*>))/gi, "");

          editor.setData(data);

				},
				canUndo: false
			} );

			editor.ui.addButton && editor.ui.addButton( 'removelinks', {
				label: editor.lang.removelinks.toolbar,
				command: 'removeLinks',
				toolbar: 'selection,10'
			} );
		}
	} );
} )();
