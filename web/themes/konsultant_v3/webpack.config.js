const webpack = require('webpack')
const { VueLoaderPlugin } = require('vue-loader')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const WebpackShellPlugin = require('webpack-shell-plugin-next')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const { WebpackManifestPlugin } = require('webpack-manifest-plugin')
const IgnoreEmitPlugin = require('ignore-emit-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  stats: {
    builtAt: true,
    modules: false,
    assets: false,
    entrypoints: false,
    hash: false,
    version: false
  },
  entry: {
    app: {
      import: './src/main.ts',
      filename: 'js/[name].[fullhash].js',
    },
    icons: {
      import: './src/icons/font.icons',
      filename: 'css/[name].ignore.css',
    },
    style: {
      import: './src/styles/style.scss',
      filename: 'css/[name].ignore.css',
    },
    pdf: {
      import: './src/styles/pdf.scss',
      filename: 'css/[name].ignore.css',
    },
    ckeditor: {
      import: './src/styles/ckeditor.scss',
      filename: 'css/[name].ignore.css',
    },
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.vue'],
  },
  mode: 'development',
  output: {
    path: __dirname + '/dist',
    chunkFilename: 'chunks/[name].[fullhash].js',
    assetModuleFilename: 'images/[name].[hash][ext]'
  },
  target: ['browserslist'],
  //devtool: 'eval-source-map',
  devtool: false,
  module: {

    rules: [

      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use:[
          {
            loader: 'ts-loader',
            options: {
              appendTsSuffixTo: [/\.vue$/],
            }
          }
        ]
      },

      {
        test: /\.js$/,
        exclude: /node_modules\/underscore/,
        //exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },

      {
        test: /\.vue$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'vue-loader',
            options: {
              sourceMap: false,
            }
          }
        ]
      },

      {
        test: /\.vue$/,
        include: [/node_modules\/vue-pdf/, /node_modules\/vue-resize-sensor/],
        use: [
          {
            loader: 'vue-loader',
            options: {
              sourceMap: false,
            }
          }
        ]
      },

      {
        test: /\.css$/,
        include: [/node_modules\/vue-pdf/, /node_modules\/vue-resize-sensor/],
        use: [
          {
            loader: 'vue-style-loader',
            options: {
              // sourceMap: true
            }
          },
          {
            loader: 'css-loader',
            options: {
              // sourceMap: true
            }
          }
        ]
      },

      {
        test: /\.scss$/,
        exclude: /node_modules/,
        include: /styles/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            // options: {
            //   esModule: false,
            // }
          },
          {
            loader: 'css-loader',
            options: {
              // sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              // sourceMap: true
            }
          }
        ]
      },

      {
        test: /\.scss$/,
        exclude: [/node_modules/, /styles/],
        use: [
          {
            loader: 'vue-style-loader',
            options: {
              // sourceMap: true
            }
          },
          {
            loader: 'css-loader',
            options: {
              // sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              // sourceMap: true
            }
          }
        ]
      },

      {
        test: /\.css$/,
        include: [/src\/games\/minesweeper/],
        exclude: /node_modules/,
        use: [
          {
            loader: 'vue-style-loader',
            options: {
              // sourceMap: true
            }
          },
          {
            loader: 'css-loader',
            options: {
              // sourceMap: true
            }
          }
        ]
      },

      {
        test: /\.css$/,
        exclude: [/styles/, /\/games\/minesweeper/],
        use: [
          {
            loader: 'css-loader',
            options: {
              // sourceMap: true
            }
          }
        ]
      },

      {
        test: /\.(png|jpg)$/,
        exclude: /node_modules/,
        type: 'asset/resource'
      },

      {
        test: /\.(woff|woff2|eot|ttf|svg)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
              sourceMap: false
            }
          }
        ]
      },

      {
        test: /\.icons$/,
        exclude: /node_modules/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            // options: {
            //   esModule: false,
            // },
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: false,
              url: false
            }
          },
          {
            loader: 'webfonts-loader',
            options: {
              sourceMap: false,
              publicPath: '..'
            }

          }
        ]
      }

    ]
  },

  plugins: [
    new VueLoaderPlugin(),
    new webpack.ProvidePlugin({
      process: 'process/browser',
    }),
    new CleanWebpackPlugin(),
    new WebpackManifestPlugin({
      basePath: false,
      publicPath: 'dist/',
      removeKeyHash: false,
      filter: file => {
        if ( file.name === 'dist/app.js' ) {
          console.log(file)
        }
        return file.isChunk && file.isInitial
      }
    }),
    new MiniCssExtractPlugin({
      filename: data => {
        return ['pdf', 'ckeditor'].indexOf(data.chunk.name) === -1 ? 'css/[name].[fullhash].css' : 'css/[name].css'
      },
      chunkFilename: 'css/[name].[fullhash]c.css'
    }),
    new IgnoreEmitPlugin([ /\.ignore\.css$/ ]),
    new WebpackShellPlugin({
      safe: true,
      onAfterDone:{
        scripts: ['drush cr'],
        blocking: true,
        parallel: false
      },
    }),
    // new BundleAnalyzerPlugin({
    //   analyzerMode: 'static'
    // })
  ]

}
