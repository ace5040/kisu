import { Store } from 'vuex'

declare global {
  interface Window {
    system_logo: String
    menuState: Boolean,
    logo_animation: String,
    system_logo_login: String,
    system_logo_crib: String,
    title: String,
    example_types: any,
    doc_types: any,
    doc_producers: any,
    doc_statuses: any,
    shop_link: String,
    buy_link: String,
    limited_examples: Boolean,
    system: any,
    modules: any,
    filters: any,
    newTypes: any,
    user: any,
    level: String,
    level_label: String,
    theme_filters: any,
    new_types: any
  }
}

declare module '@vue/runtime-core' {
  // declare your own store states
  interface State {
    system_logo: String,
    menuState: Boolean,
    logo_animation: String,
    system_logo_login: String,
    system_logo_crib: String,
    title: String,
    example_types: any,
    doc_types: any,
    doc_producers: any,
    doc_statuses: any,
    shop_link: String,
    buy_link: String,
    limited_examples: Boolean,
    system: any,
    modules: any,
    filters: any,
    newTypes: any,
    user: any,
    level: String,
    level_label: String,
    currentFilters: Array<Number>,
    activeModule: String,
    searchKey: String,
    searchParams: Object,
    loading: Boolean,
    filtersOpened: Boolean,
    show_login_screen: Boolean,
    show_request_screen: Boolean,
    scrollBlocked: Boolean,
    show_help_screen: Boolean,
    mobile_menu_active: Boolean,
    newConsultationsCount: Number,
    newMessagesCount: Number,
    newEventsCount: Number
  }

  // provide typings for `this.$store`
  interface ComponentCustomProperties {
    $store: Store<State>
  }

}
