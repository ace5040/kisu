import { createApp } from 'vue'
import App from './App.vue'
import router from './router/router.js'
import store from './store/store'
import VueClickAway from "vue3-click-away";
import PerfectScrollbar from 'vue3-perfect-scrollbar'
import VCalendar from 'v-calendar';

createApp(App).use(VCalendar).use(VueClickAway).use(PerfectScrollbar).use(store).use(router).mount('#app')
