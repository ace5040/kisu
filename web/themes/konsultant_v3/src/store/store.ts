import { createStore } from 'vuex'
import { map as _map, findIndex as _findIndex } from 'underscore'
import axios from 'axios'
import Cookies from 'js-cookie'

export default createStore({

  state () {
    return {
      system_logo: window.system_logo,
      menuState: window.menuState,
      logo_animation: window.logo_animation,
      system_logo_login: window.system_logo_login,
      system_logo_crib: window.system_logo_crib,
      title: window.system.html_name,
      example_types: window.example_types,
      doc_types: window.doc_types,
      doc_producers: window.doc_producers,
      doc_statuses: window.doc_statuses,
      shop_link: window.system.shop_link,
      buy_link: window.system.buy_link,
      limited_examples: window.system.limited_examples,
      system: window.system,
      modules: window.system.modules,
      filters: window.theme_filters,
      newTypes: window.new_types,
      user: window.user,
      level: window.user.level,
      level_label: window.user.level_label,
      currentFilters: [],
      activeModule: '',
      searchKey: '',
      searchParams: {},
      loading: false,
      filtersOpened: false,
      show_login_screen: false,
      show_request_screen: false,
      scrollBlocked: false,
      show_help_screen: false,
      mobile_menu_active: false,
      newConsultationsCount: 0,
      newMessagesCount: 0,
      newEventsCount: 0,
    }
  },

  getters: {

    getModuleTitle: (state: any) => (moduleName: String) => {
      return typeof state.modules[moduleName as keyof Object] !== 'undefined' ? state.modules[moduleName as keyof Object].name : ''
    },
    isAuthenticatedUser (state) {
      return state.user.id != 0
    },
    isExpert (state) {
      return state.user.roles.indexOf('expert') !== -1
    },
    canEditMaterials (state) {
      return state.user.roles.indexOf('editor') !== -1 || state.user.roles.indexOf('root') !== -1 || state.user.roles.indexOf('administrator') !== -1
    },
    limited_examples ( state ) {
      return state.limited_examples
    },
    filters ( state ) {
      return state.filters
    },
    filtersLabel ( state ) {
      if ( state.currentFilters.length ) {
        return state.currentFilters.length < 2 ? state.currentFilters[0].label : ('Обрано фільтрів: ' + state.currentFilters.length)
      } else {
        return 'Фільтр'
      }
    },
    currentFiltersIds ( state ) {
      return _map(state.currentFilters, (filter)=>{ return filter.id })
    }
  },

  mutations: {

    mobileMenuActivate (state) {
      state.mobile_menu_active = true
    },

    setNewConsultationsCount (state, count) {
      state.newConsultationsCount = count
    },

    setNewMessagesCount (state, count) {
      state.newMessagesCount = count
    },

    setNewEventsCount (state, count) {
      state.newEventsCount = count
    },

    mobileMenuHide (state) {
      state.mobile_menu_active = false
    },

    toggleFilter (state, filter) {
      let filterIndex = _findIndex(state.currentFilters, f => { return f.id == filter.id });
      if ( filterIndex === -1 ) {
        state.currentFilters.push(filter)
      } else {
        state.currentFilters.splice(filterIndex, 1)
      }
    },

    removeFilter (state, filterId) {
      let filterIndex = state.currentFilters.indexOf(filterId)
      if ( filterIndex !== -1 ) {
        state.currentFilters.splice(filterIndex, 1)
      }
    },

    resetFilters (state) {
      state.currentFilters = []
    },

    toggleFilters (state) {
      state.filtersOpened = !state.filtersOpened
    },

    hideFilters (state) {
      state.filtersOpened = false
    },

    startLoading (state) {
      state.loading = true
    },

    finishLoading (state) {
      state.loading = false
    },

    setTitle (state, title) {
      state.title = title
    },

    showLoginScreen (state) {
      state.show_login_screen = true
    },

    hideLoginScreen (state) {
      state.show_login_screen = false
    },

    showRequestScreen (state) {
      state.show_request_screen = true
    },

    hideRequestScreen (state) {
      state.show_request_screen = false
    },

    blockScreenScroll (state) {
      state.scrollBlocked = true
    },

    unBlockScreenScroll (state) {
      state.scrollBlocked = false
    },

    showHelpScreen (state) {
      state.mobile_menu_active = false
      state.show_help_screen = true
    },

    hideHelpScreen (state) {
      state.show_help_screen = false
    },

    setSearchKey (state, key) {
      state.searchKey = key
    },

    setActiveModule (state, moduleName) {
      state.activeModule = moduleName
    },

    saveSearchParams (state, params) {
      state.searchParams[params.name] = JSON.parse(JSON.stringify(params.params))
    },

    setMenuState (state, value) {
      state.menuState = value
    }

  },

  actions: {

    toggleMenuState({ commit, state }) {
      Cookies.set('menu-toggle', !state.menuState, {path: '/'})
      commit('setMenuState', !state.menuState)
    },

    checkNewConsultations({ commit, state }) {
      if ( document.visibilityState == "visible" && document.hasFocus() && state.user.id != 0 ) {
        axios.post('/system/request/checkNewConsultations',
        {},
        {
          //emulateJSON: true,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          }
        }
        ).then((response) => {
          if ( response && response.data && response.data.error === 0 ) {
            commit('setNewConsultationsCount', response.data.data.count)
          }
        })
      }
    },

    checkNewMessages({ commit, state }) {
      if ( document.visibilityState == "visible" && document.hasFocus() && state.user.id != 0 ) {
        axios.post('/system/request/checkNewMessages',
        {},
        {
          //emulateJSON: true,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          }
        }
        ).then((response) => {
          if ( response && response.data && response.data.error === 0 ) {
            commit('setNewMessagesCount', response.data.data.count)
          }
        })
      }
    },

    checkNewEvents({ commit, state }) {
      if ( document.visibilityState == "visible" && document.hasFocus() && state.user.id != 0 ) {
        axios.post('/system/request/checkNewEvents',
        {},
        {
          //emulateJSON: true,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          }
        }
        ).then((response) => {
          if ( response && response.data && response.data.error === 0 ) {
            commit('setNewEventsCount', response.data.data.count)
          }
        })
      }
    }

  },

  modules: {
  }

})
