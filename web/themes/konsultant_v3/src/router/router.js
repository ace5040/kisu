import { createRouter, createWebHistory } from 'vue-router'

const Home = () => import('../views/Home.vue')
const Orientations = () => import('../views/Orientations.vue')
const Orientation = () => import('../views/Orientation.vue')
const Consultations = () => import('../views/Consultations.vue')
const Consultation = () => import('../views/Consultation.vue')
const Interviews = () => import('../views/Interviews.vue')
const Interview = () => import('../views/Interview.vue')
const Cribs = () => import('../views/Cribs.vue')
const Crib = () => import('../views/Crib.vue')
const Norm = () => import('../views/Norm.vue')
const Normdoc = () => import('../views/Normdoc.vue')
const News = () => import('../views/News.vue')
const New = () => import('../views/New.vue')
const Examples = () => import('../views/Examples.vue')
const Example = () => import('../views/Example.vue')
const AccountingLogs = () => import('../views/AccountingLogs.vue')
const AccountingLog = () => import('../views/AccountingLog.vue')
const Instructions = () => import('../views/Instructions.vue')
const Instruction = () => import('../views/Instruction.vue')
const Calendar = () => import('../views/Calendar.vue')
const Calculator1 = () => import('../views/Calculator1.vue')
const Calculator2 = () => import('../views/Calculator2.vue')
const Calculator3 = () => import('../views/Calculator3.vue')
const Profile = () => import('../views/Profile.vue')
const ConsultationsOwn = () => import('../views/ConsultationsOwn.vue')
const ConsultationsExpert = () => import('../views/ConsultationsExpert.vue')
const ConsultationsArchive = () => import('../views/ConsultationsArchive.vue')
const Favorites = () => import('../views/Favorites.vue')
const Search = () => import('../views/Search.vue')
const Messages = () => import('../views/Messages.vue')
const Message = () => import('../views/Message.vue')
const Classifier = () => import("../views/Classifier.vue")
const Relaxation = () => import("../views/Relaxation.vue")
const RelaxationTetris = () => import("../views/RelaxationTetris.vue")
const RelaxationSolitaire = () => import("../views/RelaxationSolitaire.vue")
const RelaxationMinesweeper = () => import("../views/RelaxationMinesweeper.vue")
const RelaxationLines = () => import("../views/RelaxationLines.vue")

const routes = [

  {
    path: '/',
    name: 'home',
    component: Home
  },

  {
    path: '/relaxation',
    name: 'relaxation',
    component: Relaxation
  },

  {
    path: '/relaxation/solitaire',
    name: 'RelaxationSolitaire',
    component: RelaxationSolitaire
  },

  {
    path: '/relaxation/tetris',
    name: 'RelaxationTetris',
    component: RelaxationTetris
  },

  {
    path: '/relaxation/minesweeper',
    name: 'RelaxationMinesweeper',
    component: RelaxationMinesweeper
  },

  {
    path: '/relaxation/lines',
    name: 'RelaxationLines',
    component: RelaxationLines
  },

  {
    path: '/consultations/own',
    name: 'consultations_own',
    component: ConsultationsOwn
  },

  {
    path: '/consultations',
    name: 'consultations',
    component: Consultations
  },

  {
    path: '/norm',
    name: 'norm',
    component: Norm
  },

  {
    path: '/norm/:id',
    name: 'normdoc',
    component: Normdoc
  },

  {
    path: '/orientation',
    name: 'orientation',
    component: Orientations
  },

  {
    path: '/examples',
    name: 'examples',
    component: Examples
  },

  {
    path: '/accountinglogs',
    name: 'accountinglogs',
    component: AccountingLogs
  },

  {
    path: '/accountinglogs/:id',
    name: 'accountinglog',
    component: AccountingLog
  },

  {
    path: '/instructions',
    name: 'instructions',
    component: Instructions
  },

  {
    path: '/interviews',
    name: 'interviews',
    component: Interviews
  },

  {
    path: '/interviews/:id',
    name: 'interview',
    component: Interview
  },

  {
    path: '/cribs',
    name: 'cribs',
    component: Cribs
  },

  {
    path: '/cribs/:id',
    name: 'crib',
    component: Crib
  },

  {
    path: '/calculators',
    name: 'calculators',
    component: Calculator1
  },

  {
    path: '/calculators/calc1',
    name: 'calculator1',
    component: Calculator1
  },

  {
    path: '/calculators/calc2',
    name: 'calculator2',
    component: Calculator2
  },

  {
    path: '/calculators/calc3',
    name: 'calculator3',
    component: Calculator3
  },

  {
    path: '/calculators/calc1',
    name: 'calculators1',
    component: Calculator1
  },

  {
    path: '/calendar/production/:proyear(\\d+)?/:quater?',
    name: 'calendarProduction',
    component: Calendar
  },

  {
    path: '/calendar/:year(\\d+)?',
    name: 'calendarYear',
    component: Calendar
  },

  {
    path: '/calendar/:yearmonth(\\d+-\\d+)?',
    name: 'calendarYearMonth',
    component: Calendar
  },

  {
    path: '/calendar/:date(\\d+-\\d+-\\d+)?',
    name: 'calendar',
    component: Calendar
  },

  {
    path: '/calendar/:id',
    name: 'calendarEvent',
    component: Calendar
  },

  {
    path: '/news/:id',
    name: 'new',
    component: New
  },

  {
    path: '/orientation/:id',
    name: 'orientationItem',
    component: Orientation
  },

  {
    path: '/examples/:id',
    name: 'example',
    component: Example
  },

  {
    path: '/instructions/:id',
    name: 'instruction',
    component: Instruction
  },

  {
    path: '/norm/31696-nacionalniy-klasifikator-ukraini-klasifikator-profesiy-zmist',
    name: 'classifier',
    props: {
      id: 31696
    },
    component: Normdoc
  },

  {
    path: '/classifier',
    name: 'classifier',
    component: Classifier
  },

  {
    path: '/norm/:id/:rev',
    name: 'normdocrev',
    component: Normdoc
  },

  {
    path: '/consultations/expert',
    name: 'consultationsExpert',
    component: ConsultationsExpert
  },

  {
    path: '/consultations/archive',
    name: 'consultationsArchive',
    component: ConsultationsArchive
  },

  {
    path: '/consultations/:id',
    name: 'consultation',
    component: Consultation
  },

  {
    path: '/news',
    name: 'news',
    component: News
  },

  {
    path: '/help',
    name: 'help',
    component: Home
  },

  {
    path: '/profile',
    name: 'profile',
    component: Profile
  },

  {
    path: '/messages',
    name: 'messages',
    component: Messages
  },

  {
    path: '/messages/:id',
    name: 'message',
    component: Message
  },

  {
    path: '/consultations/own',
    name: 'consultationsOwn',
    component: Home
  },

  {
    path: '/favorites',
    name: 'favorites',
    component: Favorites
  },

  {
    path: '/search',
    name: 'search',
    component: Search
  },

  {
    path: '/help',
    name: 'help',
    component: Home
  }

]

const router = createRouter({
  history: createWebHistory('/'),
  routes
})

export default router
