<?php

/**
 * @file
 * Local development override configuration feature.
 *
 * To activate this feature, copy and rename it such that its path plus
 * filename is 'sites/example.com/settings.local.php', where example.com
 * is the name of your site. Then, go to the bottom of
 * 'sites/example.com/settings.php' and uncomment the commented lines that
 * mention 'settings.local.php'.
 */

/**
 * Assertions.
 *
 * The Drupal project primarily uses runtime assertions to enforce the
 * expectations of the API by failing when incorrect calls are made by code
 * under development.
 *
 * @see http://php.net/assert
 * @see https://www.drupal.org/node/2492225
 *
 * If you are using PHP 7.0 it is strongly recommended that you set
 * zend.assertions=1 in the PHP.ini file (It cannot be changed from .htaccess
 * or runtime) on development machines and to 0 in production.
 *
 * @see https://wiki.php.net/rfc/expectations
 */
assert_options(ASSERT_ACTIVE, TRUE);
\Drupal\Component\Assertion\Handle::register();

/**
 * Enable local development services.
 */

$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';

/**
 * Show all error messages, with backtrace information.
 *
 * In case the error level could not be fetched from the database, as for
 * example the database connection failed, we rely only on this value.
 */
#$config['system.logging']['error_level'] = 'verbose';

/**
 * Disable CSS and JS aggregation.
 */
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

/**
 * Disable the render cache (this includes the page cache).
 *
 * Note: you should test with the render cache enabled, to ensure the correct
 * cacheability metadata is present. However, in the early stages of
 * development, you may want to disable it.
 *
 * This setting disables the render cache by using the Null cache back-end
 * defined by the development.services.yml file above.
 *
 * Do not use this setting until after the site is installed.
 */

$settings['cache']['bins']['render'] = 'cache.backend.null';

/**
 * Disable Dynamic Page Cache.
 *
 * Note: you should test with Dynamic Page Cache enabled, to ensure the correct
 * cacheability metadata is present (and hence the expected behavior). However,
 * in the early stages of development, you may want to disable it.
 */

$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';

$settings['cache']['bins']['page'] = 'cache.backend.null';

/**
 * Allow test modules and themes to be installed.
 *
 * Drupal ignores test modules and themes by default for performance reasons.
 * During development it can be useful to install test extensions for debugging
 * purposes.
 */
#$settings['extension_discovery_scan_tests'] = FALSE;

/**
 * Enable access to rebuild.php.
 *
 * This setting can be enabled to allow Drupal's php and database cached
 * storage to be cleared via the rebuild.php page. Access to this page can also
 * be gained by generating a query string from rebuild_token_calculator.sh and
 * using these parameters in a request to rebuild.php.
 */
#$settings['rebuild_access'] = TRUE;

$settings['memcache']['servers'] = ['memcache:11211' => 'default'];
$settings['memcache']['bins'] = ['default' => 'default'];
$settings['memcache']['key_prefix'] = '';
$settings['cache']['default'] = 'cache.backend.memcache';

$databases['default']['default'] = array (
  'database' => 'konsultant',
  'username' => '',
  'password' => '',
  'prefix' => '',
  'host' => 'mysql',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

$settings['trusted_host_patterns'] = array(
	'konsultant\.test$'
);

$settings['hash_salt'] = '';
$settings['config_sync_directory'] = '../config';
$settings['file_temp_path'] = '../tmp';

$config['system.site']['default_langcode'] = 'uk';
$config['language.negotiation']['selected_langcode'] = 'uk';

$settings['default_system_domain'] = 'http://konsultant.test';
$settings['unoconv_bin'] = '/usr/bin/unoconv';
$settings['unoconv_url'] = "http://doctohtml:3000/unoconv/xhtml";
$settings['htmltopdf_url'] = "http://htmltopdf:3000/pdf";
$settings['norm_api_url'] = 'http://kadrovik.konsultant.test/api';
$settings['norm_api_user'] = 'api';
$settings['norm_api_pass'] = 'apipass';
$settings['local_norm_search'] = 'no';

$settings['soap_user'] = '';
$settings['soap_pass'] = '';

$settings['access_levels'] = [
	'set-product-id-here-1' => 'start',
	'set-product-id-here-2' => 'standard',
	'set-product-id-here-3' => 'maximum',
	'set-product-id-here-4' => 'lite'
];

$config['search_api.server.solr']['backend_config']['connector_config']['host'] = 'solr';
$config['search_api.server.solr']['backend_config']['connector_config']['core'] = 'konsultant';
$config['printable.settings']['path_to_binary'] = '/usr/bin/wkhtmltopdf --disable-javascript';
$config['printable.settings']['path_to_xfb_run'] = '/usr/bin/xvfb-run';
#$config['swiftmailer.transport']['transport'] = 'spool';
$config['swiftmailer.transport']['spool_directory'] = '../tmp/emails';

#devel webprofiler
#$class_loader->addPsr4('Drupal\\webprofiler\\', [ __DIR__ . '/../../modules/contrib/devel/webprofiler/src']);
#$settings['container_base_class'] = '\Drupal\webprofiler\DependencyInjection\TraceableContainer';
