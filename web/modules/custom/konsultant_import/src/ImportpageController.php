<?php
/**
 * @file
 * Contains \Drupal\konsultant_import\ImportpageController.
 */

namespace Drupal\konsultant_import;

use Drupal\Component\Utility\Unicode;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use \Drupal\file\Entity\File;
use Drupal\entityQuery;
use Sunra\PhpSimple\HtmlDomParser;
use \ForceUTF8\Encoding;

class ImportpageController extends ControllerBase {

  public function content() {

  	return [
  		'#theme' => 'import_page',
  	];
  }

  public function adminEvents() {

  	return [
  		'#theme' => 'import_events_page',
  	];
  }

  public function importNodesBatch(){

    $chunks = [];
    $chunk_size = 10;

    $systems = [3]; //1 = экология 2 = от //3 = кадровик
    $operations = [];

    foreach ( $systems as $system_id ) {

      $file = "sync/faq-" . $system_id . ".txt";

      $linecount = 0;
      $handle = fopen($file, "r");
      if ( $handle ) {
        while(!feof($handle)){
          $line = fgets($handle);
          $linecount++;
        }
        fclose($handle);
      }
      for ( $i = 0; $i < $linecount; $i += $chunk_size ) {
        if ( $i + $chunk_size < $linecount ) {
          $chunks[] = (object)['begin' => $i, 'end' => $i + $chunk_size - 1, 'system_id' => $system_id];
        } else {
          $chunks[] = (object)['begin' => $i, 'end' => $linecount - 1, 'system_id' => $system_id];
        }
      }

      foreach ($chunks as $chunk) {
        $operations[] = ['\Drupal\konsultant_import\ImportpageController::importNodesBatchOperation', [$chunk]];
      }

    }


    $batch = array(
        'title' => 'Importing nodes',
        'operations' => $operations,
        'finished' => 'importNodesBatchFinished',
    );

    batch_set($batch);
    return batch_process('/admin/import-nodes');

  }

  public function importEventsBatch(){
    $file_tmp_name = $_FILES['file']['tmp_name'];
    $file_real_name = $_FILES['file']['name'];
    $drupal_temp = file_directory_temp();
    $data = file_get_contents($file_tmp_name);
    $drupal_temp = file_directory_temp();
    $file_name = $drupal_temp . '/' . $file_real_name;
    file_put_contents($file_name, $data);

    $system_id = \Drupal::request()->request->get('system_id');
    $year = \Drupal::request()->request->get('year');
    $operations = [];
    $linecount = 0;

    $query = \Drupal::entityQuery('node')
      ->condition('type', ['event'], 'IN')
      ->condition('field_systems', [$system_id])
      ->condition('field_event_type', 'audit')
      ->condition('field_event_date_start', \DateTime::createFromFormat('Y-m-d H:i:s', $year . '-01-01 00:00:00')->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT), '>=')
      ->condition('field_event_date_start', \DateTime::createFromFormat('Y-m-d H:i:s', ($year+1) . '-01-01 00:00:00')->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT), '<');
    $nids = $query->execute();
    $nids_count = count($nids);
    $chunks = [];
    $chunk_size = 100;
    $chunk_counter = 0;
    $nids_chunk = [];

    foreach ( $nids as $nid_key => $nid_value ) {

      if ( $chunk_counter == $chunk_size ) {
        $chunk_counter = 0;
        if ( !empty($nids_chunk) ) {
          $chunks[] = (object)['nids' => $nids_chunk];
        }
        $nids_chunk = [];
      }
      $nids_chunk[$nid_key] = $nid_value;
      $chunk_counter++;

    }

    if ( !empty($nids_chunk) ) {
      $chunks[] = (object)['nids' => $nids_chunk];
    }

    foreach ($chunks as $chunk) {
      $operations[] = ['\Drupal\konsultant_import\ImportpageController::deleteEventsBatchOperation', [$chunk]];
    }

    $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name);
    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
    $reader->setReadDataOnly(TRUE);
    $spreadsheet = $reader->load($file_name);
    $worksheet = $spreadsheet->getActiveSheet();
    $linecount = $worksheet->getHighestRow();

    $chunks = [];
    $chunk_size = 10;
    for ( $i = 0; $i < $linecount; $i += $chunk_size ) {
      if ( $i + $chunk_size < $linecount ) {
        $end = $i + $chunk_size - 1;
      } else {
        $end = $linecount - 1;
      }
      $chunks[] = (object)['begin' => $i+1, 'end' => $end+1, 'system_id' => $system_id, 'file_name' => $file_name];
    }

    foreach ($chunks as $chunk) {
      $operations[] = ['\Drupal\konsultant_import\ImportpageController::importEventsBatchOperation', [$chunk]];
    }


    $batch = array(
        'title' => 'Importing nodes',
        'operations' => $operations,
        'finished' => 'importEventsBatchFinished',
    );

    batch_set($batch);
    return batch_process('/admin/import-events');

  }

  public function importArticlesBatch(){

    $chunks = [];
    $chunk_size = 10;

    $systems = [3]; //1 = экология 2 = от //3 = кадровик
    $operations = [];

    foreach ( $systems as $system_id ) {

      $file = "sync/art-" . $system_id . ".txt";

      $linecount = 0;
      $handle = fopen($file, "r");
      if ( $handle ) {
        while(!feof($handle)){
          $line = fgets($handle);
          $linecount++;
        }
        fclose($handle);
      }
      for ( $i = 0; $i < $linecount; $i += $chunk_size ) {
        if ( $i + $chunk_size < $linecount ) {
          $chunks[] = (object)['begin' => $i, 'end' => $i + $chunk_size - 1, 'system_id' => $system_id];
        } else {
          $chunks[] = (object)['begin' => $i, 'end' => $linecount - 1, 'system_id' => $system_id];
        }
      }

      foreach ($chunks as $chunk) {
        $operations[] = ['\Drupal\konsultant_import\ImportpageController::importArticlesBatchOperation', [$chunk]];
      }

    }


    $batch = array(
        'title' => 'Importing articles',
        'operations' => $operations,
        'finished' => 'importNodesBatchFinished',
    );

    batch_set($batch);
    return batch_process('/admin/import-nodes');

  }

  public static function importNodesBatchOperation($chunk, &$context){

    $line_counter = 0;
    $chunk_data = [];
    $system_id = $chunk->system_id;
    $file_name = "sync/faq-" . $system_id . ".txt";
    $file = new \SplFileObject($file_name);

    if ( $file ) {

      while (!$file->eof()) {
        $line = $file->fgets();
        if ($line_counter >= $chunk->begin && $line_counter <= $chunk->end ){
          $chunk_data[] = $line;
        }
        if ($line_counter > $chunk->end ){
          break; //
        }
        $line_counter++;
      }
    }

    $message = 'Importing Node...';
    $results = [];
    foreach ($chunk_data as $data_line) {
      $json = json_decode($data_line);
      if ( !empty($json->nid) ) {

        $nodes = \Drupal::entityTypeManager()
          ->getStorage('node')
          ->loadByProperties(['type' => 'consultation', 'field_import_id' => $json->nid]);

        $node = reset($nodes);

        $filters = [];

        if ( !empty($json->themes) ) {

          foreach ( $json->themes as $theme ) {
            $terms = \Drupal::entityTypeManager()
            ->getStorage('taxonomy_term')
            ->loadByProperties(['vid' => 'filters', 'field_import_id' => $theme]);

            $term = reset($terms);

            if ( !empty($term) ) {
              $filters[] = $term->Id();
            }

          }

        }

        if ( empty($node) ) {
          $node = Node::create([
            'type' => 'consultation',
            'title' => $json->title,
            'field_question' => [
              'value' => $json->question
            ],
            'field_answer' => [
              'value' => $json->answer
            ],
            'field_import_id' => [
              'value' => $json->nid
            ],
            'field_expert' => [567],
            'field_filter' => $filters,
            'field_date_placed' => $json->created,
            'field_consultation_type' => 'consultation'
          ]);
          $node->set('field_systems', [$system_id]); //1 = экология 2 = от 3 = кадровик
          $node->set('field_status', 'placed');
          $node->setCreatedTime($json->created);
          $node->save();
          $results[] = $json->nid;
        } else {

          $changed = false;

          // if ( $json->title != $node->getTitle() )  {
          //   $changed = true;
          //   $node->set('title', $json->title);
          // }
          //
          // if ( $json->question != $node->get('field_question')->getValue()[0]['value'] )  {
          //   $changed = true;
          //   $node->set('field_question', $json->question);
          // }
          //
          // if ( $json->answer != $node->get('field_answer')->getValue()[0]['value'] )  {
          //   $changed = true;
          //   $node->set('field_answer', $json->answer);
          // }
          //
          // if ( 45 != $node->get('field_expert')->getValue()[0]['target_id'] )  {
          //   $changed = true;
          //   $node->set('field_expert', 45);
          // }
          //
          // if ( $json->created != $node->getCreatedTime() )  {
          //   $changed = true;
          //   $node->setCreatedTime($json->created);
          // }

          if ( $changed ) {

            $node->field_filter->setValue($filters);

            $changed_date = $node->changed->value;
            $node->save();
            konsultant_restore_changed_date($changed_date, $node->Id());
            $results[] = $node->Id();
          }

        }

      }
    }
    if ( empty($context['results']) ) {
      $context['results'] = 0;
    }
    $context['message'] = $message;
    $context['results'] += count($results);
  }

  public static function importEventsBatchOperation($chunk, &$context){

    $line_counter = 0;
    $chunk_data = [];
    $system_id = $chunk->system_id;
    $file_name = $chunk->file_name;

    $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name);
    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
    $reader->setReadDataOnly(TRUE);
    $spreadsheet = $reader->load($file_name);
    $worksheet = $spreadsheet->getActiveSheet();
    $highestColumn = $worksheet->getHighestColumn();

    for ($row = $chunk->begin; $row <= $chunk->end; ++$row) {
        $raw_date = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
        $date_start = \DateTime::createFromFormat('d.m.Y H:i:s', trim($raw_date) . ' 00:00:00');
        $subject = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
        $subject = preg_replace("/\r\n|\r|\n/", ' ', $subject);
        $subject = str_replace("  ", ' ', $subject);
        $name_company = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
        $name_company = preg_replace("/\r\n|\r|\n/", ' ', $name_company);
        $name_company = str_replace("  ", ' ', $name_company);
        $edrpou = trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
        if ( !$date_start ) {
          \Drupal::logger('import_events')->info('error in date: "' . $raw_date . '", for: ' . $edrpou);
        }
        if (!empty($edrpou)) {
          $chunk_data[] = [
            'event_name_company' => trim($name_company),
            'event_edrpou' => $edrpou,
            'event_subject_audit' => trim($subject),
            'event_date_start' => $date_start->format('Y-m-d'),
            'event_days' => trim($worksheet->getCellByColumnAndRow(8, $row)->getValue()),
            'event_organ_audit' => trim($worksheet->getCellByColumnAndRow(9, $row)->getValue()),
            'event_complex_plan' => trim($worksheet->getCellByColumnAndRow(10, $row)->getValue()) == 'Так' ? true : false
          ];
        }

    }

    $message = 'Importing Event...';
    $results = [];

    foreach ($chunk_data as $data) {

      $event_days = $data['event_days'];
      $event_organ_audit = $data['event_organ_audit'];
      $event_subject_audit = $data['event_subject_audit'];
      $event_date_start = $data['event_date_start'];
      $event_name_company = $data['event_name_company'];
      $event_complex_plan = $data['event_complex_plan'];

      $vars = [
        'company-name' => $event_name_company,
        'event-subject' => $event_subject_audit,
        'event-auditor' => $event_organ_audit,
        'event-days-count' => $event_days,
        'event-date-begin' => \DateTime::createFromFormat('Y-m-d', $event_date_start)->format('d.m.Y'),
        'event-complex-plan' => $event_complex_plan ? 'Так' : 'Ні'
      ];

      $content = get_system_template('audit-text', $vars);

      $title = 'Перевірка на предмет ' . $event_subject_audit;

      $edrpou = $data['event_edrpou'];

      $node = Node::create([
        'type' => 'event',
        'title' => Unicode::truncate($title, 255),
        'field_event_edrpou' => $edrpou,
        'field_event_content' => $content,
        'field_event_date_start' => $event_date_start,
        'field_event_days' => $event_days,
        'field_event_organ_audit' => $event_organ_audit,
        'field_event_subject_audit' => $event_subject_audit,
        'field_event_name_company' => $event_name_company,
        'field_event_complex_plan' => $event_complex_plan,
      ]);
      $node->set('field_systems', [$system_id]);
      $node->set('field_event_type', 'audit');
      $node->save();
      $results[] = $node->Id();

    }

    if ( empty($context['results']) ) {
      $context['results'] = [];
    }

    if ( empty($context['results']['count']) ) {
      $context['results']['count'] = 0;
    }

    $context['message'] = $message;
    $context['results']['file_name'] = $file_name;
    $context['results']['count'] += count($results);
  }

  public static function deleteEventsBatchOperation($chunk, &$context){
    $nids = $chunk->nids;

    if ( !empty($nids) ) {
      $storage_handler = \Drupal::entityTypeManager()
        ->getStorage('node');
      $entities = $storage_handler
        ->loadMultiple($nids);
      $storage_handler
        ->delete($entities);
    }

    if ( empty($context['results']) ) {
      $context['results'] = [];
    }

    if ( empty($context['results']['count']) ) {
      $context['results']['count'] = 0;
    }
    $context['message'] = 'Deleting Events';
    $context['results']['count'] += count($nids);
  }

  public static function importArticlesBatchOperation($chunk, &$context){

    $line_counter = 0;
    $chunk_data = [];
    $system_id = $chunk->system_id;
    $file_name = "sync/art-" . $system_id . ".txt";
    $file = new \SplFileObject($file_name);

    if ( $file ) {

      while (!$file->eof()) {
        $line = $file->fgets();
        if ($line_counter >= $chunk->begin && $line_counter <= $chunk->end ){
          $chunk_data[] = $line;
        }
        if ($line_counter > $chunk->end ){
          break; //
        }
        $line_counter++;
      }
    }

    $message = 'Importing Article...';
    $results = [];
    foreach ($chunk_data as $data_line) {
      $json = json_decode($data_line);
      if ( !empty($json->nid) ) {

        $nodes = \Drupal::entityTypeManager()
          ->getStorage('node')
          ->loadByProperties(['type' => 'interview', 'field_import_id' => $json->nid]);

        $node = reset($nodes);

        $filters = [];

        if ( !empty($json->themes) ) {

          foreach ( $json->themes as $theme ) {
            $terms = \Drupal::entityTypeManager()
            ->getStorage('taxonomy_term')
            ->loadByProperties(['vid' => 'filters', 'field_import_id' => $theme]);

            $term = reset($terms);

            if ( !empty($term) ) {
              $filters[] = $term->Id();
            }

          }

        }

        if ( empty($node) ) {
          $node = Node::create([
            'type' => 'interview',
            'title' => $json->title,
            'field_import_id' => [
              'value' => $json->nid
            ],
            'field_filter' => $filters
          ]);
          if ( !empty($json->document) ) {
            $data = base64_decode($json->document);
            $node->field_interview_html->setValue($data);
          }
          $node->set('field_systems', [$system_id]); //1 = экология 2 = от 3 = кадровик
          $node->setCreatedTime($json->created);
          $node->save();
          $results[] = $json->nid;
        } else {

          $changed = false;

          // if ( $json->title != $node->getTitle() )  {
          //   $changed = true;
          //   $node->set('title', $json->title);
          // }
          //

          // if ( $json->created != $node->getCreatedTime() )  {
          //   $changed = true;
          //   $node->setCreatedTime($json->created);
          // }

          if ( $changed ) {

            //$node->field_filter->setValue($filters);

            $changed_date = $node->changed->value;
            $node->save();
            konsultant_restore_changed_date($changed_date, $node->Id());
            $results[] = $node->Id();
          }

        }

      }
    }
    if ( empty($context['results']) ) {
      $context['results'] = 0;
    }
    $context['message'] = $message;
    $context['results'] += count($results);
  }

  public function importNormBatch(){

    $chunks = [];
    $chunk_size = 10;

    $systems = [3]; //1 = экология 2 = от //3 = кадровик
    $operations = [];

    foreach ( $systems as $system_id ) {

      $file = "sync/npb-" . $system_id . ".txt";

      $linecount = 0;
      $handle = fopen($file, "r");

      if ( $handle ) {
        while(!feof($handle)){
          $line = fgets($handle);
          $linecount++;
        }
        fclose($handle);
      }

      for ( $i = 0; $i < $linecount; $i += $chunk_size ) {
        if ( $i + $chunk_size < $linecount ) {
          $chunks[] = (object)['begin' => $i, 'end' => $i + $chunk_size - 1, 'system_id' => $system_id];
        } else {
          $chunks[] = (object)['begin' => $i, 'end' => $linecount - 1, 'system_id' => $system_id];
        }
      }

      foreach ($chunks as $chunk) {
        $operations[] = ['\Drupal\konsultant_import\ImportpageController::importNormBatchOperation', [$chunk]];
      }

    }


    $batch = array(
        'title' => 'Importing nodes',
        'operations' => $operations,
        'finished' => 'importNodesBatchFinished',
    );

    batch_set($batch);
    return batch_process('/admin/import-nodes');

  }

  public static function importNormBatchOperation($chunk, &$context){

    $line_counter = 0;
    $chunk_data = [];
    $system_id = $chunk->system_id;
    $file_name = "sync/npb-" . $system_id . ".txt";
    $file = new \SplFileObject($file_name);

    if ( $file ) {

      while (!$file->eof()) {
        $line = $file->fgets();
        if ($line_counter >= $chunk->begin && $line_counter <= $chunk->end ){
          $chunk_data[] = $line;
        }
        if ($line_counter > $chunk->end ){
          break; //
        }
        $line_counter++;
      }

    };

    $message = 'Importing Node...';
    $results = [];
    foreach ($chunk_data as $data_line) {
      $json = json_decode($data_line);
      if ( !empty($json->nid) ) {

        $nodes = \Drupal::entityTypeManager()
          ->getStorage('node')
          ->loadByProperties(['type' => 'normdoc', 'field_import_id' => $json->nid]);

        $node = reset($nodes);

        $filters = [];

        if (!empty($json->themes)) {

          foreach ( $json->themes as $theme ) {
            $terms = \Drupal::entityTypeManager()
              ->getStorage('taxonomy_term')
              ->loadByProperties(['vid' => 'filters', 'field_import_id' => $theme]);

            $term = reset($terms);

            if ( !empty($term) ) {
              $filters[] = $term->Id();
            }

          }

        }

        if ( empty($node) ) {
          // $newnode = [
          //   'type' => 'normdoc',
          //   'title' => $json->title,
          //   'field_document_number' => [
          //     'value' => $json->document_number
          //   ],
          //   'field_systems' => [$system_id],
          //   'field_document_status' => [
          //     'value' => empty($json->document_status) ? 'valid' : $json->document_status
          //   ],
          //   'field_import_id' => [
          //     'value' => $json->nid
          //   ],
          //   'field_filter' => $filters,
          // ];
          //
          // if ( !empty($json->doc_producer) ) {
          //
          //   $import_id = self::getNormImportId($json->doc_producer);
          //
          //   $terms = \Drupal::entityTypeManager()
          //   ->getStorage('taxonomy_term')
          //   ->loadByProperties(['vid' => 'doc_producer', 'field_import_id' => $import_id]);
          //   $term = reset($terms);
          //
          //   if ( !empty($term) ) {
          //     $newnode['field_doc_producer'] = $term->Id();
          //   }
          //
          // }
          //
          // if ( !empty($json->doc_type) ) {
          //
          //   $doc_type_id = self::getDocTypeId($json->doc_type);
          //
          //   if ( !empty($doc_type_id) ) {
          //     $newnode['field_doc_type'] = $doc_type_id;
          //   }
          //
          // }
          //
          // // if ( !empty($json->instruction_type) ) {
          // //     $newnode['field_instruction_type'] = $json->instruction_type;
          // // }
          //
          // if ( !empty($json->date_init) ) {
          //     $newnode['field_date_init'] = $json->date_init;
          // }
          //
          // if ( !empty($json->date_invalid) ) {
          //     $newnode['field_date_invalid'] = $json->date_invalid;
          // }
          //
          // $node = Node::create($newnode);
          //
          //
          // $node->setCreatedTime($json->created);
          // $node->save();
          //
          // // if ( !empty($json->document_file) ) {
          // //   $data = base64_decode($json->document_file);
          // //   $file_name = konsultant_transliteration_filename( $json->nid . '-' . $json->document_file_name);
          // //   $directory = 'public://normdocs/';
          // //   if ( file_prepare_directory($directory, FILE_CREATE_DIRECTORY) ) {
          // //
          // //     $file = file_save_data($data, $directory . $file_name);
          // //
          // //     $field_collection_item = entity_create('field_collection_item', array('field_name' => 'field_documents'));
          // //     $field_collection_item->field_document_file->setValue($file->Id());
          // //
          // //     if ( !empty($json->document_date_valid) ) {
          // //       $field_collection_item->field_document_date_valid->setValue($json->document_date_valid);
          // //     }
          // //
          // //     $field_collection_item->setHostEntity($node);
          // //     $field_collection_item->save();
          // //
          // //   };
          // //
          // // }
          //
          // if ( !empty($node->field_documents) && !empty($json->revisions) ) {
          //
          //   foreach ( $json->revisions as $key => $revision) {
          //     $revision_data = base64_decode($revision->revision_document);
          //     $field_collection_item = entity_create('field_collection_item', ['field_name' => 'field_documents']);
          //     $field_collection_item->field_document->setValue($revision_data);
          //     if ( !empty($revision->revision_date_valid) ) {
          //       $field_collection_item->field_document_date_valid->setValue($revision->revision_date_valid);
          //     }
          //     $field_collection_item->setHostEntity($node);
          //     $field_collection_item->save();
          //   }
          //
          // }
          //
          // if ( !empty($json->document) ) {
          //   $data = base64_decode($json->document);
          //   $field_collection_item = entity_create('field_collection_item', array('field_name' => 'field_documents'));
          //   $field_collection_item->field_document->setValue($data);
          //   if ( !empty($json->document_date_valid) ) {
          //     $field_collection_item->field_document_date_valid->setValue($json->document_date_valid);
          //   }
          //   $field_collection_item->setHostEntity($node);
          //   $field_collection_item->save();
          // }
          //
          //
          // $results[] = $json->nid;
        } else {

          $changed = false;

          // if ( $json->title != $node->getTitle() )  {
          //   $changed = true;
          //   $node->set('title', $json->title);
          // }

          // if ( !empty($node->get('field_document_number')->getValue()) && !empty($json->document_number) && $json->document_number != $node->get('field_document_number')->getValue()[0]['value'] )  {
          //   $changed = true;
          //   $node->set('field_document_number', $json->document_number);
          // }

          //$json->document_status = empty($json->document_status) ? 'valid' : $json->document_status;

          // if ( empty($node->get('field_systems')->getValue()) ) {
          //   $node->set('field_systems', [$system_id]);
          //   $changed = true;
          // }

          // if (
          //     (
          //       !empty($node->get('field_document_status')->getValue()) &&
          //       $node->get('field_document_status')->getValue()[0]['value'] != $json->document_status
          //     ) || (
          //       empty($node->get('field_document_status')->getValue())
          //     )
          // ) {
          //   $node->set('field_document_status', $json->document_status);
          //   $changed = true;
          // }

          // if ( $json->created != $node->getCreatedTime() )  {
          //   $changed = true;
          //   $node->setCreatedTime($json->created);
          // }

          // if ( !empty($json->doc_producer) ) {
          //
          //   $import_id = self::getNormImportId($json->doc_producer);
          //   $terms = \Drupal::entityTypeManager()
          //   ->getStorage('taxonomy_term')
          //   ->loadByProperties(['vid' => 'doc_producer', 'field_import_id' => $import_id]);
          //   $term = reset($terms);
          //
          //   if ( !empty($term) ) {
          //     if (
          //       empty($node->get('field_doc_producer')->getValue()) ||
          //       $term->Id() != $node->get('field_doc_producer')->getValue()[0]['target_id']
          //     ) {
          //       $node->field_doc_producer->setValue($term->Id());
          //       $changed = true;
          //     }
          //   }
          //
          // }

          // if ( !empty($json->doc_type) ) {
          //   $doc_type_id = self::getDocTypeId($json->doc_type);
          //
          //   if ( !empty($doc_type_id) ) {
          //     if (
          //       empty($node->get('field_doc_type')->getValue()) ||
          //       $doc_type_id != $node->get('field_doc_type')->getValue()[0]['target_id']
          //     ) {
          //       $node->field_doc_type->setValue($doc_type_id);
          //       $changed = true;
          //     }
          //   }
          // }

          // if ((
          //       !empty($node->get('field_instruction_type')->getValue()) &&
          //       !empty($json->instruction_type) &&
          //       $json->instruction_type != $node->get('field_instruction_type')->getValue()[0]['value']
          //     ) ||
          //     (
          //       empty($node->get('field_instruction_type')->getValue()) &&
          //       !empty($json->instruction_type)
          //     )
          // ) {
          //   $node->field_instruction_type->setValue($json->instruction_type);
          //   $changed = true;
          // }
          //
          // if ((
          //       !empty($node->get('field_date_init')->getValue()) &&
          //       !empty($json->date_init) &&
          //       $json->date_init != $node->get('field_date_init')->getValue()[0]['value']
          //     ) ||
          //     (
          //       empty($node->get('field_date_init')->getValue()) &&
          //       !empty($json->date_init)
          //     )
          // ) {
          //   $node->field_date_init->setValue($json->date_init);
          //   $changed = true;
          // }
          // if ((
          //       !empty($node->get('field_date_invalid')->getValue()) &&
          //       !empty($json->date_invalid) &&
          //       $json->date_invalid != $node->get('field_date_invalid')->getValue()[0]['value']
          //     ) ||
          //     (
          //       empty($node->get('field_date_invalid')->getValue()) &&
          //       !empty($json->date_invalid)
          //     )
          // ) {
          //   $node->field_date_invalid->setValue($json->date_invalid);
          //   $changed = true;
          // }

          //$document_date_changed = false;

          // if ( !empty($json->document_date_valid) ) {
          //
          //   if ( !empty($node->field_documents) ) {
          //
          //     foreach ( $node->field_documents as $key => $item) {
          //       $item = $item->value;
          //       $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($item);
          //       $fc->field_document_date_valid->setValue($json->document_date_valid);
          //       $fc->save();
          //       $document_date_changed = true;
          //       break; // // check only first item
          //     }
          //
          //   }
          //
          // }

          if ( !empty($node->field_documents) && count($node->field_documents) > 1 && !empty($json->revisions) ) {

            $temp_fc = 0;

            $prevdate = '';
            $tempdate = '';
            $revisions_count = count($node->field_documents);

            foreach ( $node->field_documents as $temp_key => $temp_item) {
              $revisions_count--;
              $temp_item = $temp_item->value;
              $temp_fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($temp_item);

              $tempdate = '';
              if ( !empty($temp_fc->field_document_date_valid->getValue()[0]['value']) ) {
                $tempdate = $temp_fc->field_document_date_valid->getValue()[0]['value'];
              }

              if ( $revisions_count > 0 ) {
                $prevdate = $tempdate;
              }

            }

            if ( !empty($prevdate) && !empty($tempdate) && $tempdate === $prevdate) {
              $temp_fc->delete();

              $revision_data = base64_decode($json->document);
              $field_collection_item = entity_create('field_collection_item', ['field_name' => 'field_documents']);
              $field_collection_item->field_document->setValue($revision_data);
              if ( !empty($json->document_date_valid) ) {
                $field_collection_item->field_document_date_valid->setValue($json->document_date_valid);
              }
              $field_collection_item->setHostEntity($node);
              $field_collection_item->save();

            }

            $changed = true;

          }

          //$document_file_changed = false;

          // if ( !empty($json->document_file) ) {
          //
          //   if ( !empty($node->field_documents) ) {
          //
          //     foreach ( $node->field_documents as $key => $item) {
          //       $item = $item->value;
          //       $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($item);
          //
          //       $file = false;
          //
          //       if ( !empty($fc->field_document_file->getValue()) ) {
          //         $file = \Drupal\file\Entity\File::load($fc->field_document_file->getValue()[0]['target_id']);
          //       }
          //
          //       $data = base64_decode($json->document_file);
          //
          //       if ( empty($file) || intVal($file->getSize()) != strlen($data) ) {
          //
          //         $file_name = konsultant_transliteration_filename( $json->nid . '-' . $json->document_file_name);
          //         $directory = 'public://normdocs/';
          //
          //         if ( file_prepare_directory($directory, FILE_CREATE_DIRECTORY) ) {
          //
          //           $file = file_save_data($data, $directory . $file_name);
          //           $fc->field_document_file->setValue($file->Id());
          //           $fc->save();
          //           $document_file_changed = true;
          //
          //         };
          //
          //       }
          //
          //
          //       break; // // check only first item
          //     }
          //
          //   } else {
          //
          //       $data = base64_decode($json->document_file);
          //       $file_name = konsultant_transliteration_filename( $json->nid . '-' . $json->document_file_name);
          //       $directory = 'public://normdocs/';
          //
          //       if ( file_prepare_directory($directory, FILE_CREATE_DIRECTORY) ) {
          //
          //         $file = file_save_data($data, $directory . $file_name);
          //
          //         if ( !empty($file) ) {
          //           $field_collection_item = entity_create('field_collection_item', array('field_name' => 'field_documents'));
          //           $field_collection_item->field_document_file->setValue($file->Id());
          //           $field_collection_item->setHostEntity($node);
          //           $field_collection_item->save();
          //           $document_file_changed = true;
          //
          //         }
          //
          //       };
          //
          //   }
          //
          // }

          if ( $changed ) {
            $changed_date = $node->changed->value;
            $node->save();
            konsultant_restore_changed_date($changed_date, $node->Id());
            $results[] = $node->Id();
          }

        }

      }
    }
    if ( empty($context['results']) ) {
      $context['results'] = 0;
    }
    $context['message'] = $message;
    $context['results'] += count($results);
  }

  public function importConvertBatch(){

    $chunks = [];
    $chunk_size = 1;

    $operations = [];

    $query = \Drupal::entityQuery('node');
    // $group = $query->orConditionGroup()
    //    ->condition('field_converted', NULL, 'IS NULL')
    //    ->condition('field_converted', '1', '<>');
    // $query->condition($group);
    $query->condition('type', 'normdoc');
    $query->condition('nid', 49601, '>');

    $nids = $query->execute();

    $chunks = array_chunk($nids, $chunk_size);

    foreach ($chunks as $chunk) {
      $operations[] = ['\Drupal\konsultant_import\ImportpageController::importConvertBatchOperation', [$chunk]];
    }

    $batch = array(
        'title' => 'Converting docs',
        'operations' => $operations,
        'finished' => 'importNodesBatchFinished',
    );

    batch_set($batch);
    return batch_process('/admin/import-nodes');

  }

  public static function importConvertBatchOperation($chunk, &$context){

    $message = 'Converting Node...';
    $results = [];

    $nodes = node_load_multiple($chunk);

    foreach ($nodes as $node) {

      $changed = false;

      if ( !empty($node) && !empty($node->field_documents) ) {

        foreach ( $node->field_documents as $key => $item) {

          $item = $item->value;

          $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($item);

          $file = false;

          if ( !empty($fc->field_document_file->getValue()) ) {
            $file = \Drupal\file\Entity\File::load($fc->field_document_file->getValue()[0]['target_id']);
          }

          if ( !empty($file) && empty($fc->field_document->getValue()[0]['value']) ) {

            $uri = $file->getFileUri();

            $path = \Drupal::service('file_system')->realpath($uri);

            if ( !empty($path) ) {

              $converted_path = self::importConvert($path);

              if ( !empty($converted_path) ) {

                konsultant_unoconv($path, $converted_path);
                sleep(1);

                $data = file_get_contents($converted_path);

                unlink($converted_path);

              }

              if ( !empty($data) ) {
                  //\Drupal::logger('import')->info('Node id ' . $node->id());
                  $fixed_data = self::fixImportedHtml($data);
                  //\Drupal::logger('import')->info('Data size ' . strlen($fixed_data));
                  $fc->field_document->setValue($fixed_data);
                  //$fc->setHostEntity($node);
                  try {
                    $fc->save(true);
                  }
                  catch (Exception $e) {
                    \Drupal::logger('import')->error('Error while saving a node: ' . $node->Id());
                  }

              }

            }

          }

          break; // // check only first item

        }

        $changed = true;
        if ( $changed ) {
          $node->field_converted->setValue(1);
          $changed_date = $node->changed->value;
          $node->save();
          konsultant_restore_changed_date($changed_date, $node->Id());
          $results[] = $node->Id();
        }

      }

    }

    if ( empty($context['results']) ) {
      $context['results'] = 0;
    }

    $context['message'] = $message;
    $context['results'] += count($results);

  }

  public static function importConvert ($path) {

    $ext = explode('.', $path);
    $ext = end($ext);

    $newpath = false;

    //if ( in_array($ext, ['doc', 'docx', 'xls', 'xlsx', 'rtf']) ) {

    if ( in_array($ext, ['doc', 'docx', 'rtf']) ) {

       $filename = substr($path, 0, -(strlen($ext) + 1));
       $path =  $filename . '.html';
       $newpath = $path;

    }

    return $newpath;

  }

  static function importDocToDocx($path) {

    $ext = explode('.', $path);
    $ext = end($ext);

    $newpath = false;

    if ( $ext == 'doc' ) {

       $filename = substr($path, 0, -(strlen($ext) + 1));
       $path =  $filename . '.docx';
       $newpath = $path;

    }

    return $newpath;

  }

  function importNodesBatchFinished($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
      $results,
      '1 node processed.', '@count nodes processed.'
      );
    } else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
  }

  function importEventsBatchFinished($success, $results, $operations) {

    unlink($results['file_name']);

    if ($success) {
      $message = \Drupal::translation()->formatPlural(
      $results['count'],
      '1 events processed.', '@count nodes processed.'
      );
    } else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
  }

  public function importTaxonomyOperation(){

     // $result = \Drupal::entityQuery('taxonomy_term')
     //   ->condition('vid', 'filters')
     //   ->execute();
     // entity_delete_multiple('taxonomy_term', $result);
     // return false;

    $systems = [3]; //1 = экология 2 = от //3 = кадровик
    $results = [];

    foreach ( $systems as $system_id ) {
      $line_counter = 0;
      $file_name = "sync/theme-" . $system_id . ".txt";
      $file = new \SplFileObject($file_name);
      if ( $file ) {

        while (!$file->eof()) {
          $line = $file->fgets();
          $json = json_decode($line);
          if ( !empty($json->tid) ) {

            $terms = \Drupal::entityTypeManager()
            ->getStorage('taxonomy_term')
            ->loadByProperties(['vid' => 'filters', 'field_import_id' => $json->tid]);

            $term = reset($terms);

            if ( empty($term) ) {

              $term = Term::create([
                'vid' => 'filters',
                'name' => $json->name,
                'field_import_id' => [
                  'value' => $json->tid
                ],
              ]);

              if ( !empty($json->parent) ) {

                $parents = \Drupal::entityTypeManager()
                  ->getStorage('taxonomy_term')
                 ->loadByProperties(['vid' => 'filters', 'field_import_id' => $json->parent]);
                 $parent = reset($parents);

                if ( !empty($parent) ) {
                   $term->parent = $parent->Id();
                }

              }

              $term->set('field_system', $system_id);
              $term->save();
              $results[] = $json->tid;

            } else {

              $changed = false;

              if ( $json->name != $term->getName() )  {
                $changed = true;
                $term->set('name', $json->name);
              }

              if ( $changed ) {
                $term->save();
                $results[] = $term->Id();
              }

            }
          }
        }
      }

    }


    drupal_set_message('Imported terms: ' . count($results));
    return new RedirectResponse('/admin/import-nodes', 302);
  }

  public static function getDocTypeId($foreign_id) {

    switch ($foreign_id) {

      case "3-769": case "1-72": case "2-76": $local_id = 146; break; //
      case "3-770": case "1-73": case "2-41": $local_id = 147; break; //
      case "3-772": case "1-74": case "2-113": $local_id = 148; break; //
      case "3-773": case "1-75": case "2-27": $local_id = 149; break; //
      case "3-774": case "1-76": case "2-28": $local_id = 150; break; //
      case "3-777": case "1-253": case "2-44": $local_id = 151; break; //
      case "3-776": case "1-78": $local_id = 152; break; //
      case "3-775": case "1-251": $local_id = 153; break; //
      case "3-835": case "1-30": $local_id = 154; break; //
      case "3-778": case "1-80": $local_id = 155; break; //
      case "3-768": case "1-96": case "2-123": $local_id = 156; break; //
      case "1-99": case "2-33": $local_id = 157; break; //
      case "1-115": case "2-29": $local_id = 158; break; //
      case "2-265": $local_id = 159; break; //
      case "1-255": $local_id = 160; break; //
      case "1-97": $local_id = 161; break; //
      case "3-771": $local_id = 162; break; //
      case "3-779": $local_id = 169; break; //
      case "2-118": $local_id = 163; break; //
      case "2-48": $local_id = 164; break; //
      case "2-25": $local_id = 165; break; //
      case "2-46": $local_id = 166; break; //
      case "2-31": $local_id = 167; break; //
      case "2-43": $local_id = 168; break; //
      default: $local_id = 0; break; //
    }

    return $local_id;
  }

  public static function getNormImportId ($import_id){

    switch ($import_id) {

      case "1-256": $id = "1-256"; break; // //Національна комісія, що здійснює державне регулювання у сферах енергетики та комунальних послуг
      case "1-63": $id = "3-709"; break; // //Верховна рада україни
      case "1-259": $id = "1-259"; break; // //Державна комісія україни по запасах корисних копалин
      case "1-260": $id = "3-823"; break; // //Державна фіскальна служба україни
      case "1-246": $id = "1-246"; break; // //Державна податкова адміністрація україни
      case "1-249": $id = "1-249"; break; // //Державна податкова служба україни
      case "1-267": $id = "1-267"; break; // //Державна регуляторна служба україни --- 1-249
      case "1-245": $id = "3-843"; break; // //Державна служба статистики україни
      case "1-92": $id = "1-92"; break; // //Головний державний санітарний лікар україни
      case "1-100": $id = "1-100"; break; // //Державні комітети
      case "1-104": $id = "1-104"; break; // //Державний комітет стандартизації, метрології та сертифікації україни --- 1-100
      case "1-112": $id = "1-112"; break; // //Державний комітет лісового господарства україни --- 1-100
      case "1-60": $id = "3-707"; break; // //Державний комітет статистики україни  --- 1-100
      case "1-86": $id = "1-86"; break; // //Державний комітет україни з питань регуляторної політики та підприємництва --- 1-100
      case "1-81": $id = "1-81"; break; // //Державний комітет україни з промислової безпеки, охорони праці та гірничого нагляду --- 1-100
      case "1-114": $id = "1-114"; break; // //Державний комітет будівництва,  архітектури та житлової політики україни --- 1-100
      case "1-190": $id = "1-190"; break; // //Державний комітет україни з питань житлово-комунального господарства
      case "1-244": $id = "1-244"; break; // //Державна служба геології та надр україни
      case "1-64": $id = "3-710"; break; // //Кабінет міністрів україни
      case "1-65": $id = "3-711"; break; // //Міністерства --- 1-64
      case "1-137": $id = "3-712"; break; // //Міністерство аграрної політики --- 1-64
      case "1-84": $id = "1-84"; break; // //Міністерство екології та природних ресурсів україни --- 1-64
      case "1-107": $id = "3-841"; break; // //Міністерство економічного розвитку і торгівлі україни --- 1-64
      case "1-262": $id = "3-718"; break; // //Міністерство енергетики та вугільної промисловості україни --- 1-64
      case "1-243": $id = "1-243"; break; // //Міністерство з питань житлово-комунального господарства україни --- 1-64
      case "1-111": $id = "3-719"; break; // //Міністерство інфрастуктури україни --- 1-64
      case "1-83": $id = "3-726"; break; // //Міністерство охорони здоров'я україни  --- 1-64
      case "1-131": $id = "3-727"; break; // //Міністерство охорони навколишнього природного середовища україни --- 1-64
      case "1-108": $id = "3-730"; break; // //Міністерство промислової політики україни --- 1-64
      case "1-90": $id = "1-90"; break; // //Міністерство регіонального розвитку, будівництва та житлово-комунального господарства україни --- 1-64
      case "1-235": $id = "1-235"; break; // //Міністерство статистики україни --- 1-64
      case "1-173": $id = "3-732"; break; // //Міністерство транспорту україни --- 1-64
      case "1-154": $id = "3-734"; break; // //Міністерство фінансів україни --- 1-64
      case "1-62": $id = "3-708"; break; // //Президент україни
      case "1-87": $id = "1-87"; break; // //Рада національної безпеки і оборони україни

      case "2-15": $id = "3-709"; break; //  //Верховна рада україни
      case "2-132": $id = "1-267"; break; //  //Державна регуляторна служба україни
      case "2-127": $id = "3-843"; break; //  //Державна служба статистики україни
      case "2-119": $id = "2-119"; break; //  //Державна служба україни з надзвичайних ситуацій
      case "2-126": $id = "3-844"; break; //  //Державна служба україни з питань праці
      case "2-39": $id = "3-826"; break; //  //Державна служба гірничого нагляду та промислової безпеки україни
      case "2-129": $id = "3-823"; break; //  //Державна фіскальна служба україни
      case "2-16": $id = "2-16"; break; //  //Державний комітет україни з нагляду за охороною праці
      case "2-14": $id = "1-81"; break; //  //Державний комітет україни з промислової безпеки, охорони праці та гірничого нагляду
      case "2-18": $id = "3-710"; break; //  //Кабінет міністрів україни
      case "2-112": $id = "3-712"; break; //  //Міністерство аграрної політики україни
      case "2-122": $id = "3-714"; break; //  //Міністерство внутрішніх справ україни
      case "2-71": $id = "3-718"; break; //  //Міністерство енергетики та вугільної промисловості україни
      case "2-128": $id = "3-719"; break; //  //Міністерство інфраструктури україни
      case "2-42": $id = "3-723"; break; //  //Міністерство надзвичайних ситуацій україни
      case "2-24": $id = "3-825"; break; //  //Міністерство освіти і науки україни
      case "2-23": $id = "3-726"; break; //  //Міністерство охорони здоров’я україни
      case "2-47": $id = "3-728"; break; //  //Міністерство палива та енергетики україни
      case "2-38": $id = "3-729"; break; //  //Міністерство праці україни
      case "2-21": $id = "3-730"; break; //  //Міністерство промислової політики україни
      case "2-22": $id = "3-731"; break; //  //Міністерство соціальної політики україни
      case "2-121": $id = "2-121"; break; //  //Міністерство україни з питань надзвичайних ситуацій
      case "2-20": $id = "3-708"; break; //  //Президент україни
      case "2-72": $id = "2-72"; break; //  //Президія верховної ради української рср
      case "2-19": $id = "2-19"; break; //  //Президія федерації професійних спілок україни
      case "2-17": $id = "2-17"; break; //  //Фонд соціального страхування від нещасних випадків на виробництві та професійних захворювань україни

      default: $id = $import_id; break;
    }

    return $id;
  }

  public function importTaxonomyNormOperation(){

   //  $tids = \Drupal::entityQuery('taxonomy_term')
   //    ->condition('vid', 'doc_producer')
   //    ->execute();
   //
   //  $controller = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
   //  $entities = $controller->loadMultiple($tids);
   //  $controller->delete($entities);
   //
   // return false;

    $systems = [3,1,2]; //1 = экология 2 = от //3 = кадровик
    $results = [];

    foreach ( $systems as $system_id ) {
      $line_counter = 0;
      $file_name = "sync/prod-" . $system_id . ".txt";
      $file = new \SplFileObject($file_name);
      if ( $file ) {

        while (!$file->eof()) {
          $line = $file->fgets();
          $json = json_decode($line);
          if ( !empty($json->tid) ) {

            $import_id = self::getNormImportId($json->tid);

            $terms = \Drupal::entityTypeManager()
            ->getStorage('taxonomy_term')
            ->loadByProperties(['vid' => 'doc_producer', 'field_import_id' => $import_id]);
            $term = reset($terms);

            if ( empty($term) ) {

              $term = Term::create([
                'vid' => 'doc_producer',
                'name' => $json->name,
                'field_import_id' => [
                  'value' => self::getNormImportId($json->tid)
                ],
              ]);

              if ( !empty($json->parent) ) {

                $parents = \Drupal::entityTypeManager()
                  ->getStorage('taxonomy_term')
                 ->loadByProperties(['vid' => 'doc_producer', 'field_import_id' => self::getNormImportId($json->parent)]);
                 $parent = reset($parents);

                if ( !empty($parent) ) {
                   $term->parent = $parent->Id();
                 }

              }

              $term->save();
              $results[] = self::getNormImportId($json->tid);

            } else {

              $changed = false;

              if ( $json->name != $term->getName() )  {
                $changed = true;
                $term->set('name', $json->name);
              }

              if ( $changed ) {
                $term->save();
                $results[] = $term->Id();
              }

            }
          }
        }
      }

    }


    drupal_set_message('Imported terms: ' . count($results));
    return new RedirectResponse('/admin/import-nodes', 302);
  }

  public function importTest () {

    $file = file_get_contents("/home/ace/projects/konsultant/konsultant.test/sites/default/files/normdocs/pro_zatverdzhennya_gigiyenichnyh_reglamentiv_himichnyh_rechov.html");
    $html = self::fixImportedHtml($file);

    return [
      '#type' => 'inline_template',
      '#template' => $html
    ];

  }

  public static function fixImportedHtml($data) {

    $emogrifier = new \Pelago\Emogrifier();
    $emogrifier->setHtml($data);

    //конвертируем стили в inline формат
    $mergedHtml = $emogrifier->emogrify();
    $html = $emogrifier->emogrifyBodyContent();

    //убираем аттрибуты class и id
    $html = preg_replace('/class=".*?"/i', '', $html);
    $html = preg_replace('/id=".*?"/i', '', $html);
    $html = str_replace('.', '', $html);//булет
    $html = str_replace('.', '', $html);//галочка
    //убираем первый вложеный p в li
    $html = preg_replace('#<li(.*?)><p(.*?)>(.*?)</p></li>#is', '<li$1>$3</li>', $html);
    //убираем пробел перед </li>
    $html = str_replace(' </li>', '</li>', $html);

    //вырезаем теги img
    $html = preg_replace("/<img[^>]+\>/i", '', $html);

    //помогаем определить кодировку
    $doc = new \DOMDocument();
    libxml_use_internal_errors(true);
    $doc->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=utf-8">' . $html);

    //выполняем чистку dom
    $body = $doc->getElementsByTagName('body');

    $iterations = 5;

    for( $i=0; $i < $iterations; $i++) {
      self::htmlCleanUp($body, $doc);
    }

    $html = $doc->saveHTML();

    //оставляем только содержимое тега body
    $html = preg_replace("/.*<body[^>]*>|<\/body>.*/si", "", $html);

    $html = trim($html);

    //удаляем пустые инлайн теги до 5 вложенностей
    for ($times=5; $times > 0; --$times) {
      $html = preg_replace("/<(span|a|strong|em)><\/(span|a|strong|em)>/i", '', $html);
    }

    //удаляем лишние переходы строк в начале тегов
    $html = preg_replace("/<(.*?) (.*?)>\n/i", "<$1 $2>", $html);

    //удаляем комментарии
    $html = preg_replace("/<!--.*?--\>/ms", "", $html);

    return $html;

  }

  public static function htmlCleanUp($elements, $dom){

    foreach ( $elements as $element ) {
      // выносим содержимое тегов font наверх, сами теги удаляем.
      if ( $element->nodeName == 'font' ) {
        $newParent = $element->parentNode;
        foreach ( $element->childNodes as $child ){
            $newParent->insertBefore($child->cloneNode(true), $element);
        }
        $newParent->removeChild($element);
      }

      //читаем стили
      if ( $element->hasAttributes()) {
        $style = $element->getAttribute('style');
      } else {
        $style = '';
      }

      if ( !empty($style) ) {
        //конвертируем пункты в пиксели
        $style = preg_replace_callback('|([\d]+)pt|i', function ($m) {
          return round(3 / 2.3 * $m[1]) . 'px';
        }, $style);

        //конвертируем дюймы в пиксели
        $style = preg_replace_callback('#(\d+\.\d+|\d+)in#i', function ($m) {
          return round(96 * $m[1]) . 'px';
        }, $style);

        //конвертируем сантиметры в пиксели
        $style = preg_replace_callback('#(\d+\.\d+|\d+)cm#i', function ($m) {
          return round(96 * $m[1] / 2.54) . 'px';
        }, $style);

        //убираем мусор
        $style = preg_replace('#(clear|widows|orphans|empty-cells|border-spacing|border-collapse|writing-mode|line-height|text-indent|mso-|background).*?:.*?;#i', '', $style);
        //убираем list-style: none;
        $style = preg_replace('#list-style: none;#i', '', $style);
        //убираем display: block;;
        $style = preg_replace('#display: block;;#i', '', $style);
        //убираем margin-ы
        $style = preg_replace('#margin.*?;#i', '', $style);
        //убираем float-ы
        $style = preg_replace('#float.*?;#i', '', $style);
        //убираем position-ы
        $style = preg_replace('#position.*?;#i', '', $style);
        $style = preg_replace('#($| |;)(left|right|top|bottom):.*?;#i', '', $style);
        //убираем шрифты
        $style = preg_replace('#font-family.*?;#i', '', $style);
        //убираем размеры шрифтов
        $style = preg_replace('#font-size.*?;#i', '', $style);
        //убираем цвета
        $style = preg_replace('#($| |;)color:.*?;#i', '', $style);
        //$style = str_replace(' color: #000;', '', $style);
        //$style = str_replace(' color: #0000ff;', '', $style);
        //$style = str_replace(' color: #000000;', '', $style);
        //$style = str_replace(';color: #000', '', $style);
        //$style = str_replace('; color: #000000', '', $style);
        $style = str_replace('! important', '', $style);
        $style = str_replace('!important', '', $style);

        //пытаемся стандартизировать стили границ у таблиц
        $style = preg_replace('#($| |;)border-(left|right|top|bottom)-width:.*?;#i', '$1border-$2-width: 1px;', $style);
        $style = preg_replace('#($| |;)border-(left|right|top|bottom)-style: double;#i', '$1border-$2-style: solid;', $style);
        $style = preg_replace('#($| |;)border-(left|right|top|bottom)-color:.*?;#i', '', $style);
        $style = preg_replace('#($| |;)border-width:.*?;#i', '$1border-width: 1px;', $style);
        $style = preg_replace('#($| |;)border-style: double;#i', '$1border-style: solid;', $style);
        $style = preg_replace('#($| |;)border-color:.*?;#i', '', $style);
      }

      //вырезаем margin-ы из p и h тегов
      // if ( $element->nodeName == 'p' || in_array($element->nodeName, ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']) ) {
      //   $margin_parts = explode('margin:', $style, 2);
      //   if ( count($margin_parts) > 1 ) {
      //     $margin_before = $margin_parts[0];
      //     $margin_after = '';
      //     if ( $margin_parts[1] ) {
      //       $margin_after = explode(';', $margin_parts[1], 2);
      //       if ( count($margin_after) > 1 ) {
      //         $margin_after = $margin_after[1];
      //       }
      //     }
      //     $style = $margin_before . $margin_after;
      //   }
      // }


      if ( $element->hasAttributes() ) {
        //удаляем атрибут 'lang'
        $element->removeAttribute('lang');

        // if ( strpos($element->getAttribute('id'), 'Frame') !== false ) {
        //   $element->removeAttribute('id');
        //   $style = preg_replace('#($| |;)width:.*?;#', '', $style);
        //   $style = str_replace('float: left;', '', $style);
        // }

        //заменяем стиль italic и bold на теги em и strong
        if ( strpos($style, "font-style: italic;") !== false && strpos($style, "font-weight: bold;") !== false ) {
          if ( !in_array($element->nodeName, ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']) && !in_array($element->parentNode->nodeName, ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']) ) {
            if ( count($element->childNodes) == 1 ) {
              $style = preg_replace('#font-weight: bold;#i', '', $style);
              $style = preg_replace('#font-style: italic;#i', '', $style);
              $strong = $dom->createElement('strong');
              $italic = $dom->createElement('em');
              $italic->nodeValue = $element->nodeValue;
              $strong->appendChild($italic);
              $element->nodeValue = '';
              $element->appendChild($strong);
            }
          }
        }

        //заменяем стиль bold на тег strong
        if ( strpos($style, "font-weight: bold;") !== false ) {
          if ( !in_array($element->nodeName, ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']) && !in_array($element->parentNode->nodeName, ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']) ) {
            if ( count($element->childNodes) == 1 ) {
              $style = preg_replace('#font-weight: bold;#i', '', $style);
              $strong = $dom->createElement('strong');
              $strong->nodeValue = $element->nodeValue;
              $element->nodeValue = '';
              $element->appendChild($strong);
            }
          }
        }

        //заменяем стиль italic на тег em
        if ( strpos($style, "font-style: italic;") !== false ) {
          if ( !in_array($element->nodeName, ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']) && !in_array($element->parentNode->nodeName, ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']) ) {
            if ( count($element->childNodes) == 1 ) {
              $style = preg_replace('#font-style: italic;#i', '', $style);
              $italic = $dom->createElement('em');
              $italic->nodeValue = $element->nodeValue;
              $element->nodeValue = '';
              $element->appendChild($italic);
            }
          }
        }

        $style = preg_replace('#font-style.*?;#i', '', $style);
        $style = preg_replace('#font-weight.*?;#i', '', $style);

        //чиним ширину table и выставляем border-collapse: collapse;
        if ( $element->nodeName == 'table') {

          $element->removeAttribute('cellspacing');
          //$element->removeAttribute('border');
          $element->removeAttribute('cellpadding');
          $el_width = $element->getAttribute('width');
          if ( substr($el_width, -1, 1) == '%' && intVal($el_width) > 100) {
            $element->setAttribute('width', '100%');
          }
          $style = preg_replace('#($| |;)width:.*?;#', '', $style);
          if ( empty($style) ) {
            $style = 'border-collapse: collapse;';
          } else {
            if ( strpos($style, "border-collapse: collapse;") == false ) {
              $style .= ' border-collapse: collapse;';
            }
          }
        }

      }

      if ( $element->hasAttributes() ) {
        if ( empty(trim($style)) ) {
          $element->removeAttribute('style');
        } else {
          //очистка стиля от лишних пробелов
          $style = preg_replace('/\s+/', ' ', trim($style));
          $style = preg_replace('! ;!', ';', $style);
          $element->setAttribute('style', $style);
        }
      }

      if ( !empty($element->childNodes) && count($element->childNodes) ) {
         self::htmlCleanUp($element->childNodes, $dom);
      }

    }

  }

  public function importExamplesBatch(){

    $chunks = [];
    $chunk_size = 1;

    $systems = [1,2,3]; //1 = экология 2 = от //3 = кадровик
    $operations = [];

    foreach ( $systems as $system_id ) {

      $file = "sync/xmp-" . $system_id . ".txt";

      $linecount = 0;
      $handle = fopen($file, "r");

      if ( $handle ) {
        while(!feof($handle)){
          $line = fgets($handle);
          $linecount++;
        }
        fclose($handle);
      }

      for ( $i = 0; $i < $linecount; $i += $chunk_size ) {
        if ( $i + $chunk_size < $linecount ) {
          $chunks[] = (object)['begin' => $i, 'end' => $i + $chunk_size - 1, 'system_id' => $system_id];
        } else {
          $chunks[] = (object)['begin' => $i, 'end' => $linecount - 1, 'system_id' => $system_id];
        }
      }

      foreach ($chunks as $chunk) {
        $operations[] = ['\Drupal\konsultant_import\ImportpageController::importExamplesBatchOperation', [$chunk]];
      }

    }

    $batch = array(
        'title' => 'Importing nodes',
        'operations' => $operations,
        'finished' => 'importNodesBatchFinished',
    );

    batch_set($batch);
    return batch_process('/admin/import-nodes');

  }

  public static function importExamplesBatchOperation($chunk, &$context){

    $line_counter = 0;
    $chunk_data = [];
    $system_id = $chunk->system_id;
    $file_name = "sync/xmp-" . $system_id . ".txt";
    $file = new \SplFileObject($file_name);

    if ( $file ) {

      while (!$file->eof()) {
        $line = $file->fgets();
        if ($line_counter >= $chunk->begin && $line_counter <= $chunk->end ){
          $chunk_data[] = $line;
        }
        if ($line_counter > $chunk->end ){
          break;
        }
        $line_counter++;
      }

    };

    $message = 'Importing Node...';
    $results = [];
    foreach ($chunk_data as $data_line) {
      $json = json_decode($data_line);
      if ( !empty($json->nid) ) {

        $nodes = \Drupal::entityTypeManager()
          ->getStorage('node')
          ->loadByProperties(['type' => 'example', 'field_import_id' => $json->nid]);

        $node = reset($nodes);

        $filters = [];

        if (!empty($json->themes)) {

          foreach ( $json->themes as $theme ) {
            $terms = \Drupal::entityTypeManager()
              ->getStorage('taxonomy_term')
              ->loadByProperties(['vid' => 'filters', 'field_import_id' => $theme]);

            $term = reset($terms);

            if ( !empty($term) ) {
              $filters[] = $term->Id();
            }

          }

        }

        if ( empty($node) ) {
          // $newnode = [
          //   'type' => 'example',
          //   'title' => $json->title,
          //   'field_systems' => [$system_id],
          //   'field_import_id' => [
          //     'value' => $json->nid
          //   ],
          //   'field_filter' => $filters,
          // ];
          //
          // if ( !empty($json->example_type) ) {
          //
          //   $example_type_id = self::getExampleTypeId($json->example_type);
          //
          //   if ( empty($example_type_id) ) {
          //     $newnode['field_example_type'] = $example_type_id;
          //   }
          //
          // }
          //
          // $node = Node::create($newnode);
          //
          // $node->setCreatedTime($json->created);
          // $node->save();
          //
          // if ( !empty($json->example_file) ) {
          //   $data = base64_decode($json->example_file);
          //   $file_name = konsultant_transliteration_filename( $json->nid . '-' . $json->example_file_name);
          //   $directory = 'public://examples/';
          //   if ( file_prepare_directory($directory, FILE_CREATE_DIRECTORY) ) {
          //
          //     $file = file_save_data($data, $directory . $file_name);
          //
          //     $node->field_example_file->setValue($file->Id());
          //     $node->save();
          //   };
          //
          // }
          //
          // $results[] = $json->nid;

        } else {

          $changed = false;

          // if ( $json->title != $node->getTitle() )  {
          //   $changed = true;
          //   $node->set('title', $json->title);
          // }
          //
          // if ( $json->created != $node->getCreatedTime() )  {
          //   $changed = true;
          //   $node->setCreatedTime($json->created);
          // }

          // if ( !empty($json->example_type) ) {
          //   $example_type_id = self::getExampleTypeId($json->example_type);
          //
          //   if ( !empty($example_type_id) ) {
          //     if (
          //       empty($node->get('field_example_type')->getValue()) ||
          //       $example_type_id != $node->get('field_example_type')->getValue()[0]['target_id']
          //     ) {
          //       $node->field_example_type->setValue($example_type_id);
          //       $changed = true;
          //     }
          //   }
          // }

          // if ( !empty($json->example_file) ) {
          //
          //   if ( !empty($node->field_example_file->getValue()) ) {
          //     $file = \Drupal\file\Entity\File::load($node->field_example_file->getValue()[0]['target_id']);
          //   }
          //
          //   $data = base64_decode($json->example_file);
          //
          //   if ( empty($file) || intVal($file->getSize()) != strlen($data) ) {
          //
          //     $file_name = konsultant_transliteration_filename( $json->nid . '-' . $json->example_file_name);
          //     $directory = 'public://examples/';
          //
          //     if ( file_prepare_directory($directory, FILE_CREATE_DIRECTORY) ) {
          //
          //       $file = file_save_data($data, $directory . $file_name);
          //       $node->field_example_file->setValue($file->Id());
          //       $changed = true;
          //
          //     };
          //
          //   }
          //
          // }

          $node->field_filter->setValue($filters);

          //if ( $changed ) {
            $changed_date = $node->changed->value;
            $node->save();
            konsultant_restore_changed_date($changed_date, $node->Id());
            $results[] = $node->Id();
          //}

        }

      }
    }
    if ( empty($context['results']) ) {
      $context['results'] = 0;
    }
    $context['message'] = $message;
    $context['results'] += count($results);
  }

  public function importNewsBatch(){

    $chunks = [];
    $chunk_size = 10;

    $systems = [1,2]; //1 = экология 2 = от //3 = кадровик
    $operations = [];

    foreach ( $systems as $system_id ) {

      $file = "sync/new-" . $system_id . ".txt";

      $linecount = 0;
      $handle = fopen($file, "r");

      if ( $handle ) {
        while(!feof($handle)){
          $line = fgets($handle);
          $linecount++;
        }
        fclose($handle);
      }

      for ( $i = 0; $i < $linecount; $i += $chunk_size ) {
        if ( $i + $chunk_size < $linecount ) {
          $chunks[] = (object)['begin' => $i, 'end' => $i + $chunk_size - 1, 'system_id' => $system_id];
        } else {
          $chunks[] = (object)['begin' => $i, 'end' => $linecount - 1, 'system_id' => $system_id];
        }
      }

      foreach ($chunks as $chunk) {
        $operations[] = ['\Drupal\konsultant_import\ImportpageController::importNewsBatchOperation', [$chunk]];
      }

    }


    $batch = array(
        'title' => 'Importing nodes',
        'operations' => $operations,
        'finished' => 'importNodesBatchFinished',
    );

    batch_set($batch);
    return batch_process('/admin/import-nodes');

  }

  public static function importNewsBatchOperation($chunk, &$context){

    $line_counter = 0;
    $chunk_data = [];
    $system_id = $chunk->system_id;
    $file_name = "sync/new-" . $system_id . ".txt";
    $file = new \SplFileObject($file_name);

    if ( $file ) {

      while (!$file->eof()) {
        $line = $file->fgets();
        if ($line_counter >= $chunk->begin && $line_counter <= $chunk->end ){
          $chunk_data[] = $line;
        }
        if ($line_counter > $chunk->end ){
          break;
        }
        $line_counter++;
      }

    };

    $message = 'Importing Node...';
    $results = [];
    foreach ($chunk_data as $data_line) {
      $json = json_decode($data_line);
      if ( !empty($json->nid) ) {

        $nodes = \Drupal::entityTypeManager()
          ->getStorage('node')
          ->loadByProperties(['type' => 'new', 'field_import_id' => $json->nid]);

        $node = reset($nodes);

        if ( empty($node) ) {
          $newnode = [
            'type' => 'new',
            'title' => $json->title,
            'field_systems' => [$system_id],
            'field_import_id' => [
              'value' => $json->nid
            ]
          ];

          if ( !empty($json->new_type) ) {

            $new_type_id = self::getNewTypeId($json->new_type);

            if ( !empty($new_type_id) ) {
              $newnode['field_new_type'] = $new_type_id;
            }

          }

          if ( !empty($json->new_html) ) {
              $newnode['field_new_html'] = $json->new_html;
          }

          $node = Node::create($newnode);
          $node->setCreatedTime($json->created);
          $node->save();

          if ( !empty($json->new_image_file) ) {
            $data = base64_decode($json->new_image_file);
            $file_name = konsultant_transliteration_filename( $json->nid . '-' . $json->new_image_file_name);
            $directory = 'public://new/';
            if ( file_prepare_directory($directory, FILE_CREATE_DIRECTORY) ) {

              $file = file_save_data($data, $directory . $file_name);

              $node->field_new_image->setValue($file->Id());
              $node->save();
            };

          }

          $results[] = $json->nid;

        } else {

          $changed = false;

          if ( $json->title != $node->getTitle() )  {
            $changed = true;
            $node->set('title', $json->title);
          }


          if ( !empty($json->new_type) ) {

            $new_type_id = self::getNewTypeId($json->new_type);

            if ( !empty($new_type_id) ) {
              $node->field_new_type->setValue($new_type_id);
              $changed = true;
            }

          }

          if ( $json->created != $node->getCreatedTime() )  {
            $changed = true;
            $node->setCreatedTime($json->created);
          }

          if ( $changed ) {
            $changed_date = $node->changed->value;
            $node->save();
            konsultant_restore_changed_date($changed_date, $node->Id());
            $results[] = $node->Id();
          }

        }

      }
    }
    if ( empty($context['results']) ) {
      $context['results'] = 0;
    }
    $context['message'] = $message;
    $context['results'] += count($results);
  }

  public function importInstructionsBatch(){

    $chunks = [];
    $chunk_size = 10;

    $systems = [1,2,3]; //1 = экология 2 = от //3 = кадровик
    $operations = [];

    foreach ( $systems as $system_id ) {

      $file = "sync/ins-" . $system_id . ".txt";

      $linecount = 0;
      $handle = fopen($file, "r");

      if ( $handle ) {
        while(!feof($handle)){
          $line = fgets($handle);
          $linecount++;
        }
        fclose($handle);
      }

      for ( $i = 0; $i < $linecount; $i += $chunk_size ) {
        if ( $i + $chunk_size < $linecount ) {
          $chunks[] = (object)['begin' => $i, 'end' => $i + $chunk_size - 1, 'system_id' => $system_id];
        } else {
          $chunks[] = (object)['begin' => $i, 'end' => $linecount - 1, 'system_id' => $system_id];
        }
      }

      foreach ($chunks as $chunk) {
        $operations[] = ['\Drupal\konsultant_import\ImportpageController::importInstructionsBatchOperation', [$chunk]];
      }

    }


    $batch = array(
        'title' => 'Importing nodes',
        'operations' => $operations,
        'finished' => 'importNodesBatchFinished',
    );

    batch_set($batch);
    return batch_process('/admin/import-nodes');

  }

  public static function importInstructionsBatchOperation($chunk, &$context){

    $line_counter = 0;
    $chunk_data = [];
    $system_id = $chunk->system_id;
    $file_name = "sync/ins-" . $system_id . ".txt";
    $file = new \SplFileObject($file_name);

    if ( $file ) {

      while (!$file->eof()) {
        $line = $file->fgets();
        if ($line_counter >= $chunk->begin && $line_counter <= $chunk->end ){
          $chunk_data[] = $line;
        }
        if ($line_counter > $chunk->end ){
          break;
        }
        $line_counter++;
      }

    };

    $message = 'Importing Node...';
    $results = [];
    foreach ($chunk_data as $data_line) {
      $json = json_decode($data_line);
      if ( !empty($json->nid) ) {

        $nodes = \Drupal::entityTypeManager()
          ->getStorage('node')
          ->loadByProperties(['type' => 'instruction', 'field_import_id' => $json->nid]);

        $node = reset($nodes);

        $filters = [];

        if (!empty($json->themes)) {

          foreach ( $json->themes as $theme ) {
            $terms = \Drupal::entityTypeManager()
              ->getStorage('taxonomy_term')
              ->loadByProperties(['vid' => 'filters', 'field_import_id' => $theme]);

            $term = reset($terms);

            if ( !empty($term) ) {
              $filters[] = $term->Id();
            }

          }

        }

        if ( empty($node) ) {
          $newnode = [
            'type' => 'instruction',
            'title' => $json->title,
            'field_systems' => [$system_id],
            'field_import_id' => [
              'value' => $json->nid
            ],
            'field_filter' => $filters,
          ];

          if ( !empty($json->instruction_type) ) {
            $newnode['field_instruction_type'] = $json->instruction_type;
          }

          $node = Node::create($newnode);

          $node->setCreatedTime($json->created);
          $node->save();

          if ( !empty($json->document_file) ) {
            $data = base64_decode($json->document_file);
            $file_name = konsultant_transliteration_filename( $json->nid . '-' . $json->document_file_name);
            $directory = 'public://instructions/';
            if ( file_prepare_directory($directory, FILE_CREATE_DIRECTORY) ) {

              $file = file_save_data($data, $directory . $file_name);

              $node->field_instruction_file->setValue($file->Id());
              $node->save();
            };

          }

          $results[] = $json->nid;

        } else {

          $changed = false;

          if ( $json->title != $node->getTitle() )  {
            $changed = true;
            $node->set('title', $json->title);
          }

          if ( $json->created != $node->getCreatedTime() )  {
            $changed = true;
            $node->setCreatedTime($json->created);
          }

          if ( !empty($json->instruction_type) && empty($node->get('field_instruction_type')->getValue())) {
            $node->field_instruction_type->setValue($json->instruction_type);
            $changed = true;
          }

          if ( !empty($json->document_file) ) {

            if ( !empty($node->field_instruction_file->getValue()) ) {
              $file = \Drupal\file\Entity\File::load($node->field_instruction_file->getValue()[0]['target_id']);
            }

            $data = base64_decode($json->document_file);

            if ( empty($file) || intVal($file->getSize()) != strlen($data) ) {

              $file_name = konsultant_transliteration_filename( $json->nid . '-' . $json->document_file_name);
              $directory = 'public://instructions/';

              if ( file_prepare_directory($directory, FILE_CREATE_DIRECTORY) ) {

                $file = file_save_data($data, $directory . $file_name);
                $node->field_instruction_file->setValue($file->Id());
                $changed = true;

              };

            }

          }

          if ( empty($node->get('field_systems')->getValue())) {
            $node->set('field_systems', [$system_id] );
            $changed = true;
          }

          if ( $changed ) {
            $changed_date = $node->changed->value;
            $node->save();
            konsultant_restore_changed_date($changed_date, $node->Id());
            $results[] = $node->Id();
          }

        }

      }
    }
    if ( empty($context['results']) ) {
      $context['results'] = 0;
    }
    $context['message'] = $message;
    $context['results'] += count($results);
  }

  public static function getExampleTypeId($foreign_id) {
    $foreign_id = current($foreign_id);
    switch ($foreign_id) {

      case "3-648": $local_id = 618; break; // Акти
      case "3-546": $local_id = 619; break; // Графік відпусток
      case "3-782": $local_id = 620; break; // Договір
      case "3-642": $local_id = 621; break; // Доповідні записки
      case "3-548": $local_id = 622; break; // Записи у трудові книжки
      case "3-641": $local_id = 623; break; // Заяви
      case "3-640": $local_id = 624; break; // Звітність
      case "3-649": $local_id = 625; break; // Клопотання
      case "3-596": $local_id = 626; break; // Колективний договір
      case "3-465": $local_id = 627; break; // Накази
      case "3-595": $local_id = 628; break; // Номенклатура справ
      case "3-460": $local_id = 629; break; // Облікові документи з персоналу
      case "3-643": $local_id = 630; break; // Повідомлення
      case "3-767": $local_id = 631; break; // Подання
      case "3-594": $local_id = 632; break; // Положення про структурний підрозділ
      case "3-545": $local_id = 633; break; // Посадові інструкції
      case "3-593": $local_id = 634; break; // Правила внутрішнього трудового розпорядку
      case "3-651": $local_id = 635; break; // Протоколи
      case "3-702": $local_id = 636; break; // Табель обліку робочого часу
      case "3-698": $local_id = 637; break; // Форми
      case "3-547": $local_id = 638; break; // Штатний розпис
      case "3-838": $local_id = 639; break; // Довідка

      default: $local_id = 0; break;
    }

    return $local_id;
  }

  public static function getNewTypeId($foreign_id) {

    switch ($foreign_id) {

      case "1-239": case "2-106": $local_id = 668; break; // Останні новини
      case "1-240": case "2-107": $local_id = 669; break; // Зміни законодавства
      case "1-241": case "2-108": $local_id = 670; break; // Події
      default: $local_id = 0; break;

    }

    return $local_id;
  }


  public function importConvertExamplesBatch(){

    $chunks = [];
    $chunk_size = 1;

    $operations = [];

    $query = \Drupal::entityQuery('node');
    // $group = $query->orConditionGroup()
    //   ->condition('field_converted', NULL, 'IS NULL')
    //   ->condition('field_converted', '1', '<>');
    // $query->condition($group);
    $query->condition('nid', '49666', '>');
    $query->condition('type', 'example');

    $nids = $query->execute();
    //$nids = array_slice($nids, 1416);//1416 682
    //echo (count($nids));die();
    $chunks = array_chunk($nids, $chunk_size);

    foreach ($chunks as $chunk) {
      $operations[] = ['\Drupal\konsultant_import\ImportpageController::importConvertExamplesBatchOperation', [$chunk]];
    }

    $batch = array(
      'title' => 'Converting docs',
      'operations' => $operations,
      'finished' => 'importNodesBatchFinished',
    );

    batch_set($batch);
    return batch_process('/admin/import-nodes');

  }

  public static function importConvertExamplesBatchOperation($chunk, &$context){
    $message = 'Converting Node...';
    $results = [];

    $nodes = node_load_multiple($chunk);

    foreach ($nodes as $node) {

      $changed = false;

      if ( !empty($node) ) {

        $file = false;

        if ( !empty($node->field_example_file->getValue()) ) {
          $file = \Drupal\file\Entity\File::load($node->field_example_file->getValue()[0]['target_id']);
        }

        if ( !empty($file) ) {

          $uri = $file->getFileUri();

          $path = \Drupal::service('file_system')->realpath($uri);

          if ( !empty($path) ) {

            $converted_path = self::importConvert($path);

            if ( !empty($converted_path) ) {

              konsultant_unoconv($path, $converted_path);
              sleep(1);

              $data = file_get_contents($converted_path);

              unlink($converted_path);

            }

            if ( !empty($data) ) {
              $fixed_data = self::fixImportedHtml($data);
              $node->field_example_html->setValue($fixed_data);
            }

          }

        }

        $changed = true;

      }

      if ( $changed ) {
        $node->field_converted->setValue(1);
        $changed_date = $node->changed->value;
        $node->save();
        konsultant_restore_changed_date($changed_date, $node->Id());
        $results[] = $node->Id();
      }

      sleep(1);

    }

    if ( empty($context['results']) ) {
      $context['results'] = 0;
    }

    $context['message'] = $message;
    $context['results'] += count($results);

  }

  public function importConvertInstructionsBatch(){

    $chunks = [];
    $chunk_size = 1;

    $operations = [];

    $query = \Drupal::entityQuery('node');
    $group = $query->orConditionGroup()
      ->condition('field_converted', NULL, 'IS NULL')
      ->condition('field_converted', '1', '<>');
    $query->condition($group);
    $query->condition('type', 'instruction');

    $nids = $query->execute();
    //$nids = array_slice($nids, 1416);//1416 682
    $chunks = array_chunk($nids, $chunk_size);

    foreach ($chunks as $chunk) {
      $operations[] = ['\Drupal\konsultant_import\ImportpageController::importConvertInstructionsBatchOperation', [$chunk]];
    }

    $batch = array(
      'title' => 'Converting docs',
      'operations' => $operations,
      'finished' => 'importNodesBatchFinished',
    );

    batch_set($batch);
    return batch_process('/admin/import-nodes');

  }

  public static function importConvertInstructionsBatchOperation($chunk, &$context){

    $message = 'Converting Node...';
    $results = [];

    $nodes = node_load_multiple($chunk);

    foreach ($nodes as $node) {

      $changed = false;

      if ( !empty($node) ) {

        $file = false;

        if ( !empty($node->field_instruction_file->getValue()) ) {
          $file = \Drupal\file\Entity\File::load($node->field_instruction_file->getValue()[0]['target_id']);
        }

        if ( !empty($file) && empty($node->field_instruction_html->getValue()[0]['value']) ) {

          $uri = $file->getFileUri();

          $path = \Drupal::service('file_system')->realpath($uri);

          if ( !empty($path) ) {

            $converted_path = self::importConvert($path);

            if ( !empty($converted_path) ) {

              konsultant_unoconv($path, $converted_path);
              sleep(1);

              $data = file_get_contents($converted_path);

              unlink($converted_path);

            }

            if ( !empty($data) ) {
              $fixed_data = self::fixImportedHtml($data);
              $node->field_instruction_html->setValue($fixed_data);
            }

          }

        }

      }

      $changed = true;
      if ( $changed ) {
        $node->field_converted->setValue(1);
        $changed_date = $node->changed->value;
        $node->save();
        konsultant_restore_changed_date($changed_date, $node->Id());
        $results[] = $node->Id();
      }

      sleep(1);

    }

    if ( empty($context['results']) ) {
      $context['results'] = 0;
    }

    $context['message'] = $message;
    $context['results'] += count($results);

  }

  public function systemFieldConvertionBatch(){

    $chunks = [];
    $chunk_size = 50;

    $operations = [];

    $query = \Drupal::entityQuery('node')
    ->condition('type', ['new', 'example', 'message', 'consultation'], 'IN');

    $nids = $query->execute();
    //$nids = array_slice($nids, 1416);//1416 682
    $chunks = array_chunk($nids, $chunk_size);

    foreach ($chunks as $chunk) {
      $operations[] = ['\Drupal\konsultant_import\ImportpageController::systemFieldConvertionBatchOperation', [$chunk]];
    }

    $batch = array(
      'title' => 'Converting nodes',
      'operations' => $operations,
      'finished' => 'importNodesBatchFinished',
    );

    batch_set($batch);
    return batch_process('/admin/import-nodes');

  }

  public static function systemFieldConvertionBatchOperation($chunk, &$context){

    $message = 'Converting Node...';
    $results = [];

    $nodes = node_load_multiple($chunk);

    $converted = false;

    foreach ($nodes as $node) {

      if( empty($node->field_systems->getValue()) || empty($node->field_systems->getValue()[0]['target_id']) ) {

        if( !empty($node->field_system->getValue()) && !empty($node->field_system->getValue()[0]['target_id']) ) {
          $system_id = $node->field_system->getValue()[0]['target_id'];
          $node->field_systems->setValue($system_id);
          $converted = true;
        };

      };

      if ( $converted ) {
        $changed_date = $node->changed->value;
        $node->save();
        konsultant_restore_changed_date($changed_date, $node->Id());
        $results[] = $node->Id();
      }

    }

    sleep(5);

    if ( empty($context['results']) ) {
      $context['results'] = 0;
    }

    $context['message'] = $message;
    $context['results'] += count($results);

  }

}
