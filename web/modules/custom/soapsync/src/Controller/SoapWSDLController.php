<?php

/**
 * @file
 * Contains \Drupal\soapsync\Controller\SoapWSDLController.
 */

namespace Drupal\soapsync\Controller;


use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

class SoapWSDLController extends ControllerBase {

  public function content() {
      $system = get_current_system();
      $domain = $system->domain;
      $proto = \Drupal::request()->isSecure() ? 'https://' : 'http://';
      $domain = $proto . \Drupal::request()->getHost();
      $xml = '<?xml version="1.0" encoding="utf-8"?>
<wsdl:definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:tns="' . $domain . '" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" targetNamespace="' . $domain . '" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:documentation xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"></wsdl:documentation>
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="' . $domain . '">
		<s:element name="SetUserRequest" type="s:string"/>
		<s:element name="SetUserResponse" type="s:boolean"/>
		<s:element name="SetProductRequest" type="s:string"/>
		<s:element name="SetProductResponse" type="s:boolean"/>
		<s:element name="GetUserAccessRequest" type="s:string"/>
		<s:element name="GetUserAccessResponse" type="s:dateTime"/>
    </s:schema>
  </wsdl:types>
   <wsdl:message name="SetUserRequestMessage">
		<wsdl:part name="parameters" element="tns:SetUserRequest" />
  	</wsdl:message>
	<wsdl:message name="SetUserResponseMessage">
		<wsdl:part name="parameters" element="tns:SetUserResponse" />
	</wsdl:message>
   <wsdl:message name="SetProductRequestMessage">
		<wsdl:part name="parameters" element="tns:SetProductRequest" />
  	</wsdl:message>
	<wsdl:message name="SetProductResponseMessage">
		<wsdl:part name="parameters" element="tns:SetProductResponse" />
	</wsdl:message>
   <wsdl:message name="GetUserAccessRequestMessage">
		<wsdl:part name="UserID" element="tns:GetUserAccessRequest" />
  	</wsdl:message>
	<wsdl:message name="GetUserAccessResponseMessage">
		<wsdl:part name="parameters" element="tns:GetUserAccessResponse" />
	</wsdl:message>
  <wsdl:portType name="MediaProSoap">
  		<wsdl:operation name="SetUser">
			<wsdl:input message="tns:SetUserRequestMessage" />
			<wsdl:output message="tns:SetUserResponseMessage" />
		</wsdl:operation>
  		<wsdl:operation name="SetProduct">
			<wsdl:input message="tns:SetProductRequestMessage" />
			<wsdl:output message="tns:SetProductResponseMessage" />
		</wsdl:operation>
		<wsdl:operation name="GetUserAccess">
			<wsdl:input message="tns:GetUserAccessRequestMessage" />
			<wsdl:output message="tns:GetUserAccessResponseMessage" />
		</wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="MediaProSoap" type="tns:MediaProSoap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http" />
 	 <wsdl:operation name="SetUser">
	   <soap12:operation soapAction="' . $domain . '/soapserver/SetUser" style="document"/>
	   <wsdl:input>
		  <soap12:body use="literal" />
	   </wsdl:input>
	   <wsdl:output>
		  <soap12:body use="literal" />
	   </wsdl:output>
	 </wsdl:operation>
 	 <wsdl:operation name="SetProduct">
	   <soap12:operation soapAction="' .$domain . '/soapserver/SetProduct" style="document"/>
	   <wsdl:input>
		  <soap12:body use="literal" />
	   </wsdl:input>
	   <wsdl:output>
		  <soap12:body use="literal" />
	   </wsdl:output>
	 </wsdl:operation>
	 <wsdl:operation name="GetUserAccess">
	   <soap12:operation soapAction="' .$domain . '/soapserver/GetUserAccess" style="document"/>
	   <wsdl:input>
		  <soap12:body use="literal" />
	   </wsdl:input>
	   <wsdl:output>
		  <soap12:body use="literal" />
	   </wsdl:output>
	 </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="MediaPro">
    <wsdl:port name="MediaProSoap" binding="tns:MediaProSoap">
      <soap12:address location="' .$domain . '/soapserver" />
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>';

	return new Response($xml, Response::HTTP_OK, ['Content-Type' => 'application/xml', 'charset' => 'utf-8']);
  }

}
