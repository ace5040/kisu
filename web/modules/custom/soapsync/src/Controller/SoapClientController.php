<?php

/**
 * @file
 * Contains \Drupal\soapsync\Controller\SoapClientController.
 */

namespace Drupal\soapsync\Controller;


use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use SoapClient;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Component\Transliteration\PhpTransliteration;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Site\Settings;
use Drupal\user\Entity\User;

/**
 * Soap client controller. Used to import data from 1C database.
 */
class SoapClientController extends ControllerBase {

  private $soapClient = NULL;

  public function content() {

    try{
	  $soapClient = new SoapClient('http://e.mediapro.com.ua/soap.wsdl', array('login' => 'online', 'password' => 'QmKE3NrN', 'cache_wsdl' => WSDL_CACHE_NONE));

/*
$data = [
	'data' => [
        'refUsers' => [
			[
				'id' => '21c54233-ddbd-11e6-8753-001e6745e90d',//field_customer_id1c
				'marked' => 0,
				'document_id' => '2a9c56d1-dd6a-11e6-8753-001e6745e90d',
				'document_type' => 'Order',
				'client_id' => 'e2d3340e-aac1-11e4-b304-001e6745e90d',//field_customer_client_id
				'client_name' => 'МИРГОРОДТЕПЛОЕНЕРГО!!',
				'client_edrpo' => '25682207',//field_customer_edrpo
				'contact_id' => '5561bddc-e4de-11e4-a599-001e6745e90d',//field_customer_contact_id
				'contact_name' => 'Оксана Викторовна',
				'product_id' => 'a1bfdef8-3cc9-451f-b7f3-2059ebdefde8',
				'date_begin' => '2017-01-01 00:00:00',
				'date_end' => '2017-12-31 00:00:00',
				'login' => 'pl000001',//name
				'password' => 'R9j6Zs5',//field_customer_password
				'email' => 'michailichenkod@mail.ru',//email  i-zarinka@mail.ru
				'access_type' => 'soft',
				'product_name' => 'ОППБ Пільга',
				'product_name_full' => "Журнал 'Охорона праці і пожежна безпека'",
            ]
		]
	]
];


$data = json_encode($data);
$data = json_decode($data);
$customer = $data->data->refUsers[0];
*/

/*

$query = \Drupal::entityQuery('user');
$group_and = $query->andConditionGroup()
  ->condition('field_customer_client_id', $customer->client_id)
  ->condition('field_customer_contact_id', $customer->contact_id);
$group_or = $query->orConditionGroup()
  ->condition('field_customer_id1c', $customer->id)
  ->condition($group_and);
$uid = $query->condition($group_or)->execute();

if (!empty($uid)) {
  //Обновление информации существующего пользователя по id1c или client_id + contact_id
  $user_id = current($uid);
  $account = User::load($user_id);
  $account_password = $account->field_customer_password->value;

  $account->setPassword($customer->password);
  $account->setUsername($customer->login);
  $account->setEmail($customer->email);
  $account->set('field_customer_id1c', $customer->id);
  $account->set('field_customer_client_id', $customer->client_id);
  $account->set('field_customer_contact_id', $customer->contact_id);
  $account->set('field_customer_edrpo', $customer->client_edrpo);
  $account->set('field_customer_name', $customer->contact_name);
  $account->set('field_customer_password', $customer->password);
  $account->save();
  \Drupal::logger('soapsync-SetUser-update')->debug(dbg($customer));

  if ($account_password != $customer->password) {
	$new_password = $customer->password;
	$user_email = $account->getEmail();
	$subject = 'Онлайн платформа електронних видань ТОВ «МЕДІА-ПРО»';
	//------------------------------------------------------------------------------
	  $message = '<p align="center"><span style="font-size:26px;"><span style="color: rgb(54, 95, 145);"><strong>Шановний передплатнику!</strong></span></span></p>';
	  $message .= '<p align="center"><span style="font-size:26px;"><span style="color: rgb(54, 95, 145);"><strong>Вам надано доступ до Онлайн платформи електронних видань ТОВ &laquo;МЕДІА-ПРО&raquo;</strong></span></span></p>';
      $message .= '<p>&nbsp;</p>';
	  $message .= '<p align="center">Для активації перейдіть за посиланням</p>';
	  $message .= '<p align="center"><span style="font-size:16px;"><a href="http://e.mediapro.com.ua"><span style="color:#365f91;"><strong>e</strong><strong>.</strong><strong>mediapro</strong><strong>.</strong><strong>com</strong><strong>.</strong><strong>ua</strong></span></a></span></p>';
	  $message .= '<p align="center">&nbsp;</p>'. "\n\n";
	  $message .= '<p align="center"><img alt="" src="http://e.mediapro.com.ua/themes/emedia/images/login-block.jpg" style="width: 329px; height: 298px;" /></p>'. "\n\n";
	  $message .= '<p align="center"><strong>Введіть свої персональні<br />Логін</strong><br />
        <span style=""><a href="mailto:' . $user_email . '" style="text-decoration:none;font-size:26px;color:#365f91;"><strong>' . $user_email . '</strong></a></p>'. "\n\n";
	  $message .= '<p align="center"><strong>Пароль</strong><br />
        <span style="color:#365f91;"><span style="font-size: 26px;"><strong>' . $customer->password . '</strong></span></span></p>'. "\n\n";
      $message .= '<p>&nbsp;</p>'. "\n\n";
      $message .= '<p align="center"><span style="color:#365f91;"><span style="font-size: 14px;"><strong>Будь-ласка, збережіть присвоєний вам логін та пароль в окремому файлі.</strong></span></span></p>'. "\n\n";
      $message .= '<p>Введіть в поле &laquo;логін&raquo; надісланий вам логін, тобто ваш e-mail. Зверніть увагу на регістр (написання з великої літери), мову введення та наявність/відсутність пробілів. Або просто скопіюйте без пробілів надісланий логін (Ctrl+C) та вставте його в поле &laquo;логін&raquo; (Ctrl+V).</p>'. "\n\n";
      $message .= '<p>За таким же принципом заповніть поле &laquo;пароль&raquo; (зверніть увагу на регістр (написання з великої літери), мову введення та наявність/відсутність пробілів).</p>' . "\n\n";
      $message .= '<p>Натисніть &laquo;УВІЙТИ&raquo;.</p>'. "\n\n";
      $message .= '<p align="center">Відділ клієнтської підтримки: (44) 507 22 26 (внутр. 720)</p>'. "\n\n";
      $message .= '<p style="text-align: center;">Бажаємо приємної роботи!</p>'. "\n\n";
	//------------------------------------------------------------------------------
	$to = $user_email;
	$headers = 'From: no-reply@e.mediapro.com.ua' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    mail($to, $subject, $message, $headers);
    $to = 'michailichenkod@gmail.com';
	$subject .= ' ' . '(' . $user_email . ')';
    mail($to, $subject, $message, $headers);
  }

}
else {
  print dbg($customer);
  $user_email = $customer->email;
  if (!empty($user_email)) {
    $account = user_load_by_mail($user_email);
    if (empty($account)) {
	  $user_create = User::create();
	  $user_create->setPassword($customer->password);
	  $user_create->enforceIsNew();
	  $user_create->setEmail($customer->email);
	  $user_create->setUsername($customer->login);
	  $user_create->addRole('customer');
	  $user_create->set('field_customer_id1c', $customer->id);
	  $user_create->set('field_customer_client_id', $customer->client_id);
	  $user_create->set('field_customer_contact_id', $customer->contact_id);
	  $user_create->set('field_customer_edrpo', $customer->client_edrpo);
	  $user_create->set('field_customer_name', $customer->contact_name);
	  $user_create->set('field_customer_password', $customer->password);
	  $user_create->activate();
	  $user_create->save();

	  $user_id = $user_create->id();

	  $user_email = $user_create->getEmail();
	  $subject = 'Доступ до Онлайн платформи електронних видань ТОВ «МЕДІА-ПРО»';
	  //------------------------------------------------------------------------------
	  $message = '<p align="center"><span style="font-size:26px;"><span style="color: rgb(54, 95, 145);"><strong>Шановний передплатнику!</strong></span></span></p>';
	  $message .= '<p align="center"><span style="font-size:26px;"><span style="color: rgb(54, 95, 145);"><strong>Вам надано доступ до Онлайн платформи електронних видань ТОВ &laquo;МЕДІА-ПРО&raquo;</strong></span></span></p>';
      $message .= '<p>&nbsp;</p>';
	  $message .= '<p align="center">Для активації перейдіть за посиланням</p>';
	  $message .= '<p align="center"><span style="font-size:16px;"><a href="http://e.mediapro.com.ua"><span style="color:#365f91;"><strong>e</strong><strong>.</strong><strong>mediapro</strong><strong>.</strong><strong>com</strong><strong>.</strong><strong>ua</strong></span></a></span></p>';
	  $message .= '<p align="center">&nbsp;</p>'. "\n\n";
	  $message .= '<p align="center"><img alt="" src="http://e.mediapro.com.ua/themes/emedia/images/login-block.jpg" style="width: 329px; height: 298px;" /></p>'. "\n\n";
	  $message .= '<p align="center"><strong>Введіть свої персональні<br />Логін</strong><br />
        <span style=""><a href="mailto:' . $user_email . '" style="text-decoration:none;font-size:26px;color:#365f91;"><strong>' . $user_email . '</strong></a></p>'. "\n\n";
	  $message .= '<p align="center"><strong>Пароль</strong><br />
        <span style="color:#365f91;"><span style="font-size: 26px;"><strong>' . $customer->password . '</strong></span></span></p>'. "\n\n";
      $message .= '<p>&nbsp;</p>'. "\n\n";
      $message .= '<p align="center"><span style="color:#365f91;"><span style="font-size: 14px;"><strong>Будь-ласка, збережіть присвоєний вам логін та пароль в окремому файлі.</strong></span></span></p>'. "\n\n";
      $message .= '<p>Введіть в поле &laquo;логін&raquo; надісланий вам логін, тобто ваш e-mail. Зверніть увагу на регістр (написання з великої літери), мову введення та наявність/відсутність пробілів. Або просто скопіюйте без пробілів надісланий логін (Ctrl+C) та вставте його в поле &laquo;логін&raquo; (Ctrl+V).</p>'. "\n\n";
      $message .= '<p>За таким же принципом заповніть поле &laquo;пароль&raquo; (зверніть увагу на регістр (написання з великої літери), мову введення та наявність/відсутність пробілів).</p>' . "\n\n";
      $message .= '<p>Натисніть &laquo;УВІЙТИ&raquo;.</p>'. "\n\n";
      $message .= '<p align="center">Відділ клієнтської підтримки: (44) 507 22 26 (внутр. 720)</p>'. "\n\n";
      $message .= '<p style="text-align: center;">Бажаємо приємної роботи!</p>'. "\n\n";
	//------------------------------------------------------------------------------


	  $to = $user_email;
	  $headers = 'From: no-reply@e.mediapro.com.ua' . "\r\n";
	  $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	  mail($to, $subject, $message, $headers);
	  // $to = 'michailichenkod@gmail.com';
	  // $subject .= ' ' . '(' . $user_email . ')';
	  // mail($to, $subject, $message, $headers);

	}

  }
}


if (!empty($user_id)) {
  $query = \Drupal::database()->upsert('users_soapsync');
  $query->fields([
    'uid' => $user_id,
	'product_id' => $customer->product_id,
	'date_begin' => $customer->date_begin,
	'date_end' => $customer->date_end,
	'login' => $customer->login,
	'password' => $customer->password,
	'email' => $customer->email,
	'id1c' => $customer->id,
	'client_id' => $customer->client_id,
	'client_name' => $customer->client_name,
	'client_edrpo' => $customer->client_edrpo,
	'contact_id' => $customer->contact_id,
	'contact_name' => $customer->contact_name,
	'document_id' => $customer->document_id,
  ]);
  $query->key('id1c');
  $query->execute();
}








*/


$entityTaxonomy = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term');

$product_id = 'f1b60464-6a66-43ff-8d0b-3f1ad7fb7e61';
$term_ids = self::getTermEdition($product_id);
foreach ($term_ids as $term_id){
  $termChildrens = $entityTaxonomy->loadChildren($term_id);
  if (!empty($termChildrens)) {
	$term_child_tids = array_keys($termChildrens);
	foreach ($term_child_tids as $term_child_tid) {
	  $term_ids[] = $term_child_tid;
	}
  }
}

print dbg($term_ids);





/*
$data = [];
$data = json_encode($data);
$data = json_decode($data);
$refProducts = $data->data->refProducts[0];
print dbg($refProducts);
	if (!empty($refProducts)) {
	  	$product_id = $refProducts->id;
		$periodicity = $refProducts->periodicity;
		$term_id = self::getTermEdition($product_id);
		if (!empty($term_id)) {
		  print 'term_id: ' . $term_id . "\n";
		  $term = Term::load($term_id);
		  $termName = $term->getName();
		  $termPathDir = $term->field_path_dir->value;

		  preg_match('/(.*)\s(\«.*\»)(.*?)\,?(.*?)/', $termName, $matchesTermName);
		  //print dbg($matchesTermName);


		  // $term_product_ids = $term->get('field_product_id')->getValue();
		  // $productIDs = [];
		  // if (!empty($term_product_ids)) {
			// foreach ($term_product_ids as $term_product_id) {
			  // $productIDs[] = $term_product_id['value'];
			// }
			// //print dbg($productIDs);
		  // }


		  $refRelease = $data->data->refRelease;
		  $date_import_start = strtotime('2017-01-01 00:00:00');
		  $date_import_end = strtotime('2017-12-28 00:00:00');
		  $countRelise = count($refRelease);
		  foreach ($refRelease as $i => $release) {
		    $release_date = strtotime($release->date);
			if ($release_date >= $date_import_start && $release_date <= $date_import_end) {
			  $nodeNum = $i+1;
			  $dir_flipper = $nodeNum;
			  if ($nodeNum < 10) {
				$dir_flipper = '0' . $nodeNum;
			  }
			  $nodeYear = date('Y', $release_date);
			  $nodeEditionDate = date('Y-m-d', $release_date);
              $nodeTitle = $termName . ' № ' . $nodeNum . ', ' . $nodeYear;
			  print dbg($release);

			  $nid = \Drupal::entityQuery('node')
			    ->condition('type', 'edition')
                ->condition('field_edition', $term_id)
			    ->condition('field_edition_date', $nodeEditionDate)
                ->execute();

              if (empty($nid)) {
			    $newNode = [
				  'type' => 'edition',
				  'title' => $nodeTitle,
				  'langcode' => 'uk',
				  'status' => 0,
				  'field_edition' => [
				    ['target_id' => $term_id,]
				  ],
				  'field_refrelease_id' => [
				    ['value' => $release->id,]
				  ],
				  'field_edition_date' => [
				    ['value' => $nodeEditionDate,]
				  ],
				  'field_edition_flipper' => [
				    ['value' => $dir_flipper,]
				  ],
			    ];
			    //$node_create = Node::create($newNode);
	            //$node_create->save();
			  }
			  else
			  {
				$nidRelease = \Drupal::entityQuery('node')
			    ->condition('type', 'edition')
                ->condition('field_edition', $term_id)
			    ->condition('field_refrelease_id', $release->id)
                ->execute();
				if (empty($nidRelease)) {
				  $nid = current($nid);
				  $node = Node::load($nid);
				  if (!empty($node)) {
					$refreleaseIDs = $node->get('field_refrelease_id')->getValue();
					$refreleaseIDs[] = ['value' => $release->id];
					//print dbg($refreleaseIDs);
					$node->set('field_refrelease_id', $refreleaseIDs);
					//$node->save();
				  }
				}
			  }

			}
		  }
		}
	}

*/


	  //$response = $soapClient->SetUser($data);

	  //$response = $soapClient->SetProduct($data);

	  //print dbg($response);
    }
	catch (SoapFault $exception) {
	  print dbg($exception);
	}

    $output = 'soap client';
	return new Response($output, Response::HTTP_OK, ['Content-Type' => 'text/html']);
  }



   private function getTermEdition($product_id) {
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', 'edition');
    $query->condition('field_product_id', $product_id);
    $tids = $query->execute();

	$tids = array_keys($tids);

	return $tids;
  }

}