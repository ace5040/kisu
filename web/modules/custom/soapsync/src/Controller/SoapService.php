<?php
/**
 * @file
 * Contains \Drupal\soapsync\Controller\SoapService.
 */

namespace Drupal\soapsync\Controller;
use Drupal\user\Entity\User;

class SoapService {

  private static function findUser($customer) {
    $system = get_current_system();
    $query = \Drupal::entityQuery('user');
    $query->condition('field_access_id', $customer->id);
    $uid = $query->execute();
    \Drupal::logger('soapsync-SetUser-edit')->debug(dbg($uid));
    if ( empty($uid) ) {
      $query = \Drupal::entityQuery('user');
      $query->condition('field_company_id', $customer->client_id);
      $query->condition('field_user_id', $customer->contact_id);
      $query->condition('field_system', $system->id);
      $uid = $query->execute();
      \Drupal::logger('soapsync-SetUser-update')->debug(dbg($uid));
    }
    return !empty($uid) ? current($uid) : false;
  }

  public function SetUser($data){
    $system = get_current_system();
    file_put_contents('../sync/users-' . $system->id . '.txt', $data, FILE_APPEND);
  	$data = json_decode ( trim( preg_replace( '/\t+/', '', preg_replace('/\s+/', ' ', $data) ) ) );

    $customer = $data->data->refUsers[0];

    if ( empty($customer) ) {
      \Drupal::logger('soapsync-SetUser')->debug('empty customer');
      return false;
    }

    \Drupal::logger('soapsync-SetUser')->debug(dbg($customer));

    $found_user_id = self::findUser($customer);

    \Drupal::logger('soapsync-SetUser-debug-id')->debug(dbg($found_user_id));

    $user_email = $customer->email;

    if ( empty($found_user_id) ) {


      if ( !empty($user_email) ) {

        $user_create = User::create();
        $user_create->enforceIsNew();
        $user_create->setPassword($customer->password);
        $user_create->setEmail( $customer->login . '.' . $customer->email . '-' . $system->id );
        $user_create->setUsername($customer->login);
        $user_create->set('field_system', $system->id);
        $user_create->set('field_access_type', $customer->access_type);
        $user_create->set('field_user_email', $customer->email);
        $user_create->set('field_fio', $customer->contact_name);
        $user_create->set('field_company', $customer->client_name);
        $user_create->set('field_edrpou', $customer->client_edrpo);
        $user_create->set('field_password', $customer->password);
        $user_create->set('field_access_id', $customer->id);
        $user_create->set('field_user_id', $customer->contact_id);
        $user_create->set('field_company_id', $customer->client_id);
        $user_create->set('field_access_begin', substr($customer->date_begin, 0, 10));

        $user_create->activate();

        if ( $customer->access_type == 'full' ) {
          $user_create->addRole('customer');
          $user_create->set('field_access_end', substr($customer->date_end, 0, 10));
        } else {
          $user_create->addRole('demo');
          $week = date_create_from_format('Y-m-d', substr($customer->date_begin, 0, 10));
          date_add($week, date_interval_create_from_date_string("7 days"));
          $week = date('Y-m-d', date_timestamp_get($week));
          $user_create->set('field_access_end', $week);
        }

        $user_create->save();

        \Drupal::logger('soapsync-SetUser-create')->debug(dbg($customer));

        $user_id = $user_create->id();

        //------------------------------------------------------------------------------
        $vars = [];
        $vars['password'] = $customer->password;
        $vars['login'] = $customer->login;
        $vars['fio'] = $customer->contact_name;

        if ( $customer->access_type == 'full' ) {
          $tmpl = get_system_template_email('new-user-access-email', $vars);
          $subject = 'Доступ до платформи ' . $system->name;
        } else {
          $tmpl = get_system_template_email('new-demo-access-email', $vars);
          $subject = 'Демо-доступ до платформи ' . $system->name;
        }

        if ( !empty( $tmpl->subject ) ) {
          $subject = $tmpl->subject;
        }

        konsultant_send_message($user_id, $subject, $tmpl->html, $tmpl);

        return 1;

      }

	  } else {

      if ( !empty($user_email) ) {

        $found_user = user_load($found_user_id);

        if  ( !empty($found_user) ) {

          $found_user->setPassword($customer->password);
          $found_user->setEmail( $customer->login . '.' . $customer->email . '-' . $system->id );
          $found_user->setUsername($customer->login);
          $found_user->set('field_user_email', $customer->email);
          $found_user->set('field_fio', $customer->contact_name);
          $found_user->set('field_company', $customer->client_name);
          $found_user->set('field_edrpou', $customer->client_edrpo);
          $found_user->set('field_password', $customer->password);
          $found_user->set('field_access_id', $customer->id);
          $found_user->set('field_user_id', $customer->contact_id);
          $found_user->set('field_company_id', $customer->client_id);
          $found_user->set('field_access_begin', substr($customer->date_begin, 0, 10));
          $found_user->set('field_access_end', substr($customer->date_end, 0, 10));

          if ( $customer->access_type == 'full' && $found_user->hasRole('demo') ) {
            $found_user->removeRole('demo');
            $found_user->addRole('customer');
          }

          if ( $customer->access_type == 'demo' && $found_user->hasRole('customer') ) {
            $found_user->addRole('demo');
            $found_user->removeRole('customer');
            $week = date_create_from_format('Y-m-d', substr($customer->date_begin, 0, 10));
          	date_add($week, date_interval_create_from_date_string("7 days"));
            $week = date('Y-m-d', date_timestamp_get($week));
            $found_user->set('field_access_end', $week);
          }

          $found_user->save();

          \Drupal::logger('soapsync-SetUser-update')->debug(dbg($customer));

          $user_id = $found_user->id();

          //------------------------------------------------------------------------------
          $vars = [];
          $vars['password'] = $customer->password;
          $vars['login'] = $customer->login;
          $vars['fio'] = $customer->contact_name;

          if ( $customer->access_type == 'full' ) {
            $tmpl = get_system_template_email('new-user-access-email', $vars);
            $subject = 'Доступ до платформи ' . $system->name;
          } else {
            $tmpl = get_system_template_email('new-demo-access-email', $vars);
            $subject = 'Демо-доступ до платформи ' . $system->name;
          }

          if ( !empty( $tmpl->subject ) ) {
            $subject = $tmpl->subject;
          }

          konsultant_send_message($user_id, $subject, $tmpl->html, $tmpl);

          return 1;

        }

      }

    }

	  return 0;

  }


  public function SetProduct($data){
    \Drupal::logger('soapsync-SetProduct')->debug(dbg($data));
    return 1;
  }


  public function GetUserAccess($data){
    \Drupal::logger('soapsync-GetUserAccess')->debug(dbg($data));

    $system = get_current_system();
    $query = \Drupal::entityQuery('user');
    $query->condition('field_system', $system->id);
    $query->condition('field_access_id', $data);
    $uid = $query->execute();
    $uid = reset($uid);

    if ( !empty($uid) ) {
      $user = user_load($uid);
      $last_access = $user->getLastLoginTime();
      if ( !empty($last_access) ) {
        $date_access = date('Y-m-d\TH:i:s', $last_access);
        \Drupal::logger('soapsync-GetUserAccess response')->debug(dbg($date_access));
        return $date_access;
      }
    }

    return false;

  }

}
