<?php
/**
 * @file
 * Contains \Drupal\soapsync\Controller\SoapServerController.
 */

namespace Drupal\soapsync\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use SoapServer;

class SoapServerController extends ControllerBase {

  private $soapServer = NULL;

  public function content() {

    ini_set("soap.wsdl_cache_enabled","0");

    $system = get_current_system();

    try {
      $domain = $system->domain;
      //$proto = \Drupal::request()->isSecure() ? 'https://' : 'http://';
      //$domain = $proto . \Drupal::request()->getHost();
      $this->soapServer = new SoapServer($domain . '/soap.wsdl');
      $this->soapServer->setClass('\Drupal\soapsync\Controller\SoapService');
      $this->soapServer->handle();
      //$func = $this->soapServer->getFunctions();
      //print dbg($func);
    } catch (SoapFault $exception) {
      \Drupal::logger('soapsync')->error('getData error:'.dbg($exception));
      return new Response('getdata error' , Response::HTTP_INTERNAL_SERVER_ERROR, ['Content-Type' => 'text/html']);
    }

    return new Response('', Response::HTTP_OK, ['Content-Type' => 'text/html']);

  }

}
