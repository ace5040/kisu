jQuery(function($){

  $('#edit-roles').on('change', function(){

    var value = $(this).val();

    if ( value == 'editor_norm' ) {

      $('.field--name-field-system').hide();

    } else {
      $('.field--name-field-system').show();
    };

  });

  $('#edit-field-document-status').on('change', function(){

    var value = $(this).val();

    if ( value == 'invalid' ) {
      $('.field--name-field-date-invalid').show();
      $('.field--name-field-date-partially-invalid').hide();
    } else if ( value == 'partially_invalid' ) {
      $('.field--name-field-date-invalid').hide();
      $('.field--name-field-date-partially-invalid').show();
    } else {
      $('.field--name-field-date-invalid').hide();
      $('.field--name-field-date-partially-invalid').hide();
    };

  });

  $('#edit-field-document-status').trigger('change');
  $('#edit-roles').trigger('change');

  $('#edit-field-document-status').on('change', function(){

    var value = $(this).val();

    if ( value == 'invalid' ) {
      $('.field--name-field-date-invalid').show();
    } else {
      $('.field--name-field-date-invalid').hide();
    };

  });

  $('\
    .node-normdoc-form .field--name-field-systems input,\
    .node-normdoc-edit-form .field--name-field-systems input,\
    .node-example-form .field--name-field-systems input,\
    .node-example-edit-form .field--name-field-systems input,\
    .node-accountinglog-form .field--name-field-systems input,\
    .node-accountinglog-edit-form .field--name-field-systems input,\
    .node-instruction-form .field--name-field-systems input,\
    .node-instruction-edit-form .field--name-field-systems input\
  ').on('change', function(){
    var system_id = $(this).val();
    var state = $(this).prop('checked');

    if ( system_id ) {

      if ( state ) {
        $('.filter-' + system_id).show();
        $('.example-type-' + system_id).show();
      } else {
        $('.filter-' + system_id).hide();
        $('.example-type-' + system_id).hide();
      }

    }

  });

  $('\
    .node-normdoc-form .field--name-field-systems input,\
    .node-normdoc-edit-form .field--name-field-systems input,\
    .node-example-form .field--name-field-systems input,\
    .node-example-edit-form .field--name-field-systems input,\
    .node-accountinglog-form .field--name-field-systems input,\
    .node-accountinglog-edit-form .field--name-field-systems input,\
    .node-instruction-form .field--name-field-systems input,\
    .node-instruction-edit-form .field--name-field-systems input\
  ').each(function(){
    var system_id = $(this).val();
    $('.filter-' + system_id).css('visibility', 'visible');
    $('.example-type-' + system_id).css('visibility', 'visible');
    $(this).trigger('change');
  })

  $('#edit-field-template').on('change', function(){
    var html = CKEDITOR.instances['edit-field-message-html-0-value'].getData();
    var value = $(this).val();

    if ( html.length == 0 && value != '_none') {

      $.ajax({
        url: '/system/request',
        data: {
          command: 'getTemplate',
          params: JSON.stringify({template: value})
        },
        method: 'POST',
        success: function ( response ) {
          if ( response && response.error === 0 ) {
            CKEDITOR.instances['edit-field-message-html-0-value'].setData(response.data.html);
          } else {
            console.info(response.error, response.errorText);
          }
        }
      });

    }

  });


  if ($('.node-event-form').length || $('.node-event-edit-form').length) {
    var type_default = $("#edit-field-event-type").chosen().val();

    $('.field--name-field-event-organ-audit').hide();
    $('.field--name-field-event-subject-audit').hide();
    $('.field--name-field-event-name-company').hide();
    $('.field--name-field-event-users').hide();
    $('.field--name-field-event-edrpou').hide();

    if (type_default == 'audit') {
      $('.field--name-field-event-organ-audit').show();
      $('.field--name-field-event-subject-audit').show();
      $('.field--name-field-event-name-company').show();
      $('.field--name-field-event-users').show();
      $('.field--name-field-event-edrpou').show();
    }

    $('#edit-field-event-type').chosen().on('change', function() {
      var type = $(this).val();

      if (type == 'audit') {
        $('.field--name-field-event-organ-audit').show();
        $('.field--name-field-event-subject-audit').show();
        $('.field--name-field-event-name-company').show();
        $('.field--name-field-event-users').show();
        $('.field--name-field-event-edrpou').show();
      } else {
        $('.field--name-field-event-organ-audit').hide();
        $('.field--name-field-event-subject-audit').hide();
        $('.field--name-field-event-name-company').hide();
        $('.field--name-field-event-users').hide();
        $('.field--name-field-event-edrpou').hide();
      }

    });

  }

  $('#edit-field-event-type').on('change', function(){

    var value = $(this).val();

    $('.field--name-field-event-days').hide();
    $('.field--name-field-event-date-end').hide();
    $('.field--name-field-short-day').hide();
    $('.field--name-field-additional-working-day').hide();
    $('.field--name-field-additional-holiday').hide();

    switch ( value ) {
      case '_none':
      break;
      case 'own_event':
        $('.field--name-field-event-days').show();
        $('.field--name-field-event-date-end').show();
      break;
      case 'audit':
        $('.field--name-field-event-days').show();
        $('.field--name-field-event-date-end').show();
      break;
      case 'report':
        $('.field--name-field-event-days').show();
        $('.field--name-field-event-date-end').show();
      break;
      case 'event':
        $('.field--name-field-event-days').show();
        $('.field--name-field-event-date-end').show();
      break;
      case 'professional_day':
      break;
      case 'national_holiday':
        $('.field--name-field-short-day').show();
        $('.field--name-field-additional-working-day').show();
        $('.field--name-field-additional-holiday').show();
      break;
    }

  });

  $('#edit-field-event-type').trigger('change');

})
