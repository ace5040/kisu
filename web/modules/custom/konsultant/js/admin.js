jQuery(function($){

  var lang = $('html').attr('lang');

  $('body').on('click', '.question-popup-link', function(){
    var $parent = $(this).parent();
    $('.question-popup-wrapper', $parent).addClass('active');
    var height = $('.question-popup', $parent).outerHeight();
    $('.question-popup', $parent).css('margin-top', '-' + Math.round(height/2) + 'px');
  });

  $('body').on('click', '.question-popup-close', function(e){
    $(this).parents('.question-popup-wrapper').removeClass('active');
    return false;
  });

  $('.js-form-item-created-max label').text('По');
  $('.js-form-item-field-answer-date-value-max label').text('По');

  $('body').on('click', '.button--export', function(e){
    var $link = $(e.currentTarget);
    var href = $link.attr('href');
    if ( href.split('format=xls').length < 2 ) {
      if ( document.location.search ) {
        var params = document.location.search;
        params = params.replace('order=', 'sort_by=');
        params = params.replace('&sort=', '_value&sort_order=');
        params = params.replace('sort_order=asc', 'sort_order=ASC');
        params = params.replace('sort_order=desc', 'sort_order=DESC');
        params = params.replace('=nid_value&', '=nid&');
        params = params.replace('=name_value&', '=name&');
        params = params.replace('=access_value&', '=access&');
        $link.attr('href', href + params + '&format=xls');
      } else {
        $link.attr('href', href + '?format=xls');
      }
    }
  });

  $('body').on('click', '.convert-link', function(){

    var $btn = $(this);
    var $input = false;
    var $editor = false;

    if ( $('.field--name-field-example-file input[type="hidden"]').length ) {
      $input = $('.field--name-field-example-file input[type="hidden"]');
      $textarea = $('.field--name-field-example-html textarea', $btn.parent());
      $editor = CKEDITOR.instances[$textarea.attr('id')];
    } else if ( $('.field--name-field-accountinglog-file input[type="hidden"]').length ) {
      $input = $('.field--name-field-accountinglog-file input[type="hidden"]');
      $textarea = $('.field--name-field-instruction-html textarea', $btn.parent());
      $editor = CKEDITOR.instances[$textarea.attr('id')];
    } else if ( $('.field--name-field-instruction-file input[type="hidden"]').length ) {
      $input = $('.field--name-field-instruction-file input[type="hidden"]');
      $textarea = $('.field--name-field-instruction-html textarea', $btn.parent());
      $editor = CKEDITOR.instances[$textarea.attr('id')];
    } else if ( $('.field--name-field-document-file input[type="hidden"]', $btn.parent()).length ) {
      $input = $('.field--name-field-document-file input[type="hidden"]', $btn.parent());
      $textarea = $('.field--name-field-document textarea', $btn.parent());
      $editor = CKEDITOR.instances[$textarea.attr('id')];
    }

    var fid = $input.val();

    if ( fid ) {
      $btn.append('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>');
      $.ajax({
        url: '/system/request',
        data: {
          command: 'getConvertedHtml',
          params: JSON.stringify({fid: fid})
        },
        method: 'POST',
        success: function ( response ) {
          $('.ajax-progress', $btn).remove();
          if ( response && response.error === 0 && response.data.status == 'ok') {
            $editor.setData(response.data.html);
          } else {
            if ( response && response.error === 0 && response.data.status ) {
              console.info(response.data.status, response.data.html);
            } else {
              console.info(response.error, response.errorText);
            }
          }
        }
      });
    } else {
      alert('Спочатку завантажте файл');
    }

  });
  var monthNames = {
    'uk': ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
    'ru': ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
  }
  var placeholders = {
    'uk': 'дд.мм.рррр',
    'ru': 'дд.мм.гггг',
  };

  var options = {
    dateFormat: 'dd.mm.yy',
    altFormat: 'yy-mm-dd',
    monthNames: monthNames[lang],
    yearRange: '1900:2100',
    changeYear: true,
    altField: ''
  };

  var placeholder = placeholders[lang];

  var inputs = [];

  $('.view #edit-created-from').each(function(index, input){ inputs.push(input); });
  $('.view #edit-created-to').each(function(index, input){ inputs.push(input); });
  $('.view #edit-answer-from').each(function(index, input){ inputs.push(input); });
  $('.view #edit-answer-to').each(function(index, input){ inputs.push(input); });
  $('.view #edit-placed-from').each(function(index, input){ inputs.push(input); });
  $('.view #edit-placed-to').each(function(index, input){ inputs.push(input); });
  $('.view #edit-event-start').each(function(index, input){ inputs.push(input); });
  $('.view #edit-event-end').each(function(index, input){ inputs.push(input); });

  $('.layout-node-form input[type=date]').each(function(index, input){
    inputs.push(input);
  });

  _.each(inputs, function(input, index) {
    var $input = $(input);
    if ( $input.length ) {
      var id = $input.attr('id');
      var value = $input.val();
      var visible_input_id = id + '-date-input';
      $input.attr('type', 'hidden');
      var visible_date = value.length == 10 ? value.split('-').reverse().join('.') : '';
      $input.after('<input value="' + visible_date + '" id="' + visible_input_id + '" size="12" class="form-date" placeholder="' + placeholder + '" type="text"/>');
      var $visible_input = $('#' + visible_input_id);
      options.altField = '#' + id;
      $visible_input.datepicker(options).mask('00.00.0000', { placeholder: placeholder, selectOnFocus: true, clearIfNotMatch: true });
      $visible_input.change(function(){
        if ( $visible_input.val().length == 0 ) {
          $input.val('');
        }
      });
    }
  })



  $('.toolbar-menu .consultations a').append('<i style="display: none;position: absolute; margin-top: -14px;vertical-align: middle; font-size: 10px; font-style: normal;color: #fff; border-radius:8px; min-width: 16px; height:16px; margin-left: 73px;background-color: #ff5a60; text-align: center; line-height:16px;cursor:default;" class="icon new-consultations-number"></i>');
  $('.toolbar-menu .doc-requests a').append('<i style="display: none;position: absolute; margin-top: -14px;vertical-align: middle; font-size: 10px; font-style: normal;color: #fff; border-radius:8px; min-width: 16px; height:16px; margin-left: 49px;background-color: #ff5a60; text-align: center; line-height:16px;cursor:default;" class="icon new-requests-number"></i>');

  function checkNewRequests(){
    if ( document.visibilityState == "visible" ) {
      $.ajax({
        url: '/system/request/checkNewAdminRequests',
        data: {
          params: JSON.stringify({})
        },
        method: 'POST',
        success: function ( response ) {
          if ( response && response.error === 0 ) {
            var count = response.data.count;
            if ( count ) {
              $('.toolbar-menu .doc-requests .new-requests-number').html(count);
              $('.toolbar-menu .doc-requests .new-requests-number').css('display', 'block');
            } else {
              $('.toolbar-menu .doc-requests .new-requests-number').css('display', 'none');
            }

          } else {
            console.info(response.error, response.errorText);
          }
        }
      });
    }

  }

  function checkNewConsultaions(){
    if ( document.visibilityState == "visible" ) {
      $.ajax({
        url: '/system/request/checkNewAdminConsultations',
        data: {
          params: JSON.stringify({})
        },
        method: 'POST',
        success: function ( response ) {
          if ( response && response.error === 0 ) {
            var count = response.data.count;
            if ( count ) {
              $('.toolbar-menu .consultations .new-consultations-number').html(count);
              $('.toolbar-menu .consultations .new-consultations-number').css('display', 'block');
            } else {
              $('.toolbar-menu .consultations .new-consultations-number').css('display', 'none');
            }

          } else {
            console.info(response.error, response.errorText);
          }
        }
      });
    }

  }

  if ( $('.toolbar-tab li.consultations').length ) {
    checkNewConsultaions();
    setInterval(function(){ checkNewConsultaions() }, 120000);
  }

  if ( $('.toolbar-tab li.doc-requests').length ) {
    checkNewRequests();
    setInterval(function(){ checkNewRequests() }, 120000);
  }


});
