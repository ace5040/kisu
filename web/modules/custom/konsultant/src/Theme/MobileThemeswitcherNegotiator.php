<?php
/**
 * @file
 * Contains \Drupal\konsultant\Theme\MobileThemeswitcherNegotiator.
 */

namespace Drupal\konsultant\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

class MobileThemeswitcherNegotiator implements ThemeNegotiatorInterface {

	public function applies(RouteMatchInterface $route_match) {

		$route = $route_match->getRouteObject();

		$is_admin = \Drupal::service('router.admin_context')->isAdminRoute($route);

		$applies = $is_admin ? FALSE : TRUE;

		return $applies;

	}

	/**
	 * {@inheritdoc}
	 */

	public function determineActiveTheme(RouteMatchInterface $route_match) {
		$mobileDetector = \Drupal::service('konsultant.mobile_detect');
		return $mobileDetector->isMobile() ? 'konsultant_v3' : 'konsultant_v3';
	}

}
