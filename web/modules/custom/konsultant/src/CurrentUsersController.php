<?php
/**
 * @file
 * Contains \Drupal\konsultant\CurrentUsersController.
 */

namespace Drupal\konsultant;

use Drupal\Component\Utility\Unicode;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use \Drupal\file\Entity\File;
use Drupal\entityQuery;
use Sunra\PhpSimple\HtmlDomParser;
use \ForceUTF8\Encoding;
use Drupal\Core\Database\Database;

class CurrentUsersController extends ControllerBase {

  public function CurrentUsersPage() {

    $data = [
      'users' => [],
      'commands' => [],
      'user_commands' => [],
    ];
    $current_time = time();

    $connection = Database::getConnection();

    $sth = $connection->select('konsultant_user_commands', 'kuc');
    $sth->leftJoin('users_field_data', 'ufd', 'ufd.uid=kuc.uid');
    $sth->leftJoin('taxonomy_term_field_data', 'ttfd', 'ttfd.tid=kuc.system');
    $sth->addField('ttfd', 'name', 'system');
    $sth->addExpression('count(distinct kuc.uid)', 'count_users');
    $sth->addExpression('count(kuc.command)', 'count_commands');
    $sth->addExpression('sum(kuc.time)', 'time_total');
    $sth->groupBy('ttfd.name');
    $sth->condition('kuc.begin', $current_time - 1200, '>=');
    $sth->orderBy('time_total', 'ASC');

    $result = $sth->execute();
    $rows = $result->fetchAll(\PDO::FETCH_OBJ);
    $data['systems'] = $rows;

    $sth = $connection->select('konsultant_user_commands', 'kuc');
    $sth->leftJoin('users_field_data', 'ufd', 'ufd.uid=kuc.uid');
    $sth->leftJoin('taxonomy_term_field_data', 'ttfd', 'ttfd.tid=kuc.system');
    $sth->addField('kuc', 'uid', 'uid');
    $sth->addField('ufd', 'name', 'name');
    $sth->addField('ttfd', 'name', 'system');
    $sth->addExpression('count(kuc.command)', 'count_commands');
    $sth->addExpression('sum(kuc.time)', 'time');
    $sth->groupBy('kuc.uid');
    $sth->groupBy('ufd.name');
    $sth->groupBy('ttfd.name');
    $sth->condition('kuc.begin', $current_time - 1200, '>=');
    $sth->orderBy('ufd.name', 'ASC');

    $result = $sth->execute();
    $rows = $result->fetchAll(\PDO::FETCH_OBJ);
    $data['users'] = $rows;

    $sth = $connection->select('konsultant_user_commands', 'kuc');
    $sth->leftJoin('users_field_data', 'ufd', 'ufd.uid=kuc.uid');
    $sth->leftJoin('taxonomy_term_field_data', 'ttfd', 'ttfd.tid=kuc.system');
    $sth->addField('kuc', 'uid', 'uid');
    $sth->addField('ufd', 'name', 'name');
    $sth->addField('ttfd', 'name', 'system');
    $sth->addField('kuc', 'command', 'command');
    $sth->addExpression('count(kuc.command)', 'count_commands');
    $sth->addExpression('sum(kuc.time)', 'time');
    $sth->groupBy('kuc.uid');
    $sth->groupBy('kuc.command');
    $sth->groupBy('ufd.name');
    $sth->groupBy('ttfd.name');
    $sth->condition('kuc.begin', $current_time - 1200, '>=');
    $sth->orderBy('time', 'DESC');

    $result = $sth->execute();
    $rows = $result->fetchAll(\PDO::FETCH_OBJ);
    $data['user_commands'] = $rows;

    // $sth = $connection->select('konsultant_user_commands', 'kuc');
    // $sth->leftJoin('users_field_data', 'ufd', 'ufd.uid=kuc.uid');
    // $sth->leftJoin('taxonomy_term_field_data', 'ttfd', 'ttfd.tid=kuc.system');
    // $sth->addField('kuc', 'uid', 'uid');
    // $sth->addField('ufd', 'name', 'name');
    // $sth->addField('ttfd', 'name', 'system');
    // $sth->addField('kuc', 'command', 'command');
    // $sth->addExpression('count(kuc.command)', 'count_commands');
    // $sth->addExpression('sum(kuc.time)', 'time');
    // $sth->groupBy('kuc.uid');
    // $sth->groupBy('kuc.command');
    // $sth->groupBy('ufd.name');
    // $sth->groupBy('ttfd.name');
    // $sth->condition('kuc.begin', $current_time - 1200, '>=');
    // $sth->orderBy('time', 'DESC');
    //
    // $result = $sth->execute();
    // $rows = $result->fetchAll(\PDO::FETCH_OBJ);
    // $data['commands'] = $rows;

  	return [
  		'#theme' => 'current_users_page',
      '#data' => $data
  	];
  }

}
