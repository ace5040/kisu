<?php
/**
 * @file
 * Contains \Drupal\konsultant\SystemColorsController.
 */

namespace Drupal\konsultant;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

class SystemColorsController extends ControllerBase {

  public function content() {

    $twig = \Drupal::service('twig');
    $template = $twig->loadTemplate('system-colors.html.twig');
    $html = $template->render([]);

    $response = new Response(
      $html,
      Response::HTTP_OK,
      [
        'content-type' => 'text/css'
      ]
    );
    $response->setMaxAge(86400);
    return $response;

  }

}
