<?php

/**
 * SystemAuth validator
 */
namespace Drupal\konsultant;

use Drupal\user\UserAuth;
use Drupal\user\Entity\User;

class SystemAuthValidate extends UserAuth {

  public function authenticate($username, $password) {
    $uid = parent::authenticate($username, $password);
    return $this->validateSystemUser($uid);
  }

  private function validateSystemUser ($uid) {

    if ( $uid ) {
      $user = User::load($uid);

      if ( $user->Id() != 1 ) {

        $user_roles = $user->getRoles();

        if ( !in_array('api', $user_roles ) &&!in_array('administrator', $user_roles ) && !in_array('editor_norm', $user_roles ) ) {

          $system = get_current_system();
          $user_systems = $user->get('field_system')->getValue();

          $can_login = false;

          foreach ( $user_systems as $user_system ) {

            if ( $user_system['target_id'] == $system->id || $user_system['target_id'] == $system->parent_id ) {
              $can_login = true;
            }

          }

          if ( !$can_login ) {
            return false;
          }

        }

        if ( !in_array('api', $user_roles ) && !in_array('administrator', $user_roles ) &&  !in_array('editor_norm', $user_roles ) &&  !in_array('editor', $user_roles ) ) {

          if ( !empty($user) && !empty($user->field_access_end->getValue()) ) {
            $today_date = date('Y-m-d', time());
            $access_end_date = $user->field_access_end->getValue()[0]['value'];
            if ( $access_end_date < $today_date ) {
              //$form_state->setErrorByName('name', t('У Вас закінчився доступ до системи. Для його подовження зателефонуйте за номером') . ' ' . $system->phone);
              return false;
            }
          }

          if ( !empty($user) && !empty($user->field_access_begin->getValue()) ) {
            $today_date = date('Y-m-d', time());
            $access_begin_date = $user->field_access_begin->getValue()[0]['value'];
            if ( $access_begin_date > $today_date ) {
              return false;
              //$form_state->setErrorByName('name', t('Період доступу до системи ще не розпочався'));
            }
          }

        }

      } else {
        return $uid;
      }

    }

    return $uid;

  }

}
