<?php
/**
 * @file
 * Contains \Drupal\konsultant\Plugin\QueueWorker\EmailQueue.
 */
namespace Drupal\Konsultant\Plugin\QueueWorker;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Processes Tasks for Learning.
 *
 * @QueueWorker(
 *   id = "email_queue",
 *   title = @Translation("Konsultant task worker: email queue"),
 *   cron = {"time" = 10}
 * )
 */
class EmailQueue extends QueueWorkerBase {
  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $user = $data['uid'];
    $message = Node::load($data['nid']);
    $system_id = $data['system_id'];
    $message_html = '';

    if ( !empty($message->field_message_html->getValue()) ) {
      $message_html = $message->field_message_html->getValue()[0]['value'];
    }

    $subject = $message->getTitle();

    konsultant_send_queue_message($user, $subject, $message_html, $system_id);

    if ( !empty($user) ) {
      $message->field_users_sended->appendItem(['target_id' => $user]);
      $message->save();
    }

  }

}
