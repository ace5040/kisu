<?php
/**
 * @file
 * Contains \Drupal\konsultant\Plugin\QueueWorker\AuditQueue.
 */
namespace Drupal\Konsultant\Plugin\QueueWorker;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Processes Tasks for Learning.
 *
 * @QueueWorker(
 *   id = "audit_queue",
 *   title = @Translation("Konsultant task worker: audit queue"),
 *   cron = {"time" = 10}
 * )
 */
class AuditQueue extends QueueWorkerBase {
  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    $user_id = $data['to_user'];
    $subject = $data['subject'];
    $message = $data['message'];
    $tmpl = $data['tmpl'];
    $system = $data['system'];
    konsultant_send_message($user_id, $subject, $message, $tmpl, $system);

  }

}
