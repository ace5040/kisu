<?php

/**
 * @file
 * Definition of \Drupal\konsultant\Plugin\CKEditorPlugin\RemoveLinks.
 */

namespace Drupal\konsultant\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Remove All Links" plugin.
 *
 * @CKEditorPlugin(
 *   id = "removelinks",
 *   label = @Translation("Remove All Links")
 * )
 */
class RemoveLinks extends CKEditorPluginBase {

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::isInternal().
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    // Make sure that the path to the plugin.js matches the file structure of
    // the CKEditor plugin you are implementing.
    $path = 'libraries/removelinks';
    return $path . '/plugin.js';
  }

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginButtonsInterface::getButtons().
   */
  public function getButtons() {
    return [
      'removelinks' => [
        'label' => 'Remove All Links',
        'image_alternative' => [
          '#type' => 'inline_template',
          '#template' => '<a href="#" role="button" aria-label="remove_links">Remove All Links</a>',
          '#context' => [
            'remove_links' => 'Remove All Links',
          ],
        ],
      ]
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return array();
  }

}
