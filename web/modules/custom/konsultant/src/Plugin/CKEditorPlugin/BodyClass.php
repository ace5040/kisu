<?php

namespace Drupal\konsultant\Plugin\CKEditorPlugin;

use Drupal\ckeditor\Plugin\CKEditorPlugin\Internal;
use Drupal\editor\Entity\Editor;

/**
 * Add css classes for html ckeditor fields.
 *
 * @CKEditorPlugin(
 *   id = "body_class",
 *   label = @Translation("Body Class CKEditor config")
 * )
 */
class BodyClass extends Internal {

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $system = get_current_system();
    $node = \Drupal::routeMatch()->getParameter('node');
    $type = 'default';
    if ($node instanceof \Drupal\node\NodeInterface) {
      $type = $node->getType();
    }
    $config = parent::getConfig($editor);
    if ( $system->id == 3 ) {
      $config['bodyClass'] = 'node-type-' . $type;
    }
    return $config;
  }

}
