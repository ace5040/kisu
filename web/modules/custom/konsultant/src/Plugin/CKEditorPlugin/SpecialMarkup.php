<?php

/**
 * @file
 * Definition of \Drupal\konsultant\Plugin\CKEditorPlugin\SpecialMarkup.
 */

namespace Drupal\konsultant\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Special Markup" plugin.
 *
 * @CKEditorPlugin(
 *   id = "specialmarkup",
 *   label = @Translation("Special Markup")
 * )
 */
class SpecialMarkup extends CKEditorPluginBase {

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::isInternal().
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    // Make sure that the path to the plugin.js matches the file structure of
    // the CKEditor plugin you are implementing.
    $path = 'libraries/specialmarkup';
    return $path . '/plugin.js';
  }

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginButtonsInterface::getButtons().
   */
  public function getButtons() {
    return [
      'specialmarkup' => [
        'label' => 'Special Markup',
        'image_alternative' => [
          '#type' => 'inline_template',
          '#template' => '<a href="#" role="button" aria-label="special_markup">Special Markup</a>',
          '#context' => [
            'special_markup' => 'Special Markup',
          ],
        ],
      ]
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {

    $system = get_current_system();

    $selections = [
      'selection_text_term' => $system->selection_text_term,
      'selection_text_suggestion'  => $system->selection_text_suggestion,
      'selection_text_file' => $system->selection_text_file,
      'selection_text_important' => $system->selection_text_important,
      'selection_text_law' => $system->selection_text_law,
      'selection_text_quote' => $system->selection_text_quote
    ];

    return $selections;
  }

}
