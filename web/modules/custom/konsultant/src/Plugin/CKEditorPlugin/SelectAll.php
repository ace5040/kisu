<?php

/**
 * @file
 * Definition of \Drupal\konsultant\Plugin\CKEditorPlugin\SelectAll.
 */

namespace Drupal\konsultant\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Select All" plugin.
 *
 * @CKEditorPlugin(
 *   id = "selectall",
 *   label = @Translation("Select All")
 * )
 */
class SelectAll extends CKEditorPluginBase {

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::isInternal().
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    // Make sure that the path to the plugin.js matches the file structure of
    // the CKEditor plugin you are implementing.
    $path = 'libraries/selectall';
    return $path . '/plugin.js';
  }

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginButtonsInterface::getButtons().
   */
  public function getButtons() {
    return [
      'selectall' => [
        'label' => 'Select All',
        'image_alternative' => [
          '#type' => 'inline_template',
          '#template' => '<a href="#" role="button" aria-label="select_all">Select All</a>',
          '#context' => [
            'select_all' => 'Select All',
          ],
        ],
      ]
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return array();
  }

}
