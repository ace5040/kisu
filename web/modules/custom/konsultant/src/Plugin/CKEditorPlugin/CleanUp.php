<?php

/**
 * @file
 * Definition of \Drupal\konsultant\Plugin\CKEditorPlugin\CleanUp.
 */

namespace Drupal\konsultant\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Clean Up" plugin.
 *
 * @CKEditorPlugin(
 *   id = "cleanup",
 *   label = @Translation("Clean Up")
 * )
 */
class CleanUp extends CKEditorPluginBase {

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::isInternal().
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    // Make sure that the path to the plugin.js matches the file structure of
    // the CKEditor plugin you are implementing.
    $path = 'libraries/cleanup';
    return $path . '/plugin.js';
  }

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginButtonsInterface::getButtons().
   */
  public function getButtons() {
    return [
      'cleanup' => [
        'label' => 'Clean Up',
        'image_alternative' => [
          '#type' => 'inline_template',
          '#template' => '<a href="#" role="button" aria-label="cleanup">Clean Up</a>',
          '#context' => [
            'cleanup' => 'Clean Up',
          ],
        ],
      ]
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}
