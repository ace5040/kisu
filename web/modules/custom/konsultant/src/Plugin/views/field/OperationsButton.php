<?php

namespace Drupal\konsultant\Plugin\views\field;

use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\field\Links;
use Drupal\Core\Url;
/**
 * Provides a handler that renders links as operationsbutton.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("operationsbutton")
 */
class Operationsbutton extends Links {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $links = [];
    $status = $values->_entity->field_status->value;
    $type = $values->_entity->field_consultation_type->value;
    $nid = $values->_entity->nid->value;
    $destination = in_array($type, ['accountinglog', 'example', 'norm', 'instruction']) ? '/admin/editor-norm-consultations' : '/admin/editor-consultations';

    if ( in_array($status, ['answered']) ) {
      $links['place'] = [
        'url' => Url::fromUserInput('/node/' . $nid . '/edit'),
        'title' => 'Розмістити',
        'query' => [
          'mode' => 'approve',
          'destination' => $destination
        ]
      ];
    }

    if ( in_array($status, ['new', 'set', 'accepted', 'rejected', 'timeouted', 'clarified']) ) {

      if ( in_array($type, ['consultation', 'interview', 'notfound']) ) {
        $links['forexpert'] = [
          'url' => Url::fromUserInput('/node/' . $nid . '/edit'),
          'title' => 'Передати експерту',
          'query' => [
            'mode' => 'forexpert',
            'destination' => $destination
          ]
        ];
      }

      $links['answer'] = [
        'url' => Url::fromUserInput('/node/' . $nid . '/edit'),
        'title' => 'Надати відповідь',
        'query' => [
          'mode' => 'editorreply',
          'destination' => $destination
        ]
      ];

      if (
        in_array($type, ['consultation', 'interview', 'notfound', 'help'])
        &&
        in_array($status, ['new', 'rejected', 'timeouted', 'clarified'])
      ) {
        $links['clarify'] = [
          'url' => Url::fromUserInput('/node/' . $nid . '/edit'),
          'title' => 'Уточнення',
          'query' => [
            'mode' => 'clarification',
            'destination' => $destination
          ]
        ];
      }

    }

    $links['edit'] = [
      'url' => Url::fromUserInput('/node/' . $nid . '/edit'),
      'title' => 'Редагувати',
      'query' => [
        'destination' => $destination
      ]
    ];

    return !empty($links) ? [ '#type' => 'dropbutton', '#links' => $links ] : '';

  }

}
