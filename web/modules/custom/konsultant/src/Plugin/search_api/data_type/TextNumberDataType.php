<?php

namespace Drupal\konsultant\Plugin\search_api\data_type;

use Drupal\search_api\Plugin\search_api\data_type\TextDataType;

/**
 * Provides a ngram full text data type.
 *
 * @SearchApiDataType(
 *   id = "text_number",
 *   label = @Translation("Text Number"),
 *   description = @Translation("Text Number"),
 *   fallback_type = "text",
 *   prefix = "sn",
 *   default = "true"
 * )
 */
class TextNumberDataType extends TextDataType {}
