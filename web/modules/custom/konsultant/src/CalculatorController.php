<?php
/**
 * @file
 * Contains \Drupal\konsultant\CalculatorController.
 */

namespace Drupal\konsultant;


use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\entityQuery;

class CalculatorController extends ControllerBase {

  public function print1_a() {
    $data = json_decode(\Drupal::request()->request->get('data'));
    $twig = \Drupal::service('twig');
    $template = $twig->loadTemplate('calculators-print1-a.html.twig');
    $html = $template->render([
      'fio' => $data->fio,
      'db' => $data->db,
      'de' => $data->de,
      'dayscount' => $data->dayscount,
      'results' => $data->results
    ]);

    $response = new Response(
      $html,
      Response::HTTP_OK,
      ['content-type' => 'text/html']
    );

    return $response;

  }

  public function print1_b() {
    $data = json_decode(\Drupal::request()->request->get('data'));
    $twig = \Drupal::service('twig');
    $template = $twig->loadTemplate('calculators-print1-b.html.twig');
    $html = $template->render([
      'fio' => $data->fio,
      'db' => $data->db,
      'de' => $data->de,
      'dayscount' => $data->dayscount,
      'results' => $data->results
    ]);

    $response = new Response(
      $html,
      Response::HTTP_OK,
      ['content-type' => 'text/html']
    );

    return $response;

  }

  public function print1_c() {
    $data = json_decode(\Drupal::request()->request->get('data'));
    $twig = \Drupal::service('twig');
    $template = $twig->loadTemplate('calculators-print1-c.html.twig');
    $html = $template->render([
      'fio' => $data->fio,
      'db' => $data->db,
      'de' => $data->de,
      'dayscount' => $data->dayscount,
      'results' => $data->results
    ]);

    $response = new Response(
      $html,
      Response::HTTP_OK,
      ['content-type' => 'text/html']
    );

    return $response;

  }

  public function print2() {
    $data = json_decode(\Drupal::request()->request->get('data'));
    $twig = \Drupal::service('twig');
    $template = $twig->loadTemplate('calculators-print2.html.twig');
    $html = $template->render([
      'fio' => $data->fio,
      'periods' => $data->periods,
      'results' => $data->results
    ]);

    $response = new Response(
      $html,
      Response::HTTP_OK,
      ['content-type' => 'text/html']
    );

    return $response;

  }

}
