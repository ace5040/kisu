<?php

namespace Drupal\konsultant;
use Drupal\node\Entity\Node;

/**
 * Class BatchService.
 */
class BatchService {


  /**
   * Batch process callback.
   *
   * @param int $id
   *   Id of the batch.
   * @param int $nid
   *   Nid of the operation.
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public function processFindLinks($id, $nids, $operation_details, &$context) {
    $eta=-hrtime(true);
    konsultant_findLinks($nids);
    $context['results'][] = $id;
    $memory = round(memory_get_usage(true)/1024/1024) . 'm';
    $eta+=hrtime(true);
    $seconds = round($eta/1e+6);
    $context['message'] = t('Running Batch "@id" @details ' . $memory . ' ' . $seconds . 'ms',
    ['@id' => $id, '@details' => $operation_details]
  );

}

  /**
   * Batch process callback.
   *
   * @param int $id
   *   Id of the batch.
   * @param int $nid
   *   Nid of the operation.
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public function processJobsRebuild($id, $jobs, $operation_details, &$context) {
    $eta=-hrtime(true);
    if ( !empty($jobs) ) {
      foreach($jobs as $job) {
        $node = Node::create([
          'type' => 'job',
          'uid' => 1,
          'title' => $job->field_job_name,
          'field_kod_kp' => $job->field_kod_kp,
          'field_kod_zkpptr' => $job->field_kod_zkpptr,
          'field_vypusk_yetkd' => $job->field_vypusk_yetkd,
          'field_vypusk_dkkhp' => $job->field_vypusk_dkkhp,
          'field_job_name' => $job->field_job_name
        ]);
        $node->save();
      }
    }
    $context['results'][] = $id;
    $memory = round(memory_get_usage(true)/1024/1024) . 'm';
    $eta+=hrtime(true);
    $seconds = round($eta/1e+6);
    $context['message'] = t('Running Batch "@id" @details ' . $memory . ' ' . $seconds . 'ms',
    ['@id' => $id, '@details' => $operation_details]
  );

}

public static function listLinks($html) {
  $ids = [];

  $chunks = explode('href="', $html);
  if ( count($chunks) > 1 ) {
    array_shift($chunks);
    foreach ( $chunks as $chunk ) {
      $link_str = explode('"', $chunk)[0];
      $id = self::parseNormLink($link_str);
      if ( !empty($id) ) {
        $ids[] = $id;
      }
    }
  }

  return $ids;
}

private static function parseNormLink($str) {
  /*
    konsultant: '/#norm/general/{id}'
    kisu: '/norm/{id}-', '/norm/{id}'
    lj: '/#norm/general/{id}', '/norm/{id}-', '/norm/{id}'
    norm: '/norm/{id}-', '/norm/{id}'
  */
  $nid = 0;
  $chunks = explode('/#norm/general/', $str);
  if ( count($chunks) > 1 ) {
    $nid = $chunks[1];
  }
  if ( empty($nid) ) {
    $chunks = explode('/norm/', $str);
    if ( count($chunks) > 1 ) {
      $chunks_2 = explode('-', $chunks[1]);
      if ( count($chunks_2) > 1 ) {
        $nid = $chunks_2[0];
      } else {
        $nid = $chunks[1];
      }
    }
  }
  return $nid;
}

  /**
 * Batch Finished callback.
 *
 * @param bool $success
 *   Success of the operation.
 * @param array $results
   *   Array of results for post processing.
   * @param array $operations
   *   Array of operations.
   */
  public function processFindLinksFinished($success, array $results, array $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      // Here we could do something meaningful with the results.
      // We just display the number of nodes we processed...
      $messenger->addMessage(t('@count results processed.', ['@count' => count($results)]));
    } else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $messenger->addMessage(
        t('An error occurred while processing @operation with arguments : @args',
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }

  /**
 * Batch Finished callback.
 *
 * @param bool $success
 *   Success of the operation.
 * @param array $results
   *   Array of results for post processing.
   * @param array $operations
   *   Array of operations.
   */
  public function processJobsRebuildFinished($success, array $results, array $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      // Here we could do something meaningful with the results.
      // We just display the number of nodes we processed...
      $messenger->addMessage(t('@count results processed.', ['@count' => count($results)]));
    } else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $messenger->addMessage(
        t('An error occurred while processing @operation with arguments : @args',
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }

}
