<?php

/**
 * SystemAuth validator
 */
namespace Drupal\konsultant;

use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ContainerBuilder;


class KonsultantServiceProvider extends ServiceProviderBase {

  public function alter(ContainerBuilder $container)
  {
      $definition = $container->getDefinition('user.auth');
      $definition->setClass('Drupal\konsultant\SystemAuthValidate');
  }

}
