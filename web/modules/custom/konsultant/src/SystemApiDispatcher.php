<?php
/**
 * @file
 * Contains \Drupal\konsultant\SystemApiDispatcher.
 */

namespace Drupal\konsultant;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Controller\ControllerBase;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\entityQuery;
use Drupal\Core\Site\Settings;
use Drupal\file\Entity\File;
use Drupal\flag;
use Drupal\Core\Database\Database;
use Drupal\konsultant\SystemRequestDispatcher;
use Drupal\konsultant\TwigExtension\KonsultantTwigExtension;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\RequestException;

class SystemApiDispatcher extends ControllerBase {

  public static function writeLog($system_id, $uid, $command, $params, $begin, $time) {
    $connection = Database::getConnection();
    $result = $connection->insert('konsultant_user_commands')
      ->fields([
        'uid' => $uid,
        'system' => $system_id,
        'begin' => $begin,
        'time' => $time,
        'command' => $command,
        'params' => $params
      ])
      ->execute();
  }

  public function apiHandler() {

    $command = \Drupal::request()->request->get('command');
    $raw_params = \Drupal::request()->request->get('params');
    $params = json_decode($raw_params);

    if ( !empty($_FILES['file']) ) {
      $params->file = $_FILES['file'];
    }

    $system = get_current_system();

    $error = 0;
    $errorText = '';

    $uid = \Drupal::currentUser()->Id();

    $isAnonymous = \Drupal::currentUser()->isAnonymous();

    $data = (object)[];

    $begin = time();
    $time_start = microtime(true);

    if ( $isAnonymous ) {

      $error = 999;
      $errorText = 'not authorized';

    } else {

      switch ( $command ) {

        case 'getNormFiltersDocStatuses':

          if ( isset($system->modules['norm']) ) {
            $data = KonsultantTwigExtension::doc_statuses();
          } else {
            $error = 100;
            $errorText = 'Module norm is not enabled';
          }
        break;

        case 'getNormFiltersDocTypes':

          if ( isset($system->modules['norm']) ) {
            $data = KonsultantTwigExtension::doc_types();
          } else {
            $error = 100;
            $errorText = 'Module norm is not enabled';
          }
        break;

        case 'getNormFiltersDocProducers':

          if ( isset($system->modules['norm']) ) {
            $data = KonsultantTwigExtension::doc_producers();
          } else {
            $error = 100;
            $errorText = 'Module norm is not enabled';
          }
        break;

        case 'getNormList':

          if ( isset($system->modules['norm']) ) {
            $data = SystemRequestDispatcher::getNormListLocal($params);
          } else {
            $error = 100;
            $errorText = 'Module norm is not enabled';
          }
        break;

        case 'getExamplesList':

          if ( isset($system->modules['examples']) ) {
            $data = SystemRequestDispatcher::getExamplesList($params);
          } else {
            $error = 100;
            $errorText = 'Module examples is not enabled';
          }

        break;

        case 'getCribsList':

          if ( isset($system->modules['cribs']) ) {
            $data = SystemRequestDispatcher::getCribsList($params);
          } else {
            $error = 100;
            $errorText = 'Module cribs is not enabled';
          }

        break;

        case 'getInterviewsList':

          if ( isset($system->modules['interviews']) ) {
            $data = SystemRequestDispatcher::getInterviewsList($params);
          } else {
            $error = 100;
            $errorText = 'Module interviews is not enabled';
          }

        break;

        case 'getOrientationTree':

          if ( isset($system->modules['orientation']) ) {
            $data = SystemRequestDispatcher::getOrientationTree($params);
          } else {
            $error = 100;
            $errorText = 'Module interviews is not enabled';
          }

        break;

        case 'getOrientationNodes':

          if ( isset($system->modules['orientation']) ) {
            $data = SystemRequestDispatcher::getOrientationNodes($params);
          } else {
            $error = 100;
            $errorText = 'Module interviews is not enabled';
          }

        break;

        case 'getNewsList':

          if ( isset($system->modules['news']) ) {
            $data = SystemRequestDispatcher::getNewsList($params);
          } else {
            $error = 100;
            $errorText = 'Module norm is not enabled';
          }

        break;

        case 'getNewsListGlobal':
          $data = SystemRequestDispatcher::getNewsListGlobal($params);
        break;

        case 'getSearchList':
          $data = SystemRequestDispatcher::getSearchList($params);
        break;

        case 'getSearchLinks':
          $data = SystemRequestDispatcher::getSearchLinks($params);
        break;

        case 'getInstructionsList':

          if ( isset($system->modules['instructions']) ) {
            $data = SystemRequestDispatcher::getInstructionsList($params);
          } else {
            $error = 100;
            $errorText = 'Module norm is not enabled';
          }

        break;

        case 'getConsultationsList':

          if ( isset($system->modules['consultations']) ) {
            $data = SystemRequestDispatcher::getConsultationsList($params);
          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'getNorm':

          if ( isset($system->modules['norm']) ) {
            $data = SystemRequestDispatcher::getNormLocal($params);
          } else {
            $error = 100;
            $errorText = 'Module norm is not enabled';
          }

        break;

        case 'getExample':

          if ( isset($system->modules['examples']) ) {
            $data = SystemRequestDispatcher::getExample($params);
          } else {
            $error = 100;
            $errorText = 'Module examples is not enabled';
          }

        break;

        case 'getCrib':

          if ( isset($system->modules['cribs']) ) {
            $data = SystemRequestDispatcher::getCrib($params);
          } else {
            $error = 100;
            $errorText = 'Module cribs is not enabled';
          }

        break;

        case 'getInterview':

          if ( isset($system->modules['interviews']) ) {
            $data = SystemRequestDispatcher::getInterview($params);
          } else {
            $error = 100;
            $errorText = 'Module itnerviews is not enabled';
          }

        break;

        case 'getNew':

          if ( isset($system->modules['news']) ) {
            $data = SystemRequestDispatcher::getNew($params);
          } else {
            $error = 100;
            $errorText = 'Module new is not enabled';
          }

        break;

        case 'getInstruction':

          if ( isset($system->modules['instructions']) ) {
            $data = SystemRequestDispatcher::getInstruction($params);
          } else {
            $error = 100;
            $errorText = 'Module instructions is not enabled';
          }

        break;

        default:
          $error = 100;
          $errorText = 'Unknown command';
          $data = (object)[];
        break;

      }

    }

    $responseData = (object)[
      'error' => $error,
      'errorText' => $errorText,
      'data' => $data
    ];

    $time_end = microtime(true);
    $time = $time_end - $time_start;

    self::writeLog($system->id, $uid, $command, $raw_params, $begin, $time);

    $response = new JsonResponse($responseData);
    $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    return $response;

  }

}
