<?php

namespace Drupal\konsultant\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;

/**
 * Class DeleteNodesCommand.
 *
 * @DrupalCommand (
 *     extension="konsultant",
 *     extensionType="module"
 * )
 */
class DeleteNodesCommand extends ContainerAwareCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('delete:nodes')
      ->setDescription('DELETE ALL NODES');
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {

    $this->getIo()->info('Starting batch...');

    $deleted = 0;

    //$delete_types = ['consultation', 'normdoc', 'example', 'instruction', 'new', 'interview', 'crib'];
    $delete_types = ['job'];

    $systems = [];

    $vid = 'system';

    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);

    foreach ($terms as $term) {
     $systems[] = $term->tid;
    }

    $chunkSize = 100;

    foreach ($delete_types as $delete_type) {

      $query = \Drupal::entityQuery('node');
      $query->condition('type', $delete_type);
      $query->sort('nid', 'ASC');

      $ids = $query->execute();

      $chunks = array_chunk($ids, $chunkSize);
      $progress = new ProgressBar($output, count($chunks));
      $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% processing ' . $delete_type  . ' : %message%');
      $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm');
      foreach ($chunks as $chunk) {
        entity_delete_multiple('node', $chunk);
        $deleted+=count($chunk);
        $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm');
        $progress->advance();
      }
      $progress->finish();
      $output->writeln('');
    }

    $this->getIo()->info('deleted: ' . $deleted);

    $this->getIo()->info('Batch done!');

  }

}
