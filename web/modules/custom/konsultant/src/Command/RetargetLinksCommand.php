<?php

namespace Drupal\konsultant\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;

/**
 * Class RetargetLinksCommand.
 *
 * @DrupalCommand (
 *     extension="konsultant",
 *     extensionType="module"
 * )
 */
class RetargetLinksCommand extends ContainerAwareCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('links:retarget')
      ->setDescription('Retarget links for norm');

  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {

    $processed = 0;
    $rebuilt = 0;

    $recompute_types = ['normdoc'];

    $systems = [];

    $vid = 'system';

    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);

    foreach ($terms as $term) {
     $systems[] = $term->tid;
    }

    $systems = [3];

    foreach ($systems as $system_id) {

      foreach ($recompute_types as $recompute_type) {

        $query = \Drupal::entityQuery('node');
        $query->condition('field_systems', $system_id, 'IN');
        $query->condition('type', $recompute_type);
        $query->sort('nid', 'ASC');

        $entity_ids = $query->execute();
        $entity_ids = array_values($entity_ids);

        $progress = new ProgressBar($output, count($entity_ids));
        $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% processing ' . $recompute_type . ' in ' . $system_id . ' : %message%');
        $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm');

        foreach ($entity_ids as $node_id) {
          $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm - ' . $node_id);
          $node = Node::load($node_id);
          $changed_date = $node->changed->value;

          //apply actions
          $node_type = $node->getType();

          $changes_made = false;

          //if ( $processed > 1800 ) {
          if ( true ) {

            switch ( $node_type ) {

              case 'consultation':

                $answer = '';

                if ( !empty($node->get('field_answer')->getValue()) ) {
                  $answer = $node->get('field_answer')->getValue()[0]['value'];
                }

                if ( !empty($answer) ) {

                  $answer = str_replace('href="/#norm/general', 'target="_blank" href="/#norm/general', $answer);
                  $changes_made = true;
                  if ( $changes_made ) {
                    $node->get('field_answer')->setValue($answer);
                  }

                }

              break;
              case 'interview':

                $html = '';

                if ( !empty($node->get('field_interview_html')->getValue()) ) {
                  $html = $node->get('field_interview_html')->getValue()[0]['value'];
                }

                if ( !empty($html) ) {

                  $html = str_replace('href="/#norm/general', 'target="_blank" href="/#norm/general', $html);
                  $changes_made = true;

                  if ( $changes_made ) {
                    $node->get('field_interview_html')->setValue($html);
                  }

                }

              break;

              case 'new':

              $html = '';

              if ( !empty($node->get('field_new_html')->getValue()) ) {
                $html = $node->get('field_new_html')->getValue()[0]['value'];
              }

              if ( !empty($html) ) {

                $html = str_replace('href="/#norm/general', 'target="_blank" href="/#norm/general', $html);
                $changes_made = true;

                if ( $changes_made ) {
                  $node->get('field_new_html')->setValue($html);
                }

              }

              break;

              case 'normdoc':

              if ( !empty($node->field_documents) ) {


                foreach ( $node->field_documents as $key => $item) {
                  $item = $item->value;
                  $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($item);

                  if ( !empty($fc->field_document->getValue()[0]['value']) ) {
                    $html = $fc->field_document->getValue()[0]['value'];

                    if ( !empty($html) ) {

                      $count_before = strlen($html);
                      $html = str_replace('href="/#norm/general', 'target="_blank" href="/#norm/general', $html);
                      $count_after = strlen($html);

                      if ( $count_after != !$count_before ) {
                        $changes_made = true;
                      }

                      if ( $changes_made ) {
                        $fc->field_document->setValue($html);
                        try {
                          $fc->save(true);
                        }
                        catch (Exception $e) {
                          \Drupal::logger('links rebuild')->error('Error while saving a node: ' . $node->Id());
                        }
                      }

                    }

                  }

                }

              }

              break;

            }


            if ( $changes_made ) {
              $rebuilt++;
              //save
              $node->save();
              konsultant_restore_changed_date($changed_date, $node_id);
            }

          }

          $processed++;
          $progress->advance();
        }

        $progress->finish();
        $output->writeln('');
      }

    }

    $this->getIo()->info('processed: ' . $processed);
    $this->getIo()->info('rebuilt: ' . $rebuilt);

  }

}
