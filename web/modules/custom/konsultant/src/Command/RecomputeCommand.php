<?php

namespace Drupal\konsultant\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;

/**
 * Class RecomputeCommand.
 *
 * @DrupalCommand (
 *     extension="konsultant",
 *     extensionType="module"
 * )
 */
class RecomputeCommand extends ContainerAwareCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('recompute')
      ->setDescription('Recompute computed fields');

  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {

    $processed = 0;

    $recompute_types = ['normdoc', 'consultation', 'new', 'instruction', 'example', 'accountinglogs', 'interview', 'crib'];
    //$recompute_types = ['normdoc'];

    $vid = 'system';

    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);

    $systems = [];

    foreach ($terms as $term) {
     $systems[] = $term->tid;
    }

    //$systems = [3];

    foreach ($systems as $system_id) {

      foreach ($recompute_types as $recompute_type) {

        $query = \Drupal::entityQuery('node');
        $query->condition('field_systems', $system_id, 'IN');
        $query->condition('type', $recompute_type);
        $query->sort('nid', 'ASC');

        $entity_ids = $query->execute();
        $entity_ids = array_values($entity_ids);

        $progress = new ProgressBar($output, count($entity_ids));
        $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% processing ' . $recompute_type . ' in ' . $system_id . ' : %message%');
        $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm');

        foreach ($entity_ids as $id) {

          $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm - ' . $id);

          switch ($recompute_type) {

            case 'normdoc':
              $fields = [
                //'field_computed_title',
                //'field_last_date_init',
                //'field_date_errors',
                'field_sort_date',
              ];
            break;

            default:
              $fields = [
                'field_sort_date'
              ];

          }
          foreach ($fields as $field_name) {
            $functionName = 'computed_field_' . $field_name . '_compute_custom';
            $value = $functionName(intval($id),null,null,null);
            konsultant_save_computed_field_value($field_name, $value, $id);
          }

          $processed++;
          $progress->advance();
        }

        $progress->finish();
        $output->writeln('');
      }

    }

    $this->getIo()->info('processed: ' . $processed);

  }

}
