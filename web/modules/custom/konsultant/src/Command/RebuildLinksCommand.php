<?php

namespace Drupal\konsultant\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;

/**
 * Class RebuildLinksCommand.
 *
 * @DrupalCommand (
 *     extension="konsultant",
 *     extensionType="module"
 * )
 */
class RebuildLinksCommand extends ContainerAwareCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('links:rebuild')
      ->setDescription('Rebuild links to norm docs and examples');

  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {

    $processed = 0;
    $rebuilt = 0;

    //$recompute_types = ['normdoc'];
    $recompute_types = ['consultation', 'interview', 'crib', 'new'];

    $systems = [];

    $vid = 'system';

    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);

    foreach ($terms as $term) {
     $systems[] = $term->tid;
    }

    $systems = [3];

    $nodes_with_lost_links = [];

    foreach ($systems as $system_id) {

      foreach ($recompute_types as $recompute_type) {

        $query = \Drupal::entityQuery('node');
        $query->condition('field_systems', $system_id, 'IN');
        $query->condition('type', $recompute_type);
        $query->sort('nid', 'ASC');

        $entity_ids = $query->execute();
        $entity_ids = array_values($entity_ids);

        $progress = new ProgressBar($output, count($entity_ids));
        $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% processing ' . $recompute_type . ' in ' . $system_id . ' : %message%');
        $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm');

        foreach ($entity_ids as $node_id) {
          $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm - ' . $node_id);
          $node = Node::load($node_id);
          $changed_date = $node->changed->value;

          //apply actions
          $node_type = $node->getType();

          $changes_made = false;

          if ( $processed > 2500 ) {

            switch ( $node_type ) {

              case 'consultation':

              $html = '';

              if ( !empty($node->get('field_answer')->getValue()) ) {
                $html = $node->get('field_answer')->getValue()[0]['value'];
              }

              if ( !empty($html) ) {

                $links_to_replace = [];

                $patterns = [
                  'href="/node/',
                  'href="http://online.kadrovik.ua/node/'
                ];

                foreach ($patterns as $pattern) {

                  $parts = explode($pattern, $html);

                  if ( count($parts) > 1 ) {
                    array_shift($parts);
                    foreach ($parts as $part) {
                      $link_parts = explode('"', $part);
                      if ( !empty($link_parts) ) {
                        $links_to_replace[$link_parts[0]] = [ 'pattern' => $pattern, 'real_nid' => 0, 'real_type' => '' ];
                      }
                    }
                  }

                }

                if ( count($links_to_replace) ) {

                  foreach ($links_to_replace as $nid => $replace) {

                    $nodes = \Drupal::entityTypeManager()
                    ->getStorage('node')
                    ->loadByProperties(['field_import_id' => $system_id . '-' . $nid]);

                    $real_node = reset($nodes);
                    if ( !empty($real_node) ) {
                      $links_to_replace[$nid]['real_nid'] = $real_node->Id();
                      $links_to_replace[$nid]['real_type'] = $real_node->getType();
                    }
                  }

                  foreach ($links_to_replace as $nid => $replace) {
                    $type = '';
                    if ( $replace['real_nid'] ) {
                      switch ($replace['real_type']){
                        case 'normdoc':
                        $type = 'norm';
                        break;
                        case 'example':
                        $type = 'examples';
                        break;
                        case 'interview':
                        $type = 'interviews';
                        break;
                        case 'instruction':
                        $type = 'instructions';
                        break;
                        case 'new':
                        $type = 'news';
                        break;
                        case 'crib':
                        $type = 'cribs';
                        break;
                      }
                    }
                    if ( !empty($type) ) {
                      $html = str_replace($replace['pattern'] . $nid, 'href="/#' . $type . '/general/' . $replace['real_nid'], $html);
                      //$changes_made = true;
                    } else {
                      $nodes_with_lost_links[] = $node_id;
                    }
                  }

                  if ( $changes_made ) {
                    $node->get('field_answer')->setValue($html);
                  }

                }

              }

              break;
              case 'interview':

              $html = '';

              if ( !empty($node->get('field_interview_html')->getValue()) ) {
                $html = $node->get('field_interview_html')->getValue()[0]['value'];
              }

              if ( !empty($html) ) {

                $links_to_replace = [];

                $patterns = [
                  'href="/node/',
                  'href="http://online.kadrovik.ua/node/'
                ];

                foreach ($patterns as $pattern) {

                  $parts = explode($pattern, $html);

                  if ( count($parts) > 1 ) {
                    array_shift($parts);
                    foreach ($parts as $part) {
                      $link_parts = explode('"', $part);
                      if ( !empty($link_parts) ) {
                        $links_to_replace[$link_parts[0]] = [ 'pattern' => $pattern, 'real_nid' => 0, 'real_type' => '' ];
                      }
                    }
                  }

                }

                if ( count($links_to_replace) ) {

                  foreach ($links_to_replace as $nid => $replace) {

                    $nodes = \Drupal::entityTypeManager()
                    ->getStorage('node')
                    ->loadByProperties(['field_import_id' => $system_id . '-' . $nid]);

                    $real_node = reset($nodes);
                    if ( !empty($real_node) ) {
                      $links_to_replace[$nid]['real_nid'] = $real_node->Id();
                      $links_to_replace[$nid]['real_type'] = $real_node->getType();
                    }
                  }

                  foreach ($links_to_replace as $nid => $replace) {
                    $type = '';
                    if ( $replace['real_nid'] ) {
                      switch ($replace['real_type']){
                        case 'normdoc':
                        $type = 'norm';
                        break;
                        case 'example':
                        $type = 'examples';
                        break;
                        case 'interview':
                        $type = 'interviews';
                        break;
                        case 'instruction':
                        $type = 'instructions';
                        break;
                        case 'new':
                        $type = 'news';
                        break;
                        case 'crib':
                        $type = 'cribs';
                        break;
                      }
                    }
                    if ( !empty($type) ) {
                      $html = str_replace($replace['pattern'] . $nid, 'href="/#' . $type . '/general/' . $replace['real_nid'], $html);
                      //$changes_made = true;
                    } else {
                      $nodes_with_lost_links[] = $node_id;
                    }
                  }

                  if ( $changes_made ) {
                    $node->get('field_interview_html')->setValue($html);
                  }

                }

              }

              break;

              case 'crib':

              $html = '';

              if ( !empty($node->get('field_crib_html')->getValue()) ) {
                $html = $node->get('field_crib_html')->getValue()[0]['value'];
              }

              if ( !empty($html) ) {

                $links_to_replace = [];

                $patterns = [
                  'href="/node/',
                  'href="http://online.kadrovik.ua/node/'
                ];

                foreach ($patterns as $pattern) {

                  $parts = explode($pattern, $html);

                  if ( count($parts) > 1 ) {
                    array_shift($parts);
                    foreach ($parts as $part) {
                      $link_parts = explode('"', $part);
                      if ( !empty($link_parts) ) {
                        $links_to_replace[$link_parts[0]] = [ 'pattern' => $pattern, 'real_nid' => 0, 'real_type' => '' ];
                      }
                    }
                  }

                }

                if ( count($links_to_replace) ) {

                  foreach ($links_to_replace as $nid => $replace) {

                    $nodes = \Drupal::entityTypeManager()
                    ->getStorage('node')
                    ->loadByProperties(['field_import_id' => $system_id . '-' . $nid]);

                    $real_node = reset($nodes);
                    if ( !empty($real_node) ) {
                      $links_to_replace[$nid]['real_nid'] = $real_node->Id();
                      $links_to_replace[$nid]['real_type'] = $real_node->getType();
                    }
                  }

                  foreach ($links_to_replace as $nid => $replace) {
                    $type = '';
                    if ( $replace['real_nid'] ) {
                      switch ($replace['real_type']){
                        case 'normdoc':
                        $type = 'norm';
                        break;
                        case 'example':
                        $type = 'examples';
                        break;
                        case 'interview':
                        $type = 'interviews';
                        break;
                        case 'instruction':
                        $type = 'instructions';
                        break;
                        case 'new':
                        $type = 'news';
                        break;
                        case 'crib':
                        $type = 'cribs';
                        break;
                      }
                    }
                    if ( !empty($type) ) {
                      $html = str_replace($replace['pattern'] . $nid, 'href="/#' . $type . '/general/' . $replace['real_nid'], $html);
                      //$changes_made = true;
                    } else {
                      $nodes_with_lost_links[] = $node_id;
                    }
                  }

                  if ( $changes_made ) {
                    $node->get('field_crib_html')->setValue($html);
                  }

                }

              }

              break;

              case 'new':

              $html = '';

              if ( !empty($node->get('field_new_html')->getValue()) ) {
                $html = $node->get('field_new_html')->getValue()[0]['value'];
              }

              if ( !empty($html) ) {

                $links_to_replace = [];

                $patterns = [
                  'href="/node/',
                  'href="http://online.kadrovik.ua/node/'
                ];

                foreach ($patterns as $pattern) {

                  $parts = explode($pattern, $html);

                  if ( count($parts) > 1 ) {
                    array_shift($parts);
                    foreach ($parts as $part) {
                      $link_parts = explode('"', $part);
                      if ( !empty($link_parts) ) {
                        $links_to_replace[$link_parts[0]] = [ 'pattern' => $pattern, 'real_nid' => 0, 'real_type' => '' ];
                      }
                    }
                  }

                }

                if ( count($links_to_replace) ) {

                  foreach ($links_to_replace as $nid => $replace) {

                    $nodes = \Drupal::entityTypeManager()
                    ->getStorage('node')
                    ->loadByProperties(['field_import_id' => $system_id . '-' . $nid]);

                    $real_node = reset($nodes);
                    if ( !empty($real_node) ) {
                      $links_to_replace[$nid]['real_nid'] = $real_node->Id();
                      $links_to_replace[$nid]['real_type'] = $real_node->getType();
                    }
                  }

                  foreach ($links_to_replace as $nid => $replace) {
                    $type = '';
                    if ( $replace['real_nid'] ) {
                      switch ($replace['real_type']){
                        case 'normdoc':
                        $type = 'norm';
                        break;
                        case 'example':
                        $type = 'examples';
                        break;
                        case 'interview':
                        $type = 'interviews';
                        break;
                        case 'instruction':
                        $type = 'instructions';
                        break;
                        case 'new':
                        $type = 'news';
                        break;
                        case 'crib':
                        $type = 'cribs';
                        break;
                      }
                    }
                    if ( !empty($type) ) {
                      $html = str_replace($replace['pattern'] . $nid, 'href="/#' . $type . '/general/' . $replace['real_nid'], $html);
                      //$changes_made = true;
                    } else {
                      $nodes_with_lost_links[] = $node_id;
                    }
                  }

                  if ( $changes_made ) {
                    $node->get('field_new_html')->setValue($html);
                  }

                }

              }

              break;

              case 'normdoc':
              if ( !empty($node->field_documents) ) {


                foreach ( $node->field_documents as $key => $item) {
                  $item = $item->value;
                  $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($item);

                  if ( !empty($fc->field_document->getValue()[0]['value']) ) {
                    $html = $fc->field_document->getValue()[0]['value'];

                    if ( !empty($html) ) {

                      $links_to_replace = [];
                      $patterns = [
                        'href="/node/',
                        'href="http://online.kadrovik.ua/node/'
                      ];

                      foreach ($patterns as $pattern) {

                        $parts = explode($pattern, $html);

                        if ( count($parts) > 1 ) {
                          array_shift($parts);
                          foreach ($parts as $part) {
                            $link_parts = explode('"', $part);
                            if ( !empty($link_parts) ) {
                              $links_to_replace[$link_parts[0]] = [ 'pattern' => $pattern, 'real_nid' => 0, 'real_type' => '' ];
                            }
                          }
                        }

                      }

                      if ( count($links_to_replace) ) {

                        foreach ($links_to_replace as $nid => $replace) {

                          $nodes = \Drupal::entityTypeManager()
                          ->getStorage('node')
                          ->loadByProperties(['field_import_id' => $system_id . '-' . $nid]);

                          $real_node = reset($nodes);
                          if ( !empty($real_node) ) {
                            $links_to_replace[$nid]['real_nid'] = $real_node->Id();
                            $links_to_replace[$nid]['real_type'] = $real_node->getType();
                          }
                        }

                        foreach ($links_to_replace as $nid => $replace) {
                          $type = '';
                          if ( $replace['real_nid'] ) {
                            switch ($replace['real_type']){
                              case 'normdoc':
                              $type = 'norm';
                              break;
                              case 'example':
                              $type = 'examples';
                              break;
                              case 'interview':
                              $type = 'interviews';
                              break;
                              case 'instruction':
                              $type = 'instructions';
                              break;
                              case 'new':
                              $type = 'news';
                              break;
                              case 'crib':
                              $type = 'cribs';
                              break;
                            }
                          }
                          if ( !empty($type) ) {
                            $html = str_replace($replace['pattern'] . $nid, 'href="/#' . $type . '/general/' . $replace['real_nid'], $html);
                            //$changes_made = true;
                          } else {
                            $nodes_with_lost_links[] = $node_id;
                          }
                        }

                        if ( $changes_made ) {
                          $fc->field_document->setValue($html);
                          try {
                            $fc->save(true);
                          }
                          catch (Exception $e) {
                            \Drupal::logger('links rebuild')->error('Error while saving a node: ' . $node->Id());
                          }
                        }

                      }

                    }

                  }

                }

              }

              break;

            }

            if ( $changes_made ) {
              $rebuilt++;
              //save
              $node->save();
              konsultant_restore_changed_date($changed_date, $node_id);
            }

          }

          $processed++;
          $progress->advance();
        }

        $progress->finish();
        $output->writeln('');
      }

    }
    $nodes_with_lost_links = array_unique($nodes_with_lost_links);
    foreach($nodes_with_lost_links as $node_link) {
      $this->getIo()->info($node_link);
    }

    $this->getIo()->info('processed: ' . $processed);
    $this->getIo()->info('rebuilt: ' . $rebuilt);

  }

}
