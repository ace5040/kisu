<?php

namespace Drupal\konsultant\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;

/**
 * Class ConvertEmptyCommand.
 *
 * @DrupalCommand (
 *     extension="konsultant",
 *     extensionType="module"
 * )
 */
class ConvertEmptyCommand extends ContainerAwareCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('convertempty')
      ->setDescription('Convert not converted empty docs');

  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {

    $processed = 0;
    $converted = 0;

    $types = ['example'];

    $systems = [];

    $vid = 'system';

    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);

    foreach ($terms as $term) {
     $systems[] = $term->tid;
    }

    //$systems[] = 3;

    foreach ($systems as $system_id) {

      foreach ($types as $type) {

        $query = \Drupal::entityQuery('node');
        $query->condition('field_systems', $system_id, 'IN');
        $query->condition('type', $type);
        $query->sort('nid', 'ASC');

        $entity_ids = $query->execute();
        $entity_ids = array_values($entity_ids);

        $progress = new ProgressBar($output, count($entity_ids));
        $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% processing ' . $type . ' in ' . $system_id . ' : %message%');
        $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm');


        foreach ($entity_ids as $node_id) {
          $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm - ' . $node_id);
          $node = Node::load($node_id);

          //apply actions
          $node_type = $node->getType();

          $changes_made = false;


          switch ( $node_type ) {

            case 'example':

              $html = '';

              if ( !empty($node->get('field_example_html')->getValue()) ) {
                $html = $node->get('field_example_html')->getValue()[0]['value'];
              }

              if ( empty($html) ) {

                $file = false;

                if ( !empty($node->field_example_file->getValue()) ) {
                  $file = $node->field_example_file->getValue()[0]['target_id'];
                }

                if ( !empty($file) ) {
                  $data = \Drupal\konsultant\SystemRequestDispatcher::getConvertedHtml((object)['fid' => $file]);

                  if ( !empty($data->html) ) {
                    $node->field_example_html->setValue($data->html);
                    $changes_made = true;
                  }

                }

              }

            break;

          }

          if ( $changes_made ) {
            $converted++;
            //save
            $changed_date = $node->changed->value;
            $node->save();
            konsultant_restore_changed_date($changed_date, $node_id);
          }

          $processed++;
          $progress->advance();
        }

        $progress->finish();
        $output->writeln('');
      }

    }

    $this->getIo()->info('processed: ' . $processed);
    $this->getIo()->info('converted: ' . $converted);

  }

}
