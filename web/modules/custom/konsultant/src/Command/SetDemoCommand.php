<?php

namespace Drupal\konsultant\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;

/**
 * Class SetDemoCommand.
 *
 * @DrupalCommand (
 *     extension="konsultant",
 *     extensionType="module"
 * )
 */
class SetDemoCommand extends ContainerAwareCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('demo:set')
      ->setDescription('Marks every N materials as demo')
      ->addArgument(
         'n',
         InputArgument::OPTIONAL,
         'number for every N node (default n = 5)'
      );

  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {

    $n_arg = $input->getArgument('n');
    $n = 5;

    if ( !empty($n_arg) ) {
      $n_parts = explode('n=', $n_arg);
      if ( count($n_parts) == 2 ) {
        $n = intval($n_parts[1]);
      }
    }

    $this->getIo()->info('Starting batch...');

    $parsed = 0;
    $marked = 0;
    $unmarked = 0;

    $demo_types = ['consultation', 'normdoc', 'example', 'accounting', 'instruction', 'new', 'interview', 'crib'];

    $systems = [];

    $vid = 'system';

    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);

    foreach ($terms as $term) {
     $systems[] = $term->tid;
    }

    $systems = [3];

    foreach ($systems as $system_id) {

      foreach ($demo_types as $demo_type) {

        $query = \Drupal::entityQuery('node');
        $query->condition('field_systems', $system_id, 'IN');
        $query->condition('type', $demo_type);
        $query->sort('nid', 'ASC');

        $entity_ids = $query->execute();
        $entity_ids = array_values($entity_ids);

        $progress = new ProgressBar($output, count($entity_ids));
        $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% processing ' . $demo_type . ' in ' . $system_id . ' : %message%');
        $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm');

        $counter = 0;
        foreach ($entity_ids as $node_id) {

          $setDemo = ($counter % $n == 0) ? 1 : 0;
          $counter++;

          $changed = false;

          $connection = Database::getConnection();
          $q = $connection->select('node_field_data', 'nd');
          $q->leftJoin('node__field_demo', 'fd', 'fd.entity_id=nd.nid');
          $q->condition('nd.nid', $node_id);
          $q->addField('nd', 'nid', 'nid');
          $q->addField('nd', 'title', 'title');
          $q->addField('nd', 'changed', 'changed');
          $q->addField('fd', 'field_demo_value', 'demo');
          $q = $q->execute();
          $q = $q->fetch(\PDO::FETCH_OBJ);

          $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm - ' . $node_id);

          if ( !empty($q) ) {

            $demo = 0;

            if ( !empty($q->demo) ) {
              $demo = $q->demo;
            }

            if ( $demo != $setDemo )  {
              $changed = true;
            }

            if ( $changed ) {

              $changed_date = $q->changed;

              $node = \Drupal\node\Entity\Node::load($node_id);
              $node->set('field_demo', $setDemo);
              $node->save();

              konsultant_restore_changed_date($changed_date, $node_id);

              if ( $setDemo ) {
                $marked++;
              } else {
                $unmarked++;
              }
            }

          }

          $parsed++;
          $progress->advance();
        }

        $progress->finish();
        $output->writeln('');
      }

    }

    $this->getIo()->info('parsed: ' . $parsed);
    $this->getIo()->info('marked: ' . $marked);
    $this->getIo()->info('unmarked: ' . $unmarked);

    $this->getIo()->info('Batch done!');

  }

}
