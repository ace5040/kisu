<?php

namespace Drupal\konsultant\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;

/**
 * Class SetPlacedCommand.
 *
 * @DrupalCommand (
 *     extension="konsultant",
 *     extensionType="module"
 * )
 */
class SetPlacedCommand extends ContainerAwareCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('placed:set')
      ->setDescription('Set placed date field');

  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {

    $processed = 0;

    $recompute_types = ['consultation'];

    $vid = 'system';

    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);

    $systems = [];

    foreach ($terms as $term) {
     $systems[] = $term->tid;
    }

    $systems = [1,2,3];

    foreach ($systems as $system_id) {

      foreach ($recompute_types as $recompute_type) {

        $query = \Drupal::entityQuery('node');
        $query->condition('field_systems', $system_id, 'IN');
        $query->condition('field_status', ['placed', 'viewed'], 'IN');
        $query->condition('type', $recompute_type);
        $query->sort('nid', 'ASC');

        $entity_ids = $query->execute();
        $entity_ids = array_values($entity_ids);

        $progress = new ProgressBar($output, count($entity_ids));
        $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% processing ' . $recompute_type . ' in ' . $system_id . ' : %message%');
        $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm');


        foreach ($entity_ids as $node_id) {
          $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm - ' . $node_id);
          $node = Node::load($node_id);
          $changed_date = $node->changed->value;

          if ( !empty($node->field_date_placed->getValue()[0]['value']) ) {

            $date = $node->field_date_placed->getValue()[0]['value'];

            if ( substr($node->field_date_placed->getValue()[0]['value'], 0, 1) === 2 ) {
              continue;
              $processed++;
              $progress->advance();
            }

          }

          $node->get('field_date_placed')->setValue(date('Y-m-d', $changed_date));
          $node->save();
          konsultant_restore_changed_date($changed_date, $node_id);
          $processed++;
          $progress->advance();
        }

        $progress->finish();
        $output->writeln('');
      }

    }

    $this->getIo()->info('processed: ' . $processed);

  }

}
