<?php

namespace Drupal\konsultant\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;

/**
 * Class InterviewsAuthorCommand.
 *
 * @DrupalCommand (
 *     extension="konsultant",
 *     extensionType="module"
 * )
 */
class InterviewsAuthorCommand extends ContainerAwareCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('interviews:author')
      ->setDescription('Set articles author');

  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {

    $processed = 0;
    $rebuilt = 0;

    $recompute_types = ['interview'];

    $systems = [];

    $vid = 'system';

    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);

    foreach ($terms as $term) {
     $systems[] = $term->tid;
    }

    $systems = [3];

    foreach ($systems as $system_id) {

      foreach ($recompute_types as $recompute_type) {

        $query = \Drupal::entityQuery('node');
        $query->condition('field_systems', $system_id, 'IN');
        $query->condition('type', $recompute_type);
        $query->sort('nid', 'ASC');

        $entity_ids = $query->execute();
        $entity_ids = array_values($entity_ids);

        $progress = new ProgressBar($output, count($entity_ids));
        $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% processing ' . $recompute_type . ' in ' . $system_id . ' : %message%');
        $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm');

        foreach ($entity_ids as $node_id) {
          $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm - ' . $node_id);
          $node = Node::load($node_id);
          $changed_date = $node->changed->value;

          //apply actions
          $node_type = $node->getType();

          $changes_made = false;

          //if ( $processed > 1800 ) {
          if ( true ) {

            switch ( $node_type ) {

              case 'interview':

                $html = '';

                if ( !empty($node->get('field_interview_html')->getValue()) ) {
                  $html = $node->get('field_interview_html')->getValue()[0]['value'];
                }

                $authors = [

                  [
                    'patterns' => [
                      '<p style="text-align: right;"><strong><em>Анна Мілієнко-Самсонова</em></strong><em>, головний редактор Єдиної електронної системи «КАДРОВИК-онлайн»</em></p>',
                      '<p style="text-align: right;"><em><strong>Анна Мілієнко-Самсонова</strong></em><em>, головний редактор Єдиної електронної системи «КАДРОВИК-онлайн»</em></p>',
                      '<p align="right"><strong><em>Анна Мілієнко-Самсонова</em></strong><em>, головний редактор Єдиної електронної системи «КАДРОВИК-онлайн»</em></p>',
                      '<p style="text-align: right;"><strong><em>Анна Мілієнко-Самсонова</em></strong><em>, головний редактор та експерт Єдиної електронної системи «КАДРОВИК-онлайн»</em>&nbsp;</p>',
                      '<p style="text-align: right;"><strong><em>Анна Мілієнко-Самсонова</em></strong><em>, головний редактор та експерт Єдиної електронної системи &laquo;КАДРОВИК-онлайн&raquo;</em>&nbsp;</p>',
                      '<p style="text-align: right;"><em><strong>Анна Мілієнко-Самсонова</strong></em><em>, головний редактор Єдиної електронної системи &laquo;КАДРОВИК-онлайн&raquo;</em></p>'
                    ],
                    'uid' => 567
                  ],

                  [
                    'patterns' => [
                      '<p style="text-align: right;"><strong><em>Олександр Клименко, </em></strong><em>старший науковий співробітник відділу наукового забезпечення питань економіки праці та розвитку професійно-класифікаційної сфери ДУ НДІ соціально-трудових відносин Мінсоцполітики України</em></p>',
                      '<p align="right"><strong><em>Олександр Клименко, </em></strong><em>старший науковий співробітник відділу наукового забезпечення питань економіки праці та розвитку професійно-класифікаційної сфери ДУ НДІ соціально-трудових відносин Мінсоцполітики України</em></p>',
                    ],
                    'uid' => 1139
                  ],

                  [
                    'patterns' => [
                      '<p style="text-align: right;><em><strong>Вікторія </strong></em><strong><em>Ліпчанська</em></strong><em><strong>,</strong> шеф-редактор кадрового напрямку ВД «МЕДІА-ПРО», юрист за фахом</em></p>',
                      '<p style="text-align: right;><em><strong>Вікторія </strong></em><strong><em>Ліпчанська</em></strong><em><strong>,</strong> шеф-редактор кадрового напрямку ВД &laquo;МЕДІА-ПРО&raquo;, юрист за фахом</em></p>',
                      '<p align="right"><em><strong>Вікторія </strong></em><strong><em>Ліпчанська</em></strong><em><strong>,</strong> шеф-редактор кадрового напрямку ВД «МЕДІА-ПРО», юрист за фахом</em></p>',
                      '<p align="right"><em><strong>Вікторія </strong></em><strong><em>Ліпчанська</em></strong><em><strong>,</strong> шеф-редактор кадрового напрямку ВД &laquo;МЕДІА-ПРО&raquo;, юрист за фахом</em></p>',
                    ],
                    'uid' => 569
                  ],

                  [
                    'patterns' => [
                      '<p style="margin-left: 14.2pt; text-align: right;"><strong><em>Васильєв Юрій Борисович, </em></strong><em>начальник відділу кадрів Фізико-технічного інституту низьких температур ім. Б. І. Вєркіна Національної академії наук України, полковник запасу</em></p>',
                      '<p align="right"><strong><em>Васильєв Юрій Борисович, </em></strong><em>начальник відділу кадрів Фізико-технічного інституту низьких температур ім. Б. І. Вєркіна Національної академії наук України, полковник запасу</em></p>',
                    ],
                    'uid' => 1149
                  ],

                  [
                    'patterns' => [
                      '<p style="text-align: right;"><em><strong>Сергій Кравцов</strong></em><em>,</em> <em>завідувач відділу з наукового забезпечення питань зайнятості, соціального діалогу та стандартів праці і зайнятості населення Мінсоцполітики і НАН України, канд. соціол. наук</em></p>',
                      '<p align="right"><em><strong>Сергій Кравцов</strong></em><em>,</em> <em>завідувач відділу з наукового забезпечення питань зайнятості, соціального діалогу та стандартів праці і зайнятості населення Мінсоцполітики і НАН України, канд. соціол. наук</em></p>',
                    ],
                    'uid' => 1138
                  ],

                  [
                    'patterns' => [
                      '<p style="text-align: right;"><strong><em>Микола </em></strong><strong><em>Постернак</em></strong><strong><em>, </em></strong><em>експерт з питань соціального страхування</em>&nbsp;</p>',
                      '<p align="right"><strong><em>Микола </em></strong><strong><em>Постернак</em></strong><strong><em>, </em></strong><em>експерт з питань соціального страхування</em>&nbsp;</p>',
                    ],
                    'uid' => 144
                  ],

                  [
                    'patterns' => [
                      '<p align="right"><em><strong>В’ячеслав </strong></em><strong><em>Коломійчук</em></strong><em><strong>,</strong> помічник адвоката, юрист, Адвокатське бюро «Штокалов та Партнери»</em></p>',
                      '<p align="right"><em><strong>В’ячеслав </strong></em><strong><em>Коломійчук</em></strong><em><strong>,</strong> помічник адвоката, юрист, Адвокатське бюро &laquo;Штокалов та Партнери&raquo;</em></p>',
                      '<p align="right"><em><strong>В&rsquo;ячеслав </strong></em><strong><em>Коломійчук</em></strong><em><strong>,</strong> помічник адвоката, юрист, Адвокатське бюро &laquo;Штокалов та Партнери&raquo;</em></p>',
                    ],
                    'uid' => 1147
                  ],

                ];

                $new_uid = 0;

                foreach ($authors as $author) {
                  $patterns = $author['patterns'];
                  $uid = $author['uid'];

                  foreach ($patterns as $pattern) {

                    if ( mb_strpos($html, $pattern) !== false ) {
                      $html = str_replace($pattern, '', $html);
                      $new_uid = $uid;
                      $changes_made = true;

                    }

                  }

                }

                if ( $changes_made ) {
                  $node->get('field_expert')->setValue($new_uid);
                  $node->get('field_interview_html')->setValue($html);
                }

              break;

            }

            if ( $changes_made ) {
              $rebuilt++;
              //save
              $node->save();
              konsultant_restore_changed_date($changed_date, $node_id);
            }

          }

          $processed++;
          $progress->advance();
        }

        $progress->finish();
        $output->writeln('');
      }

    }

    $this->getIo()->info('processed: ' . $processed);
    $this->getIo()->info('rebuilt: ' . $rebuilt);

  }

}
