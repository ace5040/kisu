<?php

namespace Drupal\konsultant\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;
use Drupal\konsultant\SystemRequestDispatcher;

/**
 * Class ConvertLinksCommand.
 *
 * @DrupalCommand (
 *     extension="konsultant",
 *     extensionType="module"
 * )
 */
class ConvertLinksCommand extends ContainerAwareCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('links:convert')
      ->setDescription('links convert hash to regular links');
  }

  /**
   * {@inheritdoc}
   */

  protected static function convertLinks ($html) {

    $replaces = [
      ' href="#examples',
      ' href="#interviews',
      ' href="#consultations',
      ' href="#news',
      ' href="#norm',
      ' href="#cribs',
      ' href="#search',

      ' href="/#examples',
      ' href="/#interviews',
      ' href="/#consultations',
      ' href="/#news',
      ' href="/#norm',
      ' href="/#cribs',
      ' href="/#search',

      ' href="https://k.isu.net.ua/#examples',
      ' href="https://k.isu.net.ua/#interviews',
      ' href="https://k.isu.net.ua/#consultations',
      ' href="https://k.isu.net.ua/#news',
      ' href="https://k.isu.net.ua/#norm',
      ' href="https://k.isu.net.ua/#cribs',
      ' href="https://k.isu.net.ua/#search',

      ' href="https://k.isu.net.ua#examples',
      ' href="https://k.isu.net.ua#interviews',
      ' href="https://k.isu.net.ua#consultations',
      ' href="https://k.isu.net.ua#news',
      ' href="https://k.isu.net.ua#norm',
      ' href="https://k.isu.net.ua#cribs',
      ' href="https://k.isu.net.ua#search'

    ];
    foreach ($replaces as $replace_begin) {

      $code_parts_1 = explode($replace_begin, $html);
      if ( count($code_parts_1) > 1 ) {
        unset($code_parts_1[0]);
        foreach ($code_parts_1 as $code_part_1) {
          $code_parts_2 = explode('"', $code_part_1);
          if ( count($code_parts_2) > 1 ) {
            $old_url = $replace_begin . $code_parts_2[0];
            $new_url = '';
            $code_parts_3 = explode('/general/', $code_parts_2[0]);
            if ( count($code_parts_3) == 2 ) {
              $link_nid = $code_parts_3[1];
              if ( $replace_begin == ' href="#norm' || $replace_begin == ' href="/#norm' || $replace_begin == ' href="https://k.isu.net.ua/#norm' || $replace_begin == ' href="https://k.isu.net.ua#norm' ) {
                $params = (object)[
                  'id' => $link_nid
                ];

                $data = SystemRequestDispatcher::sendApiCommand('getNorm', $params);
                if ( !empty($data->url) ) {
                  $new_url = $data->url;
                }

              } else {
                $new_url = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $link_nid);
              }
              if ( !empty($new_url) ) {
                $replace = ' href="' . $new_url;
                $html = str_replace($old_url, $replace, $html);
              }
            }
          }
        }
      }

    }

    return $html;

  }

  protected function execute(InputInterface $input, OutputInterface $output) {

    $this->getIo()->info('Starting batch...');

    $generated = 0;

    $connection = Database::getConnection();

    $sth = $connection->select('node__field_interview_html', 'fd');
    $sth->addField('fd', 'entity_id', 'entity_id');
    $data = $sth->execute();
    $results = $data->fetchAll();

    $progress = new ProgressBar($output, count($results));
    $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% processing: %message%');
    $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm');


    foreach ($results as $doc) {
      $sth2 = $connection->select('node__field_interview_html', 'fd');
      $sth2->condition('fd.entity_id', $doc->entity_id);
      $sth2->addField('fd', 'field_interview_html_value', 'html');
      $data2 = $sth2->execute();
      $docData = $data2->fetchObject();

      $code = self::convertLinks($docData->html);

      if ( $code !== $docData->html ) {
        $sth3 = $connection->update('node__field_interview_html')
          ->fields(
            [
              'field_interview_html_value' => $code
            ]
          )
          ->condition('entity_id', $doc->entity_id)
          ->execute();
          $generated++;
      }
      $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm - ' . $doc->entity_id . ' ( ' . $generated . ' )');
      $progress->advance();
    }

    $progress->finish();
    $output->writeln('');

    $sth = $connection->select('node__field_answer', 'fd');
    $sth->addField('fd', 'entity_id', 'entity_id');
    $data = $sth->execute();
    $results = $data->fetchAll();

    $progress = new ProgressBar($output, count($results));
    $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% processing: %message%');
    $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm');


    foreach ($results as $doc) {
      $sth2 = $connection->select('node__field_answer', 'fd');
      $sth2->condition('fd.entity_id', $doc->entity_id);
      $sth2->addField('fd', 'field_answer_value', 'html');
      $data2 = $sth2->execute();
      $docData = $data2->fetchObject();

      $code = self::convertLinks($docData->html);

      if ( $code !== $docData->html ) {
        $sth3 = $connection->update('node__field_answer')
          ->fields(
            [
              'field_answer_value' => $code
            ]
          )
          ->condition('entity_id', $doc->entity_id)
          ->execute();
          $generated++;
      }
      $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm - ' . $doc->entity_id . ' ( ' . $generated . ' )');
      $progress->advance();
    }

    $progress->finish();
    $output->writeln('');

    $sth = $connection->select('node__field_new_html', 'fd');
    $sth->addField('fd', 'entity_id', 'entity_id');
    $data = $sth->execute();
    $results = $data->fetchAll();

    $progress = new ProgressBar($output, count($results));
    $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% processing: %message%');
    $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm');


    foreach ($results as $doc) {
      $sth2 = $connection->select('node__field_new_html', 'fd');
      $sth2->condition('fd.entity_id', $doc->entity_id);
      $sth2->addField('fd', 'field_new_html_value', 'html');
      $data2 = $sth2->execute();
      $docData = $data2->fetchObject();

      $code = self::convertLinks($docData->html);

      if ( $code !== $docData->html ) {
        $sth3 = $connection->update('node__field_new_html')
          ->fields(
            [
              'field_new_html_value' => $code
            ]
          )
          ->condition('entity_id', $doc->entity_id)
          ->execute();
          $generated++;
      }
      $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm - ' . $doc->entity_id . ' ( ' . $generated . ' )');
      $progress->advance();
    }

    $progress->finish();
    $output->writeln('');

    $this->getIo()->info('generated: ' . $generated);

    $this->getIo()->info('Batch done!');

  }

}
