<?php

namespace Drupal\konsultant\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;

/**
 * Class ConvertExamplesCommand.
 *
 * @DrupalCommand (
 *     extension="konsultant",
 *     extensionType="module"
 * )
 */
class ConvertExamplesCommand extends ContainerAwareCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('convert:examples')
      ->setDescription('Convert examples to instructions');

  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {

    $processed = 0;
    $rebuilt = 0;

    $recompute_types = ['example'];

    $systems = [];

    $vid = 'system';

    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);

    foreach ($terms as $term) {
     $systems[] = $term->tid;
    }

    $systems = [3];

    foreach ($systems as $system_id) {

      foreach ($recompute_types as $recompute_type) {

        $query = \Drupal::entityQuery('node');
        $query->condition('field_systems', $system_id, 'IN');
        $query->condition('type', $recompute_type);
        $query->condition('field_example_type', 633);
        $query->sort('nid', 'ASC');

        $entity_ids = $query->execute();
        $entity_ids = array_values($entity_ids);

        $progress = new ProgressBar($output, count($entity_ids));
        $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% processing ' . $recompute_type . ' in ' . $system_id . ' : %message%');
        $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm');

        foreach ($entity_ids as $node_id) {
          $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm - ' . $node_id);
          $node = Node::load($node_id);
          $created_date = $node->created->value;
          $changed_date = $node->changed->value;

          $field_instruction_html = '';
          if ( !empty($node->get('field_example_html')->getValue()) ) {
            $field_instruction_html = $node->get('field_example_html')->getValue()[0]['value'];
          }

          $field_views = 0;
          if ( !empty($node->get('field_views')->getValue()) ) {
            $field_views = $node->get('field_views')->getValue()[0]['value'];
          }

          $field_sort_date = 0;
          if ( !empty($node->get('field_sort_date')->getValue()) ) {
            $field_sort_date = $node->get('field_sort_date')->getValue()[0]['value'];
          }

          $field_demo = 0;
          if ( !empty($node->get('field_demo')->getValue()) ) {
            $field_demo = $node->get('field_demo')->getValue()[0]['value'];
          }

          $field_hot = 0;
          if ( !empty($node->get('field_hot')->getValue()) ) {
            $field_hot = $node->get('field_hot')->getValue()[0]['value'];
          }

          $field_import_id = 0;
          if ( !empty($node->get('field_import_id')->getValue()) ) {
            $field_import_id = $node->get('field_import_id')->getValue()[0]['value'];
          }

          $field_instruction_file = 0;
          if ( !empty($node->get('field_example_file')->getValue()) ) {
            $field_instruction_file = $node->get('field_example_file')->getValue()[0]['target_id'];
          }

          $field_filter = [];
          if ( !empty($node->get('field_filter')->getValue()) ) {
            $values = $node->get('field_filter')->getValue();
            foreach ($values as $value) {
              $field_filter[] = $value['target_id'];
            }
          }

          $new_node = Node::create([
            'type' => 'instruction',
            'uid' => $node->getOwnerId(),
            'created' => $created_date,
            'title' => $node->getTitle(),
            'field_views' => $field_views,
            'field_sort_date' => $field_sort_date,
            'field_demo' => $field_demo,
            'field_instruction_html' => $field_instruction_html,
            'field_hot' => $field_hot,
            'field_systems' => [3],
            'field_filter' => $field_filter,
            'field_import_id' => $field_import_id,
            'field_instruction_file' => $field_instruction_file,
            'status' => 1
          ]);

          $rebuilt++;
          $node->delete();
          $new_node->save();
          konsultant_restore_changed_date($changed_date, $new_node->Id());

          $processed++;
          $progress->advance();
        }

        $progress->finish();
        $output->writeln('');
      }

    }

    $this->getIo()->info('processed: ' . $processed);
    $this->getIo()->info('deleted: ' . $rebuilt);

  }

}
