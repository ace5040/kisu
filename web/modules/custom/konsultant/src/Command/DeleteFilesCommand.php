<?php

namespace Drupal\konsultant\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;

/**
 * Class DeleteFilesCommand.
 *
 * @DrupalCommand (
 *     extension="konsultant",
 *     extensionType="module"
 * )
 */
class DeleteFilesCommand extends ContainerAwareCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('delete:files')
      ->setDescription('DELETE UNUSED FILES');
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {

    $this->getIo()->info('Starting batch...');

    $deleted = 0;

    $connection = Database::getConnection();
    $sth = $connection->select('file_managed', 'f');
    $sth->condition('f.status', '0');
    $sth->addField('f', 'fid', 'fid');
    $data = $sth->execute();
    $results = $data->fetchAll();

    $progress = new ProgressBar($output, count($results));
    $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% processing: %message%');
    $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm');

    foreach ($results as $fid) {

      $file = \Drupal\file\Entity\File::load($fid->fid);
      $file->delete();

      $deleted++;
      $progress->setMessage(round(memory_get_usage(true)/1024/1024) . 'm - ' . $fid->fid);
      $progress->advance();
    }

    $progress->finish();
    $output->writeln('');

    $this->getIo()->info('deleted: ' . $deleted);

    $this->getIo()->info('Batch done!');

  }

}
