<?php

/**
 * @file
 * Contains \Drupal\konsultant\Routing\RouteSubscriber.
 */

namespace Drupal\konsultant\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    foreach ($collection as $route_name => $route ) {

      if ( strpos($route->getPath(),'/user') !== false ) {
        //echo $route->getPath() . ' - ' . $route_name . "\n";
      }

    }

    $route = $collection->get('entity.user.edit_form');
    $route->setRequirement('_custom_access', '\Drupal\konsultant\Controller\KonsultantController::userEditAccess');
    //$route = $collection->get('user.page');
    //$route->setRequirement('_custom_access', '\Drupal\konsultant\Controller\KonsultantController::userViewAccess');
    $route = $collection->get('entity.user.canonical');
    $route->setRequirement('_custom_access', '\Drupal\konsultant\Controller\KonsultantController::userViewAccess');

    $route = $collection->get('shs.term_data');
    $route->setDefault('_controller', '\Drupal\konsultant\Controller\KonsultantShsController::getTermData');

  }

}
