<?php

namespace Drupal\konsultant\Commands;

use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Component\Utility\Html;
use Drupal\konsultant\SystemRequestDispatcher;
use Drupal\Core\Database\Database;

/**
 * A drush command file.
 *
 * @package Drupal\konsultant\Commands
 */
class Commands extends DrushCommands {

  /**
   * FindLinks command.
   *
   * @command k:findLinks
   * @usage k:findLinks
   */

  public function findLinks() {

    try {
      $query = \Drupal::entityQuery('node');
      $query->condition('type', ['consultation', 'interview', 'crib', 'example', 'accountinglog', 'instruction', 'new'], 'IN');
      $query->sort('nid', 'ASC');
      $entity_ids = $query->execute();
      $entity_ids = array_values($entity_ids);
    } catch (\Exception $e) {
      $this->output()->writeln($e);
    }

    $operations = [];
    $numOperations = 0;

    if (!empty($entity_ids)) {

      $count = count($entity_ids);
      $chunk_size = 50;
      $chunks_count = ceil($count / $chunk_size) ;

      for ($chunk = 0; $chunk < $chunks_count; $chunk++ ) {
        $ids = array_slice($entity_ids, $chunk*$chunk_size, $chunk_size);
        $operations[] = [
          '\Drupal\konsultant\BatchService::processFindLinks',
          [
            $chunk+1,
            $ids,
            t('Processing chunk @chunk of @chunks', ['@chunk' => $chunk+1, '@chunks' => $chunks_count]),
          ],
        ];
        $numOperations++;

      }

      foreach ($entity_ids as $node_id) {


      }

    } else {
      $this->output()->writeln("No nodes found");
    }

    $this->output()->writeln("Preparing batches: " . $chunks_count);

    // 4. Create the batch.
    $batch = [
      'title' => t('Processed @num node(s)', ['@num' => $chunks_count]),
      'operations' => $operations,
      'finished' => '\Drupal\konsultant\BatchService::processFindLinksFinished',
    ];

    // 5. Add batch operations as new batch sets.
    batch_set($batch);

    // 6. Process the batch sets.
    drush_backend_batch_process();

    // 6. Show some information.
    $this->output()->writeln("Batch operations end.");

  }


  /**
   * k:jobs-rebuild command.
   *
   * @command k:jobs-rebuild
   * @usage k:jobs-rebuild
   */

  public function jobsRebuild() {

    try {
      $query = \Drupal::entityQuery('node');
      $query->condition('type', ['job'], 'IN');
      $query->sort('nid', 'ASC');
      $entity_ids = $query->execute();
    } catch (\Exception $e) {
      $this->output()->writeln($e);
    }

    if ( !empty($entity_ids) ) {
      $chunkSize = 50;
      $storage = \Drupal::entityTypeManager()->getStorage('node');
      $chunks = array_chunk($entity_ids, $chunkSize);
      $jobsCount = count($entity_ids);
      $counter = 0;
      foreach ($chunks as $chunk) {
        $nodes = $storage->loadMultiple($chunk);
        $counter += count($chunk);
        $storage->delete($nodes);
        $this->output()->writeln('Deleted ' . $counter .  '/' . $jobsCount . ' jobs');
      }
    }

    $norm = SystemRequestDispatcher::sendApiCommand('getNorm', (object)['id' => 31706, 'revision' => 0]);

    if (!empty($norm->html)) {
      $doc = new \DOMDocument();
      libxml_use_internal_errors(true);
      $doc->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=utf-8">' . $norm->html);
      $body = $doc->getElementsByTagName('body');
      $jobs = $this->findJobs($body);
      array_shift($jobs);
    }

    $operations = [];
    $numOperations = 0;

    if (!empty($jobs)) {

      $count = count($jobs);
      $chunk_size = 10;
      $chunks_count = ceil($count / $chunk_size) ;

      for ($chunk = 0; $chunk < $chunks_count; $chunk++ ) {
        $jobs_chunk = array_slice($jobs, $chunk*$chunk_size, $chunk_size);
        $operations[] = [
          '\Drupal\konsultant\BatchService::processJobsRebuild',
          [
            $chunk+1,
            $jobs_chunk,
            t('Processing chunk @chunk of @chunks', ['@chunk' => $chunk+1, '@chunks' => $chunks_count]),
          ],
        ];
        $numOperations++;

      }

    } else {
      $this->output()->writeln("No jobs found");
    }

    $this->output()->writeln("Preparing batches: " . $chunks_count);

    // 4. Create the batch.
    $batch = [
      'title' => t('Processed @num node(s)', ['@num' => $chunks_count]),
      'operations' => $operations,
      'finished' => '\Drupal\konsultant\BatchService::processJobsRebuildFinished',
    ];

    // 5. Add batch operations as new batch sets.
    batch_set($batch);

    // 6. Process the batch sets.
    drush_backend_batch_process();

    // 6. Show some information.
    $this->output()->writeln("Batch operations end.");


  }

  private function findJobs($elements) {
    $jobs = [];
    foreach ( $elements as $element ) {
      if ( $element->nodeName == 'tbody' ) {
        if ( count($element->childNodes) > 100 ) {
          foreach ( $element->childNodes as $tableNode ) {
            if ( $tableNode->nodeName == 'tr' && count($tableNode->childNodes) == 11 ) {
              $jobs[] = (object)[
                'field_kod_kp' => $this->cleanupString($tableNode->childNodes[1]->textContent),
                'field_kod_zkpptr' => $this->cleanupString($tableNode->childNodes[3]->textContent),
                'field_vypusk_yetkd' => $this->cleanupString($tableNode->childNodes[5]->textContent),
                'field_vypusk_dkkhp' => $this->cleanupString($tableNode->childNodes[7]->textContent),
                'field_job_name' => $this->cleanupString($tableNode->childNodes[9]->textContent)
              ];
            }
          }
        }
      }
      if ( !empty($element->childNodes) ) {
        $jobs = array_merge($jobs, $this->findJobs($element->childNodes));
      }
    }
    return $jobs;

  }
  private function cleanupString($o) {
    $o = trim($o);
    //$o = trim($o);
    //if ( strpos($o,'спубліки Крим') !== false ) {
      $o = str_replace([mb_chr(160)], '', $o);
      //echo($o);
      //foreach(mb_str_split($o) as $char){
        //echo(mb_ord($char) . ' (' . $char . ') ');
      //}
      //die();
    //}
    return $o == '-' ? '' : $o;
  }

}