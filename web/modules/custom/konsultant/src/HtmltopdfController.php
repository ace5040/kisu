<?php
/**
 * @file
 * Contains \Drupal\konsultant\HtmltopdfController.
 */

namespace Drupal\konsultant;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Site\Settings;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\entityQuery;

class HtmltopdfController extends ControllerBase {

  public static function notfound(Request $request) {
    $url = Url::fromRoute('<front>');
    $response = new RedirectResponse($url->toString());
    return $response;
  }

  public static function generate(Request $request) {
    $system = get_current_system();
    $system_type = $system->id;
    $entity = $request->get('entity');
    $data = self::get_data($system_type, $entity);
    return self::get_response($system_type, $data);
  }

  public static function generate_norm(Request $request) {
    $system_type = 'norm';
    $entity = $request->get('entity');
    $data = self::get_data($system_type, $entity);
    return self::get_response($system_type, $data);
  }

  public static function norm_print(Request $request) {
    $system_type = 'norm';
    $entity = $request->get('entity');
    $data = self::get_data($system_type, $entity);
    $response = new Response(
      $data->html,
      Response::HTTP_OK,
      ['content-type' => 'text/html']
    );
    return $response;
  }

  public static function get_response($system_type, $data) {
    $response = new Response(
      'Service is temporary unavailable',
      Response::HTTP_OK,
      ['content-type' => 'text/html']
    );
    if ( !empty($data->html) ) {
      $file_path = self::get_pdf_file($system_type, $data->html, $data->title, $data->nid);
      $response = new RedirectResponse(file_create_url($file_path));
    }
    return $response;
  }

  public static function get_data($system_type, $entity) {
    $html = '';
    $title = '';
    $nid = '';
    if ( $system_type == 'norm' ) {
      $norm_doc = \Drupal\konsultant\SystemRequestDispatcher::sendApiCommand('getNorm', (object)['id' => $entity]);
      $title = $norm_doc->title;
      $nid = $norm_doc->id;
    } else {
      $title = $entity->getTitle();
      $nid = $entity->Id();
    }
    $twig = \Drupal::service('twig');
    $template = $twig->loadTemplate('themes/konsultant_v3/templates/printable.html.twig');
    $html = $template->render([]);
    return (object)[
      'nid' => $nid,
      'title' => $title,
      'html' => $html
    ];
  }

  public static function get_pdf_file($system_type, $html, $title, $nid) {
    $directory = 'public://pdf/';
    $file_name = substr(konsultant_transliteration_filename($title), 0, 32) . '-' . $system_type . '-' . $nid . '.pdf';
    $html = str_replace('text/javascript', 'text/disabled', $html);
    if ( !file_exists($directory . $file_name) ) {
      $pdf = self::convert($html);
      if ( !empty($pdf) ) {
        file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
        file_unmanaged_save_data($pdf, $directory . $file_name , FILE_EXISTS_REPLACE);
      }
    }
    return $directory . $file_name;
  }

  public static function convert($html) {
  	$htmltopdf_url = Settings::get('htmltopdf_url', '');
    $pdf = '';
  	if ( !empty($htmltopdf_url) ) {
  		$post = [
        'html' => $html,
        'options' => [
          'format' => 'A4'
        ]
      ];
      $ch = curl_init();
  		curl_setopt($ch, CURLOPT_URL, $htmltopdf_url);
  		curl_setopt($ch, CURLOPT_POST, 1);
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
      curl_setopt($ch, CURLOPT_HTTPHEADER,['Content-Type: application/json']);
  		$result=curl_exec($ch);
  		$getInfo = curl_getinfo($ch);
  		$status = 'unknown';
      $error = '';
  		if ($result === false) {
  			$status = "sending request error";
  			if ( curl_error($ch) ) {
      		$error = curl_error($ch);
  			}
  		} else if ( $getInfo['http_code'] != 200) {
  			$status = $getInfo['http_code'];
  			if ( curl_error($ch) ) {
      		$error = curl_error($ch);
  			}
  		} else {
        $result = json_decode($result);
        if ( !empty($result->status) && $result->status == 'ok' ) {
          $pdf = base64_decode($result->pdf);
        }
  		}
  		curl_close ($ch);
  	}
    return $pdf;
  }
}
