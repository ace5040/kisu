<?php

/**
 * @file
 * Contains \Drupal\konsultant\TwigExtension\KonsultantTwigExtension.
 */

namespace Drupal\konsultant\TwigExtension;

/**
 * Outputs linked Specification terms
 */

use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Cache\Cache;

class KonsultantTwigExtension extends \Twig_Extension {

  public function getName() {
    return 'konsultant.twig_extension';
  }

  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('menu_toggle', [$this, 'menu_toggle'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('page_title', [$this, 'page_title'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('file_exists', [$this, 'file_exists'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('get_var', [$this, 'get_var'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('get_current_params', [$this, 'get_current_params'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('colorize', [$this, 'colorize'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('hex_to_rgb', [$this, 'hex_to_rgb'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('current_system', [$this, 'current_system'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('current_user', [$this, 'current_user'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('theme_filters', [$this, 'theme_filters'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('doc_types', [$this, 'doc_types'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('example_types', [$this, 'example_types'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('new_types', [$this, 'new_types'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('doc_producers', [$this, 'doc_producers'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('doc_statuses', [$this, 'doc_statuses'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('print_content', [$this, 'print_content'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('get_item', [$this, 'get_item'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('get_print_node_type', [$this, 'get_print_node_type'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('emogrify_content', [$this, 'emogrify_content'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('home_block_new_services', [$this, 'home_block_new_services'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('home_block_consultations', [$this, 'home_block_consultations'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('home_block_news', [$this, 'home_block_news'], [
        'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('home_block_interviews', [$this, 'home_block_interviews'], [
        'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('home_block_norm', [$this, 'home_block_norm'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('home_block_examples', [$this, 'home_block_examples'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('home_block_accountinglogs', [$this, 'home_block_accountinglogs'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('home_block_cribs', [$this, 'home_block_cribs'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('home_block_instructions', [$this, 'home_block_instructions'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('home_block_hot', [$this, 'home_block_hot'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('get_template', [$this, 'get_template'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('get_image_path_pdf', [$this, 'get_image_path_pdf'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('cast_to_array', [$this, 'cast_to_array'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('theme_name', [$this, 'theme_name'], [
          'is_safe' => ['html'],
      ]),
      new \Twig_SimpleFunction('getConsultation', [$this, 'getConsultation'], [
          'is_safe' => ['html'],
      ]),

    ];
  }

  public static function cast_to_array($stdClassObject) {
    $response = [];
    foreach ($stdClassObject as $key => $value) {
      $response[] = $key;
    }
    return $response;
  }

  public static function theme_name() {
    $theme_name = 'konsultant_v3';
    return $theme_name;
  }

  public static function get_image_path_pdf($image_uri, $image_style) {
    return get_image_path_pdf($image_uri, $image_style);
  }

  public static function get_var($name) {

    $value = \Drupal::config('konsultant.settings')->get($name);
    return $value;

  }

  public static function get_current_params() {

    $value = \Drupal::request()->getQueryString();
    return $value;

  }

  public static function menu_toggle() {
    return !empty($_COOKIE["menu-toggle"]) && $_COOKIE["menu-toggle"] === 'true';
  }

  public static function page_title() {
    $system = get_current_system();
    $title = '';
    $route = \Drupal::routeMatch()->getRouteName();
    switch ($route) {
      case 'frontpage': $title = 'Електронна система ' . $system->html_name; break;
      case 'node-page-search': $title = "Результати Пошуку"; break;
      case 'node-page-consultations': $title = !empty($system->modules['consultations']) ? $system->modules['consultations']->name : ''; break;
      case 'node-page-norm': $title = !empty($system->modules['norm']) ? $system->modules['norm']->name : ''; break;
      case 'node-page-orientation':
      case 'node-page-orientation-search':
      case 'node-page-orientation-letter': $title = !empty($system->modules['orientation']) ? $system->modules['orientation']->name : ''; break;
      case 'node-page-examples': $title = !empty($system->modules['examples']) ? $system->modules['examples']->name : ''; break;
      case 'node-page-accountinglogs': $title = !empty($system->modules['accountinglogs']) ? $system->modules['accountinglogs']->name : ''; break;
      case 'node-page-instructions': $title = !empty($system->modules['instructions']) ? $system->modules['instructions']->name : ''; break;
      case 'node-page-news': $title = !empty($system->modules['news']) ? $system->modules['news']->name : ''; break;
      case 'node-page-interviews': $title = !empty($system->modules['interviews']) ? $system->modules['interviews']->name : ''; break;
      case 'node-page-cribs': $title = !empty($system->modules['cribs']) ? $system->modules['cribs']->name : ''; break;
      case 'node-page-calendar':
      case 'node-page-calendar-days': $title = 'Календар'; break;
      case 'node-page-calendar-production': $title = 'Виробничий календар'; break;
      case 'node-page-calculators': $title = 'Виробничий календар'; break;
      case 'node-page-calculators-calc1': $title = 'Виробничий календар'; break;
      case 'node-page-calculators-calc2': $title = 'Виробничий календар'; break;
      case 'node-page-calculators-calc3': $title = 'Виробничий календар'; break;
      case 'entity.node.canonical':
        $node = \Drupal::routeMatch()->getParameter('node');
        if ( $node ) {
          switch ( $node->getType() ) {
            case 'consultation': $title = !empty($system->modules['consultations']) ? $system->modules['consultations']->name : ''; break;
            case 'normdoc': $title = !empty($system->modules['norm']) ? $system->modules['norm']->name : ''; break;
            case 'profession': $title = !empty($system->modules['orientation']) ? $system->modules['orientation']->name : ''; break;
            case 'example': $title = !empty($system->modules['examples']) ? $system->modules['examples']->name : ''; break;
            case 'accountinglog': $title = !empty($system->modules['accountinglogs']) ? $system->modules['accountinglogs']->name : ''; break;
            case 'instruction': $title = !empty($system->modules['instructions']) ? $system->modules['instructions']->name : ''; break;
            case 'new': $title = !empty($system->modules['news']) ? $system->modules['news']->name : ''; break;
            case 'interview': $title = !empty($system->modules['interviews']) ? $system->modules['interviews']->name : ''; break;
            case 'crib': $title = !empty($system->modules['cribs']) ? $system->modules['cribs']->name : ''; break;
            case 'event': $title = 'Календар'; break;
          }
        }
      break;
    }
    return $title;
  }

  public static function hex_to_rgb($hex) {

    list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");

    return ['r' => $r, 'g' => $g, 'b' => $b];

  }

  public static function colorize($hex, $index) {

    self::hex_to_rgb($hex);

    list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");

    $r = round( $r + $r * $index / 100 );
    $g = round( $g + $g * $index / 100 );
    $b = round( $b + $b * $index / 100 );

    if ( $r > 255) { $r = 255; }
    if ( $r < 0) { $r = 0; }
    if ( $g > 255) { $g = 255; }
    if ( $g < 0) { $g = 0; }
    if ( $b > 255) { $b = 255; }
    if ( $b < 0) { $b = 0; }

    $rgb = dechex($r) . dechex($g) . dechex($b);

    return '#' . $rgb;

  }

  public static function current_system() {
    return get_current_system();
  }

  public static function doc_types() {
    $system = get_current_system();
    $cid = 'konsultant:doc_types:'. $system->id;
    $doc_types = self::get_cache($cid, function(){
      $system = get_current_system();
      $doc_types = \Drupal\konsultant\SystemRequestDispatcher::sendApiCommand('getNormFiltersDocTypes', ['trends' => $system->trends]);
      usort($doc_types, function($a, $b){return strcmp_ukr($a->label, $b->label);});
      return (object)[
        'tags' => [],
        'expire' => time() + 3600,
        'data' => $doc_types
      ];
    });
    return $doc_types;
  }

  public static function doc_statuses() {
    $system = get_current_system();
    $cid = 'konsultant:doc_statuses:'. $system->id;
    $doc_statuses = self::get_cache($cid, function(){
      $doc_statuses = \Drupal\konsultant\SystemRequestDispatcher::sendApiCommand('getNormFiltersDocStatuses', []);
      return (object)[
        'tags' => [],
        'expire' => time() + 3600,
        'data' => $doc_statuses
      ];
    });
    return $doc_statuses;
  }

  public static function example_types() {
    $system = get_current_system();
    $cid = 'konsultant:example_types:'. $system->id;
    $example_types = self::get_cache($cid, function(){
      $system = get_current_system();
      $systems = [$system->id];
      if ( !empty($system->parent_id) ) {
        $systems[] = $system->parent_id;
      }
      $query = \Drupal::database()->select('taxonomy_term_data', 'ttd');
      $query->rightJoin('node__field_example_type', 'fet', 'fet.field_example_type_target_id = ttd.tid');
      $query->rightJoin('node', 'n', 'n.nid = fet.entity_id');
      $query->rightJoin('node__field_systems', 'fs', 'fs.entity_id = n.nid');
      $query->rightJoin('taxonomy_term__field_system', 'tfs', 'tfs.entity_id = fet.field_example_type_target_id');
      $query->condition('ttd.vid', 'example_type');
      $query->condition('fs.field_systems_target_id', $systems, 'IN');
      $query->condition('tfs.field_system_target_id', $systems, 'IN');
      $query->range(0, 1000);
      $query->fields('ttd', ['tid']);
      $query->groupBy('ttd.tid');
      $result = $query->execute();
      $term_ids = $result->fetchAll();
      $example_types = [];
      foreach ( $term_ids as $term_id_obj ) {
        $term = taxonomy_term_load($term_id_obj->tid);
        $example_types[] = (object)[
          'id' => intval($term->Id()),
          'label' => $term->getName()
        ];
      };
      usort($example_types, function($a, $b){return strcmp_ukr($a->label, $b->label);});
      return (object)[
        'tags' => ['taxonomy_term_list:example_type'],
        'data' => $example_types
      ];
    });
    return $example_types;
  }

  public static function new_types() {

    $system = get_current_system();
    $cid = 'konsultant:new_types:'. $system->id;
    $new_types = self::get_cache($cid, function(){
      $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['vid' => 'new_type']);
      $new_types = [];
      foreach ( $terms as $term ) {
        $new_types[] = (object)[
          'id' => $term->Id(),
          'label' => $term->getName()
        ];
      };
      return (object)[
        'tags' => ['taxonomy_term_list:new_type'],
        'data' => $new_types
      ];
    });

    return $new_types;

  }

  public static function doc_producers() {
    $system = get_current_system();
    $cid = 'konsultant:doc_producers:'. $system->id;
    $doc_producers = self::get_cache($cid, function(){
      $system = get_current_system();
      $doc_producers = \Drupal\konsultant\SystemRequestDispatcher::sendApiCommand('getNormFiltersDocProducers', ['trends' => $system->trends]);
      return (object)[
        'tags' => [],
        'expire' => time() + 3600,
        'data' => $doc_producers
      ];
    });
    return $doc_producers;
  }

  public static function theme_filters() {

    $system = get_current_system();
    $cid = 'konsultant:theme_filters:'. $system->id;
    $tree = self::get_cache($cid, function(){

      $system = get_current_system();
      $systems = [$system->id];
      if ( !empty($system->parent_id) ) {
        $systems[] = $system->parent_id;
      }

      $query = \Drupal::database()->select('taxonomy_term_data', 'ttd');
      $query->rightJoin('node__field_filter', 'ff', 'ff.field_filter_target_id = ttd.tid');
      $query->rightJoin('node', 'n', 'n.nid = ff.entity_id');
      $query->rightJoin('node__field_systems', 'fs', 'fs.entity_id = n.nid');
      $query->rightJoin('taxonomy_term__field_system', 'tfs', 'tfs.entity_id = ff.field_filter_target_id');
      $query->condition('ttd.vid', 'filters');
      $query->condition('fs.field_systems_target_id', $systems, 'IN');
      $query->condition('tfs.field_system_target_id', $systems, 'IN');
      $query->range(0, 1000);
      $query->fields('ttd', ['tid']);
      $query->groupBy('ttd.tid');
      $result = $query->execute();
      $term_ids = $result->fetchAll();

      $available_filters = [];

      foreach ( $term_ids as $term_id_obj ) {
        $term = taxonomy_term_load($term_id_obj->tid);
        $available_filters[] = intval($term->Id());
      }

      $vid = 'filters';
      $filters_tree =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid, false, null, false);

      $filters = [];

      foreach ($filters_tree as $filter) {
        $parent = reset($filter->parents);
        $filter_id = intval($filter->tid);
        $filters[] = (object)[
          'id' => $filter_id,
          'parent' => intval($parent),
          'label' => $filter->name,
          'available' => in_array($filter_id, $available_filters),
          'subtree' => []
        ];
      }

      function addItem ( &$branch, $item ) {

        if ( $item->parent === 0 ) {
          $branch[] = $item;
          return;
        }

        foreach ($branch as $branchItem) {

          if ( $branchItem->id == $item->parent ) {

            $branchItem->subtree[] = $item;
            return;

          } else {

          if ( count($branchItem->subtree) ) {

            addItem ( $branchItem->subtree, $item );

          }

          }

        }

      }

      $tree = [];

      foreach ($filters as $item) {

        addItem ( $tree, $item );

      }

      function checkConsistency ( &$branch ) {

        $has_availabale = false;

        foreach ($branch as $index => $branchItem) {

          if ( count($branchItem->subtree) ) {

            if ( !checkConsistency ( $branchItem->subtree ) ) {
              unset($branch[$index]);
            } else {
              $has_availabale = true;
            }

          } else {

            if ( $branchItem->available === false ) {
              unset($branch[$index]);
            }

          }

          if ( $branchItem->available ) {
            $has_availabale = true;
          }

        }

        return $has_availabale;
      }

      checkConsistency ( $tree );
      return (object)[
        'tags' => ['node_list', 'taxonomy_term_list:filters'],
        'expire' => 3600,
        'data' => $tree
      ];
    });
    return $tree;
  }

  public static function current_user() {

    $user_id = \Drupal::currentUser()->Id();
    $system = get_current_system();
    $cid = 'konsultant:current_user:'. $user_id . '-' . $system->id;
    $user = self::get_cache($cid, function(){
      $user = user_load(\Drupal::currentUser()->Id());
      $roles = $user->getRoles();
      $user_role = 'customer';
      if (in_array('expert', $roles) ) {
        $user_role = 'expert';
      }
      if (in_array('editor', $roles) ) {
        $user_role = 'editor';
      }
      if (in_array('editor_norm', $roles) ) {
        $user_role = 'editor_norm';
      }
      if (in_array('administrator', $roles) ) {
        $user_role = 'editor_norm';
      }
      if (in_array('root', $roles) ) {
        $user_role = 'root';
      }
      if (in_array('demo', $roles) ) {
        $user_role = 'demo';
      }
      $user_photo = false;
      if ( !empty($user->get('field_photo')->getValue()) ) {
        $fid = $user->get('field_photo')->getValue()[0]['target_id'];
        if ( $fid ) {
          $file = file_load($fid);
          if ( !empty($file) ) {
            $user_photo_uri = $file->getFileUri();
            $style = ImageStyle::load('photo_profile');
            $user_photo = file_url_transform_relative($style->buildUrl($user_photo_uri));
          }
        };
      }
      $fio = '';
      if ( !empty($user->get('field_fio')->getValue()) ) {
        $fio = $user->get('field_fio')->getValue()[0]['value'];
      }
      $position = '';
      if ( !empty($user->get('field_position')->getValue()) ) {
        $position = $user->get('field_position')->getValue()[0]['value'];
      }
      $company = '';
      if ( !empty($user->get('field_company')->getValue()) ) {
        $company = $user->get('field_company')->getValue()[0]['value'];
      }
      $edrpou = '';
      if ( !empty($user->get('field_edrpou')->getValue()) ) {
        $edrpou = $user->get('field_edrpou')->getValue()[0]['value'];
      }
      $user_email = '';
      if ( !empty($user->get('field_user_email')->getValue()) ) {
        $user_email = $user->get('field_user_email')->getValue()[0]['value'];
      }
      $user_level = 'standard';
      $user_level_label = 'Стандарт';
      if ( !empty($user->get('field_access_level')->getValue()) ) {
        $user_level = $user->get('field_access_level')->getValue()[0]['value'];
        $user_level_label = $user->field_access_level->getSetting('allowed_values')[$user_level];
      }
      $system = get_current_system();
      $consultation_types = ['consultation', 'notfound', 'help', 'interview', 'crib'];
      $date_begin = date('Y-m') . '-01';
      $date_end = date('Y-m-d') . ' 23:59:59';
      $index = \Drupal\search_api\Entity\Index::load('default');
      $query = $index->query();
      $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
      $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
      $query->setParseMode($parse_mode);
      $query->sort('search_api_relevance', 'DESC');
      $query->addCondition('field_systems', $system->id, 'IN')
        ->addCondition('uid', $user->Id())
        ->addCondition('field_consultation_type', $consultation_types, 'IN')
        ->addCondition('created', $date_begin, '>=')
        ->addCondition('created', $date_end, '<=');
      $query->sort('field_answer_date', 'DESC');
      $user_questions_count = $query->execute()->getResultCount();

      return (object)[
        'tags' => ['node_list:consultation', 'user:'. $user->Id()],
        'data' => [
          'id' => $user->Id(),
          'name' => $user->getDisplayName(),
          'fio' => $fio,
          'position' => $position,
          'company' => $company,
          'edrpou' => $edrpou,
          'user_email' => $user_email,
          'role' => $user_role,
          'photo' => $user_photo,
          'roles' => $roles,
          'level' => $user_level,
          'level_label' => $user_level_label,
          'questions_count' => $user_questions_count,
        ]
      ];
    });
    return $user;
  }

  public static function get_item($id) {
    $item = (object)[];
    $entity = \Drupal\node\Entity\Node::load($id);
    if ( !empty($entity) ) {
      $nid = $entity->id();
      $type = $entity->getType();
      switch ( $type ) {
        case 'accountinglog':
          $item = \Drupal\konsultant\SystemRequestDispatcher::getAccountingLog((object)['id'=>$nid]);
        break;
        case 'example':
          $item = \Drupal\konsultant\SystemRequestDispatcher::getExample((object)['id'=>$nid]);
        break;
        case 'instruction':
          $item = \Drupal\konsultant\SystemRequestDispatcher::getInstruction((object)['id'=>$nid]);
        break;
        case 'crib':
          $item = \Drupal\konsultant\SystemRequestDispatcher::getCrib((object)['id'=>$nid]);
        break;
        case 'consultation':
          $item = \Drupal\konsultant\SystemRequestDispatcher::getConsultation((object)['id'=>$nid]);
        break;
        case 'new':
          $item = \Drupal\konsultant\SystemRequestDispatcher::getNew((object)['id'=>$nid]);
        break;
        case 'message':
          $item = \Drupal\konsultant\SystemRequestDispatcher::getMessage((object)['id'=>$nid]);
        break;
        case 'interview':
          $item = \Drupal\konsultant\SystemRequestDispatcher::getInterview((object)['id'=>$nid]);
        break;
      }
    }
    return $item;
  }

  public static function print_content(){
    $item = (object)[];
    $isNorm = strpos(\Drupal::request()->getRequestUri(), '/norm/') > -1;
    if ($entity = \Drupal::routeMatch()->getParameter('entity')) {
      if ( !empty($entity) ) {
        $type = $isNorm ? 'normdoc' : $entity->getType();
        $nid = $isNorm ? $entity : $entity->Id();
        switch ( $type ) {
          case 'normdoc':
            $item = \Drupal\konsultant\SystemRequestDispatcher::getNorm((object)['id'=>$nid]);
          break;
          case 'example':
            $item = \Drupal\konsultant\SystemRequestDispatcher::getExample((object)['id'=>$nid]);
          break;
          case 'accountinglog':
            $item = \Drupal\konsultant\SystemRequestDispatcher::getAccountingLog((object)['id'=>$nid]);
          break;
          case 'instruction':
            $item = \Drupal\konsultant\SystemRequestDispatcher::getInstruction((object)['id'=>$nid]);
          break;
          case 'crib':
            $item = \Drupal\konsultant\SystemRequestDispatcher::getCrib((object)['id'=>$nid]);
          break;
          case 'consultation':
            $item = \Drupal\konsultant\SystemRequestDispatcher::getConsultation((object)['id'=>$nid]);
          break;
          case 'new':
            $item = \Drupal\konsultant\SystemRequestDispatcher::getNew((object)['id'=>$nid]);
          break;
          case 'message':
            $item = \Drupal\konsultant\SystemRequestDispatcher::getMessage((object)['id'=>$nid]);
          break;
          case 'interview':
            $item = \Drupal\konsultant\SystemRequestDispatcher::getInterview((object)['id'=>$nid]);
          break;
        }
      }
    }
    return [
  		'#theme' => 'print_content',
      '#item' => $item,
      '#node_type' => $type
  	];
  }

  public static function get_print_node_type() {
    $type = '';
    if ($node = \Drupal::routeMatch()->getParameter('entity')) {
      if ( !empty($node) && $node instanceof \Drupal\node\Entity\Node ) {
        $type = $node->getType();
      }
    }
    return $type;
  }

  public static function emogrify_content($content){

    $system = get_current_system();
    $content = str_replace(' src="/', 'src="' . $system->domain . '/', $content);

    //пока отключено за ненадобностью
    return $content;

    $emogrifier = new \Pelago\Emogrifier();
    //$content = parseStyleBlocksContent($content);

    $theme_name = 'konsultant_v3';
    $css = file_get_contents('themes/' . $theme_name . '/dist/css/pdf.css');
    //$css .= file_get_contents('/sites/default/files/system-styles/system-colors-' . $system->id . '.css');

    $emogrifier->setCss( $css );

    $emogrifier->setHtml($content);

    $content = $emogrifier->emogrify();

    return $content;

  }

  private static function get_cache($cid , $callback) {
    if ($cache = \Drupal::cache()->get($cid)) {
      $cdata = $cache->data;
    } else {
      $data = call_user_func($callback);
      \Drupal::cache()->set($cid, $data->data, empty($data->expire) ? Cache::PERMANENT: $data->expire, $data->tags);
      $cdata = $data->data;
    }
    return $cdata;
  }

  public static function home_block_consultations(){

    $system = get_current_system();
    $cid = 'konsultant:home-block-consultations:'. $system->id;
    $list = self::get_cache($cid, function(){
      $system = get_current_system();
      $systems = [$system->id];
      if ( !empty($system->parent_id) ) {
        $systems[] = $system->parent_id;
      }
      $list = [];
      $query = \Drupal::entityQuery('node')
      ->condition('type', 'consultation')
      ->condition('status', true)
      ->condition('field_systems', $systems, 'IN');
      $group = $query->orConditionGroup()
        ->condition('field_consultation_type', 'consultation')
        ->condition('field_consultation_type', null, 'IS NULL');
      $query->condition($group);
      $group = $query->orConditionGroup()
        ->condition('field_user_only', 0)
        ->condition('field_user_only', null, 'IS NULL');
      $query->condition($group);
      $query->condition('field_status', ['viewed','placed'], 'IN')
        ->sort('field_sort_date', 'DESC')
        ->range(0, 5);
      $nids = $query->execute();
      $nodes = node_load_multiple($nids);
      foreach ( $nodes as $node ) {
        $list[] = (object)[
          'id' => $node->Id(),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
          'demo' => !empty($node->get('field_demo')->getValue()[0]['value']),
          'date' => \Drupal::service('date.formatter')->format($node->field_sort_date->value, 'date_reversed'),
          'title' => $node->getTitle()
        ];
      }
      return (object)[
        'tags' => ['node_list:consultation'],
        'data' => $list
      ];
    });
    return $list;
  }

  public static function home_block_new_services(){

    $system = get_current_system();
    $cid = 'konsultant:home-block-new-services:'. $system->id;
    $services_list = self::get_cache($cid, function(){
      $system = get_current_system();
      $system_term = taxonomy_term_load($system->id);
      $services_list = [];
      if ( !empty($system_term->field_services) && !empty($system_term->field_services->getValue()) ) {
        $services_ids = $system_term->field_services->getValue();
        foreach ( $services_ids as $service_id ) {
          $service_p = \Drupal\paragraphs\Entity\Paragraph::load( $service_id['target_id'] );
          $service_title = '';
          $service_url = '';
          if ( !empty($service_p->field_service_name) && !empty($service_p->field_service_name->getValue()) ) {
            $service_title = $service_p->get('field_service_name')->getValue()[0]['value'];
          }
          if ( !empty($service_p->field_service_url) && !empty($service_p->field_service_url->getValue()) ) {
            $service_url = $service_p->get('field_service_url')->getValue()[0]['value'];
          }
          if ( !empty($service_title) && !empty($service_url) ) {
            $services_list[] = (object)[
              'title' => $service_title,
              'url' => $service_url
            ];
          }
        }
      }
      return (object)[
        'tags' => ['taxonomy_term:'. $system->id],
        'data' => $services_list
      ];
    });
    return $services_list;
  }

  public static function get_template($template_id, $vars = []){
    return get_system_template($template_id, $vars);
  }

  public static function getConsultation($node_id){
    return \Drupal\konsultant\SystemRequestDispatcher::getConsultation((object)['id' => $node_id]);
  }

  public static function home_block_norm(){
    $system = get_current_system();
    $cid = 'konsultant:home-block-norm:'. $system->id;
    $data = self::get_cache($cid, function(){
      $system = get_current_system();
      $params = (object)[];
      $params->trends = $system->trends;
      $data = \Drupal\konsultant\SystemRequestDispatcher::sendApiCommand('getNormList', $params);
      unset($data->list[6]);
      unset($data->list[5]);
      unset($data->list[4]);
      return (object)[
        'tags' => [],
        'expire' => time() + 3600,
        'data' => $data->list
      ];
    });
    return $data;
  }

  public static function home_block_examples(){

    $system = get_current_system();
    $cid = 'konsultant:home-block-examples:'. $system->id;
    $list = self::get_cache($cid, function(){
      $system = get_current_system();
      $systems = [$system->id];
      if ( !empty($system->parent_id) ) {
        $systems[] = $system->parent_id;
      }
      $query = \Drupal::entityQuery('node')
      ->condition('type', 'example')
      ->condition('status', true)
      ->condition('field_systems', $systems, 'IN')
      ->sort('field_sort_date', 'DESC')
      ->range(0, 4);
      $nids = $query->execute();
      $nodes = node_load_multiple($nids);
      $list = [];
      foreach ( $nodes as $node ) {
        $list[] = (object)[
          'id' => $node->Id(),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
          'demo' => !empty($node->get('field_demo')->getValue()[0]['value']),
          'date' => \Drupal::service('date.formatter')->format($node->field_sort_date->value, 'date_reversed'),
          'title' => $node->getTitle()
        ];
      }
      return (object)[
        'tags' => ['node_list:example'],
        'data' => $list
      ];
    });
    return $list;
  }

  public static function home_block_accountinglogs(){

    $system = get_current_system();
    $cid = 'konsultant:home-block-accountinglogs:'. $system->id;
    $list = self::get_cache($cid, function(){
      $system = get_current_system();
      $systems = [$system->id];
      if ( !empty($system->parent_id) ) {
        $systems[] = $system->parent_id;
      }
      $query = \Drupal::entityQuery('node')
      ->condition('type', 'accountinglog')
      ->condition('status', true)
      ->condition('field_systems', $systems, 'IN')
      ->sort('field_sort_date', 'DESC')
      ->range(0, 4);
      $nids = $query->execute();
      $nodes = node_load_multiple($nids);
      $list = [];
      foreach ( $nodes as $node ) {
        $list[] = (object)[
          'id' => $node->Id(),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
          'demo' => !empty($node->get('field_demo')->getValue()[0]['value']),
          'date' => \Drupal::service('date.formatter')->format($node->field_sort_date->value, 'date_reversed'),
          'title' => $node->getTitle()
        ];
      }
      return (object)[
        'tags' => ['node_list:accountinglog'],
        'data' => $list
      ];
    });
    return $list;
  }

  public static function home_block_cribs(){

    $system = get_current_system();
    $cid = 'konsultant:home-block-cribs:'. $system->id;
    $list = self::get_cache($cid, function(){
      $system = get_current_system();
      $systems = [$system->id];
      if ( !empty($system->parent_id) ) {
        $systems[] = $system->parent_id;
      }
      $query = \Drupal::entityQuery('node')
      ->condition('type', 'crib')
      ->condition('status', true)
      ->condition('field_systems', $systems, 'IN')
      ->sort('field_sort_date', 'DESC')
      ->range(0, 1);
      $nids = $query->execute();
      $nodes = node_load_multiple($nids);
      $list = [];
      foreach ( $nodes as $node ) {
        $image = false;
        if ( !empty($node->field_frontpage_image->getValue()[0]['target_id']) ) {
          $image_target = \Drupal\file\Entity\File::load($node->field_frontpage_image->getValue()[0]['target_id']);
          if ( !empty($image_target) ){
            $image_uri = $image_target->getFileUri();
            $image = get_image_path_pdf($image_uri, 'crib');
          }
        }
        $list[] = (object)[
          'id' => $node->Id(),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
          'image' => $image,
          'title' => $node->getTitle()
        ];
      }
      return (object)[
        'tags' => ['node_list:crib'],
        'data' => $list
      ];
    });
    return $list;
  }

  public static function home_block_instructions(){

    $system = get_current_system();
    $cid = 'konsultant:home-block-instructions:'. $system->id;
    $list = self::get_cache($cid, function(){
      $system = get_current_system();
      $systems = [$system->id];
      if ( !empty($system->parent_id) ) {
        $systems[] = $system->parent_id;
      }
      $query = \Drupal::entityQuery('node')
      ->condition('type', 'instruction')
      ->condition('status', true)
      ->condition('field_systems', $systems, 'IN')
      ->sort('field_sort_date', 'DESC')
      ->range(0, 4);
      $nids = $query->execute();
      $nodes = node_load_multiple($nids);
      $list = [];
      foreach ( $nodes as $node ) {
        $list[] = (object)[
          'id' => $node->Id(),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
          'demo' => !empty($node->get('field_demo')->getValue()[0]['value']),
          'date' => \Drupal::service('date.formatter')->format($node->field_sort_date->value, 'date_reversed'),
          'title' => $node->getTitle()
        ];
      }
      return (object)[
        'tags' => ['node_list:instruction'],
        'data' => $list
      ];
    });
    return $list;
  }

  public static function home_block_news(){

    $system = get_current_system();

    $cid = 'konsultant:home-block-news:'. $system->id;
    $list = self::get_cache($cid, function(){
      $system = get_current_system();
      $systems = [$system->id];
      if ( !empty($system->parent_id) ) {
        $systems[] = $system->parent_id;
      }
      $query = \Drupal::entityQuery('node')
      ->condition('type', 'new')
      ->condition('status', true)
      ->condition('field_systems', $systems, 'IN')
      ->sort('field_sort_date', 'DESC')
      ->range(0, 8);
      $nids = $query->execute();
      $nodes = node_load_multiple($nids);
      $list = [];
      foreach ( $nodes as $node ) {
        $image = '';
        $image_big = '';
        if ( !empty($node->field_new_image->getValue()) ) {
          $file = file_load($node->field_new_image->getValue()[0]['target_id'])->getFileUri();
          if ( !empty($file) ){
            $style = ImageStyle::load('new_preview');
            $image = file_url_transform_relative($style->buildUrl($file));
            $style = ImageStyle::load('new_preview_big');
            $image_big = file_url_transform_relative($style->buildUrl($file));
          }
        }
        $new_type = '';
        if ( !empty($node->get('field_new_type')->getValue()) ) {
          $new_type_id = $node->get('field_new_type')->getValue()[0]['target_id'];
          $new_type_term = taxonomy_term_load($new_type_id);
          if ( !empty($new_type_term) ) {
            $new_type = $new_type_term->getName();
          }
        }
        $list[] = (object)[
          'id' => $node->Id(),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
          'image' => $image,
          'image' => $image,
          'image_big' => $image_big,
          'new_type' => $new_type,
          'demo' => !empty($node->get('field_demo')->getValue()[0]['value']),
          'date' => \Drupal::service('date.formatter')->format($node->field_sort_date->value, 'date_reversed'),
          'title' => $node->getTitle()
        ];
      }
      return (object)[
        'tags' => ['node_list:new'],
        'data' => $list
      ];
    });
    return $list;
  }

  public static function home_block_interviews(){

    $system = get_current_system();
    $cid = 'konsultant:home-block-interviews:'. $system->id;
    $list = self::get_cache($cid, function(){
      $system = get_current_system();
      $systems = [$system->id];
      if ( !empty($system->parent_id) ) {
        $systems[] = $system->parent_id;
      }
      $query = \Drupal::entityQuery('node')
      ->condition('type', 'interview')
      ->condition('field_systems', $systems, 'IN')
      ->condition('status', true)
      ->sort('field_sort_date', 'DESC')
      ->range(0, 5);
      $nids = $query->execute();
      $nodes = node_load_multiple($nids);
      $list = [];

      foreach ( $nodes as $node ) {
        $text = truncateHTML(300, $node->field_interview_html->value);
        if ( empty($text) ) {
          $interview_fields = konsultant_getFieldsByNid( $node->Id(), 'field_interview');
          if ( !empty($interview) ) {
            $qas = konsultant_getParagraphFields($interview_fields, ['field_question_text', 'field_answer_text']);
            if ( empty($text) ) {
              $text = $qas->field_question_text;
            }
            if ( empty($text) ) {
              $text = $qas->field_answer_text;
            }
          }
        }
        $list[] = (object)[
          'id' => $node->Id(),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
          'demo' => !empty($node->get('field_demo')->getValue()[0]['value']),
          'date' => \Drupal::service('date.formatter')->format($node->field_sort_date->value, 'date_reversed'),
          'text' => $text,
          'title' => $node->getTitle()
        ];
      }
      return (object)[
        'tags' => ['node_list:interview'],
        'data' => $list
      ];
    });
    return $list;
  }

  public static function file_exists($filename){
    return file_exists($filename);
  }

  public static function home_block_hot(){

    $system = get_current_system();
    $cid = 'konsultant:home-block-hot:'. $system->id;
    $list = self::get_cache($cid, function(){
      $system = get_current_system();
      $systems = [$system->id];
      if ( !empty($system->parent_id) ) {
        $systems[] = $system->parent_id;
      }
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'example')
        ->condition('field_systems', $system->id)
        ->condition('field_hot', 1)
        ->condition('status', true)
        ->sort('field_sort_date', 'DESC')
        ->range(0, 7);
      $nids = $query->execute();
      $nodes = node_load_multiple($nids);
      $list_examples = [];
      foreach ( $nodes as $node ) {
        $sort_date = !empty($node->field_sort_date->value) ? $node->field_sort_date->value : 0;
        $list_examples[] = (object)[
          'id' => $node->Id(),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
          'demo' => !empty($node->get('field_demo')->getValue()[0]['value']),
          'date' => \Drupal::service('date.formatter')->format($sort_date, 'date_reversed'),
          'title' => $node->getTitle(),
          'module' => 'examples'
        ];
      }
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'accountinglog')
        ->condition('field_systems', $system->id)
        ->condition('field_hot', 1)
        ->sort('field_sort_date', 'DESC')
        ->range(0, 7);
      $nids = $query->execute();
      $nodes = node_load_multiple($nids);
      $list_accountinglogs = [];
      foreach ( $nodes as $node ) {
        $sort_date = !empty($node->field_sort_date->value) ? $node->field_sort_date->value : 0;
        $list_accountinglogs[] = (object)[
          'id' => $node->Id(),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
          'demo' => !empty($node->get('field_demo')->getValue()[0]['value']),
          'date' => \Drupal::service('date.formatter')->format($sort_date, 'date_reversed'),
          'title' => $node->getTitle(),
          'module' => 'accountinglogs'
        ];
      }
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'consultation')
        ->condition('field_systems', $system->id)
        ->condition('field_status', ['viewed', 'placed'], 'IN')
        ->condition('field_hot', 1)
        ->sort('field_sort_date', 'DESC')
        ->range(0, 7);
      $nids = $query->execute();
      $nodes = node_load_multiple($nids);
      $list_consultations = [];
      foreach ( $nodes as $node ) {
        $sort_date = !empty($node->field_sort_date->value) ? $node->field_sort_date->value : 0;
        $list_consultations[] = (object)[
          'id' => $node->Id(),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
          'demo' => !empty($node->get('field_demo')->getValue()[0]['value']),
          'date' => \Drupal::service('date.formatter')->format($sort_date, 'date_reversed'),
          'title' => $node->getTitle(),
          'module' => 'consultations'
        ];
      }
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'new')
        ->condition('field_systems', $system->id)
        ->condition('field_hot', 1)
        ->sort('field_sort_date', 'DESC')
        ->range(0, 7);
      $nids = $query->execute();
      $nodes = node_load_multiple($nids);
      $list_news = [];
      foreach ( $nodes as $node ) {
        $sort_date = !empty($node->field_sort_date->value) ? $node->field_sort_date->value : 0;
        $list_news[] = (object)[
          'id' => $node->Id(),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
          'demo' => !empty($node->get('field_demo')->getValue()[0]['value']),
          'date' => \Drupal::service('date.formatter')->format($sort_date, 'date_reversed'),
          'title' => $node->getTitle(),
          'module' => 'news'
        ];
      }
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'interview')
        ->condition('field_systems', $system->id)
        ->condition('field_hot', 1)
        ->sort('field_sort_date', 'DESC')
        ->range(0, 4);
      $nids = $query->execute();
      $nodes = node_load_multiple($nids);
      $list_interviews = [];
      foreach ( $nodes as $node ) {
        $sort_date = !empty($node->field_sort_date->value) ? $node->field_sort_date->value : 0;
        $list_interviews[] = (object)[
          'id' => $node->Id(),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
          'demo' => !empty($node->get('field_demo')->getValue()[0]['value']),
          'date' => \Drupal::service('date.formatter')->format($sort_date, 'date_reversed'),
          'title' => $node->getTitle(),
          'module' => 'interviews'
        ];
      }
      if ( $system->id == 2 ) {
        $query = \Drupal::entityQuery('node')
          ->condition('type', 'instruction')
          ->condition('field_hot', 1)
          ->sort('field_sort_date', 'DESC')
          ->range(0, 7);
        $nids = $query->execute();
        $nodes = node_load_multiple($nids);
        $list_instructions = [];
        foreach ( $nodes as $node ) {
          $sort_date = !empty($node->field_sort_date->value) ? $node->field_sort_date->value : 0;
          $list_instructions[] = (object)[
            'id' => $node->Id(),
            'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
            'demo' => !empty($node->get('field_demo')->getValue()[0]['value']),
            'date' => \Drupal::service('date.formatter')->format($sort_date, 'date_reversed'),
            'title' => $node->getTitle(),
            'module' => 'list_instructions'
          ];
        }
      }
      $list = [];
      for ( $i = 0; $i < 9; $i++ ) {
        if ( !empty($list_consultations[$i]) ) {
          $list[] = $list_consultations[$i];
        }
        if ( !empty($list_examples[$i]) ) {
          $list[] = $list_examples[$i];
        }
        if ( !empty($list_accountinglogs[$i]) ) {
          $list[] = $list_accountinglogs[$i];
        }
        if ( !empty($list_news[$i]) ) {
          $list[] = $list_news[$i];
        }
        if ( !empty($list_instructions[$i]) ) {
          $list[] = $list_instructions[$i];
        }

        if ( !empty($list_interviews[$i]) ) {
          $list[] = $list_interviews[$i];
        }
        if ( count($list) >= 9 ) {
          $list = array_slice($list, 0, 9);
          break;
        }
      }
      usort($list,
        function($a, $b){
          $dateA = date_create_from_format('d.m.Y', $a->date)->format('Y-m-d');
          $dateB = date_create_from_format('d.m.Y', $b->date)->format('Y-m-d');
          return -strcmp($dateA,$dateB);
        }
      );
      return (object)[
        'tags' => ['node_list:example', 'node_list:accountinglog', 'node_list:consultation', 'node_list:new', 'node_list:interview', 'node_list:instruction'],
        'data' => $list
      ];
    });
    return $list;
  }

}
