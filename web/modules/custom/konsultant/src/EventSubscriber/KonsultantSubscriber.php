<?php

/**
 * @file
 * Contains \Drupal\konsultant\EventSubscriber\KonsultantSubscriber.
 */

namespace Drupal\konsultant\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
// use Drupal\Core\Cache\Cache;

/**
 * Event subscriber subscribing to KernelEvents::REQUEST.
 */
class KonsultantSubscriber implements EventSubscriberInterface {

  public function __construct() {

  }

  public function checkAuthStatus(FilterResponseEvent $event) {

      $response = $event->getResponse();

      if ( $response instanceOf RedirectResponse ) {

      } else {
        $route = \Drupal::routeMatch()->getRouteName();
        if($route != 'session_limit.limit_form' && $route != 'system-request-handler' && $route != 'system-api-handler' && $route != 'system-api-client' && $route != 'system.403') {

          if ( false ) {

            if (
                $route != 'image.style_public' &&
                $route != 'autologout.ahah_logout' &&
                $route != 'convert-test' &&
                $route != 'user.login' &&
                $route != 'send-expert-message' &&
                $route != 'soapsync.soapwsdl' &&
                $route != 'soapsync.soapserver' &&
                $route != 'system-api-handler' &&
                $route != 'soapsync.soapserverSetUser' &&
                $route != 'system.401' &&
                $route != 'system.403'
              ) {
              $destination = \Drupal::request()->query->get('destination');
              if ( !empty($destination) ) {
                $destination = '?destination=' . urlencode($destination);
              } else {
                $destination = '';
              }
              $response = new RedirectResponse('/user/login' . $destination, 302);
              $event->setResponse($response);
              $event->stopPropagation();
            }

          } else {

            //$current_uri = \Drupal::request()->getRequestUri();
            //$theme_name = \Drupal::service('theme.manager')->getActiveTheme()->getName();
            //if ( ( $route != 'system-request-handler' && strpos($current_uri, 'batch') === false && strpos($current_uri, 'admin') === false && $current_uri !== '/') {
              //$response = new RedirectResponse('/', 302);
              //$event->setResponse($response);
              //$event->stopPropagation();
            //}

          }

        }

      };

  }

  public function defaultSystemRedirect(FilterResponseEvent $event) {

      $response = $event->getResponse();

      if ( $response instanceOf RedirectResponse ) {

      } else {

        $proto = \Drupal::request()->isSecure() ? 'https://' : 'http://';
        $host = $proto . \Drupal::request()->getHost();

        $current_domain = get_current_system()->domain;

        if ( $current_domain != $host ) {
          $response = new TrustedRedirectResponse($current_domain);
          $event->setResponse($response);
          $event->stopPropagation();
        }

      };

  }

  public function checkManifestVersions() {

    $theme = \Drupal::theme()->getActiveTheme();
    $manifestPath = $theme->getPath() . '/dist/manifest.json';
    $lockPath = $theme->getPath() . '/app-lock.json';
    $manifest = file_get_contents($manifestPath);
    $lock = file_get_contents($lockPath);
    $hasDiffs = $manifest !== $lock;

    if ( $hasDiffs ) {
      file_put_contents($lockPath, $manifest);
      $manifest = json_decode($manifest);
      $lock = json_decode($lock);
      if ( !empty($manifest) && !empty($lock) ) {
        foreach ( $manifest as $manifestEntry ) {
          $entryFound = false;
          foreach ( $lock as $lockEntry ) {
            if ( $lockEntry === $manifestEntry ) {
              $entryFound = true; continue;
            }
          }
          if ( $entryFound ) {
          } else {
          }
        }
      }

    }

  }

  public static function getSubscribedEvents() {
    $events = [];
    $events[KernelEvents::RESPONSE][] = ['defaultSystemRedirect'];
    // $events[KernelEvents::RESPONSE][] = ['checkAuthStatus'];
    //$events[KernelEvents::CONTROLLER][] = ['checkManifestVersions'];
    return $events;
  }

}
