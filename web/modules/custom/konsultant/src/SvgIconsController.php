<?php
/**
 * @file
 * Contains \Drupal\konsultant\SvgIconsController.
 */

namespace Drupal\konsultant;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

class SvgIconsController extends ControllerBase {

  public function content() {

    $twig = \Drupal::service('twig');
    $template = $twig->loadTemplate('svgicons.html.twig');
    $style = \Drupal::request()->query->get('style');
    $html = $template->render(['style'=> $style]);
    $response = new Response(
      $html,
      Response::HTTP_OK,
      ['content-type' => 'image/svg+xml']
    );

    $response->setMaxAge(86400);
    return $response;

  }

  public function pngContent() {

    $twig = \Drupal::service('twig');
    $template = $twig->loadTemplate('svgicons.html.twig');
    $style = \Drupal::request()->query->get('style');
    $svg = $template->render(['style'=> $style]);

    $image = new \Imagick();

    $image->setBackgroundColor(new \ImagickPixel('transparent'));

    $image->readImageBlob($svg);
    $image->setImageFormat("png");

    $imageData = $image->getImageBlob();

    $response = new Response(
      $imageData,
      Response::HTTP_OK,
      ['content-type' => 'image/png']
    );

    $response->setMaxAge(86400);
    return $response;

  }


}
