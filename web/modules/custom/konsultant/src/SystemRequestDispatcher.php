<?php
/**
 * @file
 * Contains \Drupal\konsultant\SystemRequestDispatcher.
 */

namespace Drupal\konsultant;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Site\Settings;
use Drupal\file\Entity\File;
use Drupal\Core\Database\Database;
use Drupal\konsultant\TwigExtension\KonsultantTwigExtension;
use Drupal\Core\Cache\Cache;

class SystemRequestDispatcher extends ControllerBase {

  public function cleanup() {

    $html = \Drupal::request()->request->get('html');
    $html_clean = \Drupal\konsultant_import\ImportpageController::fixImportedHtml(json_decode($html));
    $response = new JsonResponse((object)['html' => $html_clean]);
    $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    return $response;

  }

  public static function writeLog($system_id, $uid, $command, $params, $begin, $time) {
    $connection = Database::getConnection();
    $result = $connection->insert('konsultant_user_commands')
      ->fields([
        'uid' => $uid,
        'system' => $system_id,
        'begin' => $begin,
        'time' => $time,
        'command' => $command,
        'params' => $params
      ])
      ->execute();
  }

  public function requestHandler() {

    $command = \Drupal::request()->request->get('command');
    $raw_params = \Drupal::request()->request->get('params');
    $params = json_decode($raw_params);
    if ( !empty($_FILES['file']) ) {
      $params->file = $_FILES['file'];
    }
    return $this->commandsParser($command, $params);

  }

  public function commandsHandler(Request $request) {

    $command = $request->get('command');
    $raw_params = $request->request->get('params');
    $params = json_decode($raw_params);
    if ( !empty($_FILES['file']) ) {
      $params->file = $_FILES['file'];
    }
    return $this->commandsParser($command, $params);

  }

  public function commandsParser($command, $params) {

    $system = get_current_system();

    $error = 0;
    $errorText = '';

    $uid = \Drupal::currentUser()->Id();

    $currentUser = \Drupal::currentUser();
    $isAnonymous = $currentUser->isAnonymous();

    $data = (object)[];
    $begin = time();
    $time_start = microtime(true);

    $anonymousCommands = [
      'sendDemoForm',
      'getNormList',
      'getNorm',
      'getNewsList',
      'getJobsList',
      'getExamplesList',
      'getAccountingLogsList',
      'getInstructionsList',
      'getNew',
      'getInterviewsList',
      'getSearchList',
      'getCribsList',
      'getProfessionList',
      'getConsultationsList',
      'getEventsYear',
      'getOrientationTree',
      'getConsultation',
      'getInterview',
      'getCrib',
      'getExample',
      'getAccountingLog',
      'getInstruction',
      'getOrientation',
      'getHomeBlocks',
      'getEvent'
    ];

    $anonymousContentCommands = [
      'getOrientationTree',
      'createConsultation',
      'getEventsList'
    ];

    if ( $isAnonymous && !in_array($command, $anonymousCommands) ) {

      if ( in_array($command, $anonymousContentCommands) ) {
        $error = 200;
        $errorText = 'Authorization required';
      } else {
        $error = 999;
        $errorText = 'Session expired';
      }
    } else {

      switch ( $command ) {

        case 'sendDemoForm':

          $data = soapsync_demo($params);

        break;

        case 'createConsultation':

          if ( isset($system->modules['consultations']) ) {
            $data = $this->createConsultation($params);
          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'getNormList':

          if ( isset($system->modules['norm']) ) {
            $data = $this->getNormList($params);
          } else {
            $error = 100;
            $errorText = 'Module norm is not enabled';
          }
        break;

        case 'getHomeBlocks':
            $data = $this->getHomeBlocks($params);
        break;

        case 'getProfessionList':

          if ( isset($system->modules['orientation']) ) {
            $data = $this->getProfessionList($params);
          } else {
            $error = 100;
            $errorText = 'Module orientation is not enabled';
          }
        break;

        case 'getExamplesList':

          if ( isset($system->modules['examples']) ) {
            $data = $this->getExamplesList($params);
          } else {
            $error = 100;
            $errorText = 'Module accountinglogs is not enabled';
          }

        break;

        case 'getCribsList':

          if ( isset($system->modules['cribs']) ) {
            $data = $this->getCribsList($params);
          } else {
            $error = 100;
            $errorText = 'Module cribs is not enabled';
          }

        break;

        case 'getAccountingLogsList':

          if ( isset($system->modules['accountinglogs']) ) {
            $data = $this->getAccountingLogsList($params);
          } else {
            $error = 100;
            $errorText = 'Module accountinglogs is not enabled';
          }

        break;

        case 'getInterviewsList':

          if ( isset($system->modules['interviews']) ) {
            $data = $this->getInterviewsList($params);
          } else {
            $error = 100;
            $errorText = 'Module interviews is not enabled';
          }

        break;

        case 'getOrientationTree':

          if ( isset($system->modules['orientation']) ) {
            $data = $this->getOrientationTree($params);
          } else {
            $error = 100;
            $errorText = 'Module interviews is not enabled';
          }

        break;

        case 'getOrientation':

          if ( isset($system->modules['orientation']) ) {
            $data = $this->getOrientation($params);
          } else {
            $error = 100;
            $errorText = 'Module interviews is not enabled';
          }

        break;

        case 'getOrientationNodes':

          if ( isset($system->modules['orientation']) ) {
            $data = $this->getOrientationNodes($params);
          } else {
            $error = 100;
            $errorText = 'Module interviews is not enabled';
          }

        break;

        case 'getMessagesList':
          $data = $this->getMessagesList($params);
        break;

        case 'deleteMessages':
          $data = $this->deleteMessages($params);
        break;

        case 'getProfile':
          $data = $this->getProfile($params);
        break;

        case 'getExpert':
          $data = $this->getExpert($params);
        break;

        case 'getTypeById':
          $data = $this->getTypeById($params);
        break;

        case 'getTemplate':
          $data = $this->getTemplate($params);
        break;

        case 'getConvertedHtml':
          $data = $this->getConvertedHtml($params);
        break;

        case 'getNewsList':

          if ( isset($system->modules['news']) ) {
            $data = $this->getNewsList($params);
          } else {
            $error = 100;
            $errorText = 'Module norm is not enabled';
          }

        break;

        case 'getJobsList':

          if ( isset($system->modules['classifier']) ) {
            $data = $this->getJobsList($params);
          } else {
            $error = 100;
            $errorText = 'Module classifier is not enabled';
          }

        break;

        case 'getSearchList':
          $data = $this->getSearchList($params);
        break;

        case 'getSearchLinks':
          $data = $this->getSearchLinks($params);
        break;

        case 'getFavoritesList':
          $data = $this->getFavoritesList($params);
        break;

        case 'getInstructionsList':

          if ( isset($system->modules['instructions']) ) {
            $data = $this->getInstructionsList($params);
          } else {
            $error = 100;
            $errorText = 'Module norm is not enabled';
          }

        break;

        case 'saveNode':
            $data = $this->saveNode($params);
        break;

        case 'unsaveNode':
            $data = $this->unsaveNode($params);
        break;

        case 'unsaveNodes':
            $data = $this->unsaveNodes($params);
        break;

        case 'getHelpList':

          $data = $this->getHelpList($params);

        break;

        case 'getConsultationsList':

          if ( isset($system->modules['consultations']) ) {
            $data = $this->getConsultationsList($params);
          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'getConsultationsOwnList':

          if ( isset($system->modules['consultations']) ) {
            $data = $this->getConsultationsOwnList($params);
          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'getConsultationsExpertList':

          if ( isset($system->modules['consultations']) ) {
            $user_roles = \Drupal::currentUser()->getRoles();
            if ( in_array('expert', $user_roles) ) {
              $data = $this->getConsultationsExpertList($params);
            } else {
              $error = 110;
              $errorText = 'Only for experts';
            }

          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'getConsultationsExpertArchive':

          if ( isset($system->modules['consultations']) ) {
            $user_roles = \Drupal::currentUser()->getRoles();
            if ( in_array('expert', $user_roles) ) {
              $data = $this->getConsultationsExpertArchive($params);
            } else {
              $error = 110;
              $errorText = 'Only for experts';
            }

          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'setStatus':

          if ( isset($system->modules['consultations']) ) {
            $user_roles = \Drupal::currentUser()->getRoles();
            if ( in_array('expert', $user_roles) ) {
              $data = $this->setStatus($params);
            } else {
              $error = 110;
              $errorText = 'Only for experts';
            }

          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'clarify':

          if ( isset($system->modules['consultations']) ) {
            $data = $this->clarify($params);
          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'clarification':

          if ( isset($system->modules['consultations']) ) {
            $data = $this->clarification($params);
          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'saveConsultation':

          if ( isset($system->modules['consultations']) ) {
            $user_roles = \Drupal::currentUser()->getRoles();
            if ( in_array('expert', $user_roles) ) {
              $data = $this->saveConsultation($params);
            } else {
              $error = 110;
              $errorText = 'Only for experts';
            }

          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'submitAnswer':

          if ( isset($system->modules['consultations']) ) {
            $user_roles = \Drupal::currentUser()->getRoles();
            if ( in_array('expert', $user_roles) ) {
              $data = $this->submitAnswer($params);
            } else {
              $error = 110;
              $errorText = 'Only for experts';
            }

          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'checkNewConsultations':

          if ( isset($system->modules['consultations']) ) {
            $data = $this->checkNewConsultations($params);
          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'checkNewAdminConsultations':

          if ( isset($system->modules['consultations']) ) {
            $data = $this->checkNewAdminConsultations($params);
          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'checkNewAdminRequests':

          if ( isset($system->modules['consultations']) ) {
            $data = $this->checkNewAdminRequests($params);
          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'checkNewMessages':
          $data = $this->checkNewMessages($params);
        break;

        case 'getConsultation':

          if ( isset($system->modules['consultations']) ) {
            $data = $this->getConsultation($params);
          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'getNorm':

          if ( isset($system->modules['norm']) ) {
            $data = $this->getNorm($params);
          } else {
            $error = 100;
            $errorText = 'Module norm is not enabled';
          }

        break;

        case 'getAccountingLog':

          if ( isset($system->modules['accountinglogs']) ) {
            $data = $this->getAccountingLog($params);
          } else {
            $error = 100;
            $errorText = 'Module accountinglogs is not enabled';
          }

        break;


        case 'getNormRevisions':

          if ( isset($system->modules['norm']) ) {
            $data = $this->getNormRevisions($params);
          } else {
            $error = 100;
            $errorText = 'Module norm is not enabled';
          }

        break;

        case 'getExample':

          if ( isset($system->modules['examples']) ) {
            $data = $this->getExample($params);
          } else {
            $error = 100;
            $errorText = 'Module examples is not enabled';
          }

        break;

        case 'getCrib':

          if ( isset($system->modules['cribs']) ) {
            $data = $this->getCrib($params);
          } else {
            $error = 100;
            $errorText = 'Module cribs is not enabled';
          }

        break;

        case 'getInterview':

          if ( isset($system->modules['interviews']) ) {
            $data = $this->getInterview($params);
          } else {
            $error = 100;
            $errorText = 'Module itnerviews is not enabled';
          }

        break;

        case 'getMessage':
          $data = $this->getMessage($params);
        break;

        case 'getNew':

          if ( isset($system->modules['news']) ) {
            $data = $this->getNew($params);
          } else {
            $error = 100;
            $errorText = 'Module new is not enabled';
          }

        break;

        case 'getInstruction':

          if ( isset($system->modules['instructions']) ) {
            $data = $this->getInstruction($params);
          } else {
            $error = 100;
            $errorText = 'Module instructions is not enabled';
          }

        break;

        case 'setRate':

          if ( isset($system->modules['consultations']) ) {
            $data = $this->setRate($params);
          } else {
            $error = 100;
            $errorText = 'Module consultations is not enabled';
          }

        break;

        case 'getProfileMenu':
          $data = $this->getProfileMenu($params);
        break;


        case 'getEventsYear':

          if ( isset($system->modules['calendar']) ) {
            $data = $this->getEventsYear($params);
          } else {
            $error = 100;
            $errorText = 'Module calendar is not enabled';
          }

        break;

        case 'checkNewEvents':
          if ( isset($system->modules['calendar']) ) {
            $data = $this->checkNewEvents($params);
          } else {
            $error = 100;
            $errorText = 'Module calendar is not enabled';
          }
        break;

        case 'createEvent':

          if ( isset($system->modules['calendar']) ) {
            $data = $this->createEvent($params);
          } else {
            $error = 100;
            $errorText = 'Module calendar is not enabled';
          }

        break;

        case 'getEvent':

          if ( isset($system->modules['calendar']) ) {
            $data = $this->getEvent($params);
          } else {
            $error = 100;
            $errorText = 'Module calendar is not enabled';
          }

        break;

        case 'getEventsList':

          if ( isset($system->modules['calendar']) ) {
            $data = $this->getEventsList($params);
          } else {
            $error = 100;
            $errorText = 'Module calendar is not enabled';
          }

        break;

        case 'deleteEvent':

          if ( isset($system->modules['calendar']) ) {
            $data = $this->deleteEvent($params);
          } else {
            $error = 100;
            $errorText = 'Module calendar is not enabled';
          }

        break;

        case 'saveEvent':

          if ( isset($system->modules['calendar']) ) {
            $data = $this->saveEvent($params);
          } else {
            $error = 100;
            $errorText = 'Module calendar is not enabled';
          }

        break;

        default:
          $error = 100;
          $errorText = 'Unknown command';
          $data = (object)[];
        break;

      }

    }

    $responseData = (object)[
      'error' => $error,
      'errorText' => $errorText,
      'data' => $data
    ];

    $time_end = microtime(true);
    $time = $time_end - $time_start;

    self::writeLog($system->id, $uid, $command, json_encode($params), $begin, $time);

    $response = new JsonResponse($responseData);
    $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    return $response;

  }

  public function getProfileMenu ($params) {
    $data = [];
    return $data;
  }

  public static function getWorkingDaysCount($from, $to, $additionalWorkingDays, $holidayDays, $additionalHolidays) {
    $workingDays = [1, 2, 3, 4, 5];
    $from = new \DateTime($from);
    $to = new \DateTime($to);
    $to->modify('+1 day');
    $interval = new \DateInterval('P1D');
    $periods = new \DatePeriod($from, $interval, $to);
    $days = 0;
    foreach ($periods as $period) {
      if (

        in_array($period->format('N'), $workingDays) &&
        !in_array($period->format('Y-m-d'), $holidayDays) &&
        !in_array($period->format('Y-m-d'), $additionalHolidays) ||

        in_array($period->format('Y-m-d'), $additionalWorkingDays)

      ) {
        $days++;
      }
    }
    return $days;

  }

  public function createConsultation ($params) {

    $system = get_current_system();

    $question = $params->question;

    $type = !empty($params->type) ? $params->type : 'consultation';

    $answer_date_end = null;

    $consultation_types = ['consultation', 'notfound', 'help', 'interview', 'crib'];
    //$request_types = ['norm', 'example', 'instruction'];

    if ( in_array($type, $consultation_types) ) {
      $user = \Drupal::currentUser();

      $index = \Drupal\search_api\Entity\Index::load('default');
      $query = $index->query();
      $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
      $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
      $query->setParseMode($parse_mode);
      $query->sort('search_api_relevance', 'DESC');
      $query->range(0, 1)
        ->addCondition('field_systems', $system->id, 'IN')
        ->addCondition('uid', $user->Id())
        ->addCondition('field_consultation_type', $consultation_types, 'IN');
      $query->sort('field_answer_date', 'DESC');
      $result = $query->execute();

      $items = array_keys($result->getResultItems());
      $nid = 0;
      $node = false;

      if ( !empty($items) ) {
        $nid = explode(':uk', explode('entity:node/', current($items))[1])[0];
      }
      if ( !empty($nid) ) {
        $node = Node::load($nid);
      }

      $set_answer_date = true;
      $answer_date_begin = date('Y-m-d', time());

      if ( !empty($node) && !empty($node->field_answer_date->getValue()) ) {

        $last_answer_date = $node->field_answer_date->getValue()[0]['value'];

        if ( !empty($last_answer_date) ) {

          $last_answer_date = date_create_from_format('Y-m-d', $last_answer_date);
          $last_answer_date = date('Y-m-d', $last_answer_date->getTimestamp());

          if ( $answer_date_begin < $last_answer_date ) {
            $answer_date_begin = $last_answer_date;
            $set_answer_date = false;
          }

        }

      }

      $year = date('Y', strtotime($answer_date_begin));

      $holidays = self::getHolidays((object)['year' => $year]);
      $additional_holidays = self::getAdditionalHolidays((object)['year' => $year]);
      $working_days = self::getWorkingdays((object)['year' => $year]);
      $date = date_create_from_format('Y-m-d', $answer_date_begin);
      date_add($date,date_interval_create_from_date_string("4 days"));
      $answer_date_end = $date->format('Y-m-d');

      while ( self::getWorkingDaysCount($answer_date_begin, $answer_date_end, $working_days, $holidays, $additional_holidays ) <= 4 ) {
        $date = date_create_from_format('Y-m-d', $answer_date_end);
        date_add($date,date_interval_create_from_date_string("1 days"));
        $answer_date_end = $date->format('Y-m-d');
      }

    }

    $node_array = [

      'type' => 'consultation',

      'field_consultation_type' => $type,

      'title' => mb_substr($question, 0, 220, 'UTF-8') . '...',

      'field_question' => [
        'value' => $question
      ],

    ];

    if ( $set_answer_date ) {
      $node_array['field_answer_date'] = $answer_date_end;
    }

    $node = Node::create($node_array);

    $node->set('field_systems', [$system->id]); //1 = экология 2 = от 3 = кадровик
    $node->set('field_status', 'new'); // 43 нове

    if ( !empty($params->file) ) {
      $tmp_name = $params->file['tmp_name'];
      $data = file_get_contents($tmp_name);
      $file_name = konsultant_transliteration_filename($params->file['name']);
      $directory = 'public://user_files/';
      if ( file_prepare_directory($directory, FILE_CREATE_DIRECTORY) ) {

        $file = file_save_data($data, $directory . $file_name);
        $node->field_file->setValue([
           'target_id' => $file->id(),
        ]);

      };

    }

    $node->save();

    $currentUser = \Drupal::currentUser();
    $currentUser = user_load($currentUser->Id());

    $user_storage = \Drupal::service('entity_type.manager')->getStorage('user');

    $vars = [];

    if ( in_array($type, $consultation_types) ) {
      $tmpl_name = 'new-consultation';
      $subject = 'Новий запит консультації у системі ' . $system->name;
      $vars['consultations-list-link'] = $system->domain . '/admin/editor-consultations';
      $ids = $user_storage->getQuery()
        ->condition('status', 1)
        ->condition('field_system', $system->id)
        ->condition('roles', 'editor')
        ->execute();
    } else {
      $tmpl_name = 'new-consultation-norm';
      $subject = 'Новий запит документа у системі ' . $system->name;
      $vars['consultations-list-link'] = $system->domain . '/admin/editor-norm-consultations';
      $ids = $user_storage->getQuery()
        ->condition('status', 1)
        ->condition('roles', 'editor_norm')
        ->execute();
    }

    $editors = $user_storage->loadMultiple($ids);

    $ids = [];

    $vars['consultation-text'] = $question;

    $vars['company-name'] = '';
    if ( !empty($currentUser) && !empty($currentUser->field_company->getValue()) ) {
      $vars['company-name'] = $currentUser->field_company->getValue()[0]['value'];
    }

    $tmpl = get_system_template_email($tmpl_name, $vars);
    if ( !empty( $tmpl->subject ) ) {
      $subject = $tmpl->subject;
    }

    foreach ( $editors as $editor ) {
      $ids[] = $editor->Id();
      $user_id = $editor->Id();
      konsultant_send_message($user_id, $subject, $tmpl->html, $tmpl);
    }

    konsultant_create_message($ids, $subject, $tmpl->html);

    $data = (object)[
      'newcid' => $node->Id(),
      'answer_date' => $set_answer_date ? $answer_date_end : null
    ];

    return $data;

  }

  public function setStatus ($params) {

    $cid = $params->cid;
    $status = $params->status;
    $message = !empty($params->message) ? $params->message : '';
    $node = node_load($cid);

    $result = false;

    $currentUser = \Drupal::currentUser();
    $currentUser = user_load($currentUser->Id());

    if ( !empty($node) ) {
      $node->set('field_status', $status);
      $node->save();
      $result = true;

      switch ( $status ) {

        case 'clarification':

          $user_storage = \Drupal::service('entity_type.manager')->getStorage('user');

          $ids = $user_storage->getQuery()
            ->condition('status', 1)
            ->condition('roles', 'editor')
            ->condition('field_system', $system->id)
            ->execute();

          $editors = $user_storage->loadMultiple($ids);
          $subject = 'Експерт повернув запит на уточнення';

          if ( !empty($currentUser) && !empty($currentUser->field_fio->getValue()) ) {
            $message = 'Запит на уточнення для консультації у системі.<br\>Експерт: ' . $currentUser->field_fio->getValue()[0]['value'];
          } else {
            $message = 'Запит на уточнення для консультації у системі.';
          }
          $ids = [];
          foreach ( $editors as $editor ) {
            $ids[] = $editor->Id();
            konsultant_send_message($editor->Id(), $subject, $message);
          }
          konsultant_create_message($ids, $subject, $message);
        break;

        case 'rejected':

          $user_storage = \Drupal::service('entity_type.manager')->getStorage('user');

          $ids = $user_storage->getQuery()
            ->condition('status', 1)
            ->condition('roles', 'editor')
            ->condition('field_system', $system->id)
            ->execute();

          $editors = $user_storage->loadMultiple($ids);
          $subject = 'Експерт відмовився від запиту консультації';

          if ( !empty($currentUser) && !empty($currentUser->field_fio->getValue()) ) {
            $message = 'Експерт відмовився від запиту консультації у системі.<br\>Експерт: ' . $currentUser->field_fio->getValue()[0]['value'];
          } else {
            $message = 'Експерт відмовився від запиту консультації.';
          }

          $ids = [];
          foreach ( $editors as $editor ) {
            $ids[] = $editor->Id();
            konsultant_send_message($editor->Id(), $subject, $message);
          }
          konsultant_create_message($ids, $subject, $message);

        break;

      }

    }

    $data = (object)[
      'result' => $result
    ];

    return $data;

  }


  public function clarify ($params) {
    $system = get_current_system();
    $cid = $params->cid;
    $clarification_answer = !empty($params->clarification_answer) ? $params->clarification_answer : '';
    $node = node_load($cid);

    $result = false;

    if ( !empty($node) ) {
      $node->set('field_status', 'clarified');

      if ( !empty($node->field_editor_clarification->getValue()) ) {
        $clarification_ids = $node->field_editor_clarification->getValue();
        $clarification_id = end($clarification_ids);
        $clarification = \Drupal\paragraphs\Entity\Paragraph::load( $clarification_id['target_id'] );
        $clarification->field_user_answer->value = $clarification_answer;
        $clarification->save();
      }

      $node->save();
      $result = true;

      $user_storage = \Drupal::service('entity_type.manager')->getStorage('user');

      $ids = $user_storage->getQuery()
        ->condition('status', 1)
        ->condition('roles', 'editor')
        ->condition('field_system', $system->id)
        ->execute();

      $editors = $user_storage->loadMultiple($ids);
      $subject = 'Користувач надав уточнення';

      $ids = [];

      foreach ( $editors as $editor ) {
        $ids[] = $editor->Id();
        konsultant_send_message($editor->Id(), $subject, $subject);
      }

      konsultant_create_message($ids, $subject, $subject);

    }

    $data = (object)[
      'result' => $result
    ];

    return $data;

  }

  public function clarification ($params) {
    $system = get_current_system();
    $cid = $params->cid;
    $clarification_request = !empty($params->clarification_request) ? $params->clarification_request : '';
    $node = node_load($cid);

    $result = false;

    if ( !empty($node) ) {
      $node->set('field_status', 'clarification');

      $clarifications_array = [];
      if ( !empty($node->field_editor_clarification->getValue()) ) {
       $clarification_ids = $node->field_editor_clarification->getValue();
       foreach ($clarification_ids as $clarification_id) {
         $paragraph = \Drupal\paragraphs\Entity\Paragraph::load($clarification_id['target_id']);
         $clarifications_array[] = [
           'target_id' => $paragraph->id(),
           'target_revision_id' => $paragraph->getRevisionId(),
         ];
       }
     }

      $paragraph_array = [
        'type' => 'clarification',
        'field_editor_question' => $clarification_request,
        'field_user_answer' => ''
      ];

      $paragraph = \Drupal\paragraphs\Entity\Paragraph::create($paragraph_array);
      $paragraph->isNew();
      $paragraph->save();

      $new_clarifications_array[] = [
        'target_id' => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId(),
      ];

      $clarifications_array = array_merge($clarifications_array, $new_clarifications_array);
      $node->set('field_editor_clarification', $clarifications_array);


      $node->save();
      $result = true;

      $vars = [];

      $author_id = $node->getOwner()->Id();
      $user_owner = user_load($author_id);
      if ( !empty($user_owner) && !empty($user_owner->field_fio->getValue()) && !empty($user_owner->field_fio->getValue()[0]['value']) ) {
        $vars['user-name'] = $user_owner->field_fio->getValue()[0]['value'];
      }

      $vars['consultation-url'] = $system->domain . '/user/login?destination=' . urlencode('/consultations/own/' . $node->Id());

      $tmpl = get_system_template_email('clarification-request', $vars);

      $subject = 'Ваш запит потребує уточнення';
      if ( !empty($tmpl->$subject) ) {
        $subject = $tmpl->$subject;
      }

      konsultant_create_message([$author_id], $subject, $tmpl->html);
      konsultant_send_message($author_id, $subject, $tmpl->html, $tmpl);

    }

    $data = (object)[
      'result' => $result
    ];

    return $data;

  }

  public function saveConsultation ($params) {

    $cid = $params->cid;
    $answer = $params->answer;
    $node = node_load($cid);

    $result = false;

    if ( !empty($node) ) {
      $node->set('field_answer', $answer);

      if ( !empty($params->file) ) {
        $tmp_name = $params->file['tmp_name'];
        $data = file_get_contents($tmp_name);
        $file_name = konsultant_transliteration_filename($params->file['name']);
        $directory = 'public://expert_files/';
        if ( file_prepare_directory($directory, FILE_CREATE_DIRECTORY) ) {

          $file = file_save_data($data, $directory . $file_name);
          $node->field_file_expert->setValue([
             'target_id' => $file->id(),
          ]);

        };
      } else {
        if ( !empty($params->removeFile) ) {
          $node->field_file_expert->setValue(null);
        }
      }

      $node->save();
      $result = true;
    }

    $data = (object)[
      'result' => $result
    ];

    return $data;

  }

  public function submitAnswer ($params) {

    $currentUser = \Drupal::currentUser();
    $currentUser = user_load($currentUser->Id());

    $cid = $params->cid;
    $answer = $params->answer;
    $node = node_load($cid);
    $system = get_current_system();
    $result = false;

    if ( !empty($node) ) {
      $node->set('field_answer', $answer);
      $node->set('field_status', 'answered');
      if ( !empty($params->file) ) {
        $tmp_name = $params->file['tmp_name'];
        $data = file_get_contents($tmp_name);
        $file_name = konsultant_transliteration_filename($params->file['name']);
        $directory = 'public://expert_files/';
        if ( file_prepare_directory($directory, FILE_CREATE_DIRECTORY) ) {

          $file = file_save_data($data, $directory . $file_name);
          $node->field_file_expert->setValue([
             'target_id' => $file->id(),
          ]);

        };

      } else {
        if ( !empty($params->removeFile) ) {
          $node->field_file_expert->setValue(null);
        }
      }
      $node->save();
      $result = true;

      $user_storage = \Drupal::service('entity_type.manager')->getStorage('user');

      $ids = $user_storage->getQuery()
        ->condition('status', 1)
        ->condition('roles', 'editor')
        ->condition('field_system', $system->id)
        ->execute();

      $editors = $user_storage->loadMultiple($ids);

      $tmpl_name = 'new-answer';
      $subject = 'Надано відповідь на запит консультації в системі ' . $system->name;

      $vars = [];

      if ( !empty($currentUser) && !empty($currentUser->field_fio->getValue()) ) {
        $vars['expert-name'] = $currentUser->field_fio->getValue()[0]['value'];
      }

      $vars['consultations-list-link'] = $system->domain . '/admin/editor-consultations';

      $ids = [];
      $tmpl = get_system_template_email($tmpl_name, $vars);
      if ( !empty( $tmpl->subject ) ) {
        $subject = $tmpl->subject;
      }

      foreach ( $editors as $editor ) {
        $ids[] = $editor->Id();
        $user_id = $editor->Id();
        konsultant_send_message($user_id, $subject, $tmpl->html, $tmpl);
      }

      konsultant_create_message($ids, $subject, $tmpl->html);

    }

    $data = (object)[
      'result' => $result
    ];

    return $data;

  }

  public function saveNode ($params) {

    $nid = $params->nid;
    $node = node_load($nid);
    if ( !empty($node) ) {

      $flag_service = \Drupal::service('flag');
      $flag = $flag_service->getFlagById('favorites');
      $favorite = false;
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }

      if ( !$favorite ) {
        $flag_service->flag($flag, $node);
      }

    }

    return (object)[
      'state' => true
    ];

  }

  public function unsaveNode ($params) {

    $nid = $params->nid;
    $node = node_load($nid);
    if ( !empty($node) ) {

      $flag_service = \Drupal::service('flag');
      $flag = $flag_service->getFlagById('favorites');
      $favorite = false;
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }

      if ( $favorite ) {
        $flag_service->unflag($flag, $node);
      }

    }

    return (object)[
      'state' => false
    ];

  }

  public function unsaveNodes ($params) {
    $nids = $params->favorites;
    $deletedCount = 0;

    foreach ( $nids as $nid ) {

      $node = node_load($nid);
      if ( !empty($node) ) {

        $flag_service = \Drupal::service('flag');
        $flag = $flag_service->getFlagById('favorites');
        $favorite = false;
        $currentUser = \Drupal::currentUser();
        if ( !empty($currentUser->Id()) ) {
          $favorite = $flag->isFlagged($node, $currentUser);
        }

        if ( $favorite ) {
          $flag_service->unflag($flag, $node);
          $deletedCount++;
        }

      }

    }

    return (object)[
      'deletedCount' => false
    ];

  }

  private function is_demo_user(){

    $user = user_load(\Drupal::currentUser()->Id());
    $roles = $user->getRoles();
    return in_array('demo', $roles);

  }

  public function getConsultationsList ($params) {

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }
    $page = $params->page;
    $itemsPerPage = 7;

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $request_types = ['norm', 'example', 'instruction'];

    $query->range($page * $itemsPerPage, $itemsPerPage)
      ->addCondition('field_consultation_type', $request_types, 'NOT IN')
      ->addCondition('field_systems', $systems, 'IN')
      ->addCondition('field_status', ['viewed','placed'], 'IN')
      ->addCondition('field_user_only', 1, '<>')
      ->addCondition('type', 'consultation')
      ->addCondition('status', '1');
    if ( empty($filters->key) ) {
      $query->sort('field_sort_date', 'DESC');
    } else {
      $query->sort('search_api_relevance', 'DESC');
    }
    self::setQueryFilters($query, $filters, 'consultation');
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };

    $totalCount = $result->getResultCount();

    if ( !empty($filters->key) && $totalCount > 35 ) {
      $totalCount = 35;
    }

    $nodes = node_load_multiple($nids);

    $list = [];

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    foreach ( $nodes as $node ) {

      $demo = !empty($node->get('field_demo')->getValue()[0]['value']);

      $favorite = false;
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }

      if ( !empty($node->field_sort_date->getValue()) ) {
        $date = \Drupal::service('date.formatter')->format($node->field_sort_date->getValue()[0]['value'], 'date_reversed');
      } else {
        $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');
      }

      $file = false;

      if ( !empty($node->get('field_file_expert')->getValue()) ) {
        $file = $node->get('field_file_expert')->getValue()[0]['target_id'];
      }

      if ( !empty($node->get('field_status')->getValue()) ) {
        $status_id = $node->get('field_status')->getValue()[0]['value'];
        switch($status_id) {
          case 'viewed':
          case 'placed':
            $status = 'ready';
          break;
          default:
            $status = 'progress';
          break;
        }
      }

      $expert = false;

      $expert_fio = '';
      $expert_id = 0;
      $expert_photo = '';
      $expert_position = '';

      if ( !empty($node->get('field_expert')->getValue()) ) {
        $expert_id = $node->get('field_expert')->getValue()[0]['target_id'];
        $expert = user_load($expert_id);

        if ( !empty($expert) ) {

          if ( !empty($expert->get('field_fio')->getValue()) ) {
            $expert_fio = $expert->get('field_fio')->getValue()[0]['value'];
          }

          if ( !empty($expert->get('field_position')->getValue()) ) {
            $expert_position = $expert->get('field_position')->getValue()[0]['value'];
          }

          if ( !empty($expert->get('field_photo')->getValue()) ) {

            $fid = $expert->get('field_photo')->getValue()[0]['target_id'];

        		if ( $fid ) {

              $expert_photo_uri = file_load($fid)->getFileUri();

              $style = ImageStyle::load('photo_expert');

              $expert_photo = file_url_transform_relative($style->buildUrl($expert_photo_uri));

        		};

          }

        }

      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'demo' => $demo,
        'title' => $node->getTitle(),
        'status' => $status,
        'file' => $file,
        'favorite' => $favorite,
        'created' => $date,
        'expert' => (object)[
          'id' => $expert_id,
          'fio' => $expert_fio,
          'photo' => $expert_photo,
          'position' => $expert_position
        ]
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getHelpList ($params) {

    $system = get_current_system();
    $page = $params->page;
    $itemsPerPage = 7;

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    $query->sort('search_api_relevance', 'DESC');
    $query->range($page * $itemsPerPage, $itemsPerPage)
      ->addCondition('field_systems', $system->id, 'IN')
      ->addCondition('type', 'help')
      ->addCondition('status', '1');
    if ( empty($filters->key) ) {
      $query->sort('changed', 'DESC');
    }
    self::setQueryFilters($query, $filters, 'help');
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };

    $totalCount = $result->getResultCount();

    if ( !empty($filters->key) && $totalCount > 35 ) {
      $totalCount = 35;
    }

    $nodes = node_load_multiple($nids);

    $list = [];

    foreach ( $nodes as $node ) {

      $answer = '';

      if ( !empty($node->get('field_help_html')->getValue()) ) {
        $answer = $node->get('field_help_html')->getValue()[0]['value'];
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'title' => $node->getTitle(),
        'answer' => $answer,
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getHomeBlocks ($params) {

    $news = KonsultantTwigExtension::home_block_news();
    $consultations = KonsultantTwigExtension::home_block_consultations();
    $norm = KonsultantTwigExtension::home_block_norm();
    $interviews = KonsultantTwigExtension::home_block_interviews();
    $examples = KonsultantTwigExtension::home_block_examples();
    $accountinglogs = KonsultantTwigExtension::home_block_accountinglogs();
    $cribs = KonsultantTwigExtension::home_block_cribs();

    $data = (object)[
      'news' => $news ? $news : [],
      'consultations' => $consultations ? $consultations : [],
      'norm' => $norm ? $norm : [],
      'interviews' => $interviews ? $interviews : [],
      'examples' => $examples ? $examples : [],
      'accountinglogs' => $accountinglogs ? $accountinglogs : [],
      'cribs' => $cribs ? $cribs : [],
    ];

    return $data;
  }

  public static function setQueryFilters ( &$query, &$filters, $entity_type ){

    $themes = [];

    if ( !empty($filters->themes) ) {
      $themes = $filters->themes;
      foreach ( $filters->themes as $theme ) {

        $child_themes = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('filters', $theme, null, false);

        if ( !empty($child_themes) ) {

          foreach ( $child_themes as $child_theme ) {
            $themes[] = $child_theme->tid;
          }

        }

      }

    }

    $words = [];
    $filters->keys_search = [];
    $filters->keys_highlight = [];

    if ( !empty($filters->key) ) {

      $filters->key = preg_replace('/[\)\(":\.\,!?\/]/', ' ', $filters->key);
      $filters->key = mb_strtolower($filters->key);

      //$words = explode(' ', preg_replace('!\s+!', ' ', trim($filters->key)));
      $words = trim(str_replace('№', '№ ', $filters->key));
      $words = preg_replace('!\s+!', ' ', $words);
      $words = explode(' ', $words);

      foreach ($words as $word) {
          $key = \UkrainianStemmer::stem($word);
          $filters->keys_highlight[] = $word;
          $filters->keys_search[] = $word;
          if ( $word != $key ) {
            $filters->keys_highlight[] = $key;
            $filters->keys_search[] = $key;
          }
      }

    }

    switch ($entity_type) {

      case 'normdoc':

        if ( !empty($filters->status) ) {
          $query->addCondition('field_document_status', $filters->status);
        }

        if ( !empty($filters->number) || !empty($filters->keys_search) ) {

          if ( !empty($filters->number) ) {
            //$num = preg_replace('/["\)\(:\.\-\,!?\/]/', '', $filters->number);
            //$filters->number = str_replace('№', '№ ', $filters->number);
            //$filters->number = preg_replace('!\s+!', ' ', trim($filters->number));

            //$filters->keys_search[] = $filters->number;
            //$filters->keys_search[] = '"' . $filters->number . '"';
            $filters->keys_highlight[] = $filters->number;

            //$filters->keys_search[] = '"' . $filters->number . '"';// . '^21';
            //$numbers = explode(' ', $filters->number);

            // foreach ($numbers as $number) {
            //   $filters->keys_search[] = '"' . $number . '"';
            //
            // }

          }

          if ( !empty($filters->keys_search) ) {
            $query->keys(implode(' ', array_unique($filters->keys_search)));
          }

        }

        if ( !empty($filters->number) ) {
          $query->addCondition('field_document_number', $filters->number);
        }


        if ( !empty($filters->doc_type) ) {
          $query->addCondition('field_doc_type', $filters->doc_type);
        }

        if ( !empty($filters->doc_producer) ) {
          $query->addCondition('field_doc_producer', $filters->doc_producer);
        }

        if ( !empty($filters->date_from) ) {
          $date = \DateTime::createFromFormat('d.m.Y', $filters->date_from)->getTimestamp();
          $query->addCondition('field_date_init', $date, '>=');
        }

        if ( !empty($filters->date_to) ) {
          $date = \DateTime::createFromFormat('d.m.Y', $filters->date_to)->getTimestamp();
          $query->addCondition('field_date_init', $date, '<=');
        }

        if ( !empty($themes) ) {
          $query->addCondition('field_filter', $themes, 'IN');
        }

      break;

      case 'example':

        if ( !empty($filters->keys_search) ) {
          $query->keys(implode(' ', $filters->keys_search));
        }

        if ( !empty($filters->example_type) ) {
          $query->addCondition('field_example_type', $filters->example_type);
        }

        if ( !empty($themes) ) {
          $query->addCondition('field_filter', $themes, 'IN');
        }

        if ( isset($filters->with_example) && $filters->with_example != 3 ) {

          switch ( $filters->with_example ) {
            case 1:
              $query->addCondition('field_with_example', 1);
            break;
            default:
              $query->addCondition('field_with_example', 1, '<>');
              // $group = $query->orConditionGroup()
              //   ->condition('field_with_example', 0)
              //   ->condition('field_with_example', NULL, 'IS NULL');
              // $query->condition($group);
            break;
          }

        }

      break;

      case 'accountinglog':

        if ( !empty($filters->keys_search) ) {
          $query->keys(implode(' ', $filters->keys_search));
        }

        if ( !empty($themes) ) {
          $query->addCondition('field_filter', $themes, 'IN');
        }

      break;

      case 'crib':

        if ( !empty($filters->keys_search) ) {
          $query->keys(implode(' ', array_unique($filters->keys_search)));
        }

        if ( !empty($themes) ) {
          $query->addCondition('field_filter', $themes, 'IN');
        }

      break;

      case 'profession':

        if ( !empty($filters->letter) ) {
          $query->addCondition('field_letter', mb_strtoupper($filters->letter));
        }

        if ( !empty($filters->keys_search) ) {
          $query->keys(implode(' ', array_unique($filters->keys_search)));
        }

      break;

      case 'interview':

        if ( !empty($filters->keys_search) ) {
          $query->keys(implode(' ', array_unique($filters->keys_search)));
        }

        if ( !empty($themes) ) {
          $query->addCondition('field_filter', $themes, 'IN');
        }

      break;

      case 'new':

        if ( !empty($filters->keys_search) ) {
          $query->keys(implode(' ', array_unique($filters->keys_search)));
        }

        if ( !empty($filters->new_type) ) {
          $query->addCondition('field_new_type', $filters->new_type);
        }

        if ( !empty($filters->time_range) ) {
          $date = new \DateTime();
          $begin = strtotime($date->format('Y-m-d 23:59:59'));
          switch ( $filters->time_range ) {
            case 'year': $date->modify( '-1 year' ); break;
            case 'half': $date->modify( '-6 month' ); break;
            case 'month': $date->modify( '-1 month' ); break;
            case 'week': $date->modify( '-7 day' ); break;
          }
          $end = strtotime($date->format('Y-m-d'));
          $query->addCondition('field_sort_date', $begin, '<=');
          $query->addCondition('field_sort_date', $end, '>=');
        }

        if ( !empty($themes) ) {
          $query->addCondition('field_filter', $themes, 'IN');
        }

      break;

      case 'job':

        if ( !empty($filters->keys_search) ) {
          $query->keys(implode(' ', array_unique($filters->keys_search)));
        }

      break;

      case 'message':

        if ( !empty($filters->keys_search) ) {
          $query->keys(implode(' ', array_unique($filters->keys_search)));
        }

      break;

      case 'search':

        if ( !empty($filters->keys_search) ) {
          $query->keys(implode(' ', array_unique($filters->keys_search)));
        }

        if ( !empty($filters->time_range) ) {
          $date = new \DateTime();
          $begin = strtotime($date->format('Y-m-d 23:59:59'));
          switch ( $filters->time_range ) {
            case 'year': $date->modify( '-1 year' ); break;
            case 'half': $date->modify( '-6 month' ); break;
            case 'month': $date->modify( '-1 month' ); break;
            case 'week': $date->modify( '-7 day' ); break;
          }
          $end = strtotime($date->format('Y-m-d'));
          $query->addCondition('field_sort_date', $begin, '<=');
          $query->addCondition('field_sort_date', $end, '>=');
        }

        if ( !empty($themes) ) {
          $query->addCondition('field_filter', $themes, 'IN');
        }

      break;

      case 'help':

        if ( !empty($filters->keys_search) ) {
          $query->keys(implode(' ', array_unique($filters->keys_search)));
        }

      break;

      case 'instruction':

        if ( !empty($filters->keys_search) ) {
          $query->keys(implode(' ', array_unique($filters->keys_search)));
        }

        if ( !empty($themes) ) {
          $query->addCondition('field_filter', $themes, 'IN');
        }

        if ( isset($filters->instruction_type) && $filters->instruction_type != 3 ) {

          switch ( $filters->instruction_type ) {
            case 1:
              $query->addCondition('field_instruction_type', 1);
            break;
            case 2:
              $query->addCondition('field_instruction_type', 2);
            break;
            default:
            $query->addCondition('field_instruction_type', [1, 2], 'NOT IN');
              // $group = $query->orConditionGroup()
              //   ->condition('field_instruction_type', 0)
              //   ->condition('field_instruction_type', NULL, 'IS NULL');
              // $query->condition($group);
            break;
          }

        }

      break;

      case 'consultation':

        if ( !empty($filters->keys_search) ) {
          $query->keys(implode(' ', array_unique($filters->keys_search)));
        }

        if ( !empty($filters->time_range) ) {
          $date = new \DateTime();
          $begin = strtotime($date->format('Y-m-d 23:59:59'));
          switch ( $filters->time_range ) {
            case 'year': $date->modify( '-1 year' ); break;
            case 'half': $date->modify( '-6 month' ); break;
            case 'month': $date->modify( '-1 month' ); break;
            case 'week': $date->modify( '-7 day' ); break;
          }
          $end = strtotime($date->format('Y-m-d'));
          $query->addCondition('field_sort_date', $begin, '<=');
          $query->addCondition('field_sort_date', $end, '>=');
        }

        if ( !empty($themes) ) {
          $query->addCondition('field_filter', $themes, 'IN');
        }

      break;

    }

  }

  public function getOrientationNodes ($params) {

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }

    $cat = $params->cat;

    $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
      'field_orientation' => $cat
    ]);

    //$nodes = node_load_multiple($nodes);

    $list = [];

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    foreach ( $nodes as $node ) {

      $demo = !empty($node->get('field_demo')->getValue()[0]['value']);

      $favorite = false;
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }

      if ( !empty($node->get('field_sort_date')->getValue()) ) {
        $date = \Drupal::service('date.formatter')->format($node->get('field_sort_date')->getValue()[0]['value'], 'date_reversed');
      } else {
        $date = \Drupal::service('date.formatter')->format($node->getCreatedTime(), 'date_reversed');
      }

      $document_number = '';

      if ( !empty($node->get('field_document_number')->getValue()) ) {
        $document_number = $node->get('field_document_number')->getValue()[0]['value'];
      }

      $date_init = '';

      if ( !empty($node->get('field_date_init')->getValue()) ) {
        $date_init = $node->get('field_date_init')->getValue()[0]['value'];
        $date_init = \Drupal::service('date.formatter')->format(strtotime($date_init), 'date_reversed');
      }

      $date_invalid = '';

      if ( !empty($node->get('field_date_invalid')->getValue()) ) {
        $date_invalid = $node->get('field_date_invalid')->getValue()[0]['value'];
        $date_invalid = \Drupal::service('date.formatter')->format(strtotime($date_invalid), 'date_reversed');
      }

      $date_partially_invalid = '';

      if ( !empty($node->get('field_date_partially_invalid')->getValue()) ) {
        $date_partially_invalid = $node->get('field_date_partially_invalid')->getValue()[0]['value'];
        $date_partially_invalid = \Drupal::service('date.formatter')->format(strtotime($date_partially_invalid), 'date_reversed');
      }

      $document_date_valid = '';

      if ( !empty($node->field_documents) ) {

        foreach ( $node->field_documents as $key => $item) {
          $item = $item->value;
          $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($item);

          if ( !empty($fc->field_document_date_valid->getValue()) ) {
            $document_date_valid = $fc->field_document_date_valid->getValue()[0]['value'];
          }

          if ( !empty($document_date_valid) ) {
            $document_date_valid = \Drupal::service('date.formatter')->format(strtotime($document_date_valid), 'date_reversed');
          }
        }

      }

      $doc_type = '';

      if ( !empty($node->get('field_doc_type')->getValue()) ) {
        $doc_type_id = $node->get('field_doc_type')->getValue()[0]['target_id'];
        $doc_type_term = taxonomy_term_load($doc_type_id);
        if ( !empty($doc_type_term) ) {
          $doc_type = $doc_type_term->getName();
        }
      }

      $document_status = '';
      $document_status_label = '';
      if ( !empty($node->get('field_document_status')->getValue()) ) {
        $document_status = $node->get('field_document_status')->getValue()[0]['value'];
        $statuses = KonsultantTwigExtension::doc_statuses();
        $document_status_label = $statuses[$document_status];
      }

      $doc_producer = '';
      $doc_producer_full = '';
      if ( !empty($node->get('field_doc_producer')->getValue()) ) {
        $doc_producer_id = $node->get('field_doc_producer')->getValue()[0]['target_id'];
        $doc_producer_term = taxonomy_term_load($doc_producer_id);
        if ( !empty($doc_producer_term) ) {
          $doc_producer = $doc_producer_term->getName();
          $doc_producer_full = $doc_producer;
          if ( !empty($doc_producer_term->field_name_short->getValue()) && !empty($doc_producer_term->field_name_short->getValue()[0]['value']) ) {
            $doc_producer = $doc_producer_term->field_name_short->getValue()[0]['value'];
          }
        }
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'demo' => $demo,
        'title' => $node->getTitle(),
        'doc_type' => $doc_type,
        'date_init' => $date_init,
        'date_invalid' => $date_invalid,
        'date_partially_invalid' => $date_partially_invalid,
        'document_date_valid' => $document_date_valid,
        'document_number' => $document_number,
        'document_status' => $document_status,
        'document_status_label' => $document_status_label,
        'doc_producer' => $doc_producer,
        'doc_producer_full' => $doc_producer_full,
        'created' => $date,
      ];

    }

    $data = (object)[
      'list' => $list,
      //'filters' => $filters,
      //'totalCount' => $totalCount,
      //'page' => $page,
      //'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  private static function get_cache($cid , $callback) {
    if ($cache = \Drupal::cache()->get($cid)) {
      $cdata = $cache->data;
    } else {
      $data = call_user_func($callback);
      \Drupal::cache()->set($cid, $data->data, empty($data->expire) ? Cache::PERMANENT : $data->expire, $data->tags);
      $cdata = $data->data;
    }
    return $cdata;
  }

  public static function addTreeItem ( &$branch, $item ) {

    if ( $item->parent === 0 ) {
      $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['field_dkhp' => $item->id]);
      foreach ($nodes as $node) {
        $item->items[] = (object)[
          'title' => $node->getTitle(),
          'nid' => $node->Id(),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id())
        ];
      }
      $branch[] = $item;
      return;
    }

    foreach ($branch as $branchItem) {
      if ( $branchItem->id == $item->parent ) {
        $branchItem->subtree[] = $item;
        $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['field_dkhp' => $item->id]);
        foreach ($nodes as $node) {
          $item->items[] = (object)[
            'title' => $node->getTitle(),
            'nid' => $node->Id(),
            'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id())
          ];
        }
        return;
      } else {
        if ( count($branchItem->subtree) ) {
          self::addTreeItem ( $branchItem->subtree, $item );
        }
      }
    }

  }

  public function getOrientationTree () {

    $system = get_current_system();
    $cid = 'konsultant:orientations_tree:'. $system->id;
    $tree = self::get_cache($cid, function() use ($system) {
      $systems = [$system->id];
      if ( !empty($system->parent_id) ) {
        $systems[] = $system->parent_id;
      }
      $vid = 'dkhp';
      $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid, false, null, false);
      $list = [];
      foreach ($terms as $term) {
        $query = \Drupal::database()->select('taxonomy_term_data', 'ttd');
        $query->leftJoin('taxonomy_term__field_empty', 'fe', 'fe.entity_id=ttd.tid');
        $query->condition('ttd.vid', 'dkhp');
        $query->condition('fe.field_empty_value', '1');
        $query->condition('fe.entity_id', $term->tid);
        $query->fields('fe', ['field_empty_value']);
        $result = $query->execute();
        $empty = $result->fetchObject();
        $list[] = (object)[
          'id' => intval($term->tid),
          'parent' => intval(reset($term->parents)),
          'label' => $term->name,
          'empty' => !empty($empty),
          'items' => [],
          'subtree' => []
        ];
      }
      $tree = [];
      foreach ($list as $item) {
        self::addTreeItem ( $tree, $item );
      }
      foreach ($tree as $item) {
        if ( empty($item->items) && empty($item->subtree) ) {
          $item->empty = true;
        }
      }
      return (object)[
        'tags' => ['taxonomy_term_list:dkhp', 'node_list:profession'],
        'data' => $tree
      ];
    });
    return $tree;
  }

  public function getNormList ($params) {
    $local_norm_search = Settings::get('local_norm_search', 'no');
    if ( $local_norm_search == 'no' ) {
      $system = get_current_system();
      $params->trends = $system->trends;
      $data = self::sendApiCommand('getNormList', $params);
    } else {
      $data = self::getNormListLocal($params);
    }
    return $data;
  }

  public static function getNormListLocal ($params) {

    if ( !empty($params->system) ) {
      $system = get_current_system($params->system);
    } else {
      $system = get_current_system();
    }

    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }
    $page = $params->page;
    $itemsPerPage = 7;
    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $fields = ['field_computed_title', 'title', 'field_question', 'field_help_html', 'field_answer'];
    if ( empty($filters->number) ) {
      $fields[] = 'field_document_number';
    }
    $query->setFulltextFields($fields);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    //$parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('phrase');
    //$parse_mode->setConjunction('OR');
    //$query->setOption('solr_param_pf', ['tm_field_computed_title^21']);
    //$query->setOption('solr_param_q', ['(tm_field_computed_title:"про державн бюджет україн на 2014 рік"~5^2)']);

    $query->setParseMode($parse_mode);
    $query->range($page * $itemsPerPage, $itemsPerPage)
      ->addCondition('type', 'normdoc');

    if ( !empty($params->system) ) {
      $query->addCondition('field_systems', $systems, 'IN');
    }

    if ( empty($filters->key) && empty($filters->number) ) {
      $query->sort('field_sort_date', 'DESC');
    } else {
      $query->sort('search_api_relevance', 'DESC');
    }
    self::setQueryFilters($query, $filters, 'normdoc');
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $totalCount = $result->getResultCount();

    if ( !empty($filters->key) && $totalCount > 35 ) {
      $totalCount = 35;
    }

    $nodes = node_load_multiple($nids);

    $list = [];

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    foreach ( $nodes as $node ) {

      $demo = !empty($node->get('field_demo')->getValue()[0]['value']);

      $favorite = false;
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }

      if ( !empty($node->get('field_sort_date')->getValue()) ) {
        $date = \Drupal::service('date.formatter')->format($node->get('field_sort_date')->getValue()[0]['value'], 'date_reversed');
      } else {
        $date = \Drupal::service('date.formatter')->format($node->getCreatedTime(), 'date_reversed');
      }

      $document_number = '';

      if ( !empty($node->get('field_document_number')->getValue()) ) {
        $document_number = $node->get('field_document_number')->getValue()[0]['value'];
      }

      $date_init = '';

      if ( !empty($node->get('field_date_init')->getValue()) ) {
        $date_init = $node->get('field_date_init')->getValue()[0]['value'];
        $date_init = \Drupal::service('date.formatter')->format(strtotime($date_init), 'date_reversed');
      }

      $date_invalid = '';

      if ( !empty($node->get('field_date_invalid')->getValue()) ) {
        $date_invalid = $node->get('field_date_invalid')->getValue()[0]['value'];
        $date_invalid = \Drupal::service('date.formatter')->format(strtotime($date_invalid), 'date_reversed');
      }

      $date_partially_invalid = '';

      if ( !empty($node->get('field_date_partially_invalid')->getValue()) ) {
        $date_partially_invalid = $node->get('field_date_partially_invalid')->getValue()[0]['value'];
        $date_partially_invalid = \Drupal::service('date.formatter')->format(strtotime($date_partially_invalid), 'date_reversed');
      }

      $document_date_valid = '';

      if ( !empty($node->field_documents) ) {

        foreach ( $node->field_documents as $key => $item) {
          $item = $item->value;
          $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($item);

          if ( !empty($fc->field_document_date_valid->getValue()) ) {
            $document_date_valid = $fc->field_document_date_valid->getValue()[0]['value'];
          }

          if ( !empty($document_date_valid) ) {
            $document_date_valid = \Drupal::service('date.formatter')->format(strtotime($document_date_valid), 'date_reversed');
          }
        }

      }

      $doc_type = '';

      if ( !empty($node->get('field_doc_type')->getValue()) ) {
        $doc_type_id = $node->get('field_doc_type')->getValue()[0]['target_id'];
        $doc_type_term = taxonomy_term_load($doc_type_id);
        if ( !empty($doc_type_term) ) {
          $doc_type = $doc_type_term->getName();
        }
      }

      $document_status = '';
      $document_status_label = '';
      if ( !empty($node->get('field_document_status')->getValue()) ) {
        $document_status = $node->get('field_document_status')->getValue()[0]['value'];
        $statuses = KonsultantTwigExtension::doc_statuses();
        $document_status_label = $statuses[$document_status];
      }

      $doc_producer = '';
      $doc_producer_full = '';
      if ( !empty($node->get('field_doc_producer')->getValue()) ) {
        $doc_producer_id = $node->get('field_doc_producer')->getValue()[0]['target_id'];
        $doc_producer_term = taxonomy_term_load($doc_producer_id);
        if ( !empty($doc_producer_term) ) {
          $doc_producer = $doc_producer_term->getName();
          $doc_producer_full = $doc_producer;
          if ( !empty($doc_producer_term->field_name_short->getValue()) && !empty($doc_producer_term->field_name_short->getValue()[0]['value']) ) {
            $doc_producer = $doc_producer_term->field_name_short->getValue()[0]['value'];
          }
        }
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'demo' => $demo,
        'title' => $node->getTitle(),
        'doc_type' => $doc_type,
        'date_init' => $date_init,
        'date_invalid' => $date_invalid,
        'date_partially_invalid' => $date_partially_invalid,
        'document_date_valid' => $document_date_valid,
        'document_number' => $document_number,
        'document_status' => $document_status,
        'document_status_label' => $document_status_label,
        'doc_producer' => $doc_producer,
        'doc_producer_full' => $doc_producer_full,
        'created' => $date,
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getProfessionList ($params) {

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }
    $page = $params->page;
    $itemsPerPage = 30;
    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $fields = ['title', 'field_profession_description', 'field_profession_code'];
    $query->setFulltextFields($fields);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    $query->range($page * $itemsPerPage, $itemsPerPage)
      //->addCondition('field_systems', $systems, 'IN')
      ->addCondition('type', 'profession')
      ->addCondition('status', '1');
    if ( empty($filters->key) ) {
      $query->sort('title', 'DESC');
    } else {
      $query->sort('search_api_relevance', 'DESC');
    }
    self::setQueryFilters($query, $filters, 'profession');
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $totalCount = $result->getResultCount();

    $nodes = node_load_multiple($nids);

    $list = [];

    //$flag_service = \Drupal::service('flag');
    //$flag = $flag_service->getFlagById('favorites');

    foreach ( $nodes as $node ) {

      $demo = !empty($node->get('field_demo')->getValue()[0]['value']);

      //$favorite = false;
      //$favorite = $flag->isFlagged($node, \Drupal::currentUser());

      $date = \Drupal::service('date.formatter')->format($node->getCreatedTime(), 'date_reversed');

      $profession_description = '';

      if ( !empty($node->get('field_profession_description')->getValue()) ) {
        $profession_description = $node->get('field_profession_description')->getValue()[0]['value'];
      }

      $dkhp_string = [];
      if ( !empty( $node->field_dkhp->getValue() ) ) {
        $term_id = $node->field_dkhp->getValue()[0]['target_id'];
        $terms = \Drupal::service('entity_type.manager')->getStorage("taxonomy_term")->loadAllParents($term_id);
        $term_labels = [];
        foreach ($terms as $term) {
          $term_labels[] = $term->label();
        }
        $term_labels = array_reverse($term_labels);
        $dkhp_string = implode(' -- ', $term_labels);
      }

      $profession_code = '';
      if ( !empty( $node->field_profession_code->getValue() ) ) {
        $profession_code = $node->field_profession_code->getValue()[0]['value'];
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'demo' => $demo,
        'code' => $profession_code,
        'name' => $node->getTitle(),
        'dkhp_string' => $dkhp_string,
        'created' => $date,
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getExamplesList ($params) {

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }
    $systemUser = \Drupal::currentUser()->Id();
    $currentUser = \Drupal::currentUser();
    $isAnonymous = $currentUser->isAnonymous() || in_array('expert', $currentUser->getRoles());
    $page = $params->page;
    $itemsPerPage = 7;

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    $query->sort('search_api_relevance', 'DESC');
    $query->range($page * $itemsPerPage, $itemsPerPage)
      ->addCondition('field_systems', $systems, 'IN')
      ->addCondition('type', 'example')
      ->addCondition('status', '1');
    if ( empty($filters->key) ) {
      $query->sort('field_sort_date', 'DESC');
    }
    self::setQueryFilters($query, $filters, 'example');
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $totalCount = $result->getResultCount();

    if ( !empty($filters->key) && $totalCount > 35 ) {
      $totalCount = 35;
    }

    $nodes = node_load_multiple($nids);

    $list = [];

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    foreach ( $nodes as $node ) {

      $demo = !empty($node->get('field_demo')->getValue()[0]['value']);

      $favorite = false;
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }

      if ( !empty($node->field_sort_date->getValue()) ) {
        $date = \Drupal::service('date.formatter')->format($node->field_sort_date->getValue()[0]['value'], 'date_reversed');
      } else {
        $date = \Drupal::service('date.formatter')->format($node->getCreatedTime(), 'date_reversed');
      }

      $example_type = '';

      if ( !empty($node->get('field_example_type')->getValue()) ) {
        $example_type_id = $node->get('field_example_type')->getValue()[0]['target_id'];
        $example_type_term = taxonomy_term_load($example_type_id);
        if ( !empty($example_term) ) {
          $example_type = $example_type_term->getName();
        }
      }

      $with_example = '';

      if ( !empty($node->get('field_with_example')->getValue()) ) {
        $with_example = $node->get('field_with_example')->getValue()[0]['value'];
      }

      $file = false;
      if ( !empty($node->field_example_file->getValue()) ) {
        $file = \Drupal\file\Entity\File::load($node->field_example_file->getValue()[0]['target_id']);
        if ( !empty($file) ){
          $file = file_url_transform_relative(file_create_url($file->getFileUri()));
          $parts = explode('.', $file);
          $file_type = end($parts);
          if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
            $file = 'demo';
          }
        }
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'demo' => $demo,
        'title' => $node->getTitle(),
        'favorite' => $favorite,
        'example_type' => $example_type,
        'with_example' => $with_example == "1",
        'file' => $isAnonymous ? 'demo' : $file,
        'changed' => $date,
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }
  public function getAccountingLogsList ($params) {

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }
    $systemUser = \Drupal::currentUser()->Id();
    $currentUser = \Drupal::currentUser();
    $isAnonymous = $currentUser->isAnonymous() || in_array('expert', $currentUser->getRoles());
    $page = $params->page;
    $itemsPerPage = 7;

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    $query->sort('search_api_relevance', 'DESC');
    $query->range($page * $itemsPerPage, $itemsPerPage)
      ->addCondition('field_systems', $systems, 'IN')
      ->addCondition('type', 'accountinglog')
      ->addCondition('status', '1');
    if ( empty($filters->key) ) {
      $query->sort('field_sort_date', 'DESC');
    }
    self::setQueryFilters($query, $filters, 'accountinglog');
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $totalCount = $result->getResultCount();

    if ( !empty($filters->key) && $totalCount > 35 ) {
      $totalCount = 35;
    }

    $nodes = node_load_multiple($nids);

    $list = [];

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    foreach ( $nodes as $node ) {

      $demo = !empty($node->get('field_demo')->getValue()[0]['value']);

      $favorite = false;
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }

      if ( !empty($node->field_sort_date->getValue()) ) {
        $date = \Drupal::service('date.formatter')->format($node->field_sort_date->getValue()[0]['value'], 'date_reversed');
      } else {
        $date = \Drupal::service('date.formatter')->format($node->getCreatedTime(), 'date_reversed');
      }

      $file = false;
      if ( !empty($node->field_accountinglog_file->getValue()) ) {
        $file = \Drupal\file\Entity\File::load($node->field_accountinglog_file->getValue()[0]['target_id']);
        if ( !empty($file) ){
          $file = file_url_transform_relative(file_create_url($file->getFileUri()));
          $parts = explode('.', $file);
          if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
            $file = 'demo';
          }
        }
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'demo' => $demo,
        'title' => $node->getTitle(),
        'favorite' => $favorite,
        'file' => $isAnonymous ? 'demo' : $file,
        'changed' => $date,
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getCribsList ($params) {

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }
    $systemUser = \Drupal::currentUser()->Id();
    $page = $params->page;
    $itemsPerPage = 12;

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    $query->sort('search_api_relevance', 'DESC');
    $query->range($page * $itemsPerPage, $itemsPerPage)
      ->addCondition('field_systems', $systems, 'IN')
      ->addCondition('type', 'crib')
      ->addCondition('status', '1');
    if ( empty($filters->key) ) {
      $query->sort('field_sort_date', 'DESC');
    }
    self::setQueryFilters($query, $filters, 'crib');
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $totalCount = $result->getResultCount();

    if ( !empty($filters->key) && $totalCount > 35 ) {
      $totalCount = 35;
    }

    $nodes = node_load_multiple($nids);

    $list = [];

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    foreach ( $nodes as $node ) {

      $demo = !empty($node->get('field_demo')->getValue()[0]['value']);

      $favorite = false;
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }

      if ( !empty($node->field_sort_date->getValue()) ) {
        $date = \Drupal::service('date.formatter')->format($node->field_sort_date->getValue()[0]['value'], 'date_reversed');
      } else {
        $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');
      }

      $file = false;
      if ( !empty($node->field_crib_file->getValue()) ) {
        $file = \Drupal\file\Entity\File::load($node->field_crib_file->getValue()[0]['target_id']);
        if ( !empty($file) ){
          $file = file_url_transform_relative(file_create_url($file->getFileUri()));
          $parts = explode('.', $file);
          $file_type = end($parts);
          if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
            $file = 'demo';
          }
        }
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'demo' => $demo,
        'title' => $node->getTitle(),
        'favorite' => $favorite,
        'file' => false,
        'changed' => $date,
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getInterviewsList ($params) {

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }

    $page = $params->page;
    $itemsPerPage = 10;

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    $query->sort('search_api_relevance', 'DESC');
    $query->range($page * $itemsPerPage, $itemsPerPage)
      ->addCondition('field_systems', $systems, 'IN')
      ->addCondition('type', 'interview')
      ->addCondition('status', '1');
    if ( empty($filters->key) ) {
      $query->sort('created', 'DESC');
    }
    self::setQueryFilters($query, $filters, 'interview');
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $totalCount = $result->getResultCount();

    if ( !empty($filters->key) && $totalCount > 35 ) {
      $totalCount = 35;
    }

    $nodes = node_load_multiple($nids);

    $list = [];

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    foreach ( $nodes as $node ) {

      $demo = !empty($node->get('field_demo')->getValue()[0]['value']);

      $favorite = false;
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }


      $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');

      $with_interview = false;

      if ( !empty($node->get('field_interview')->getValue()) ) {
         $with_interview = count($node->get('field_interview'));
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'demo' => $demo,
        'title' => $node->getTitle(),
        'favorite' => $favorite,
        'with_interview' => $with_interview,
        'changed' => $date,
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getMessagesList ($params) {

    $system = get_current_system();
    $page = $params->page;
    $itemsPerPage = 25;

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $user_id = \Drupal::currentUser()->Id();

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    $query->sort('search_api_relevance', 'DESC');
    $query->range($page * $itemsPerPage, $itemsPerPage)
      ->addCondition('field_systems', $system->id, 'IN')
      ->addCondition('field_users_deleted', $user_id, '<>')
      ->addCondition('type', 'message')
      ->addCondition('status', '1');
    if ( empty($filters->key) ) {
      $query->sort('created', 'DESC');
    }
    $orGroup = $query->createConditionGroup('OR')
      ->addCondition('field_message_to', $user_id)
      ->addCondition('field_message_to_all', 1);
    $query->addConditionGroup($orGroup);
    self::setQueryFilters($query, $filters, 'message');
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $totalCount = $result->getResultCount();

    if ( !empty($filters->key) && $totalCount > 35 ) {
      $totalCount = 35;
    }

    $nodes = node_load_multiple($nids);

    $list = [];

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    foreach ( $nodes as $node ) {

      $favorite = false;
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }

      $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');

      $status = 0;

      $viewed_users = $node->get('field_users_viewed')->getValue();
      if ( !empty($viewed_users) && !empty(array_filter($viewed_users, function($element) use ($user_id) { return $element['target_id'] == $user_id; })) ) {
        $status = 1;
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'title' => $node->getTitle(),
        'favorite' => $favorite,
        'status' => $status,
        'changed' => $date,
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public static function getNewsListGlobal ($params) {

    $page = !empty($params->page) ? $params->page : 0;
    $itemsPerPage = !empty($params->itemsPerPage) ? $params->itemsPerPage : 7;

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    $query->sort('search_api_relevance', 'DESC');
    $query->range($page * $itemsPerPage, $itemsPerPage)
      ->addCondition('type', 'new')
      ->addCondition('field_export_zakon', 1)
      ->addCondition('status', '1');
    if ( empty($filters->key) ) {
      $query->sort('created', 'DESC');
    }
    self::setQueryFilters($query, $filters, 'new');
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $totalCount = $result->getResultCount();

    if ( !empty($filters->key) && $totalCount > 35 ) {
      $totalCount = 35;
    }

    $nodes = node_load_multiple($nids);

    $list = [];

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    foreach ( $nodes as $node ) {

      $demo = !empty($node->get('field_demo')->getValue()[0]['value']);

      $favorite = false;
      $favorite = $flag->isFlagged($node, \Drupal::currentUser());

      $date = \Drupal::service('date.formatter')->format($node->getCreatedTime(), 'date_reversed');

      $new_type = '';
      if ( !empty($node->get('field_new_type')->getValue()) ) {
        $new_type_id = $node->get('field_new_type')->getValue()[0]['target_id'];
        $new_type_term = taxonomy_term_load($new_type_id);
        if ( !empty($new_type_term) ) {
          $new_type = $new_type_term->getName();
        }
      }

      $new_system_domain = '';
      if ( !empty($node->get('field_systems')->getValue()) ) {
        $new_system_id = $node->get('field_systems')->getValue()[0]['target_id'];
        $new_system_term = taxonomy_term_load($new_system_id);
        if ( !empty($new_system_term) ) {
          $new_system_domain = $new_system_term->get('field_system_domain')->getValue()[0]['value'];
        }
      }

      $new_html = '';
      if ( !empty($node->get('field_new_html')->getValue()) ) {
        $new_html = $node->get('field_new_html')->getValue()[0]['value'];
        $new_html = check_markup($new_html, 'convert_to_pain_text');
        $new_html = mb_substr($new_html, 0, 90, 'UTF-8') . '...';
      }

      $image = '';
      if ( !empty($node->field_new_image->getValue()) ) {
        $file = file_load($node->field_new_image->getValue()[0]['target_id'])->getFileUri();
        if ( !empty($file) ){
          $style = ImageStyle::load('new_preview');
          $image = file_url_transform_relative($style->buildUrl($file));
        }
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'demo' => $demo,
        'title' => $node->getTitle(),
        'favorite' => false,
        'new_type' => $new_type,
        'new_html' => $new_html,
        'image' => $new_system_domain . $image,
        'created' => $date,
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getNewsList ($params) {

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }
    $page = $params->page;
    $itemsPerPage = 16;

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    $query->sort('search_api_relevance', 'DESC');
    $query->range($page * $itemsPerPage, $itemsPerPage)
      ->addCondition('field_systems', $systems, 'IN')
      ->addCondition('field_zakon_only', 1, '<>')
      ->addCondition('type', 'new')
      ->addCondition('status', '1');
    if ( empty($filters->key) ) {
      $query->sort('field_sort_date', 'DESC');
    }
    self::setQueryFilters($query, $filters, 'new');
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $totalCount = $result->getResultCount();

    if ( !empty($filters->key) && $totalCount > 144 ) {
      $totalCount = 144;
    }

    $nodes = node_load_multiple($nids);

    $list = [];

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    foreach ( $nodes as $node ) {

      $demo = !empty($node->get('field_demo')->getValue()[0]['value']);

      $favorite = false;
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }

      $sort_date_value = 0;

      if ( !empty($node->field_sort_date->value) ) {
        $sort_date_value = $node->field_sort_date->value;
      }

      $date = \Drupal::service('date.formatter')->format($sort_date_value, 'date_reversed');

      $new_type = '';

      if ( !empty($node->get('field_new_type')->getValue()) ) {
        $new_type_id = $node->get('field_new_type')->getValue()[0]['target_id'];
        $new_type_term = taxonomy_term_load($new_type_id);
        if ( !empty($new_type_term) ) {
          $new_type = $new_type_term->getName();
        }
      }

      $new_html = '';

      if ( !empty($node->get('field_new_html')->getValue()) ) {
        $new_html = $node->get('field_new_html')->getValue()[0]['value'];
        $new_html = check_markup($new_html, 'convert_to_pain_text');
        $new_html = mb_substr($new_html, 0, 90, 'UTF-8') . '...';
      }

      $image = '';
      if ( !empty($node->field_new_image->getValue()) ) {
        $file = file_load($node->field_new_image->getValue()[0]['target_id'])->getFileUri();
        if ( !empty($file) ){
          $style = ImageStyle::load('new_preview');
          $image = file_url_transform_relative($style->buildUrl($file));
        }
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'demo' => $demo,
        'title' => $node->getTitle(),
        'favorite' => $favorite,
        'new_type' => $new_type,
        'new_html' => $new_html,
        'image' => $image,
        'created' => $date,
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getJobsList ($params) {

    $page = $params->page;
    $itemsPerPage = 50;

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('jobs');
    $query = $index->query();
    $query->setFulltextFields(['field_vypusk_yetkd', 'field_vypusk_dkkhp', 'field_kod_zkpptr', 'field_kod_kp', 'field_job_name']);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    $query->sort('search_api_relevance', 'DESC');
    $query->range($page * $itemsPerPage, $itemsPerPage);
    if ( empty($filters->key) ) {
      $query->sort('field_job_name', 'ASC');
    }
    self::setQueryFilters($query, $filters, 'job');
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $totalCount = $result->getResultCount();

    $nodes = node_load_multiple($nids);

    $list = [];

    foreach ( $nodes as $node ) {

      $field_kod_kp = '';
      if ( !empty($node->get('field_kod_kp')->getValue()) ) {
        $field_kod_kp = $node->get('field_kod_kp')->getValue()[0]['value'];
      }

      $field_kod_zkpptr = '';
      if ( !empty($node->get('field_kod_zkpptr')->getValue()) ) {
        $field_kod_zkpptr = $node->get('field_kod_zkpptr')->getValue()[0]['value'];
      }

      $field_vypusk_yetkd = '';
      if ( !empty($node->get('field_vypusk_yetkd')->getValue()) ) {
        $field_vypusk_yetkd = $node->get('field_vypusk_yetkd')->getValue()[0]['value'];
      }

      $field_vypusk_dkkhp = '';
      if ( !empty($node->get('field_vypusk_dkkhp')->getValue()) ) {
        $field_vypusk_dkkhp = $node->get('field_vypusk_dkkhp')->getValue()[0]['value'];
      }

      $field_job_name = '';
      if ( !empty($node->get('field_job_name')->getValue()) ) {
        $field_job_name = $node->get('field_job_name')->getValue()[0]['value'];
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'field_kod_kp' => $field_kod_kp,
        'field_kod_zkpptr' => $field_kod_zkpptr,
        'field_vypusk_yetkd' => $field_vypusk_yetkd,
        'field_vypusk_dkkhp' => $field_vypusk_dkkhp,
        'field_job_name' => $field_job_name
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getSearchList ($params) {

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }
    $page = $params->page;
    $itemsPerPage = 10;

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer', 'field_profession_code', 'field_profession_description']);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    $query->range($page * $itemsPerPage, $itemsPerPage);
    $query->addCondition('field_systems', $systems, 'IN');
    $query->addCondition('type', ['example', 'accountinglog', 'instruction', 'consultation', 'new', 'interview', 'crib', 'profession'], 'IN');
    $query->addCondition('status', '1');

    $groupOR = $query->createConditionGroup('OR');
    $groupAND = $query->createConditionGroup('AND')
      ->addCondition('type', 'consultation')
      ->addCondition('field_status', ['viewed','placed'], 'IN')
      ->addCondition('field_user_only', 1, '<>')
      ->addCondition('field_consultation_type', ['consultation'], 'IN');
    $groupOR
      ->addCondition('type', 'consultation', '<>')
      ->addConditionGroup($groupAND);
    $query->addConditionGroup($groupOR);

    self::setQueryFilters($query, $filters, 'search');
    if ( !empty($filters->key) ) {
      $query->sort('search_api_relevance', 'DESC');
    } else {
      $query->sort('field_sort_date', 'DESC');
    }
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $totalCount = $result->getResultCount();

    if ( !empty($filters->key) && $totalCount > 35 ) {
      $totalCount = 35;
    }

    $list = [];

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    foreach ( $nids as $nid ) {

      $node = node_load($nid);

      $favorite = false;
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }

      $sort_date_value = 0;

      if ( !empty($node->field_sort_date->value) ) {
        $sort_date_value = $node->field_sort_date->value;
      }

      $date = \Drupal::service('date.formatter')->format($sort_date_value, 'date_reversed');

      $file = false;
      $nodeType = $node->getType();
      $type = '';

      switch ( $nodeType ) {
        case 'consultation': $type = 'consultations'; break;
        case 'new': $type = 'news'; break;
        case 'normdoc': $type = 'norm'; break;
        case 'example': $type = 'examples'; break;
        case 'accountinglog': $type = 'accountinglog'; break;
        case 'instruction': $type = 'instructions'; break;
        case 'interview': $type = 'interviews'; break;
        case 'crib': $type = 'cribs'; break;
        case 'profession': $type = 'orientation'; break;
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'demo' => !empty($node->get('field_demo')->getValue()[0]['value']),
        'title' => $node->getTitle(),
        'favorite' => $favorite,
        'type' => $type,
        'file' => $file,
        'changed' => $date,
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getSearchLinks ($params) {

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $query->setFulltextFields(['node_id', 'field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    $query->range(0, 10);
    $query->addCondition('field_systems', $systems, 'IN');
    $query->addCondition('type', ['example', 'accountinglog', 'crib', 'instruction', 'consultation', 'new', 'interview'], 'IN');

    $groupOR = $query->createConditionGroup('OR');
    $groupAND = $query->createConditionGroup('AND')
      ->addCondition('type', 'consultation')
      ->addCondition('field_status', ['viewed','placed'], 'IN')
      ->addCondition('field_user_only', 1, '<>')
      ->addCondition('field_consultation_type', ['consultation'], 'IN');
    $groupOR
      ->addCondition('type', 'consultation', '<>')
      ->addConditionGroup($groupAND);
    $query->addConditionGroup($groupOR);

    self::setQueryFilters($query, $filters, 'search');
    if ( !empty($filters->key) ) {
      $query->sort('search_api_relevance', 'DESC');
    } else {
      $query->sort('field_sort_date', 'DESC');
    }
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $totalCount = $result->getResultCount();

    if ( !empty($filters->key) && $totalCount > 20 ) {
      $totalCount = 20;
    }

    $list = [];

    foreach ( $nids as $nid ) {

      $node = node_load($nid);

      $nodeType = $node->getType();
      $type = '';
      $typeName = '';
      $skip = false;

      switch ( $nodeType ) {
        case 'consultation':
          $type = 'consultations';
          $typeName = 'Конс.';
        break;
        case 'new':
          $type = 'news';
          $typeName = 'Нов.';
        break;
        case 'normdoc':
          $skip = true;
          $type = 'norm';
          $typeName = 'Н.док.';
        break;
        case 'example':
          $type = 'examples';
          $typeName = 'Зраз.';
        break;
        case 'accountinglog':
          $type = 'accountinglogs';
          $typeName = 'Журн.о.';
        break;
        case 'instruction':
          $type = 'instructions';
          $typeName = 'Інстр.';
        break;
        case 'interview':
          $type = 'interviews';
          $typeName = 'Интер.';
        break;
        case 'crib':
          $type = 'cribs';
          $typeName = 'Шпарг.';
        break;
      }

      if ( !$skip ) {

        $list[] = (object)[
          'id' => intVal($node->Id()),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
          'title' => $node->getTitle(),
          'nodeType' => $nodeType,
          'typeName' => $typeName,
          'type' => $type
        ];

      }

    }

    $system = get_current_system();
    $params->system = $system->id;
    $normList = self::sendApiCommand('getSearchLinks', $params);
    $list = array_merge($normList->list, $list);

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount
    ];

    return $data;
  }

  public function getFavoritesList ($params) {

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }
    $page = $params->page;
    $itemsPerPage = 12;

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }
    if ( !empty((array)$filters) ) {

      $index = \Drupal\search_api\Entity\Index::load('default');
      $query = $index->query();
      $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
      $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
      $query->setParseMode($parse_mode);
      $query->sort('search_api_relevance', 'DESC');
      $query->range($page * $itemsPerPage, $itemsPerPage);
      $query->addCondition('field_systems', $systems, 'IN');
      $query->addCondition('status', '1');
      $query->addCondition('flag_favorites', \Drupal::currentUser()->Id());
      $query->addCondition('type', ['new', 'consultation', 'example', 'accountinglog', 'instruction', 'message', 'interview'], 'IN');

      self::setQueryFilters($query, $filters, 'search');
      $result = $query->execute();
      $items = array_keys($result->getResultItems());
      $nids = [];
      foreach ($items as $item) {
        $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
      };
      $totalCount = $result->getResultCount();

      if ( !empty($filters->key) && $totalCount > 35 ) {
        $totalCount = 35;
      }

    } else {

      $query = \Drupal::entityQuery('node')
        ->condition('type', ['new', 'consultation', 'example', 'accountinglog', 'instruction', 'message', 'interview'], 'IN')
        ->condition('field_systems', $systems, 'IN');
      //self::setQueryFilters($query, $filters, 'search');
      $query->addTag('flagged_by_current_user');


      $totalCount = intVal($query->count()->execute());

      $query = \Drupal::entityQuery('node')
      ->condition('type', ['new', 'consultation', 'example', 'accountinglog', 'instruction', 'message', 'interview'], 'IN')
      ->condition('field_systems', $systems, 'IN')
      ->range($page * $itemsPerPage, $itemsPerPage);
      //self::setQueryFilters($query, $filters, 'search');
      $query->addTag('flagged_by_current_user');

      $nids = $query->execute();

    }

    $list = [];

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    foreach ( $nids as $nid ) {

      $node = node_load($nid);

      $favorite = false;
      $demo = !empty($node->field_demo) && !empty($node->get('field_demo')->getValue()[0]['value']);
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }
      $flagging = $flag_service->getFlagging($flag, $node);
      $date = 0;
      if ( !empty($flagging) ) {
        $flagged_date = $flagging->created->getValue()[0]['value'];
        $date = \Drupal::service('date.formatter')->format($flagged_date, 'date_reversed');
      }
      $file = false;
      $nodeType = $node->getType();
      $type = '';

      switch ( $nodeType ) {
        case 'consultation': $type = 'consultations'; break;
        case 'message': $type = 'messages'; break;
        case 'new': $type = 'news'; break;
        case 'normdoc': $type = 'norm'; break;
        case 'example': $type = 'examples'; break;
        case 'accountinglog': $type = 'accountinglogs'; break;
        case 'instruction': $type = 'instructions'; break;
        case 'interview': $type = 'interviews'; break;
        case 'crib': $type = 'cribs'; break;
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'demo' => $demo,
        'title' => $node->getTitle(),
        'favorite' => $favorite,
        'type' => $type,
        'file' => $file,
        'changed' => $date,
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getInstructionsList ($params) {

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }
    $systemUser = \Drupal::currentUser()->Id();
    $currentUser = \Drupal::currentUser();
    $isAnonymous = $currentUser->isAnonymous() || in_array('expert', $currentUser->getRoles());
    $page = $params->page;
    $itemsPerPage = 7;

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    $query->sort('search_api_relevance', 'DESC');
    $query->range($page * $itemsPerPage, $itemsPerPage)
      ->addCondition('field_systems', $systems, 'IN')
      ->addCondition('type', 'instruction')
      ->addCondition('status', '1');
    if ( empty($filters->key) ) {
      $query->sort('field_sort_date', 'DESC');
    }
    self::setQueryFilters($query, $filters, 'instruction');
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $totalCount = $result->getResultCount();

    if ( !empty($filters->key) && $totalCount > 35 ) {
      $totalCount = 35;
    }

    $nodes = node_load_multiple($nids);

    $list = [];

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    foreach ( $nodes as $node ) {

      $demo = !empty($node->get('field_demo')->getValue()[0]['value']);

      $favorite = false;
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }
      $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');

      $instruction_type = '';

      if ( !empty($node->get('field_instruction_type')->getValue()) ) {
        $instruction_type = $node->get('field_instruction_type')->getValue()[0]['value'];
      }

      $file = false;
      if ( !empty($node->field_instruction_file->getValue()[0]['target_id']) ) {
        $file = \Drupal\file\Entity\File::load($node->field_instruction_file->getValue()[0]['target_id']);
        if ( !empty($file) ){
          $file = file_url_transform_relative(file_create_url($file->getFileUri()));
          $parts = explode('.', $file);
          $file_type = end($parts);
          if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
            $file = 'demo';
          }
        }
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'demo' => $demo,
        'title' => $node->getTitle(),
        'favorite' => $favorite,
        'instruction_type' => $instruction_type,
        'file' => $isAnonymous ? 'demo' : $file,
        'changed' => $date,
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getConsultationsOwnList ($params) {

    $user = \Drupal::currentUser();

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }
    $page = $params->page;
    $itemsPerPage = 7;

    if ( empty( $params->filters ) ) {
      $filters = (object)[];
    } else {
      $filters = $params->filters;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    //$consultation_types = ['consultation', 'notfound', 'help', 'interview'];
    $query->range($page * $itemsPerPage, $itemsPerPage)
      //->addCondition('field_consultation_type', $consultation_types, 'IN')
      ->addCondition('field_systems', $systems, 'IN')
      ->addCondition('uid', $user->Id())
      ->addCondition('type', 'consultation')
      ->addCondition('status', '1');
    if ( empty($filters->key) ) {
      $query->sort('changed', 'DESC');
    } else {
      $query->sort('search_api_relevance', 'DESC');
    }
    self::setQueryFilters($query, $filters, 'consultation');
    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $totalCount = $result->getResultCount();

    if ( !empty($filters->key) && $totalCount > 35 ) {
      $totalCount = 35;
    }

    $nodes = node_load_multiple($nids);

    $list = [];

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    foreach ( $nodes as $node ) {

      $favorite = false;
      $currentUser = \Drupal::currentUser();
      if ( !empty($currentUser->Id()) ) {
        $favorite = $flag->isFlagged($node, $currentUser);
      }
      $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');

      $file = false;

      if ( !empty($node->get('field_file_expert')->getValue()) ) {
        $file = $node->get('field_file_expert')->getValue()[0]['target_id'];
      }

      $answer_date = false;

      if ( !empty($node->get('field_answer_date')) ) {

        $field_answer_date = $node->get('field_answer_date')->getValue()[0]['value'];

        if ( !empty($field_answer_date) ) {
          $answer_date = \Drupal::service('date.formatter')->format(strtotime($field_answer_date), 'date_reversed');
        }

      }

      $question = '';

      if ( !empty($node->get('field_question')->getValue()) ) {
        $question = $node->get('field_question')->getValue()[0]['value'];
      }

      if ( !empty($node->get('field_status')->getValue()) ) {
        $status_id = $node->get('field_status')->getValue()[0]['value'];
        switch($status_id) {
          case 'placed':
            $status = 'placed';
          break;
          case 'viewed':
            $status = 'viewed';
          break;
          case 'clarification':
            $status = 'clarification';
          break;
          default:
            $status = 'progress';
          break;
        }
      }

      $expert = false;

      $expert_fio = '';
      $expert_id = 0;
      $expert_photo = '';
      $expert_position = '';

      if ( !empty($node->get('field_expert')->getValue()) ) {
        $expert_id = $node->get('field_expert')->getValue()[0]['target_id'];
        $expert = user_load($expert_id);

        if ( !empty($expert) ) {

          if ( !empty($expert->get('field_fio')->getValue()) ) {
            $expert_fio = $expert->get('field_fio')->getValue()[0]['value'];
          }

          if ( !empty($expert->get('field_position')->getValue()) ) {
            $expert_position = $expert->get('field_position')->getValue()[0]['value'];
          }

          if ( !empty($expert->get('field_photo')->getValue()) ) {

            $fid = $expert->get('field_photo')->getValue()[0]['target_id'];

            if ( $fid ) {

              $expert_photo_uri = file_load($fid)->getFileUri();

              $style = ImageStyle::load('photo_expert');

              $expert_photo = file_url_transform_relative($style->buildUrl($expert_photo_uri));

            };

          }

        }

      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'title' => $node->getTitle(),
        'question' => $question,
        'favorite' => $favorite,
        'answer_date' => $answer_date,
        'status' => $status,
        'file' => $file,
        'created' => $date,
        'expert' => (object)[
          'id' => $expert_id,
          'fio' => $expert_fio,
          'photo' => $expert_photo,
          'position' => $expert_position
        ]
      ];

    }

    $data = (object)[
      'list' => $list,
      'filters' => $filters,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getConsultationsExpertArchive ($params) {

    $user = \Drupal::currentUser();

    $system = get_current_system();
    $page = $params->page;
    $itemsPerPage = 7;

    $query = \Drupal::entityQuery('node')
			->condition('type', 'consultation')
			->condition('field_expert', $user->Id())
			->condition('field_systems', $system->id)
			->condition('field_status', ['placed', 'viewed', 'answered'], 'IN');

    $totalCount = intVal($query->count()->execute());

    $query = \Drupal::entityQuery('node')
			->condition('type', 'consultation')
			->condition('field_systems', $system->id)
			->condition('field_expert', $user->Id())
      ->condition('field_status', ['placed', 'viewed', 'answered'], 'IN')
      ->sort('changed', 'DESC')
      ->range($page * $itemsPerPage, $itemsPerPage);

    $nids = $query->execute();

    $nodes = node_load_multiple($nids);

    $list = [];

    foreach ( $nodes as $node ) {

      $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');

      $question = '';

      if ( !empty($node->get('field_question')->getValue()) ) {
        $question = $node->get('field_question')->getValue()[0]['value'];
      }

      $answer = '';

      if ( !empty($node->get('field_answer')->getValue()) ) {
        $answer = $node->get('field_answer')->getValue()[0]['value'];
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'title' => $node->getTitle(),
        'question' => $question,
        'answer' => $answer,
        'created' => $date
      ];

    }

    $data = (object)[
      'list' => $list,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function getConsultationsExpertList ($params) {

    $user = \Drupal::currentUser();

    $system = get_current_system();
    $page = $params->page;
    $itemsPerPage = 7;

    $query = \Drupal::entityQuery('node')
			->condition('type', 'consultation')
			->condition('field_expert', $user->Id())
			->condition('field_systems', $system->id)
			->condition('field_status', ['set','accepted','clarification','clarified','answered'], 'IN')
      ->condition('status', '1');

    $totalCount = intVal($query->count()->execute());

    $query = \Drupal::entityQuery('node')
			->condition('type', 'consultation')
			->condition('field_systems', $system->id)
			->condition('field_expert', $user->Id())
      ->condition('field_status', ['set','accepted','clarification','clarified','answered'], 'IN')
      ->condition('status', '1')
      ->addTag('consultations_list_expert')
      ->range($page * $itemsPerPage, $itemsPerPage);

    $nids = $query->execute();

    $nodes = node_load_multiple($nids);

    $list = [];

    foreach ( $nodes as $node ) {

      $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');

      if ( !empty($node->get('field_answer_date')) ) {

        $last = $node->get('field_answer_date')->getValue()[0]['value'];

        if ( !empty($last) ) {
          $date = \Drupal::service('date.formatter')->format(strtotime($last), 'date_reversed');
        }

      }

      $file = false;

      if ( !empty($node->get('field_file')->getValue()) ) {
        $file = file_load($node->get('field_file')->getValue()[0]['target_id']);
        $file = (object)[
          'name' => $file->getFilename(),
          'uri' => file_url_transform_relative(file_create_url($file->getFileUri()))
        ];
      }

      $clarification = '';


      if ( !empty($node->field_editor_clarification->getValue()) ) {
        $clarification_ids = $node->field_editor_clarification->getValue();
        foreach ( $clarification_ids as $clarification_id ) {
          $clarification_p = \Drupal\paragraphs\Entity\Paragraph::load( $clarification_id['target_id'] );
          $clarification_request = '';
          $clarification_answer = '';
          if ( !empty($clarification_p->field_editor_question) && !empty($clarification_p->field_editor_question->getValue()) ) {
            $clarification_request = $clarification_p->get('field_editor_question')->getValue()[0]['value'];
          }
          if ( !empty($clarification_p->field_user_answer) && !empty($clarification_p->field_user_answer->getValue()) ) {
            $clarification_answer = $clarification_p->get('field_user_answer')->getValue()[0]['value'];
          }
          $clarification .= '<div class="clarification-title">Запит експерта на уточнення:</div>';
          $clarification .= '<div class="clarification">' . $clarification_request . '</div>';
          $clarification .= '<div class="clarification-title">Відповідь:</div>';
          $clarification .= '<div class="clarification">' . $clarification_answer . '</div><br/>';
        }

      }

      $status_id = false;

      $status = 'progress';

      if ( !empty($node->get('field_status')->getValue()) ) {
        $status_id = $node->get('field_status')->getValue()[0]['value'];
        $allowed_values = $node->get('field_status')->getFieldDefinition()->getFieldStorageDefinition()->getSetting('allowed_values');
        $status_label = $allowed_values[$status_id];
        switch($status_id) {
          case 'placed':
          case 'viewed':
          case 'answered':
            $status = 'ready';
          break;
          case 'set':
            $status_label = 'Чекає відповіді';
          break;
          default:
            $status = 'progress';
          break;
        }
      }

      $question = '';

      if ( !empty($node->get('field_question')->getValue()) ) {
        $question = $node->get('field_question')->getValue()[0]['value'];
      }

      $list[] = (object)[
        'id' => intVal($node->Id()),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'title' => $node->getTitle(),
        'question' => $question,
        'clarification' => $clarification,
        'status' => $status,
        'status_label' => $status_label,
        'status_id' => $status_id,
        'file' => $file,
        'answer_date' => $date
      ];

    }

    $data = (object)[
      'list' => $list,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage
    ];

    return $data;
  }

  public function checkNewConsultations () {

    $user = \Drupal::currentUser();
    $roles = $user->getRoles();
    $system = get_current_system();

    $query = \Drupal::entityQuery('node')
			->condition('type', 'consultation')
      ->condition('field_systems', $system->id);

    if ( in_array('expert', $roles) ) {
      $query->condition('field_expert', $user->Id())
        ->condition('field_status', ['set','accepted','clarified'], 'IN');
    } else {
      $query->condition('uid', $user->Id())
        ->condition('field_status', 'placed');
    }

    $count = intVal($query->count()->execute());

    $data = (object)[
      'count' => $count
    ];

    return $data;
  }

  public function checkNewAdminConsultations () {

    $user = \Drupal::currentUser();
    $roles = $user->getRoles();
    $system = get_current_system();

    $consultation_types = ['consultation', 'notfound', 'help', 'interview', 'crib'];

    $query = \Drupal::entityQuery('node')
			->condition('type', 'consultation')
      ->condition('field_systems', $system->id)
      ->condition('field_consultation_type', $consultation_types, 'IN')
      ->condition('field_status', 'new');

    $count = intVal($query->count()->execute());

    $data = (object)[
      'count' => $count
    ];

    return $data;
  }

  public function checkNewAdminRequests ($params) {

    $request_types = ['norm', 'example', 'accountinglog', 'instruction'];

    $user = \Drupal::currentUser();
    $roles = $user->getRoles();
    $system = get_current_system();
    $systems = [$system->id];
    // if ( !empty($system->parent_id) ) {
    //   $systems[] = $system->parent_id;
    // }

    $query = \Drupal::entityQuery('node')
			->condition('type', 'consultation')
      ->condition('field_systems', $systems, 'IN')
      ->condition('field_consultation_type', $request_types, 'IN')
      ->condition('field_status', 'new');

    $count = intVal($query->count()->execute());

    $data = (object)[
      'count' => $count
    ];

    return $data;
  }

  public function checkNewMessages () {

    $system = get_current_system();
    $user_id = \Drupal::currentUser()->Id();
    $cid = 'konsultant:newMessagesCount:' . $user_id . ':'. $system->id;
    $count = self::get_cache($cid, function() use ($user_id, $system) {
      $connection = Database::getConnection();
      $sq = $connection->select('node__field_users_viewed', 'fuv');
      $sq->leftJoin('node', 'nd', 'fuv.entity_id = nd.nid');
      $sq->where('fuv.field_users_viewed_target_id = ' . $user_id);
      $sq->addField('nd', 'nid', 'nid');
      $sq2 = $connection->select('node__field_users_deleted', 'fud');
      $sq2->leftJoin('node', 'nd', 'fud.entity_id = nd.nid');
      $sq2->where('fud.field_users_deleted_target_id = ' . $user_id);
      $sq2->addField('nd', 'nid', 'nid');
      $q = $connection->select('node', 'nd');
      $q->leftJoin('node__field_systems', 'fs', 'fs.entity_id = nd.nid');
      $q->leftJoin('node__field_message_to', 'fmt', 'fmt.entity_id = nd.nid');
      $q->leftJoin('node__field_message_to_all', 'fmta', 'fmta.entity_id = nd.nid');
      $q->condition('fs.field_systems_target_id', $system->id);
      $q->condition('nd.type', 'message');
      $q->condition('nd.nid', $sq, 'NOT IN');
      $q->condition('nd.nid', $sq2, 'NOT IN');
      $orGroup = $q->orConditionGroup();
      $orGroup->condition('fmta.field_message_to_all_value', 1);
      $orGroup->condition('fmt.field_message_to_target_id', $user_id);
      $q->condition($orGroup);
      $q->addField('nd', 'nid', 'nid');
      $count = $q->distinct()->countQuery()->execute()->fetchField();
      return (object)[
        'tags' => ['node_list:message'],
        'data' => intval($count)
      ];
    });

    return (object)[
      'count' => $count
    ];

  }


  public static function incViews($nid) {

    $user_roles = \Drupal::currentUser()->getRoles();

    if ( in_array('customer', $user_roles) || in_array('demo', $user_roles) ) {

      $node = node_load($nid);
      if ( !empty($node->field_views->getValue()[0]['value']) ) {
        $views = $node->field_views->getValue()[0]['value'];
      } else {
        $views = 0;
      }
      $views = $node->set('field_views', [$views + 1]);
      $changed_date = $node->changed->value;
      $node->save();
      konsultant_restore_changed_date($changed_date, $node->Id());
    }

  }


  public static function getConsultation ($params) {

    $nid = $params->id;
    $expert_id = -1;
    $mode = !empty($params->mode) ? $params->mode : 'general';
    $node = Node::load($nid);
    $user_roles = \Drupal::currentUser()->getRoles();
    if ( empty($node) || ( !empty($node) && !$node->isPublished() && !count(array_intersect(['administrator', 'editor', 'root'], $user_roles )) ) ) {
      return false;
    }

    if ( !empty($node->get('field_expert')->getValue()) ) {
      $expert_id = $node->get('field_expert')->getValue()[0]['target_id'];
    }

    self::incViews($nid);
    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    $favorite = false;
    $currentUser = \Drupal::currentUser();
    $isAnonymous = $currentUser->isAnonymous() || ( in_array('expert', $currentUser->getRoles()) && $expert_id !== $currentUser->Id());

    if ( !empty($currentUser->Id()) ) {
      $favorite = $flag->isFlagged($node, $currentUser);
    }
    $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');
    $systemUser = \Drupal::currentUser()->Id();

    $user = 0;

    if ( !empty($node->getOwner()) ) {
      $user = $node->getOwner()->Id();
    }

    $request_types = ['norm', 'example', 'accountinglog', 'instruction'];

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    if ( $mode == 'general' ) {
      $query->sort('field_sort_date', 'DESC');
      $query->addCondition('field_consultation_type', $request_types, 'NOT IN')
        ->addCondition('field_status', ['viewed','placed'], 'IN')
        ->addCondition('field_user_only', 1, '<>');
    }

    if ( $mode == 'own' ) {
      $currentUser = \Drupal::currentUser();
      $query->sort('changed', 'DESC');
      $query->addCondition('uid', $currentUser->Id());
    }

    $query->addCondition('field_systems', $systems, 'IN')
      ->addCondition('type', 'consultation');

    $result = $query->execute();

    $items = array_keys($result->getResultItems());

    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };

    $itemsPerPage = 7;

    $nid_number = array_search($nid, $nids);

    $page_number = 0;

    if ( $nid_number !== false ) {
      $page_number = floor($nid_number / $itemsPerPage);
    }

    $topics = [];

    $themes_ids = [];

    if ( !empty($node->get('field_filter')->getValue()) ) {
       $themes_array = $node->get('field_filter')->getValue();

       foreach ($themes_array as $key => $val ) {
         if ( !empty($val['target_id']) ) {
           $themes_ids[] = $val['target_id'];
           break;
         }
       }

    }

    if ( !empty($themes_ids) ) {

      $system = get_current_system();

      $index = \Drupal\search_api\Entity\Index::load('default');
      $query = $index->query();
      $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
      $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
      $query->setParseMode($parse_mode);
      $query->sort('search_api_relevance', 'DESC');
      $query->range(0, 5)
        ->addCondition('field_systems', $systems, 'IN')
        ->addCondition('field_filter', $themes_ids[0], 'IN')
        ->addCondition('nid', $nid, '<>')
        ->addCondition('field_status', ['viewed','placed'], 'IN')
        ->addCondition('field_user_only', 1, '<>')
        ->addCondition('type', 'consultation')
        ->addCondition('status', '1');

      $query->sort('changed', 'DESC');
      $result = $query->execute();
      $items = array_keys($result->getResultItems());
      $nids = [];
      foreach ($items as $item) {
        $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
      };
      $topic_nodes = node_load_multiple($nids);

      if ( !empty($topic_nodes) ) {

        foreach($topic_nodes as $topic_node) {
          $topic_id = intVal($topic_node->Id());
          $topic_title = $topic_node->getTitle();
          $topic_changed = \Drupal::service('date.formatter')->format($topic_node->getChangedTime(), 'date_reversed');
          $topic_demo = !empty($topic_node->get('field_demo')->getValue()[0]['value']);

          $topic = (object)[
            'id' => $topic_id,
            'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$topic_id),
            'title' => $topic_title,
            'demo' => $topic_demo,
            'changed' => $topic_changed
          ];
          $topics[] = $topic;
        }

      }

    }

    $question = '';

    if ( !empty($node->get('field_question')->getValue()) ) {
      $question = $node->get('field_question')->getValue()[0]['value'];
    }

    $answer = '';

    $hasAccess = isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']);

    if ( $hasAccess ) {
      $answer = get_system_template('demo-access-denied-node');
    } else if ( !empty($node->get('field_answer')->getValue()) ) {
      $answer = $node->get('field_answer')->getValue()[0]['value'];
    }

    $file = false;

    if ( !empty($node->get('field_file')->getValue()) ) {
      $file = file_load($node->get('field_file')->getValue()[0]['target_id']);
      $file = (object)[
        'name' => $file->getFilename(),
        'uri' => file_url_transform_relative(file_create_url($file->getFileUri()))
      ];
    }

    $file_expert = false;

    if ( !empty($node->get('field_file_expert')->getValue()) ) {
      $file_expert = file_load($node->get('field_file_expert')->getValue()[0]['target_id']);
      $file_uri = file_url_transform_relative(file_create_url($file_expert->getFileUri()));
      $parts = explode('.', $file_uri);
      $file_type = end($parts);
      $file_expert = (object)[
        'name' => $file_expert->getFilename(),
        'uri' => $file_uri,
        'size' => round($file_expert->getSize() / 1024),
        'type' => $file_type
      ];
    }

    if ( !empty($node->get('field_status')->getValue()) ) {
      $status_id = $node->get('field_status')->getValue()[0]['value'];

      switch ( $status_id ) {

        case 'placed':
          $status = 'ready';
          if ( $systemUser == $user ) {
            $node->get('field_status')->setValue('viewed');
            $changed_date = $node->changed->value;
            $node->save();
            konsultant_restore_changed_date($changed_date, $node->Id());
          };
        break;

        case 'clarification':
          $status = 'clarification';
        break;

        case 'viewed':
          $status = 'ready';
        break;

        default:
          $status = 'progress';
        break;

      }

    }

    $consultation_type = 'consultation';
    $request_types = ['norm', 'example', 'accountinglog', 'instruction'];

    if ( !empty($node->get('field_consultation_type')->getValue()) ) {

      $ct = $node->get('field_consultation_type')->getValue()[0]['value'];

      if ( in_array($ct, $request_types) ) {
        $consultation_type = 'request';
      }

    }

    $expert = false;

    $expert_fio = '';
    $expert_id = 0;
    $expert_photo = '';
    $expert_position = '';

    if ( !empty($node->get('field_expert')->getValue()) ) {
      $expert_id = $node->get('field_expert')->getValue()[0]['target_id'];
      $expert = user_load($expert_id);

      if ( !empty($expert) ) {

        if ( !empty($expert->get('field_fio')->getValue()) ) {
          $expert_fio = $expert->get('field_fio')->getValue()[0]['value'];
        }

        if ( !empty($expert->get('field_position')->getValue()) ) {
          $expert_position = $expert->get('field_position')->getValue()[0]['value'];
        }

        if ( !empty($expert->get('field_photo')->getValue()) ) {

          $fid = $expert->get('field_photo')->getValue()[0]['target_id'];

      		if ( $fid ) {

            $expert_photo_uri = file_load($fid)->getFileUri();
            $expert_photo = get_image_path_pdf($expert_photo_uri, 'photo_expert_100');

      		};

        }

      }

    }

    $clarification_request = '';

    if ( !empty($node->field_editor_clarification->getValue()) ) {
      $clarification_ids = $node->field_editor_clarification->getValue();
      $clarification_id = end($clarification_ids);
      $clarification = \Drupal\paragraphs\Entity\Paragraph::load( $clarification_id['target_id'] );
      if ( !empty($clarification->field_editor_question) && !empty($clarification->field_editor_question->getValue()) ) {
        $clarification_request = $clarification->get('field_editor_question')->getValue()[0]['value'];
      }
    }

    $answer_date = false;

    if ( !empty($node->field_answer_date->getValue()[0]['value']) ) {
      $answer_date = $node->field_answer_date->getValue()[0]['value'];
      $answer_date = date_create_from_format('Y-m-d', $answer_date);
      $answer_date = date('d.m.Y', $answer_date->getTimestamp());
    }

    $data = (object)[
      'id' => intVal($node->Id()),
      'title' => $node->getTitle(),
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
      'question' => $question,
      'answer' => $isAnonymous ? truncateHTML(300, $answer) : $answer,
      'answer_date' => $answer_date,
      'clarification_request' => $clarification_request,
      'status' => $status,
      'page' => $page_number,
      'file' => $isAnonymous ? false : $file,
      'favorite' => $favorite,
      'file_expert' => $file_expert,
      'created' => $date,
      'consultation_type' => $consultation_type,
      'user_id' => $user,
      'hasAccess' => $hasAccess,
      'expert' => (object)[
        'id' => $expert_id,
        'fio' => $expert_fio,
        'photo' => $expert_photo,
        'position' => $expert_position
      ],
      'topics' => $topics
    ];

    return $data;
  }


  public function getProfile () {

    $user = user_load(\Drupal::currentUser()->Id());

    $fio = '';

    if ( !empty($user->get('field_fio')->getValue()) ) {
      $fio = $user->get('field_fio')->getValue()[0]['value'];
    }

    $position  = '';

    if ( !empty($user->get('field_position')->getValue()) ) {
      $position = $user->get('field_position')->getValue()[0]['value'];
    }

    $company  = '';

    if ( !empty($user->get('field_company')->getValue()) ) {
      $company = $user->get('field_company')->getValue()[0]['value'];
    }

    $email = '';

    if ( !empty($user->get('field_user_email')->getValue()) ) {
      $email = $user->get('field_user_email')->getValue()[0]['value'];
    }

    $access_level = 'standard';
    $access_level_label = 'Стандарт';

    if ( !empty($user->get('field_access_level')->getValue()) ) {
      $access_level = $user->get('field_access_level')->getValue()[0]['value'];
      $access_level_label = $user->field_access_level->getSetting('allowed_values')[$user_level];
    }

    $access_begin  = '';

    if ( !empty($user->get('field_access_begin')->getValue()) ) {
      $access_begin = $user->get('field_access_begin')->getValue()[0]['value'];
      $access_begin = \Drupal::service('date.formatter')->format(strtotime($access_begin), 'date_reversed');
    }

    $access_end  = '';

    if ( !empty($user->get('field_access_end')->getValue()) ) {
      $access_end = $user->get('field_access_end')->getValue()[0]['value'];
      $access_end = \Drupal::service('date.formatter')->format(strtotime($access_end), 'date_reversed');
    }

    $user_photo = '';

    if ( !empty($user->get('field_photo')->getValue()) ) {

      $fid = $user->get('field_photo')->getValue()[0]['target_id'];

  		if ( $fid ) {

        $file = file_load($fid);

        if ( !empty($file) ) {
          $user_photo_uri = $file->getFileUri();
          $style = ImageStyle::load('photo_expert_163');
          $user_photo = file_url_transform_relative($style->buildUrl($user_photo_uri));
        }

  		};

    }

    $data = (object)[
      'fio' => $fio,
      'photo' => $user_photo,
      'position' => $position,
      'company' => $company,
      'access_level' => $access_level,
      'access_level_label' => $access_level_label,
      'access_begin' => $access_begin,
      'access_end' => $access_end,
      'email' => $email,
    ];

    return $data;
  }

  public function getExpert ($params) {

    $page = $params->page;
    $itemsPerPage = 7;

    $expert_id = $params->expert;

    $expert = user_load($expert_id);

    $fio = '';

    if ( !empty($expert->get('field_fio')->getValue()) ) {
      $fio = $expert->get('field_fio')->getValue()[0]['value'];
    }

    $position  = '';

    if ( !empty($expert->get('field_position')->getValue()) ) {
      $position = $expert->get('field_position')->getValue()[0]['value'];
    }

    $resume  = '';

    if ( !empty($expert->get('field_resume')->getValue()) ) {
      $resume = $expert->get('field_resume')->getValue()[0]['value'];
    }

    $photo = '';

    if ( !empty($expert->get('field_photo')->getValue()) ) {

      $fid = $expert->get('field_photo')->getValue()[0]['target_id'];

  		if ( $fid ) {

        $expert_photo_uri = file_load($fid)->getFileUri();

        $style = ImageStyle::load('photo_expert_100');

        $photo = file_url_transform_relative($style->buildUrl($expert_photo_uri));

  		};

    }

    $user_storage = \Drupal::service('entity_type.manager')->getStorage('user');

    $system = get_current_system();

    $ids = $user_storage->getQuery()
      ->condition('status', 1)
      ->condition('uid', $expert_id, '!=')
      ->condition('roles', ['expert', 'editor'], 'IN')
      ->condition('field_system', $system->id)
      ->sort('field_fio', 'ASC')
      ->range($page * $itemsPerPage, $itemsPerPage);
    $group = $ids
      ->orConditionGroup()
      ->notExists('field_hidden')
      ->condition('field_hidden', 1, '!=');
    $ids = $ids
      ->condition($group)
      ->execute();

    $totalCount = $user_storage->getQuery()
        ->condition('status', 1)
        ->condition('uid', $expert_id, '!=')
        //->exists('field_hidden')
        //->condition('field_hidden', 1, '!=')
        ->condition('roles', ['expert', 'editor'], 'IN')
        ->condition('field_system', $system->id);
    $group = $totalCount
      ->orConditionGroup()
      ->notExists('field_hidden')
      ->condition('field_hidden', 1, '!=');
    $totalCount = $totalCount
      ->condition($group)
      ->count()
      ->execute();

    $experts_data = $user_storage->loadMultiple($ids);
    $experts = [];

    foreach ( $experts_data as $expert_data ) {

      $expert_fio = '';

      if ( !empty($expert_data->get('field_fio')->getValue()) ) {
        $expert_fio = $expert_data->get('field_fio')->getValue()[0]['value'];
      }

      $expert_position  = '';

      if ( !empty($expert_data->get('field_position')->getValue()) ) {
        $expert_position = $expert_data->get('field_position')->getValue()[0]['value'];
      }

      $expert_photo = '';

      if ( !empty($expert_data->get('field_photo')->getValue()) ) {

        $expert_fid = $expert_data->get('field_photo')->getValue()[0]['target_id'];

    		if ( $expert_fid ) {

          $expert_photo_uri = file_load($expert_fid)->getFileUri();

          $style = ImageStyle::load('photo_expert_100');

          $expert_photo = file_url_transform_relative($style->buildUrl($expert_photo_uri));

    		};

      }

      $experts[] = (object)[
        'id' => $expert_data->Id(),
        'fio' => $expert_fio,
        'photo' => $expert_photo,
        'position' => $expert_position,
      ];

    };

    $data = (object)[
      'id' => $expert_id,
      'fio' => $fio,
      'photo' => $photo,
      'position' => $position,
      'resume' => $resume,
      'experts' => $experts,
      'totalCount' => $totalCount,
      'page' => $page,
      'itemsPerPage' => $itemsPerPage

    ];

    return $data;

  }

  public function getTypeById ($params) {

    $nid = $params->id;

    $node = node_load($nid);

    $type = 'none';

    if ( !empty($node) ) {

      $type = $node->getType();

      switch ($type){
        case 'consultation':
        $type = 'consultations';
        break;
        case 'normdoc':
        $type = 'norm';
        break;
        case 'example':
        $type = 'examples';
        break;
        case 'accountinglog':
        $type = 'accountinglogs';
        break;
        case 'interview':
        $type = 'interviews';
        break;
        case 'instruction':
        $type = 'instructions';
        break;
        case 'new':
        $type = 'news';
        break;
        case 'crib':
        $type = 'cribs';
        break;
      }

    }

    $data = (object)[
      'type' => $type,
    ];

    return $data;

  }

  public function getTemplate ($params) {

    $template_id = $params->template;

    $template = node_load($template_id);

    $html = '';

    if ( !empty($template->get('field_template_html')->getValue()) ) {
      $html = $template->get('field_template_html')->getValue()[0]['value'];
    }

    $data = (object)[
      'html' => $html,
    ];

    return $data;

  }

  public function convertTest () {

    $fid = 25344;

    $file = \Drupal\file\Entity\File::load($fid);

    $html = '';

    if ( !empty($file) ) {

      $uri = $file->getFileUri();

      $path = \Drupal::service('file_system')->realpath($uri);

      if ( !empty($path) ) {

        $converted_path = \Drupal\konsultant_import\ImportpageController::importConvert($path);
        // $unoconv_bin = Settings::get('unoconv_bin', 'test');
        // $logger = null;
        // $unoconv = \Unoconv\Unoconv::create([
        //     'timeout' => 42,
        //     'unoconv.binaries' => $unoconv_bin,
        // ], $logger);
        //
        // echo $path;
        // echo ':::';
        // echo $converted_path;
        // //die();
        //
        // $unoconv->transcode('test.doc', 'xhtml', 'test2.html');
        konsultant_unoconv($path, $converted_path);

        $data = file_get_contents($converted_path);

        unlink($converted_path);

        if ( !empty($data) ) {
          $html = \Drupal\konsultant_import\ImportpageController::fixImportedHtml($data);
        }

      }

    }

    return [
  		'#theme' => 'convert_test',
      '#data' => $html
  	];

  }

  public static function getConvertedHtml ($params) {

    $fid = $params->fid;

    $file = \Drupal\file\Entity\File::load($fid);

    $html = '';
    $data = [
      'status' => 'unknown',
      'html' => ''
    ];
    if ( !empty($file) ) {

      $uri = $file->getFileUri();

      $path = \Drupal::service('file_system')->realpath($uri);

      if ( !empty($path) ) {

        $unoconv_url = Settings::get('unoconv_url', '');

      	if ( empty($unoconv_url) ) {

          $converted_path = \Drupal\konsultant_import\ImportpageController::importConvert($path);

          if ( !empty($converted_path) ) {
            $data = konsultant_unoconv($path, $converted_path);
            $data->html = file_get_contents($converted_path);
            unlink($converted_path);
          }

          if ( !empty($data->html) ) {
            $data->html = \Drupal\konsultant_import\ImportpageController::fixImportedHtml($data->html);
          }


        } else {

          $data = konsultant_unoconv($path);

          if ( $data->status == 'ok' && !empty($data->html) ) {
            $data->html = \Drupal\konsultant_import\ImportpageController::fixImportedHtml($data->html);
          }

        }

      }

    }

    return $data;

  }

  public static function getNorm ($params) {

    $local_norm_search = Settings::get('local_norm_search', 'no');
    if ( $local_norm_search == 'no' ) {
      $system = get_current_system();
      $params->system = $system->id;
      $data = self::sendApiCommand('getNorm', $params);
      $norm_api_url = Settings::get('norm_api_url', '');

      if ( !empty($data) ) {
        $currentUser = \Drupal::currentUser();
        $isAnonymous = $currentUser->isAnonymous();
        $data->html = str_replace(' src="/sites/', ' src="' . $norm_api_url . '/sites/', $data->html);
        $data->html = str_replace(' href="/sites/', ' href="' . $norm_api_url . '/sites/', $data->html);
        $data->html = $isAnonymous ? truncateHTML(300, $data->html) : $data->html;
      } else {
        $data = (object)[
          'html' => ''
        ];
      }

    } else {
      $data = self::getNormLocal($params);
    }
    return $data;

  }

  public function getNormRevisions ($params) {
    $system = get_current_system();
    $params->system = $system->id;
    $data = self::sendApiCommand('getNormRevisions', $params);
    return $data;
  }

  public static function sendApiCommand ($command, $params) {

    $norm_api_url = Settings::get('norm_api_url', '');
    $user = Settings::get('norm_api_user', '');
    $password = Settings::get('norm_api_pass', '');

    $ch = curl_init();
    $post = [
      'command' => $command,
      'params' => json_encode($params)
    ];
    curl_setopt($ch, CURLOPT_URL, $norm_api_url . '/api');
    curl_setopt($ch, CURLOPT_USERPWD, $user . ":" . $password);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
    $data = curl_exec($ch);
    curl_close($ch);
    if ( !empty($data) ) {
      return json_decode($data)->data;
    } else {
      return (object)[];
    }
  }

  public static function getNormLocal ($params) {

    $nid = $params->id;

    if ( !empty($params->revision) ) {
      $rev = $params->revision;
    } else {
      $rev = 0;
    }
    $node = node_load($nid);
    $user_roles = \Drupal::currentUser()->getRoles();
    if ( empty($node) || ( !empty($node) && !$node->isPublished() && !count(array_intersect(['administrator', 'editor', 'root'], $user_roles )) ) ) {
      return false;
    }
    self::incViews($nid);
    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    $favorite = false;
    $currentUser = \Drupal::currentUser();
    if ( !empty($currentUser->Id()) ) {
      $favorite = $flag->isFlagged($node, $currentUser);
    }

    $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');

    $systemUser = \Drupal::currentUser()->Id();

    $revisions = [];
    $html = '';
    $file = false;
    $file_type = false;
    $rev_status = '';
    $date_created = '';
    $date_valid = '';
    $date_invalid = '';

    if ( !empty($node->get('field_date_invalid')->getValue()) ) {
      $date_invalid = $node->get('field_date_invalid')->getValue()[0]['value'];
      $date_invalid = \Drupal::service('date.formatter')->format(strtotime($date_invalid), 'date_reversed');
    }

    $date_partially_invalid = '';

    if ( !empty($node->get('field_date_partially_invalid')->getValue()) ) {
      $date_partially_invalid = $node->get('field_date_partially_invalid')->getValue()[0]['value'];
      $date_partially_invalid = \Drupal::service('date.formatter')->format(strtotime($date_partially_invalid), 'date_reversed');
    }

    if ( !empty($node->field_document_status->getValue()[0]['value']) ) {
      $rev_status = $node->field_document_status->getValue()[0]['value'];
      if ( $rev_status !== 'valid' ) {
        $rev_status = 'invalid';
      }
    }

    if ( !empty($node->field_documents->getValue()) ) {
      $revision_id = count($node->field_documents->getValue()) - 1;
      foreach ( $node->field_documents as $key => $item) {

        $document_date_valid = '';
        $document_date_created = '';

        $item = $item->value;
        $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($item);

        // $revision_status = '';
        //
        // if ( !empty($fc->field_revision_status->getValue()[0]['value']) ) {
        //   $revision_status = $fc->field_revision_status->getValue()[0]['value'];
        //
        //   if ( $revision_status !== 'valid' ) {
        //     $revision_status = 'invalid';
        //   }
        //
        // }

        if ( !empty($fc->field_document_date_valid->getValue()[0]['value']) ) {
          $document_date_valid = $fc->field_document_date_valid->getValue()[0]['value'];
        }

        if ( !empty($document_date_valid) ) {
          $document_date_valid = \Drupal::service('date.formatter')->format(strtotime($document_date_valid), 'date_reversed');
        }

        if ( !empty($fc->field_document_date_created->getValue()[0]['value']) ) {
          $document_date_created = $fc->field_document_date_created->getValue()[0]['value'];
        }

        if ( !empty($document_date_created) ) {
          $document_date_created = \Drupal::service('date.formatter')->format(strtotime($document_date_created), 'date_reversed');
        }

        if ( $revision_id == $rev ) {

          if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
            $html = get_system_template('demo-access-denied-node');
          } else if ( !empty($fc->field_document->getValue()) ) {
            $html = $fc->field_document->getValue()[0]['value'];
          }

          if ( !empty($fc->field_document_file->getValue()) ) {
            $file = \Drupal\file\Entity\File::load($fc->field_document_file->getValue()[0]['target_id']);
            if ( !empty($file) ){
              $file = file_url_transform_relative(file_create_url($file->getFileUri()));
              $parts = explode('.', $file);
              $file_type = end($parts);
              if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
                $file = 'demo';
              }
            }
          }

          $date_created = $document_date_created;
          $date_valid = $document_date_valid;

        }

        $revisions[] = (object)[
          'id' => $revision_id,
          'document_date_created' => $document_date_created,
          'document_date_valid' => $document_date_valid,
          //'revision_status' => $revision_status
        ];

        $revision_id--;

      }

    }



    $system = get_current_system();

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $query->sort('field_sort_date', 'DESC');
    $query->addCondition('field_systems', $system->id, 'IN');
    $query->addCondition('type', 'normdoc');

    $result = $query->execute();

    $items = array_keys($result->getResultItems());

    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };

    $itemsPerPage = 7;

    $nid_number = array_search($nid, $nids);

    $page_number = 0;

    if ( $nid_number !== false ) {
      $page_number = floor($nid_number / $itemsPerPage);
    }


    $topics = [];

    $themes_ids = [];

    if ( !empty($node->get('field_filter')->getValue()) ) {
       $themes_array = $node->get('field_filter')->getValue();

       foreach ($themes_array as $key => $val ) {
         if ( !empty($val['target_id'])) {
           $themes_ids[] = $val['target_id'];
           break;
         }
       }

    }

    if ( !empty($themes_ids) ) {

      $system = get_current_system();

      $index = \Drupal\search_api\Entity\Index::load('default');
      $query = $index->query();
      $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
      $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
      $query->setParseMode($parse_mode);
      $query->sort('search_api_relevance', 'DESC');
      $query->range(0, 5)
        ->addCondition('field_systems', $system->id, 'IN')
        ->addCondition('field_filter', $themes_ids[0], 'IN')
        ->addCondition('nid', $nid, '<>')
        ->addCondition('type', 'normdoc')
        ->addCondition('status', '1');

      $query->sort('changed', 'DESC');
      $result = $query->execute();
      $items = array_keys($result->getResultItems());
      $nids = [];
      foreach ($items as $item) {
        $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
      };
      $topic_nodes = node_load_multiple($nids);

      if ( !empty($topic_nodes) ) {

        foreach($topic_nodes as $topic_node) {
          $topic_id = intVal($topic_node->Id());
          $topic_title = $topic_node->getTitle();
          $topic_changed = \Drupal::service('date.formatter')->format($topic_node->getChangedTime(), 'date_reversed');
          $topic_demo = !empty($topic_node->get('field_demo')->getValue()[0]['value']);

          $topic = (object)[
            'id' => $topic_id,
            'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$topic_id),
            'title' => $topic_title,
            'demo' => $topic_demo,
            'changed' => $topic_changed
          ];
          $topics[] = $topic;
        }

      }

    }

    $data = (object)[
      'id' => intVal($node->Id()),
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
      'created' => $date,
      'date_created' => $date_created,
      'date_valid' => $date_valid,
      'date_invalid' => $date_invalid,
      'date_partially_invalid' => $date_partially_invalid,
      'revisions' => array_reverse($revisions),
      'currentRevision' => $rev,
      'currentRevisionStatus' => $rev_status,
      'page' => $page_number,
      'title' => $node->getTitle(),
      'file' => $file,
      'favorite' => $favorite,
      'file_type' => $file_type,
      'html' => $html,
      'topics' => $topics
    ];

    return $data;
  }

  public static function getOrientation ($params) {

    $nid = $params->cid;
    $node = node_load($nid);
    $user_roles = \Drupal::currentUser()->getRoles();
    if ( empty($node) || ( !empty($node) && !$node->isPublished() && !count(array_intersect(['administrator', 'editor', 'root'], $user_roles )) ) ) {
      return false;
    }
    self::incViews($nid);
    $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');

    $systemUser = \Drupal::currentUser()->Id();

    $html = '';

    if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
      $html = get_system_template('demo-access-denied-node');
    } else if ( !empty($node->field_profession_description->getValue()) ) {
      $html = $node->field_profession_description->getValue()[0]['value'];
    }

    $dkhp_string = [];
    if ( !empty( $node->field_dkhp->getValue() ) ) {
      $term_id = $node->field_dkhp->getValue()[0]['target_id'];
      $terms = \Drupal::service('entity_type.manager')->getStorage("taxonomy_term")->loadAllParents($term_id);
      $term_labels = [];
      foreach ($terms as $term) {
        $term_labels[] = $term->label();
      }
      $term_labels = array_reverse($term_labels);
      $dkhp_string = implode(' -- ', $term_labels);
    }

    $profession_code = '';
    if ( !empty( $node->field_profession_code->getValue() ) ) {
      $profession_code = $node->field_profession_code->getValue()[0]['value'];
    }

    $currentUser = \Drupal::currentUser();
    $isAnonymous = $currentUser->isAnonymous() || in_array('expert', $currentUser->getRoles());

    $data = (object)[
      'id' => intVal($node->Id()),
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
      'code' => $profession_code,
      'changed' => $date,
      'dkhp_string' => $dkhp_string,
      'title' => $node->getTitle(),
      'html' => $isAnonymous ? truncateHTML(300, $html) : $html,
    ];

    return $data;
  }

  public static function getExample ($params) {

    $nid = $params->id;
    $node = node_load($nid);
    $user_roles = \Drupal::currentUser()->getRoles();
    if ( empty($node) || ( !empty($node) && !$node->isPublished() && !count(array_intersect(['administrator', 'editor', 'root'], $user_roles )) ) ) {
      return false;
    }
    self::incViews($nid);
    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    $favorite = false;
    $currentUser = \Drupal::currentUser();
    if ( !empty($currentUser->Id()) ) {
      $favorite = $flag->isFlagged($node, $currentUser);
    }

    $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');

    $systemUser = \Drupal::currentUser()->Id();

    $html = '';
    $file = false;
    $file_preview = false;
    $file_type = false;

    $system = get_current_system();

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $query->sort('changed', 'DESC');
    $query->addCondition('field_systems', $system->id, 'IN');
    $query->addCondition('type', 'example');

    $result = $query->execute();

    $items = array_keys($result->getResultItems());

    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };

    $itemsPerPage = 7;

    $nid_number = array_search($nid, $nids);

    $page_number = 0;

    if ( $nid_number !== false ) {
      $page_number = floor($nid_number / $itemsPerPage);
    }

    $topics = [];
    $themes_ids = [];

    if ( !empty($node->get('field_filter')->getValue()) ) {
       $themes_array = $node->get('field_filter')->getValue();

       foreach ($themes_array as $key => $val ) {
         if ( !empty($val['target_id'])) {
           $themes_ids[] = $val['target_id'];
           break;
         }
       }

    }

    if ( !empty($themes_ids) ) {

      $system = get_current_system();

      $index = \Drupal\search_api\Entity\Index::load('default');
      $query = $index->query();
      $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
      $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
      $query->setParseMode($parse_mode);
      $query->sort('search_api_relevance', 'DESC');
      $query->range(0, 5)
        ->addCondition('field_systems', $system->id, 'IN')
        ->addCondition('field_filter', $themes_ids[0], 'IN')
        ->addCondition('nid', $nid, '<>')
        ->addCondition('type', 'example')
        ->addCondition('status', '1');

        $query->sort('changed', 'DESC');
      $result = $query->execute();
      $items = array_keys($result->getResultItems());
      $nids = [];
      foreach ($items as $item) {
        $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
      };
      $topic_nodes = node_load_multiple($nids);

      if ( !empty($topic_nodes) ) {

        foreach($topic_nodes as $topic_node) {
          $topic_id = intVal($topic_node->Id());
          $topic_title = $topic_node->getTitle();
          $topic_changed = \Drupal::service('date.formatter')->format($topic_node->getChangedTime(), 'date_reversed');
          $topic_demo = !empty($topic_node->get('field_demo')->getValue()[0]['value']);

          $topic = (object)[
            'id' => $topic_id,
            'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$topic_id),
            'title' => $topic_title,
            'demo' => $topic_demo,
            'changed' => $topic_changed
          ];
          $topics[] = $topic;
        }

      }

    }

    $with_example = false;

    if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
      $html = get_system_template('demo-access-denied-node');
    } else if ( !empty($node->field_example_html->getValue()) ) {
      $html = $node->field_example_html->getValue()[0]['value'];
    }

    if ( !empty($node->get('field_with_example')->getValue()) ) {
      $with_example = $node->get('field_with_example')->getValue()[0]['value'];
    }

    if ( !empty($node->field_example_file->getValue()[0]['target_id']) ) {
      $file = \Drupal\file\Entity\File::load($node->field_example_file->getValue()[0]['target_id']);
      if ( !empty($file) ){
        $file = file_url_transform_relative(file_create_url($file->getFileUri()));
        $parts = explode('.', $file);
        $file_type = end($parts);
        if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
          $file = 'demo';
        }
      }
    }

    if ( !empty($node->field_example_file_preview->getValue()[0]['target_id']) ) {
      $file_preview = \Drupal\file\Entity\File::load($node->field_example_file_preview->getValue()[0]['target_id']);
      if ( !empty($file_preview) ){
        $file_preview = file_url_transform_relative(file_create_url($file_preview->getFileUri()));
        if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
          $file_preview = 'demo';
        }
      }
    }

    $currentUser = \Drupal::currentUser();
    $isAnonymous = $currentUser->isAnonymous() || in_array('expert', $currentUser->getRoles());

    $data = (object)[
      'id' => intVal($node->Id()),
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
      'changed' => $date,
      'with_example' => $with_example == "1",
      'page' => $page_number,
      'title' => $node->getTitle(),
      'file' => $isAnonymous ? false : $file,
      'file_preview' => $isAnonymous ? false : $file_preview,
      'favorite' => $favorite,
      'file_type' => $file_type,
      'html' => $isAnonymous ? truncateHTML(300, $html) : $html,
      'topics' => $topics
    ];

    return $data;
  }

  public static function getAccountingLog ($params) {

    $nid = $params->id;
    $node = node_load($nid);
    $user_roles = \Drupal::currentUser()->getRoles();
    if ( empty($node) || ( !empty($node) && !$node->isPublished() && !count(array_intersect(['administrator', 'editor', 'root'], $user_roles )) ) ) {
      return false;
    }
    self::incViews($nid);
    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    $favorite = false;
    $currentUser = \Drupal::currentUser();
    if ( !empty($currentUser->Id()) ) {
      $favorite = $flag->isFlagged($node, $currentUser);
    }

    $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');

    $systemUser = \Drupal::currentUser()->Id();

    $html = '';
    $file = false;
    $file_preview = false;
    $file_type = false;

    $system = get_current_system();

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $query->sort('changed', 'DESC');
    $query->addCondition('field_systems', $system->id, 'IN');
    $query->addCondition('type', 'accountinglog');

    $result = $query->execute();

    $items = array_keys($result->getResultItems());

    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };

    $itemsPerPage = 7;

    $nid_number = array_search($nid, $nids);

    $page_number = 0;

    if ( $nid_number !== false ) {
      $page_number = floor($nid_number / $itemsPerPage);
    }

    $topics = [];
    $themes_ids = [];

    if ( !empty($node->get('field_filter')->getValue()) ) {
       $themes_array = $node->get('field_filter')->getValue();

       foreach ($themes_array as $key => $val ) {
         if ( !empty($val['target_id'])) {
           $themes_ids[] = $val['target_id'];
           break;
         }
       }

    }

    if ( !empty($themes_ids) ) {

      $system = get_current_system();

      $index = \Drupal\search_api\Entity\Index::load('default');
      $query = $index->query();
      $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
      $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
      $query->setParseMode($parse_mode);
      $query->sort('search_api_relevance', 'DESC');
      $query->range(0, 5)
        ->addCondition('field_systems', $system->id, 'IN')
        ->addCondition('field_filter', $themes_ids[0], 'IN')
        ->addCondition('nid', $nid, '<>')
        ->addCondition('type', 'accountinglog')
        ->addCondition('status', '1');

      $query->sort('changed', 'DESC');
      $result = $query->execute();
      $items = array_keys($result->getResultItems());
      $nids = [];
      foreach ($items as $item) {
        $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
      };
      $topic_nodes = node_load_multiple($nids);

      if ( !empty($topic_nodes) ) {

        foreach($topic_nodes as $topic_node) {
          $topic_id = intVal($topic_node->Id());
          $topic_title = $topic_node->getTitle();
          $topic_changed = \Drupal::service('date.formatter')->format($topic_node->getChangedTime(), 'date_reversed');
          $topic_demo = !empty($topic_node->get('field_demo')->getValue()[0]['value']);

          $topic = (object)[
            'id' => $topic_id,
            'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$topic_id),
            'title' => $topic_title,
            'demo' => $topic_demo,
            'changed' => $topic_changed
          ];
          $topics[] = $topic;
        }

      }

    }


    if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
      $html = get_system_template('demo-access-denied-node');
    } else if ( !empty($node->field_accountinglog_html->getValue()) ) {
      $html = $node->field_accountinglog_html->getValue()[0]['value'];
    }

    if ( !empty($node->field_accountinglog_file->getValue()[0]['target_id']) ) {
      $file = \Drupal\file\Entity\File::load($node->field_accountinglog_file->getValue()[0]['target_id']);
      if ( !empty($file) ){
        $file = file_url_transform_relative(file_create_url($file->getFileUri()));
        $parts = explode('.', $file);
        $file_type = end($parts);
        if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
          $file = 'demo';
        }
      }
    }

    if ( !empty($node->field_accountinglog_file_preview->getValue()[0]['target_id']) ) {
      $file_preview = \Drupal\file\Entity\File::load($node->field_accountinglog_file_preview->getValue()[0]['target_id']);
      if ( !empty($file_preview) ){
        $file_preview = file_url_transform_relative(file_create_url($file_preview->getFileUri()));
        if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
          $file_preview = 'demo';
        }
      }
    }

    $currentUser = \Drupal::currentUser();
    $isAnonymous = $currentUser->isAnonymous() || in_array('expert', $currentUser->getRoles());

    $data = (object)[
      'id' => intVal($node->Id()),
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
      'changed' => $date,
      'page' => $page_number,
      'title' => $node->getTitle(),
      'file' => $isAnonymous ? false : $file,
      'file_preview' => $isAnonymous ? false : $file_preview,
      'favorite' => $favorite,
      'file_type' => $file_type,
      'html' => $isAnonymous ? truncateHTML(300, $html) : $html,
      'topics' => $topics
    ];

    return $data;
  }

  public static function getCrib ($params) {

    $nid = $params->id;
    $node = node_load($nid);
    $user_roles = \Drupal::currentUser()->getRoles();
    if ( empty($node) || ( !empty($node) && !$node->isPublished() && !count(array_intersect(['administrator', 'editor', 'root'], $user_roles )) ) ) {
      return false;
    }
    self::incViews($nid);
    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    $favorite = false;
    $currentUser = \Drupal::currentUser();
    if ( !empty($currentUser->Id()) ) {
      $favorite = $flag->isFlagged($node, $currentUser);
    }

    if ( !empty($node->field_sort_date->getValue()) ) {
      $date = \Drupal::service('date.formatter')->format($node->field_sort_date->getValue()[0]['value'], 'date_reversed');
    } else {
      $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');
    }

    $systemUser = \Drupal::currentUser()->Id();

    $html = '';
    $image = false;

    $system = get_current_system();

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $query->sort('field_sort_date', 'DESC');
    $query->addCondition('field_systems', $system->id, 'IN');
    $query->addCondition('type', 'crib');

    $result = $query->execute();

    $items = array_keys($result->getResultItems());

    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };

    $itemsPerPage = 12;

    $nid_number = array_search($nid, $nids);

    $page_number = 0;

    if ( $nid_number !== false ) {
      $page_number = floor($nid_number / $itemsPerPage);
    }

    $topics = [];

    $themes_ids = [];

    if ( !empty($node->get('field_filter')->getValue()) ) {
       $themes_array = $node->get('field_filter')->getValue();

       foreach ($themes_array as $key => $val ) {
         if ( !empty($val['target_id'])) {
           $themes_ids[] = $val['target_id'];
           break;
         }
       }

    }

    if ( !empty($themes_ids) ) {

      $system = get_current_system();

      $index = \Drupal\search_api\Entity\Index::load('default');
      $query = $index->query();
      $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
      $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
      $query->setParseMode($parse_mode);
      $query->sort('search_api_relevance', 'DESC');
      $query->range(0, 5)
        ->addCondition('field_systems', $system->id, 'IN')
        ->addCondition('field_filter', $themes_ids[0], 'IN')
        ->addCondition('nid', $nid, '<>')
        ->addCondition('type', 'crib')
        ->addCondition('status', '1');

      $query->sort('changed', 'DESC');
      $result = $query->execute();
      $items = array_keys($result->getResultItems());
      $nids = [];
      foreach ($items as $item) {
        $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
      };
      $topic_nodes = node_load_multiple($nids);

      if ( !empty($topic_nodes) ) {

        foreach($topic_nodes as $topic_node) {
          $topic_id = intVal($topic_node->Id());
          $topic_title = $topic_node->getTitle();
          $topic_changed = \Drupal::service('date.formatter')->format($topic_node->getChangedTime(), 'date_reversed');
          $topic_demo = !empty($topic_node->get('field_demo')->getValue()[0]['value']);

          $topic = (object)[
            'id' => $topic_id,
            'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$topic_id),
            'title' => $topic_title,
            'demo' => $topic_demo,
            'changed' => $topic_changed
          ];
          $topics[] = $topic;
        }

      }

    }

    if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
      $html = get_system_template('demo-access-denied-node');
    } else if ( !empty($node->field_crib_html->getValue()) ) {
      $html = $node->field_crib_html->getValue()[0]['value'];
    }

    $currentUser = \Drupal::currentUser();
    $isAnonymous = $currentUser->isAnonymous() || in_array('expert', $currentUser->getRoles());

    if ( !empty($node->field_crib_file->getValue()[0]['target_id']) ) {
      $image = \Drupal\file\Entity\File::load($node->field_crib_file->getValue()[0]['target_id']);
      if ( !empty($image) ){
        $image_uri = $image->getFileUri();
        $image = get_image_path_pdf($image_uri, $isAnonymous ? 'demo' : 'original');
      }
    }
    $data = (object)[
      'id' => intVal($node->Id()),
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
      'changed' => $date,
      'page' => $page_number,
      'file' => false,
      'file_preview' => false,
      'title' => $node->getTitle(),
      'favorite' => $favorite,
      'html' => $isAnonymous ? truncateHTML(300, $html) : $html,
      'image' => $image,
      'topics' => $topics
    ];

    return $data;
  }

  public static function getInterview ($params) {

    $nid = $params->id;
    $node = node_load($nid);
    $user_roles = \Drupal::currentUser()->getRoles();
    if ( empty($node) || ( !empty($node) && !$node->isPublished() && !count(array_intersect(['administrator', 'editor', 'root'], $user_roles )) ) ) {
      return false;
    }
    self::incViews($nid);
    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    $favorite = false;
    $currentUser = \Drupal::currentUser();
    if ( !empty($currentUser->Id()) ) {
      $favorite = $flag->isFlagged($node, $currentUser);
    }

    $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');

    $systemUser = \Drupal::currentUser()->Id();

    $html = '';
    $file = false;
    $file_type = false;


    $system = get_current_system();

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $query->sort('created', 'DESC');
    $query->addCondition('field_systems', $system->id, 'IN');
    $query->addCondition('type', 'interview');

    $result = $query->execute();

    $items = array_keys($result->getResultItems());

    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };

    $itemsPerPage = 10;

    $nid_number = array_search($nid, $nids);

    $page_number = 0;

    if ( $nid_number !== false ) {
      $page_number = floor($nid_number / $itemsPerPage);
    }


    $topics = [];

    $themes_ids = [];

    if ( !empty($node->get('field_filter')->getValue()) ) {
       $themes_array = $node->get('field_filter')->getValue();

       foreach ($themes_array as $key => $val ) {
         if ( !empty($val['target_id'])) {
           $themes_ids[] = $val['target_id'];
           break;
         }
       }

    }

    if ( !empty($themes_ids) ) {

      $system = get_current_system();

      $index = \Drupal\search_api\Entity\Index::load('default');
      $query = $index->query();
      $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
      $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
      $query->setParseMode($parse_mode);
      $query->sort('search_api_relevance', 'DESC');
      $query->range(0, 5)
        ->addCondition('field_systems', $system->id, 'IN')
        ->addCondition('field_filter', $themes_ids[0], 'IN')
        ->addCondition('nid', $nid, '<>')
        ->addCondition('type', 'interview')
        ->addCondition('status', '1');

      $query->sort('changed', 'DESC');
      $result = $query->execute();
      $items = array_keys($result->getResultItems());
      $nids = [];
      foreach ($items as $item) {
        $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
      };
      $topic_nodes = node_load_multiple($nids);

      if ( !empty($topic_nodes) ) {

        foreach($topic_nodes as $topic_node) {
          $topic_id = intVal($topic_node->Id());
          $topic_title = $topic_node->getTitle();
          $topic_changed = \Drupal::service('date.formatter')->format($topic_node->getChangedTime(), 'date_reversed');
          $topic_demo = !empty($topic_node->get('field_demo')->getValue()[0]['value']);

          $topic = (object)[
            'id' => $topic_id,
            'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$topic_id),
            'title' => $topic_title,
            'demo' => $topic_demo,
            'changed' => $topic_changed
          ];
          $topics[] = $topic;
        }

      }

    }

    if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
      $html = get_system_template('demo-access-denied-node');
    } else if ( !empty($node->field_interview_html->getValue()) ) {
      $html = $node->field_interview_html->getValue()[0]['value'];
    }

    $html = str_replace('src="/sites/default', 'src="' . $system->domain . '/sites/default', $html);
    $currentUser = \Drupal::currentUser();
    $isAnonymous = $currentUser->isAnonymous() || in_array('expert', $currentUser->getRoles());

    $interviews = [];
    if ( !empty($node->field_interview->getValue()) ) {
      $interview_parts = $node->field_interview->getValue();
      foreach($interview_parts as $interview_part){
        $interview_part = \Drupal\paragraphs\Entity\Paragraph::load( $interview_part['target_id'] );
        if ( !empty($interview_part->field_question_text) && !empty($interview_part->field_question_text->getValue()) ) {

          $interviewer_2 = false;

          $interviewer_2_fio = '';
          $interviewer_2_id = 0;
          $interviewer_2_photo = '';
          $interviewer_2_photo_78 = '';
          $interviewer_2_position = '';

          if ( !empty($interview_part->get('field_interviewer')->getValue()) ) {
            $interviewer_2_id = $interview_part->get('field_interviewer')->getValue()[0]['target_id'];
            $interviewer_2 = user_load($interviewer_2_id);

            if ( !empty($interviewer_2->get('field_fio')->getValue()) ) {
              $interviewer_2_fio = $interviewer_2->get('field_fio')->getValue()[0]['value'];
            }

            if ( !empty($interviewer_2->get('field_position')->getValue()) ) {
              $interviewer_2_position = $interviewer_2->get('field_position')->getValue()[0]['value'];
            }

            if ( !empty($interviewer_2->get('field_photo')->getValue()) ) {

              $fid = $interviewer_2->get('field_photo')->getValue()[0]['target_id'];

              if ( $fid ) {

                $file = file_load($fid);
                if ( !empty($file) ) {
                  $interviewer_2_photo_uri = $file->getFileUri();
                  $interviewer_2_photo = get_image_path_pdf($interviewer_2_photo_uri, 'photo_expert');
                  $interviewer_2_photo_78 = get_image_path_pdf($interviewer_2_photo_uri, 'photo_expert_78');
                }
              };

            }

          }

          $interviews[] = (object)[
            'type' => 'question',
            'interviewer' => (object)[
              'id' => $interviewer_2_id,
              'fio' => $interviewer_2_fio,
              'photo' => $interviewer_2_photo,
              'photo_78' => $interviewer_2_photo_78,
              'position' => $interviewer_2_position
            ],
            'html' => $interview_part->field_question_text->getValue()[0]['value']
          ];
        };

        if ( !empty($interview_part->field_answer_text) && !empty($interview_part->field_answer_text->getValue()) ) {

          $answer_html = '';

          if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
            $answer_html = get_system_template('demo-access-denied-node');
          } else if ( !empty($interview_part->field_answer_text->getValue()[0]['value']) ) {
            $answer_html = $interview_part->field_answer_text->getValue()[0]['value'];
          }

          $interviewee_2 = false;

          $interviewee_2_fio = '';
          $interviewee_2_id = 0;
          $interviewee_2_photo = '';
          $interviewee_2_photo_78 = '';
          $interviewee_2_position = '';

          if ( !empty($interview_part->get('field_interviewee')->getValue()) ) {
            $interviewee_2_id = $interview_part->get('field_interviewee')->getValue()[0]['target_id'];
            $interviewee_2 = node_load($interviewee_2_id);

            if ( !empty($interviewee_2->getTitle()) ) {
              $interviewee_2_fio = $interviewee_2->getTitle();
            }

            if ( !empty($interviewee_2->get('field_position')->getValue()) ) {
              $interviewee_2_position = $interviewee_2->get('field_position')->getValue()[0]['value'];
            }

            if ( !empty($interviewee_2->get('field_interviewee_photo')->getValue()) ) {

              $fid = $interviewee_2->get('field_interviewee_photo')->getValue()[0]['target_id'];

              if ( $fid ) {

                $interviewee_2_photo_uri = file_load($fid)->getFileUri();
                $interviewee_2_photo = get_image_path_pdf($interviewee_2_photo_uri, 'photo_expert_100');
                $interviewee_2_photo_78 = get_image_path_pdf($interviewee_2_photo_uri, 'photo_expert_78');

              };

            }

          }


          if ( !empty($interview_part->get('field_interviewee_expert')->getValue()) ) {

            $interviewee_2 = false;

            $interviewee_2_fio = '';
            $interviewee_2_id = $interview_part->get('field_interviewee_expert')->getValue()[0]['target_id'];
            $interviewee_2_photo = '';
            $interviewee_2_photo_78 = '';
            $interviewee_2_position = '';

            $interviewee_2 = user_load($interviewee_2_id);

            if ( !empty($interviewee_2->get('field_fio')->getValue()) ) {
              $interviewee_2_fio = $interviewee_2->get('field_fio')->getValue()[0]['value'];
            }

            if ( !empty($interviewee_2->get('field_position')->getValue()) ) {
              $interviewee_2_position = $interviewee_2->get('field_position')->getValue()[0]['value'];
            }

            if ( !empty($interviewee_2->get('field_photo')->getValue()) ) {

              $fid = $interviewee_2->get('field_photo')->getValue()[0]['target_id'];

              if ( $fid ) {
                $file = file_load($fid);
                if ( !empty($file) ) {
                  $interviewee_2_photo_uri = $file->getFileUri();
                  $interviewee_2_photo = get_image_path_pdf($interviewee_2_photo_uri, 'photo_expert_100');
                  $interviewee_2_photo_78 = get_image_path_pdf($interviewee_2_photo_uri, 'photo_expert_78');
                }

              };

            }
          }

          $interviews[] = (object)[
            'type' => 'answer',
            'interviewee' => (object)[
              'id' => $interviewee_2_id,
              'fio' => $interviewee_2_fio,
              'photo' => $interviewee_2_photo,
              'photo_78' => $interviewee_2_photo_78,
              'position' => $interviewee_2_position
            ],
            'html' => $isAnonymous ? truncateHTML(300, $answer_html) : $answer_html,
          ];
        }
      };
    }

    $with_interview = false;

    if ( !empty($node->get('field_interview')->getValue()) ) {
       $with_interview = count($node->get('field_interview'));
    }

    $interviewer = false;

    $interviewer_fio = '';
    $interviewer_id = 0;
    $interviewer_photo = '';
    $interviewer_photo_78 = '';
    $interviewer_position = '';

    if ( !empty($node->get('uid')) ) {
      $interviewer_id = $node->getOwner()->id();
      $interviewer = user_load($interviewer_id);

      if ( !empty($interviewer->get('field_fio')->getValue()) ) {
        $interviewer_fio = $interviewer->get('field_fio')->getValue()[0]['value'];
      }

      if ( !empty($interviewer->get('field_position')->getValue()) ) {
        $interviewer_position = $interviewer->get('field_position')->getValue()[0]['value'];
      }

      if ( !empty($interviewer->get('field_photo')->getValue()) ) {

        $fid = $interviewer->get('field_photo')->getValue()[0]['target_id'];

        if ( $fid ) {

          $file = file_load($fid);
          if ( !empty($file) ) {
            $interviewer_photo_uri = file_load($fid)->getFileUri();
            $interviewer_photo = get_image_path_pdf($interviewer_photo_uri, 'photo_expert');
            $interviewer_photo_78 = get_image_path_pdf($interviewer_photo_uri, 'photo_expert_78');
          }

        };

      }

    }

    $interviewees = [];

    if ( !empty($node->get('field_interviewee')->getValue()) ) {

      foreach ($node->get('field_interviewee')->getValue() as $key => $value) {

        $interviewee = false;

        $interviewee_fio = '';
        $interviewee_id = $value['target_id'];
        $interviewee_photo = '';
        $interviewee_photo_78 = '';
        $interviewee_position = '';

        $interviewee = node_load($interviewee_id);

        if ( !empty($interviewee->getTitle()) ) {
          $interviewee_fio = $interviewee->getTitle();
        }

        if ( !empty($interviewee->get('field_position')->getValue()) ) {
          $interviewee_position = $interviewee->get('field_position')->getValue()[0]['value'];
        }

        if ( !empty($interviewee->get('field_interviewee_photo')->getValue()) ) {

          $fid = $interviewee->get('field_interviewee_photo')->getValue()[0]['target_id'];

          if ( $fid ) {

            $interviewee_photo_uri = file_load($fid)->getFileUri();
            $interviewee_photo = get_image_path_pdf($interviewee_photo_uri, 'photo_expert_100');
            $interviewee_photo_78 = get_image_path_pdf($interviewee_photo_uri, 'photo_expert_78');

          };

        }

        $interviewees[] = (object)[
          'id' => $interviewee_id,
          'fio' => $interviewee_fio,
          'photo' => $interviewee_photo,
          'photo_78' => $interviewee_photo_78,
          'position' => $interviewee_position
        ];

      }

    }

    if ( !empty($node->get('field_expert')->getValue()) ) {

      foreach ($node->get('field_expert')->getValue() as $key => $value) {

        $interviewee = false;

        $interviewee_fio = '';
        $interviewee_id = $value['target_id'];
        $interviewee_photo = '';
        $interviewee_photo_78 = '';
        $interviewee_position = '';

        $interviewee = user_load($interviewee_id);

        if ( !empty($interviewee->get('field_fio')->getValue()) ) {
          $interviewee_fio = $interviewee->get('field_fio')->getValue()[0]['value'];
        }

        if ( !empty($interviewee->get('field_position')->getValue()) ) {
          $interviewee_position = $interviewee->get('field_position')->getValue()[0]['value'];
        }

        if ( !empty($interviewee->get('field_photo')->getValue()) ) {

          $fid = $interviewee->get('field_photo')->getValue()[0]['target_id'];

          if ( $fid ) {

            $interviewee_photo_uri = file_load($fid)->getFileUri();
            $interviewee_photo = get_image_path_pdf($interviewee_photo_uri, 'photo_expert_100');
            $interviewee_photo_78 = get_image_path_pdf($interviewee_photo_uri, 'photo_expert_78');

          };

        }

        $interviewees[] = (object)[
          'id' => $interviewee_id,
          'fio' => $interviewee_fio,
          'photo' => $interviewee_photo,
          'photo_78' => $interviewee_photo_78,
          'position' => $interviewee_position
        ];

      }

    }

    if ( count($interviewees) ) {
      $interviewee = $interviewees[0];
    } else {
      $interviewee = (object)[
        'id' => 0,
        'fio' => '',
        'photo' => '',
        'photo_78' => '',
        'position' => ''
      ];
    }

    $data = (object)[
      'id' => intVal($node->Id()),
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
      'changed' => $date,
      'with_interview' => $with_interview,
      'page' => $page_number,
      'title' => $node->getTitle(),
      'file' => $file,
      'favorite' => $favorite,
      'file_type' => $file_type,
      'html' => $isAnonymous ? truncateHTML(300, $html) : $html,
      'topics' => $topics,
      'interviews' => $interviews,
      'interviewer' => (object)[
        'id' => $interviewer_id,
        'fio' => $interviewer_fio,
        'photo' => $interviewer_photo,
        'photo_78' => $interviewer_photo_78,
        'position' => $interviewer_position
      ],
      'interviewee' => $interviewee,
      'interviewees' => $interviewees
    ];

    return $data;
  }

  public static function getMessage ($params) {

    $nid = $params->id;

    $node = node_load($nid);

    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    $favorite = false;
    $currentUser = \Drupal::currentUser();
    if ( !empty($currentUser->Id()) ) {
      $favorite = $flag->isFlagged($node, $currentUser);
    }

    $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');

    $systemUser = \Drupal::currentUser()->Id();

    $system = get_current_system();

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $query->sort('created', 'DESC')
      ->addCondition('field_systems', $system->id, 'IN')
      ->addCondition('field_users_deleted', $systemUser, '<>')
      ->addCondition('type', 'message');
    $orGroup = $query->createConditionGroup('OR')
      ->addCondition('field_message_to', $systemUser)
      ->addCondition('field_message_to_all', 1);
    $query->addConditionGroup($orGroup);

    $result = $query->execute();

    $items = array_keys($result->getResultItems());

    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };

    $itemsPerPage = 25;

    $nid_number = array_search($nid, $nids);

    $page_number = 0;

    if ( $nid_number !== false ) {
      $page_number = floor($nid_number / $itemsPerPage);
    }


    $html = '';

    if ( !empty($node) ) {

      if ( !empty($node->field_message_html->getValue()) ) {
        $html = $node->field_message_html->getValue()[0]['value'];
      }

      $viewed_users = $node->get('field_users_viewed')->getValue();
      if ( empty($viewed_users) || empty(array_filter($viewed_users, function($element) use ($systemUser) { return $element['target_id'] == $systemUser; })) ) {
        $node->field_users_viewed->appendItem($systemUser);
        $changed_date = $node->changed->value;
        $node->save();
        konsultant_restore_changed_date($changed_date, $node->Id());
      }

    }

    $data = (object)[
      'id' => intVal($node->Id()),
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
      'changed' => $date,
      'page' => $page_number,
      'title' => $node->getTitle(),
      'favorite' => $favorite,
      'html' => $html
    ];

    return $data;
  }

  public function deleteMessages($params) {

    $nids = $params->messages;
    $systemUser = \Drupal::currentUser()->Id();

    $deletedCount = 0;

    foreach ( $nids as $nid ) {

      $node = node_load($nid);

      $deleted_users = $node->get('field_users_deleted')->getValue();

      if ( empty($deleted_users) || empty(array_filter($deleted_users, function($element) use ($systemUser) { return $element['target_id'] == $systemUser; })) ) {
        $node->field_users_deleted->appendItem($systemUser);
        $changed_date = $node->changed->value;
        $node->save();
        konsultant_restore_changed_date($changed_date, $node->Id());

        $flag_service = \Drupal::service('flag');
        $flag = $flag_service->getFlagById('favorites');
        $favorite = false;
        $currentUser = \Drupal::currentUser();
        if ( !empty($currentUser->Id()) ) {
          $favorite = $flag->isFlagged($node, $currentUser);
        }


        if ( $favorite ) {
          $flag_service->unflag($flag, $node);
        }

        $deletedCount++;
      }

    }

    $data = (object)[
      'deletedCount' => $deletedCount
    ];

    return $data;
  }

  public static function getNew ($params) {

    $nid = $params->id;
    $node = Node::load($nid);
    $user_roles = \Drupal::currentUser()->getRoles();
    if ( empty($node) || ( !empty($node) && !$node->isPublished() && !count(array_intersect(['administrator', 'editor', 'root'], $user_roles )) ) ) {
      return false;
    }
    self::incViews($nid);
    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    $favorite = false;
    $currentUser = \Drupal::currentUser();
    $isAnonymous = $currentUser->isAnonymous() || in_array('expert', $currentUser->getRoles());
    if ( !empty($currentUser->Id()) ) {
      $favorite = $flag->isFlagged($node, $currentUser);
    }

    $date = \Drupal::service('date.formatter')->format($node->getCreatedTime(), 'date_reversed');

    $systemUser = \Drupal::currentUser()->Id();

    $new_type = '';

    if ( !empty($node->get('field_new_type')->getValue()) ) {
      $new_type_id = $node->get('field_new_type')->getValue()[0]['target_id'];
      $new_type_term = Term::load($new_type_id);
      if ( !empty($new_type_term) ) {
        $new_type = $new_type_term->getName();
      }
    }

    $new_html = '';


    $system = get_current_system();

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $query->sort('created', 'DESC');
    $query->addCondition('field_systems', $system->id, 'IN');
    $query->addCondition('type', 'new');

    $result = $query->execute();

    $items = array_keys($result->getResultItems());

    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };

    $itemsPerPage = 7;

    $nid_number = array_search($nid, $nids);

    $page_number = 0;

    if ( $nid_number !== false ) {
      $page_number = floor($nid_number / $itemsPerPage);
    }


    $topics = [];

    $themes_ids = [];

    if ( !empty($node->get('field_filter')->getValue()) ) {
        $themes_array = $node->get('field_filter')->getValue();

        foreach ($themes_array as $key => $val ) {
          if ( !empty($val['target_id'])) {
            $themes_ids[] = $val['target_id'];
            break;
          }
        }

    }

    if ( !empty($themes_ids) ) {

      $system = get_current_system();

      $index = \Drupal\search_api\Entity\Index::load('default');
      $query = $index->query();
      $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
      $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
      $query->setParseMode($parse_mode);
      $query->sort('search_api_relevance', 'DESC');
      $query->range(0, 5)
        ->addCondition('field_systems', $system->id, 'IN')
        ->addCondition('field_filter', $themes_ids[0], 'IN')
        ->addCondition('nid', $nid, '<>')
        ->addCondition('type', 'new')
        ->addCondition('status', '1');

      $query->sort('changed', 'DESC');
      $result = $query->execute();
      $items = array_keys($result->getResultItems());
      $nids = [];
      foreach ($items as $item) {
        $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
      };
      $topic_nodes = Node::loadMultiple($nids);

      if ( !empty($topic_nodes) ) {

        foreach($topic_nodes as $topic_node) {
          $topic_id = intVal($topic_node->Id());
          $topic_title = $topic_node->getTitle();
          $topic_changed = \Drupal::service('date.formatter')->format($topic_node->getChangedTime(), 'date_reversed');
          $topic_demo = !empty($topic_node->get('field_demo')->getValue()[0]['value']);

          $topic = (object)[
            'id' => $topic_id,
            'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$topic_id),
            'title' => $topic_title,
            'demo' => $topic_demo,
            'changed' => $topic_changed
          ];
          $topics[] = $topic;
        }

      }

    }

    $hasAccess = isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']);

    if ( $hasAccess ) {
      $new_html = get_system_template('demo-access-denied-node');
    } else if ( !empty($node->get('field_new_html')->getValue()) ) {
      $new_html = $node->get('field_new_html')->getValue()[0]['value'];
    }

    $new_source = '';
    if ( !empty($node->get('field_new_source')->getValue()) ) {
      $new_source_id = $node->get('field_new_source')->getValue()[0]['target_id'];
      if ( !empty($new_source_id) ) {
        $new_source_term = Term::load($new_source_id);
        if ( !empty($new_source_term) ) {
          $new_source = $new_source_term->getName();
        }
      }
    }


    $image = '';
    if ( !empty($node->field_new_image->getValue()) ) {
      $file = File::load($node->field_new_image->getValue()[0]['target_id'])->getFileUri();
      if ( !empty($file) ){
        $image = get_image_path_pdf($file, 'new');
      }
    }

    $accessClosed = !$currentUser->isAuthenticated() && !empty($node->get('field_access_closed')->getValue()[0]['value']);

    $data = (object)[
      'id' => intVal($node->Id()),
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
      'created' => $date,
      'new_type' => $new_type,
      'new_source' => $new_source,
      'page' => $page_number,
      'title' => $node->getTitle(),
      'image' => $image,
      'favorite' => $favorite,
      'new_html' => ($isAnonymous && $accessClosed) ? truncateHTML(300, $new_html) : $new_html,
      'hasAccess' => !$accessClosed,
      'topics' => $topics
    ];

    return $data;

  }

  public static function getInstruction ($params) {

    $nid = $params->id;
    $node = node_load($nid);
    $user_roles = \Drupal::currentUser()->getRoles();
    if ( empty($node) || ( !empty($node) && !$node->isPublished() && !count(array_intersect(['administrator', 'editor', 'root'], $user_roles )) ) ) {
      return false;
    }
    self::incViews($nid);
    $flag_service = \Drupal::service('flag');
    $flag = $flag_service->getFlagById('favorites');

    $favorite = false;
    $currentUser = \Drupal::currentUser();
    if ( !empty($currentUser->Id()) ) {
      $favorite = $flag->isFlagged($node, $currentUser);
    }

    $date = \Drupal::service('date.formatter')->format($node->getChangedTime(), 'date_reversed');

    $systemUser = \Drupal::currentUser()->Id();

    $html = '';
    $file = false;
    $file_type = false;


    $system = get_current_system();

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $query->sort('changed', 'DESC');
    $query->addCondition('type', 'instruction');

    $result = $query->execute();

    $items = array_keys($result->getResultItems());

    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };

    $itemsPerPage = 7;

    $nid_number = array_search($nid, $nids);

    $page_number = 0;

    if ( $nid_number !== false ) {
      $page_number = floor($nid_number / $itemsPerPage);
    }

    $topics = [];

    $themes_ids = [];

    if ( !empty($node->get('field_filter')->getValue()) ) {
       $themes_array = $node->get('field_filter')->getValue();

       foreach ($themes_array as $key => $val ) {
         if ( !empty($val['target_id'])) {
           $themes_ids[] = $val['target_id'];
           break;
         }
       }

    }

    if ( !empty($themes_ids) ) {

      $system = get_current_system();

      $index = \Drupal\search_api\Entity\Index::load('default');
      $query = $index->query();
      $query->setFulltextFields(['field_computed_title', 'field_document_number', 'title', 'field_question', 'field_help_html', 'field_answer']);
      $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
      $query->setParseMode($parse_mode);
      $query->sort('search_api_relevance', 'DESC');
      $query->range(0, 5)
        ->addCondition('field_systems', $system->id, 'IN')
        ->addCondition('field_filter', $themes_ids[0], 'IN')
        ->addCondition('nid', $nid, '<>')
        ->addCondition('type', 'instruction')
        ->addCondition('status', '1');

      $query->sort('changed', 'DESC');
      $result = $query->execute();
      $items = array_keys($result->getResultItems());
      $nids = [];
      foreach ($items as $item) {
        $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
      };
      $topic_nodes = node_load_multiple($nids);

      if ( !empty($topic_nodes) ) {

        foreach($topic_nodes as $topic_node) {
          $topic_id = intVal($topic_node->Id());
          $topic_title = $topic_node->getTitle();
          $topic_changed = \Drupal::service('date.formatter')->format($topic_node->getChangedTime(), 'date_reversed');
          $topic_demo = !empty($topic_node->get('field_demo')->getValue()[0]['value']);

          $topic = (object)[
            'id' => $topic_id,
            'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$topic_id),
            'title' => $topic_title,
            'demo' => $topic_demo,
            'changed' => $topic_changed
          ];
          $topics[] = $topic;
        }

      }

    }

    $instruction_type = false;

    if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
      $html = get_system_template('demo-access-denied-node');
    } else if ( !empty($node->field_instruction_html->getValue()) ) {
      $html = $node->field_instruction_html->getValue()[0]['value'];
    }

    if ( !empty($node->field_instruction_type->getValue()) ) {
      $instruction_type = $node->field_instruction_type->getValue()[0]['value'];
    }

    if ( !empty($node->field_instruction_file->getValue()) ) {
      $file = \Drupal\file\Entity\File::load($node->field_instruction_file->getValue()[0]['target_id']);
      if ( !empty($file) ){
        $file = file_url_transform_relative(file_create_url($file->getFileUri()));
        $parts = explode('.', $file);
        $file_type = end($parts);
        if ( isDemoUser($systemUser) && empty($node->get('field_demo')->getValue()[0]['value']) ) {
          $file = 'demo';
        }
      }
    }

    $currentUser = \Drupal::currentUser();
    $isAnonymous = $currentUser->isAnonymous() || in_array('expert', $currentUser->getRoles());

    $data = (object)[
      'id' => intVal($node->Id()),
      'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
      'changed' => $date,
      'instruction_type' => $instruction_type,
      'page' => $page_number,
      'title' => $node->getTitle(),
      'file' => $isAnonymous ? false : $file,
      'favorite' => $favorite,
      'file_type' => $file_type,
      'html' => $isAnonymous ? truncateHTML(300, $html) : $html,
      'topics' => $topics
    ];

    return $data;
  }

  public function checkNewEvents () {

    $params = (object)['date' => date('Y-m-d')];

    $events = $this->getEventsList($params);

    $count = (!empty($events->list)) ? count($events->list) : 0;

    $data = (object)[
      'count' => $count
    ];

    return $data;
  }

  public function createEvent ($params) {
    $system = get_current_system();

    $title = $params->title;

    $event_content = $params->event_content;

    $event_date_start = $params->event_date_start;

    $data = (object)[];

    if ( !empty($title) && !empty($event_content) && !empty($event_date_start) ) {
      $date_start = date_create_from_format('d.m.Y', $event_date_start);
      $event_date_start = $date_start->format('Y-m-d');
      $day_week = $date_start->format('w');

      $error = ($day_week == 0 || $day_week == 6)? 1 : 0;

      if (!$error) {
        $holidays = ['2018-01-01', '2018-01-07', '2018-03-08', '2018-04-08', '2018-05-01', '2018-05-09', '2018-05-27', '2018-06-28', '2018-08-24', '2018-10-14', '2018-12-25'];

        foreach ( $holidays as $holiday ) {
          if ( $event_date_start == $holiday ) {
            $error = 1;
            break;
          }
        }
      }

      if (!$error) {
        $currentUser = \Drupal::currentUser();

        $node = Node::create([
          'type' => 'event',

          'title' => mb_substr($title, 0, 250, 'UTF-8'),

          'field_event_content' => $event_content,

          'field_event_date_start' => $event_date_start,

          'field_event_type' => 'own_event',

          'field_event_days' => 1,

          'uid' => $currentUser->Id(),

          'status' => 1,
        ]);

        $node->set('field_event_users', [$currentUser->Id()]);

        $node->set('field_systems', [$system->id]); //1 = экология 2 = от 3 = кадровик

        $node->save();

        $data = (object)[
          'neweventid' => $node->Id()
        ];

      } else {
        $data = (object)[
          'error' => 'Неможливо додати власну подію на цю дату!'
        ];
      }

    }

    return $data;

  }

  public function deleteEvent ($params) {
    $data = (object)[];

    $currentUser = \Drupal::currentUser();

    $account = User::load($currentUser->id());

    $nid = $params->id;

    $system = get_current_system();

    $node = Node::load($nid);

    if ( $node && $node->getType() == 'event' ) {
      $isAccess = 0;

      $ownerId = $node->getOwnerId();

      if ( $ownerId == $account->id() && $node->field_event_type->value == 'own_event' ) {
        $isAccess  = 1;
      }

      if ( $isAccess ) {

        $node->delete();

        $data = (object)[
          'id' => $nid
        ];

      }

    }

    return $data;
  }

  public function saveEvent ($params) {
    $data = (object)[];

    $currentUser = \Drupal::currentUser();

    $account = User::load($currentUser->id());

    $nid = $params->id;
    $content = $params->content;

    $system = get_current_system();

    $node = Node::load($nid);

    if ( $node && $node->getType() == 'event' ) {
      $isAccess = 0;

      $ownerId = $node->getOwnerId();

      if ( $ownerId == $account->id() && $node->field_event_type->value == 'own_event' ) {
        $isAccess  = 1;
      }

      if ( $isAccess ) {

        $node->set('field_event_content', $content);
        $node->save();

        $data = (object)[
          'id' => $nid
        ];

      }

    }

    return $data;
  }

  public static function getEvent ($params) {
    $data = (object)[];

    $currentUser = \Drupal::currentUser();

    $account = User::load($currentUser->id());

    $nid = $params->id;

    $system = get_current_system();

    $node = Node::load($nid);

    if ( $node && $node->getType() == 'event' ) {
      $isAccess = 0;

      $ownerId = $node->getOwnerId();

      if ( $ownerId == $account->id() || $account->field_edrpou->value == $node->field_event_edrpou->value ) {
        $isAccess  = 1;
      }

      if ( !$isAccess && !empty($node->get('field_event_type')->getValue()) ) {
        $event_type = $node->field_event_type->value;
        if ( in_array($event_type, ['report', 'event', 'professional_day']) ) {
          $isAccess  = 1;
        }
      }

      if ( !$isAccess && !empty($node->get('field_event_users')->getValue()) ) {
        $event_users = $node->get('field_event_users')->getValue();

        foreach ($event_users as $key => $val ) {
          if ($val['target_id'] == $account->id()) {
            $isAccess  = 1;
            break;
          }
        }

      }

      if ( $isAccess ) {

        $event_type = '';
        $event_type_label = '';
        if (!empty($node->get('field_event_type')->getValue())) {
          $event_types = $node->getFieldDefinition('field_event_type')->getFieldStorageDefinition()->getSetting('allowed_values');
          $event_type = $node->field_event_type->value;
          $event_type_label = $event_types[$node->field_event_type->value];
        }

        $event_content = '';
        $audit = false;
        if (!empty($node->get('field_event_content')->getValue())) {
          $event_content = $node->field_event_content->value;

          if ($node->field_event_type->value == 'audit') {

            $event_type_label = get_system_template('autit-event-label');;

            $query = \Drupal::entityQuery('node')
              ->condition('type', 'example')
              ->condition('field_systems', [$system->id, $system->parent_id], 'IN')
              ->condition('field_audit', 1)
              ->sort('field_sort_date', 'DESC')
              ->range(0, 5);

            $nids = $query->execute();

            $example_nodes = Node::loadMultiple($nids);

            $example_list = [];

            foreach ($example_nodes as $example_node) {
              $example_list[] = (object)[
                'id' => $example_node->Id(),
                'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$example_node->Id()),
                'title' => $example_node->getTitle()
              ];
            }

            $query = \Drupal::entityQuery('node')
              ->condition('type', 'accountinglog')
              ->condition('field_systems', [$system->id, $system->parent_id], 'IN')
              ->condition('field_audit', 1)
              ->sort('field_sort_date', 'DESC')
              ->range(0, 5);

            $nids = $query->execute();

            $accountinglog_nodes = Node::loadMultiple($nids);

            $accountinglog_list = [];

            foreach ($accountinglog_nodes as $accountinglog_node) {
              $accountinglog_list[] = (object)[
                'id' => $accountinglog_node->Id(),
                'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$accountinglog_node->Id()),
                'title' => $accountinglog_node->getTitle()
              ];
            }

            $query = \Drupal::entityQuery('node')
              ->condition('type', 'consultation')
              ->condition('field_systems', [$system->id, $system->parent_id], 'IN')
              ->condition('field_audit', 1);
            $group = $query->orConditionGroup()
              ->condition('field_consultation_type', 'consultation')
              ->condition('field_consultation_type', null, 'IS NULL');
            $query->condition($group);
            $group = $query->orConditionGroup()
              ->condition('field_user_only', 0)
              ->condition('field_user_only', null, 'IS NULL');
            $query->condition($group);
            $query->condition('field_status', ['viewed','placed'], 'IN')
              ->sort('field_sort_date', 'DESC')
              ->range(0, 5);

            $nids = $query->execute();

            $consultation_nodes = Node::loadMultiple($nids);

            $consultation_list = [];

            foreach ($consultation_nodes as $consultation_node) {
              $consultation_list[] = (object)[
                'id' => $consultation_node->Id(),
                'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$consultation_node->Id()),
                'title' => $consultation_node->getTitle()
              ];
            }

            $query = \Drupal::entityQuery('node')
              ->condition('type', 'interview')
              ->condition('field_systems', [$system->id, $system->parent_id], 'IN')
              ->condition('field_audit', 1)
              ->sort('field_sort_date', 'DESC')
              ->range(0, 5);

            $nids = $query->execute();

            $interview_nodes = Node::loadMultiple($nids);

            $interview_list = [];

            foreach ($interview_nodes as $interview_node) {
              $interview_list[] = (object)[
                'id' => $interview_node->Id(),
                'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$interview_node->Id()),
                'title' => $interview_node->getTitle()
              ];
            }


            $audit = (object)[
              'examples' => $example_list,
              'accountinglogs' => $accountinglog_list,
              'consultations' => $consultation_list,
              'interviews' => $interview_list
            ];
          }
        }

        $event_file = false;
        if (!empty($node->get('field_event_file')->getValue())) {
          $event_file = File::load($node->get('field_event_file')->getValue()[0]['target_id']);
          $file_uri = file_url_transform_relative(file_create_url($event_file->getFileUri()));
          $parts = explode('.', $file_uri);
          $file_extension = end($parts);
          $file_type = ($file_extension == 'pdf')? 'pdf' : 'doc';

          $event_file = (object)[
            'name' => basename($event_file->getFilename(), '.' . $file_extension),
            'uri' => $file_uri,
            'type' => $file_type
          ];
        }

        $data = (object)[
          'id' => intVal($node->Id()),
          'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
          'title' => $node->getTitle(),
          'event_type' => $event_type,
          'event_type_label' => $event_type_label,
          'event_content' => $event_content,
          'event_file' => $event_file,
          'event_date_start' => $node->field_event_date_start->value,
          'audit' => $audit,
          'audit_label_interviews' => get_system_template('audit-label-interviews'),
          'audit_label_consultations' => get_system_template('audit-label-consultations'),
          'audit_consultations_help' => get_system_template('audit-consultations-help'),
          'audit_label_examples' => get_system_template('audit-label-examples'),
          'audit_label_accountinglog' => get_system_template('audit-label-accountinglog')
        ];

      }

    }

    return $data;

  }

  public function getEventsList ($params) {

    $currentUser = \Drupal::currentUser();

    $account = User::load($currentUser->id());

    $selected_date = $params->date;

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }
    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);
    $query->addCondition('type', 'event')
      ->addCondition('field_event_date_start', $selected_date, '<=')
      ->addCondition('field_event_date_end_computed', $selected_date, '>=')
      ->addCondition('status', 1);

    $orGroup = $query->createConditionGroup('OR')
      ->addCondition('field_systems', $systems, 'IN')
      ->addCondition('field_event_type', ['national_holiday'], 'IN');
    $query->addConditionGroup($orGroup);

    $orGroup = $query->createConditionGroup('OR')
      ->addCondition('field_event_users', $account->id())
      ->addCondition('uid', $account->id())
      ->addCondition('field_event_type', ['report', 'event', 'professional_day', 'national_holiday'], 'IN');

    if ( !empty($account->field_edrpou->value) ) {
      $orGroup->addCondition('field_event_edrpou', $account->field_edrpou->value);
    }
    $query->addConditionGroup($orGroup);

    $query->sort('nid', 'DESC');
    $query->range(0, 99999);

    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $nodes = node_load_multiple($nids);
    $events = [];
    $event_types = [];
    foreach ($nodes as $node) {
      if (!empty($node->get('field_event_type')->getValue())) {
        $event_types = $node->getFieldDefinition('field_event_type')->getFieldStorageDefinition()->getSetting('allowed_values');
        $type = $node->field_event_type->value;
      }

      $events[$type][] = (object)[
        'id' => $node->Id(),
        'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$node->Id()),
        'title' => $node->getTitle(),
        'type' => $node->field_event_type->value,
        'begin' => $node->field_event_date_start->value,
        'end' => $node->field_event_date_end_computed->value
      ];
    }

    $filtered_events = [];
    $year = date('Y', strtotime($selected_date));
    $holidays = self::getHolidays((object)['year' => $year]);
    $additional_holidays = self::getAdditionalHolidays((object)['year' => $year]);
    $working_days = self::getWorkingdays((object)['year' => $year]);

    foreach($events as $type => $typed_events){
      foreach ($typed_events as $event) {
        $number = date('N', strtotime($selected_date));
        if ( $type !== 'audit' || ( $number != 6 && $number != 7 && !in_array($selected_date, $holidays) && !in_array($selected_date, $additional_holidays) || in_array($selected_date, $working_days) ) ) {
          $filtered_events[$type][] = $event;
        }
      }
    }

    $data = (object)[
      'list' => $filtered_events,
      'types' => $event_types,
      'date' => $selected_date
    ];


    return $data;
  }

  public function getEventsYear ($params) {
    $system = get_current_system();
    $currentUser = \Drupal::currentUser();
    $year = $params->year;
    $cid = 'konsultant:eventsyear:' . $year . ':' . $currentUser->id() . ':'. $system->id;
    $data = self::get_cache($cid, function() use ($params) {
      $year = $params->year;
      $newEventId = 0;
      if (!empty($params->neweventid) ) {
        $newEventId = $params->neweventid;
      }
      $date_start = ( $year - 1 ) . '-01-01';
      $nids = self::getEvents($year);

      $events = [];
      $holidays = [];
      $short_days = [];

      $found = false;

      foreach ($nids as $nid) {
        if ( $newEventId == $nid ) {
          $found = true;
        }
      }

      if ( !empty($newEventId) && !$found ) {
        $nids[] = $newEventId;
      }

      if (!empty($nids)) {
        $connection = Database::getConnection();
        $q = $connection->select('node_field_data', 'nd');
        $q->leftJoin('node__field_event_date_start', 'ds', 'ds.entity_id=nd.nid');
        $q->leftJoin('node__field_short_day', 'sd', 'sd.entity_id=nd.nid');
        $q->leftJoin('node__field_event_date_end_computed', 'de', 'de.entity_id=nd.nid');
        $q->leftJoin('node__field_event_type', 'et', 'et.entity_id=nd.nid');
        $q->condition('nd.nid', $nids, 'IN');
        $q->addField('nd', 'nid', 'nid');
        $q->addField('ds', 'field_event_date_start_value', 'event_date_start');
        $q->addField('sd', 'field_short_day_value', 'short_day');
        $q->addField('de', 'field_event_date_end_computed_value', 'event_date_end');
        $q->addField('et', 'field_event_type_value', 'event_type');
        $q->addField('nd', 'title', 'title');
        $q = $q->execute();
        $q = $q->fetchAll();

        $short_days = [];
        $additional_holidays = self::getAdditionalHolidays((object)['year' => $year]);
        $working_days = self::getWorkingdays((object)['year' => $year]);
        $holidays = self::getHolidays((object)['year' => $year]);

        foreach ($q as $item) {

          if (!empty($item->title)) {

            switch ( $item->event_type ) {
              case 'audit':
                $sorting = 1;
              break;
              case 'national_holiday':
                $sorting = 2;
              break;
              case 'event':
                $sorting = 3;
              break;
              case 'own_event':
                $sorting = 4;
              break;
              case 'report':
                $sorting = 5;
              break;
              case 'professional_day':
                $sorting = 6;
              break;
              default:
                $sorting = 7;
              break;
            }

          }

          $event_date_start = $item->event_date_start;
          $event_date_end = $item->event_date_end;

          if ($event_date_start < $date_start) {
            $event_date_start = $date_start;
          }

          if ( $item->short_day ) {
            $short_days[] = $item->short_day;
          }

          $nid = $item->nid;

          $events[$event_date_start][] = (object)[
            'id' => $nid,
            'title' => $item->title,
            'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$nid),
            'type' => $item->event_type,
            'date_start' => $event_date_start,
            'date_end' => $event_date_end,
            'sorting' => $sorting
          ];

          if ($event_date_start != $event_date_end) {
            $start = new \DateTime($event_date_start);
            $interval = new \DateInterval('P1D');
            $end = new \DateTime($event_date_end);
            $end = $end->modify( '+1 day' );

            $period = new \DatePeriod($start, $interval, $end, \DatePeriod::EXCLUDE_START_DATE);

            foreach ($period as $date) {
              $date = $date->format('Y-m-d');

              $events[$date][] = (object)[
                'id' => $nid,
                'title' => $item->title,
                'url' => \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$nid),
                'type' => $item->event_type,
                'date_start' => $event_date_start,
                'date_end' => $event_date_end,
                'sorting' => $sorting
              ];

            }
          }

        }

      }

      $filtered_events = [];

      foreach($events as $date => $date_events){
        foreach ($date_events as $date_event) {
          $number = date('N', strtotime($date));
          if ( $date_event->type !== 'audit' || ( $number != 6 && $number != 7 && !in_array($date, $holidays) && !in_array($date, $additional_holidays) || in_array($date, $working_days) ) ) {
            $filtered_events[$date][] = $date_event;
          }
        }
      }

      $datetime1 = new \DateTime($year . '-01-01');
      $datetime2 = new \DateTime($year . '-12-31');
      $interval = $datetime1->diff($datetime2);
      $days_count_year = $interval->format('%a') + 1;

      $datetime1 = new \DateTime($year . '-01-01');
      $datetime2 = new \DateTime($year . '-06-30');
      $interval = $datetime1->diff($datetime2);
      $days_count_h1 = $interval->format('%a') + 1;

      $datetime1 = new \DateTime($year . '-07-01');
      $datetime2 = new \DateTime($year . '-12-31');
      $interval = $datetime1->diff($datetime2);
      $days_count_h2 = $interval->format('%a') + 1;

      $days_count_q1_m1 = intval(date("t", strtotime($year . '-01-01')));
      $days_count_q1_m2 = intval(date("t", strtotime($year . '-02-01')));
      $days_count_q1_m3 = intval(date("t", strtotime($year . '-03-01')));
      $datetime1 = new \DateTime($year . '-01-01');
      $datetime2 = new \DateTime($year . '-03-' . $days_count_q1_m3);
      $interval = $datetime1->diff($datetime2);
      $days_count_q1 = $interval->format('%a') + 1;

      $days_count_q2_m1 = intval(date("t", strtotime($year . '-04-01')));
      $days_count_q2_m2 = intval(date("t", strtotime($year . '-05-01')));
      $days_count_q2_m3 = intval(date("t", strtotime($year . '-06-01')));
      $datetime1 = new \DateTime($year . '-04-01');
      $datetime2 = new \DateTime($year . '-06-' . $days_count_q2_m3);
      $interval = $datetime1->diff($datetime2);
      $days_count_q2 = $interval->format('%a') + 1;

      $days_count_q3_m1 = intval(date("t", strtotime($year . '-07-01')));
      $days_count_q3_m2 = intval(date("t", strtotime($year . '-08-01')));
      $days_count_q3_m3 = intval(date("t", strtotime($year . '-09-01')));
      $datetime1 = new \DateTime($year . '-07-01');
      $datetime2 = new \DateTime($year . '-09-' . $days_count_q3_m3);
      $interval = $datetime1->diff($datetime2);
      $days_count_q3 = $interval->format('%a') + 1;

      $days_count_q4_m1 = intval(date("t", strtotime($year . '-10-01')));
      $days_count_q4_m2 = intval(date("t", strtotime($year . '-11-01')));
      $days_count_q4_m3 = intval(date("t", strtotime($year . '-12-01')));
      $datetime1 = new \DateTime($year . '-10-01');
      $datetime2 = new \DateTime($year . '-12-' . $days_count_q4_m3);
      $interval = $datetime1->diff($datetime2);
      $days_count_q4 = $interval->format('%a') + 1;

      $year_holidays = [];

      foreach ( $holidays as $holiday ) {

        if ( strpos($holiday, (string)$year) !== false ) {
          $year_holidays[] = $holiday;
        }

      }

      $days_count_holidays_year = self::number_of_holiday_days($year . '-01-01', $year . '-12-' . $days_count_q4_m3);
      $days_count_holidays_h1 = self::number_of_holiday_days($year . '-01-01', $year . '-06-' . $days_count_q2_m3);
      $days_count_holidays_h2 = self::number_of_holiday_days($year . '-07-01', $year . '-12-' . $days_count_q4_m3);

      $days_count_holidays_q1 = self::number_of_holiday_days($year . '-01-01', $year . '-03-' . $days_count_q1_m3);
      $days_count_holidays_q1_m1 = self::number_of_holiday_days($year . '-01-01', $year . '-01-' . $days_count_q1_m1);
      $days_count_holidays_q1_m2 = self::number_of_holiday_days($year . '-02-01', $year . '-02-' . $days_count_q1_m2);
      $days_count_holidays_q1_m3 = self::number_of_holiday_days($year . '-03-01', $year . '-03-' . $days_count_q1_m3);

      $days_count_holidays_q2 = self::number_of_holiday_days($year . '-04-01', $year . '-06-' . $days_count_q2_m3);
      $days_count_holidays_q2_m1 = self::number_of_holiday_days($year . '-04-01', $year . '-04-' . $days_count_q2_m1);
      $days_count_holidays_q2_m2 = self::number_of_holiday_days($year . '-05-01', $year . '-05-' . $days_count_q2_m2);
      $days_count_holidays_q2_m3 = self::number_of_holiday_days($year . '-06-01', $year . '-06-' . $days_count_q2_m3);

      $days_count_holidays_q3 = self::number_of_holiday_days($year . '-07-01', $year . '-09-' . $days_count_q3_m3);
      $days_count_holidays_q3_m1 = self::number_of_holiday_days($year . '-07-01', $year . '-07-' . $days_count_q3_m1);
      $days_count_holidays_q3_m2 = self::number_of_holiday_days($year . '-08-01', $year . '-08-' . $days_count_q3_m2);
      $days_count_holidays_q3_m3 = self::number_of_holiday_days($year . '-09-01', $year . '-09-' . $days_count_q3_m3);

      $days_count_holidays_q4 = self::number_of_holiday_days($year . '-10-01', $year . '-12-' . $days_count_q4_m3);
      $days_count_holidays_q4_m1 = self::number_of_holiday_days($year . '-10-01', $year . '-10-' . $days_count_q4_m1);
      $days_count_holidays_q4_m2 = self::number_of_holiday_days($year . '-11-01', $year . '-11-' . $days_count_q4_m2);
      $days_count_holidays_q4_m3 = self::number_of_holiday_days($year . '-12-01', $year . '-12-' . $days_count_q4_m3);

      $days_count_national_holidays_year = count($year_holidays);
      $days_count_national_holidays_h1 = count(self::getHolidaysBetweenDates($year . '-01-01', $year . '-06-30'));
      $days_count_national_holidays_h2 = count(self::getHolidaysBetweenDates($year . '-07-01', $year . '-12-31'));

      $days_count_national_holidays_q1 = count(self::getHolidaysBetweenDates($year . '-01-01', $year . '-03-' . $days_count_q1_m3));
      $hq1m1 = self::getHolidaysBetweenDates($year . '-01-01', $year . '-01-' . $days_count_q1_m1);
      $days_count_national_holidays_q1_m1 = count($hq1m1);
      $hq1m2 = self::getHolidaysBetweenDates($year . '-02-01', $year . '-02-' . $days_count_q1_m2);
      $days_count_national_holidays_q1_m2 = count($hq1m2);
      $hq1m3 = self::getHolidaysBetweenDates($year . '-03-01', $year . '-03-' . $days_count_q1_m3);
      $days_count_national_holidays_q1_m3 = count($hq1m3);

      $days_count_national_holidays_q2 = count(self::getHolidaysBetweenDates($year . '-04-01', $year . '-06-' . $days_count_q2_m3));
      $hq2m1 = self::getHolidaysBetweenDates($year . '-04-01', $year . '-04-' . $days_count_q2_m1);
      $days_count_national_holidays_q2_m1 = count($hq2m1);
      $hq2m2 = self::getHolidaysBetweenDates($year . '-05-01', $year . '-05-' . $days_count_q2_m2);
      $days_count_national_holidays_q2_m2 = count($hq2m2);
      $hq2m3 = self::getHolidaysBetweenDates($year . '-06-01', $year . '-06-' . $days_count_q2_m3);
      $days_count_national_holidays_q2_m3 = count($hq2m3);

      $days_count_national_holidays_q3 = count(self::getHolidaysBetweenDates($year . '-07-01', $year . '-09-' . $days_count_q3_m3));
      $hq3m1 = self::getHolidaysBetweenDates($year . '-07-01', $year . '-07-' . $days_count_q3_m1);
      $days_count_national_holidays_q3_m1 = count($hq3m1);
      $hq3m2 = self::getHolidaysBetweenDates($year . '-08-01', $year . '-08-' . $days_count_q3_m2);
      $days_count_national_holidays_q3_m2 = count($hq3m2);
      $hq3m3 = self::getHolidaysBetweenDates($year . '-09-01', $year . '-09-' . $days_count_q3_m3);
      $days_count_national_holidays_q3_m3 = count($hq3m3);

      $days_count_national_holidays_q4 = count(self::getHolidaysBetweenDates($year . '-10-01', $year . '-12-' . $days_count_q4_m3));
      $hq4m1 = self::getHolidaysBetweenDates($year . '-10-01', $year . '-10-' . $days_count_q4_m1);
      $days_count_national_holidays_q4_m1 = count($hq4m1);
      $hq4m2 = self::getHolidaysBetweenDates($year . '-11-01', $year . '-11-' . $days_count_q4_m2);
      $days_count_national_holidays_q4_m2 = count($hq4m2);
      $hq4m3 = self::getHolidaysBetweenDates($year . '-12-01', $year . '-12-' . $days_count_q4_m3);
      $days_count_national_holidays_q4_m3 = count($hq4m3);

      $working_days_count_year = $days_count_year - $days_count_national_holidays_year - $days_count_holidays_year;
      $working_days_count_h1 = $days_count_h1 - $days_count_national_holidays_h1 - $days_count_holidays_h1;
      $working_days_count_h2 = $days_count_h2 - $days_count_national_holidays_h2 - $days_count_holidays_h2;

      $working_days_count_q1 = $days_count_q1 - $days_count_national_holidays_q1 - $days_count_holidays_q1;
      $working_days_count_q1_m1 = $days_count_q1_m1 - $days_count_national_holidays_q1_m1 - $days_count_holidays_q1_m1;
      $working_days_count_q1_m2 = $days_count_q1_m2 - $days_count_national_holidays_q1_m2 - $days_count_holidays_q1_m2;
      $working_days_count_q1_m3 = $days_count_q1_m3 - $days_count_national_holidays_q1_m3 - $days_count_holidays_q1_m3;

      $working_days_count_q2 = $days_count_q2 - $days_count_national_holidays_q2 - $days_count_holidays_q2;
      $working_days_count_q2_m1 = $days_count_q2_m1 - $days_count_national_holidays_q2_m1 - $days_count_holidays_q2_m1;
      $working_days_count_q2_m2 = $days_count_q2_m2 - $days_count_national_holidays_q2_m2 - $days_count_holidays_q2_m2;
      $working_days_count_q2_m3 = $days_count_q2_m3 - $days_count_national_holidays_q2_m3 - $days_count_holidays_q2_m3;

      $working_days_count_q3 = $days_count_q3 - $days_count_national_holidays_q3 - $days_count_holidays_q3;
      $working_days_count_q3_m1 = $days_count_q3_m1 - $days_count_national_holidays_q3_m1 - $days_count_holidays_q3_m1;
      $working_days_count_q3_m2 = $days_count_q3_m2 - $days_count_national_holidays_q3_m2 - $days_count_holidays_q3_m2;
      $working_days_count_q3_m3 = $days_count_q3_m3 - $days_count_national_holidays_q3_m3 - $days_count_holidays_q3_m3;

      $working_days_count_q4 = $days_count_q4 - $days_count_national_holidays_q4 - $days_count_holidays_q4;
      $working_days_count_q4_m1 = $days_count_q4_m1 - $days_count_national_holidays_q4_m1 - $days_count_holidays_q4_m1;
      $working_days_count_q4_m2 = $days_count_q4_m2 - $days_count_national_holidays_q4_m2 - $days_count_holidays_q4_m2;
      $working_days_count_q4_m3 = $days_count_q4_m3 - $days_count_national_holidays_q4_m3 - $days_count_holidays_q4_m3;

      $year_short_days = [];

      foreach ( $short_days as $short_day ) {

        if ( strpos($short_day, (string)$year) !== false ) {
          $year_short_days[] = $short_day;
        }

      }

      $short_days_count_year = count($year_short_days);

      $short_days_h1 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-01-01' && $short_day <= $year . '-06-30'  ) {
          $short_days_h1[] = $short_day;
        }
      }
      $short_days_count_h1 = count($short_days_h1);

      $short_days_h2 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-07-01' && $short_day <= $year . '-12-31' ) {
          $short_days_h2[] = $short_day;
        }
      }
      $short_days_count_h2 = count($short_days_h2);

      $short_days_q1 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-01-01' && $short_day <= $year . '-03-'. $days_count_q1_m3 ) {
          $short_days_q1[] = $short_day;
        }
      }
      $short_days_count_q1 = count($short_days_q1);

      $short_days_q1_m1 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-01-01' && $short_day <= $year . '-01-'. $days_count_q1_m1 ) {
          $short_days_q1_m1[] = $short_day;
        }
      }
      $short_days_count_q1_m1 = count($short_days_q1_m1);

      $short_days_q1_m2 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-02-01' && $short_day <= $year . '-02-'. $days_count_q1_m2 ) {
          $short_days_q1_m2[] = $short_day;
        }
      }
      $short_days_count_q1_m2 = count($short_days_q1_m2);

      $short_days_q1_m3 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-03-01' && $short_day <= $year . '-03-'. $days_count_q1_m3 ) {
          $short_days_q1_m3[] = $short_day;
        }
      }
      $short_days_count_q1_m3 = count($short_days_q1_m3);

      $short_days_q2 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-04-01' && $short_day <= $year . '-06-'. $days_count_q2_m3 ) {
          $short_days_q2[] = $short_day;
        }
      }
      $short_days_count_q2 = count($short_days_q2);

      $short_days_q2_m1 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-04-01' && $short_day <= $year . '-04-'. $days_count_q2_m1 ) {
          $short_days_q2_m1[] = $short_day;
        }
      }
      $short_days_count_q2_m1 = count($short_days_q2_m1);

      $short_days_q2_m2 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-05-01' && $short_day <= $year . '-05-'. $days_count_q2_m2 ) {
          $short_days_q2_m2[] = $short_day;
        }
      }
      $short_days_count_q2_m2 = count($short_days_q2_m2);

      $short_days_q2_m3 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-06-01' && $short_day <= $year . '-06-'. $days_count_q2_m3 ) {
          $short_days_q2_m3[] = $short_day;
        }
      }
      $short_days_count_q2_m3 = count($short_days_q2_m3);

      $short_days_q3 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-07-01' && $short_day <= $year . '-09-'. $days_count_q3_m3 ) {
          $short_days_q3[] = $short_day;
        }
      }
      $short_days_count_q3 = count($short_days_q3);

      $short_days_q3_m1 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-07-01' && $short_day <= $year . '-07-'. $days_count_q3_m1 ) {
          $short_days_q3_m1[] = $short_day;
        }
      }
      $short_days_count_q3_m1 = count($short_days_q3_m1);

      $short_days_q3_m2 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-08-01' && $short_day <= $year . '-08-'. $days_count_q3_m2 ) {
          $short_days_q3_m2[] = $short_day;
        }
      }
      $short_days_count_q3_m2 = count($short_days_q3_m2);

      $short_days_q3_m3 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-09-01' && $short_day <= $year . '-09-'. $days_count_q3_m3 ) {
          $short_days_q3_m3[] = $short_day;
        }
      }
      $short_days_count_q3_m3 = count($short_days_q3_m3);

      $short_days_q4 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-10-01' && $short_day <= $year . '-12-'. $days_count_q4_m3 ) {
          $short_days_q4[] = $short_day;
        }
      }
      $short_days_count_q4 = count($short_days_q4);

      $short_days_q4_m1 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-10-01' && $short_day <= $year . '-10-'. $days_count_q4_m1 ) {
          $short_days_q4_m1[] = $short_day;
        }
      }
      $short_days_count_q4_m1 = count($short_days_q4_m1);

      $short_days_q4_m2 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-11-01' && $short_day <= $year . '-11-'. $days_count_q4_m2 ) {
          $short_days_q4_m2[] = $short_day;
        }
      }
      $short_days_count_q4_m2 = count($short_days_q4_m2);

      $short_days_q4_m3 = [];
      foreach ( $year_short_days as $short_day ) {
        if ( $short_day >= $year . '-12-01' && $short_day <= $year . '-12-'. $days_count_q4_m3 ) {
          $short_days_q4_m3[] = $short_day;
        }
      }
      $short_days_count_q4_m3 = count($short_days_q4_m3);

      $production_calendar = [

        'year' => [
          //Кількість календарних днів
          'days_count_year' => $days_count_year,
          'days_count_h1' => $days_count_h1,
          'days_count_h2' => $days_count_h2,
          //Кількість святкових днів і днів релігійних свят
          'days_count_national_holidays_year' => $days_count_national_holidays_year,
          'days_count_national_holidays_h1' => $days_count_national_holidays_h1,
          'days_count_national_holidays_h2' => $days_count_national_holidays_h2,
          //Кількість вихідних днів
          'days_count_holidays_year' => $days_count_holidays_year,
          'days_count_holidays_h1' => $days_count_holidays_h1,
          'days_count_holidays_h2' => $days_count_holidays_h2,
          //Кількість днів, робота в які не проводиться
          'not_working_days_count_year' => $days_count_year - $working_days_count_year,
          'not_working_days_count_h1' => $days_count_h1 - $working_days_count_h1,
          'not_working_days_count_h2' => $days_count_h2 - $working_days_count_h2,
          //Кількість робочих днів
          'working_days_count_year' => $working_days_count_year,
          'working_days_count_h1' => $working_days_count_h1,
          'working_days_count_h2' => $working_days_count_h2,
          //Кількість днів, що передують святковим та неробочим, в які тривалість робочого дня (зміни) при 40-годинному тижні зменшується на одну годину
          'short_days_count_year' => $short_days_count_year,
          'short_days_count_h1' => $short_days_count_h1,
          'short_days_count_h2' => $short_days_count_h2,
          //При 40-годинному робочому тижні
          'hours_count_40_year' => $working_days_count_year * 8 - $short_days_count_year,
          'hours_count_40_h1' => $working_days_count_h1 * 8 - $short_days_count_h1,
          'hours_count_40_h2' => $working_days_count_h2 * 8 - $short_days_count_h2,
          //При 39-годинному робочому тижні
          'hours_count_39_year' => round($working_days_count_year * (8/40*39), 1),
          'hours_count_39_h1' => round($working_days_count_h1 * (8/40*39), 1),
          'hours_count_39_h2' => round($working_days_count_h2 * (8/40*39), 1),
          //При 38,5-годинному робочому тижні
          'hours_count_385_year' => round($working_days_count_year * (8/40*38.5), 1),
          'hours_count_385_h1' => round($working_days_count_h1 * (8/40*38.5), 1),
          'hours_count_385_h2' => round($working_days_count_h2 * (8/40*38.5), 1),
          //При 36-годинному робочому тижні
          'hours_count_36_year' => round($working_days_count_year * (8/40*36), 1),
          'hours_count_36_h1' => round($working_days_count_h1 * (8/40*36), 1),
          'hours_count_36_h2' => round($working_days_count_h2 * (8/40*36), 1),
          //При 33-годинному робочому тижні
          'hours_count_33_year' => round($working_days_count_year * (8/40*33), 1),
          'hours_count_33_h1' => round($working_days_count_h1 * (8/40*33), 1),
          'hours_count_33_h2' => round($working_days_count_h2 * (8/40*33), 1),
          //При 30-годинному робочому тижні
          'hours_count_30_year' => round($working_days_count_year * (8/40*30), 1),
          'hours_count_30_h1' => round($working_days_count_h1 * (8/40*30), 1),
          'hours_count_30_h2' => round($working_days_count_h2 * (8/40*30), 1),
          //При 25-годинному робочому тижні
          'hours_count_25_year' => round($working_days_count_year * (8/40*25), 1),
          'hours_count_25_h1' => round($working_days_count_h1 * (8/40*25), 1),
          'hours_count_25_h2' => round($working_days_count_h2 * (8/40*25), 1),
          //При 24-годинному робочому тижні
          'hours_count_24_year' => round($working_days_count_year * (8/40*24), 1),
          'hours_count_24_h1' => round($working_days_count_h1 * (8/40*24), 1),
          'hours_count_24_h2' => round($working_days_count_h2 * (8/40*24), 1),
          //При 20-годинному робочому тижні
          'hours_count_20_year' => round($working_days_count_year * (8/40*20), 1),
          'hours_count_20_h1' => round($working_days_count_h1 * (8/40*20), 1),
          'hours_count_20_h2' => round($working_days_count_h2 * (8/40*20), 1),
          //При 18-годинному робочому тижні
          'hours_count_18_year' => round($working_days_count_year * (8/40*18), 1),
          'hours_count_18_h1' => round($working_days_count_h1 * (8/40*18), 1),
          'hours_count_18_h2' => round($working_days_count_h2 * (8/40*18), 1),
        ],

        'q1' => [
          //Кількість календарних днів
          'days_count_q1' => $days_count_q1,
          'days_count_q1_m1' => $days_count_q1_m1,
          'days_count_q1_m2' => $days_count_q1_m2,
          'days_count_q1_m3' => $days_count_q1_m3,
          //Кількість святкових днів і днів релігійних свят
          'days_count_national_holidays_q1' => $days_count_national_holidays_q1,
          'days_count_national_holidays_q1_m1' => $days_count_national_holidays_q1_m1 . self::parseDaysList($hq1m1),
          'days_count_national_holidays_q1_m2' => $days_count_national_holidays_q1_m2 . self::parseDaysList($hq1m2),
          'days_count_national_holidays_q1_m3' => $days_count_national_holidays_q1_m3 . self::parseDaysList($hq1m3),
          //Кількість вихідних днів
          'days_count_holidays_q1' => $days_count_holidays_q1,
          'days_count_holidays_q1_m1' => $days_count_holidays_q1_m1,
          'days_count_holidays_q1_m2' => $days_count_holidays_q1_m2,
          'days_count_holidays_q1_m3' => $days_count_holidays_q1_m3,
          //Кількість днів, робота в які не проводиться
          'not_working_days_count_q1' => $days_count_q1 - $working_days_count_q1,
          'not_working_days_count_q1_m1' => $days_count_q1_m1 - $working_days_count_q1_m1,
          'not_working_days_count_q1_m2' => $days_count_q1_m2 - $working_days_count_q1_m2,
          'not_working_days_count_q1_m3' => $days_count_q1_m3 - $working_days_count_q1_m3,
          //Кількість робочих днів
          'working_days_count_q1' => $working_days_count_q1,
          'working_days_count_q1_m1' => $working_days_count_q1_m1,
          'working_days_count_q1_m2' => $working_days_count_q1_m2,
          'working_days_count_q1_m3' => $working_days_count_q1_m3,
          //Кількість днів, що передують святковим та неробочим, в які тривалість робочого дня (зміни) при 40-годинному тижні зменшується на одну годину
          'short_days_count_q1' => $short_days_count_q1,
          'short_days_count_q1_m1' => $short_days_count_q1_m1 . self::parseDaysList($short_days_q1_m1),
          'short_days_count_q1_m2' => $short_days_count_q1_m2 . self::parseDaysList($short_days_q1_m2),
          'short_days_count_q1_m3' => $short_days_count_q1_m3 . self::parseDaysList($short_days_q1_m3),
          //При 40-годинному робочому тижні
          'hours_count_40_q1' => $working_days_count_q1 * 8 - $short_days_count_q1,
          'hours_count_40_q1_m1' => $working_days_count_q1_m1 * 8 - $short_days_count_q1_m1,
          'hours_count_40_q1_m2' => $working_days_count_q1_m2 * 8 - $short_days_count_q1_m2,
          'hours_count_40_q1_m3' => $working_days_count_q1_m3 * 8 - $short_days_count_q1_m3,
          //При 39-годинному робочому тижні
          'hours_count_39_q1' => round($working_days_count_q1 * (8/40*39), 1),
          'hours_count_39_q1_m1' => round($working_days_count_q1_m1 * (8/40*39), 1),
          'hours_count_39_q1_m2' => round($working_days_count_q1_m2 * (8/40*39), 1),
          'hours_count_39_q1_m3' => round($working_days_count_q1_m3 * (8/40*39), 1),
          //При 38,5-годинному робочому тижні
          'hours_count_385_q1' => round($working_days_count_q1 * (8/40*38.5), 1),
          'hours_count_385_q1_m1' => round($working_days_count_q1_m1 * (8/40*38.5), 1),
          'hours_count_385_q1_m2' => round($working_days_count_q1_m2 * (8/40*38.5), 1),
          'hours_count_385_q1_m3' => round($working_days_count_q1_m3 * (8/40*38.5), 1),
          //При 36-годинному робочому тижні
          'hours_count_36_q1' => round($working_days_count_q1 * (8/40*36), 1),
          'hours_count_36_q1_m1' => round($working_days_count_q1_m1 * (8/40*36), 1),
          'hours_count_36_q1_m2' => round($working_days_count_q1_m2 * (8/40*36), 1),
          'hours_count_36_q1_m3' => round($working_days_count_q1_m3 * (8/40*36), 1),
          //При 33-годинному робочому тижні
          'hours_count_33_q1' => round($working_days_count_q1 * (8/40*33), 1),
          'hours_count_33_q1_m1' => round($working_days_count_q1_m1 * (8/40*33), 1),
          'hours_count_33_q1_m2' => round($working_days_count_q1_m2 * (8/40*33), 1),
          'hours_count_33_q1_m3' => round($working_days_count_q1_m3 * (8/40*33), 1),
          //При 30-годинному робочому тижні
          'hours_count_30_q1' => round($working_days_count_q1 * (8/40*30), 1),
          'hours_count_30_q1_m1' => round($working_days_count_q1_m1 * (8/40*30), 1),
          'hours_count_30_q1_m2' => round($working_days_count_q1_m2 * (8/40*30), 1),
          'hours_count_30_q1_m3' => round($working_days_count_q1_m3 * (8/40*30), 1),
          //При 25-годинному робочому тижні
          'hours_count_25_q1' => round($working_days_count_q1 * (8/40*25), 1),
          'hours_count_25_q1_m1' => round($working_days_count_q1_m1 * (8/40*25), 1),
          'hours_count_25_q1_m2' => round($working_days_count_q1_m2 * (8/40*25), 1),
          'hours_count_25_q1_m3' => round($working_days_count_q1_m3 * (8/40*25), 1),
          //При 24-годинному робочому тижні
          'hours_count_24_q1' => round($working_days_count_q1 * (8/40*24), 1),
          'hours_count_24_q1_m1' => round($working_days_count_q1_m1 * (8/40*24), 1),
          'hours_count_24_q1_m2' => round($working_days_count_q1_m2 * (8/40*24), 1),
          'hours_count_24_q1_m3' => round($working_days_count_q1_m3 * (8/40*24), 1),
          //При 20-годинному робочому тижні
          'hours_count_20_q1' => round($working_days_count_q1 * (8/40*20), 1),
          'hours_count_20_q1_m1' => round($working_days_count_q1_m1 * (8/40*20), 1),
          'hours_count_20_q1_m2' => round($working_days_count_q1_m2 * (8/40*20), 1),
          'hours_count_20_q1_m3' => round($working_days_count_q1_m3 * (8/40*20), 1),
          //При 18-годинному робочому тижні
          'hours_count_18_q1' => round($working_days_count_q1 * (8/40*18), 1),
          'hours_count_18_q1_m1' => round($working_days_count_q1_m1 * (8/40*18), 1),
          'hours_count_18_q1_m2' => round($working_days_count_q1_m2 * (8/40*18), 1),
          'hours_count_18_q1_m3' => round($working_days_count_q1_m3 * (8/40*18), 1),
        ],

        'q2' => [
          //Кількість календарних днів
          'days_count_q2' => $days_count_q2,
          'days_count_q2_m1' => $days_count_q2_m1,
          'days_count_q2_m2' => $days_count_q2_m2,
          'days_count_q2_m3' => $days_count_q2_m3,
          //Кількість святкових днів і днів релігійних свят
          'days_count_national_holidays_q2' => $days_count_national_holidays_q2,
          'days_count_national_holidays_q2_m1' => $days_count_national_holidays_q2_m1 . self::parseDaysList($hq2m1),
          'days_count_national_holidays_q2_m2' => $days_count_national_holidays_q2_m2 . self::parseDaysList($hq2m2),
          'days_count_national_holidays_q2_m3' => $days_count_national_holidays_q2_m3 . self::parseDaysList($hq2m3),
          //Кількість вихідних днів
          'days_count_holidays_q2' => $days_count_holidays_q2,
          'days_count_holidays_q2_m1' => $days_count_holidays_q2_m1,
          'days_count_holidays_q2_m2' => $days_count_holidays_q2_m2,
          'days_count_holidays_q2_m3' => $days_count_holidays_q2_m3,
          //Кількість днів, робота в які не проводиться
          'not_working_days_count_q2' => $days_count_q2 - $working_days_count_q2,
          'not_working_days_count_q2_m1' => $days_count_q2_m1 - $working_days_count_q2_m1,
          'not_working_days_count_q2_m2' => $days_count_q2_m2 - $working_days_count_q2_m2,
          'not_working_days_count_q2_m3' => $days_count_q2_m3 - $working_days_count_q2_m3,
          //Кількість робочих днів
          'working_days_count_q2' => $working_days_count_q2,
          'working_days_count_q2_m1' => $working_days_count_q2_m1,
          'working_days_count_q2_m2' => $working_days_count_q2_m2,
          'working_days_count_q2_m3' => $working_days_count_q2_m3,
          //Кількість днів, що передують святковим та неробочим, в які тривалість робочого дня (зміни) при 40-годинному тижні зменшується на одну годину
          'short_days_count_q2' => $short_days_count_q2,
          'short_days_count_q2_m1' => $short_days_count_q2_m1 . self::parseDaysList($short_days_q2_m1),
          'short_days_count_q2_m2' => $short_days_count_q2_m2 . self::parseDaysList($short_days_q2_m2),
          'short_days_count_q2_m3' => $short_days_count_q2_m3 . self::parseDaysList($short_days_q2_m3),
          //При 40-годинному робочому тижні
          'hours_count_40_q2' => $working_days_count_q2 * 8 - $short_days_count_q2,
          'hours_count_40_q2_m1' => $working_days_count_q2_m1 * 8 - $short_days_count_q2_m1,
          'hours_count_40_q2_m2' => $working_days_count_q2_m2 * 8 - $short_days_count_q2_m2,
          'hours_count_40_q2_m3' => $working_days_count_q2_m3 * 8 - $short_days_count_q2_m3,
          //При 39-годинному робочому тижні
          'hours_count_39_q2' => round($working_days_count_q2 * (8/40*39), 1),
          'hours_count_39_q2_m1' => round($working_days_count_q2_m1 * (8/40*39), 1),
          'hours_count_39_q2_m2' => round($working_days_count_q2_m2 * (8/40*39), 1),
          'hours_count_39_q2_m3' => round($working_days_count_q2_m3 * (8/40*39), 1),
          //При 38,5-годинному робочому тижні
          'hours_count_385_q2' => round($working_days_count_q2 * (8/40*38.5), 1),
          'hours_count_385_q2_m1' => round($working_days_count_q2_m1 * (8/40*38.5), 1),
          'hours_count_385_q2_m2' => round($working_days_count_q2_m2 * (8/40*38.5), 1),
          'hours_count_385_q2_m3' => round($working_days_count_q2_m3 * (8/40*38.5), 1),
          //При 36-годинному робочому тижні
          'hours_count_36_q2' => round($working_days_count_q2 * (8/40*36), 1),
          'hours_count_36_q2_m1' => round($working_days_count_q2_m1 * (8/40*36), 1),
          'hours_count_36_q2_m2' => round($working_days_count_q2_m2 * (8/40*36), 1),
          'hours_count_36_q2_m3' => round($working_days_count_q2_m3 * (8/40*36), 1),
          //При 33-годинному робочому тижні
          'hours_count_33_q2' => round($working_days_count_q2 * (8/40*33), 1),
          'hours_count_33_q2_m1' => round($working_days_count_q2_m1 * (8/40*33), 1),
          'hours_count_33_q2_m2' => round($working_days_count_q2_m2 * (8/40*33), 1),
          'hours_count_33_q2_m3' => round($working_days_count_q2_m3 * (8/40*33), 1),
          //При 30-годинному робочому тижні
          'hours_count_30_q2' => round($working_days_count_q2 * (8/40*30), 1),
          'hours_count_30_q2_m1' => round($working_days_count_q2_m1 * (8/40*30), 1),
          'hours_count_30_q2_m2' => round($working_days_count_q2_m2 * (8/40*30), 1),
          'hours_count_30_q2_m3' => round($working_days_count_q2_m3 * (8/40*30), 1),
          //При 25-годинному робочому тижні
          'hours_count_25_q2' => round($working_days_count_q2 * (8/40*25), 1),
          'hours_count_25_q2_m1' => round($working_days_count_q2_m1 * (8/40*25), 1),
          'hours_count_25_q2_m2' => round($working_days_count_q2_m2 * (8/40*25), 1),
          'hours_count_25_q2_m3' => round($working_days_count_q2_m3 * (8/40*25), 1),
          //При 24-годинному робочому тижні
          'hours_count_24_q2' => round($working_days_count_q2 * (8/40*24), 1),
          'hours_count_24_q2_m1' => round($working_days_count_q2_m1 * (8/40*24), 1),
          'hours_count_24_q2_m2' => round($working_days_count_q2_m2 * (8/40*24), 1),
          'hours_count_24_q2_m3' => round($working_days_count_q2_m3 * (8/40*24), 1),
          //При 20-годинному робочому тижні
          'hours_count_20_q2' => round($working_days_count_q2 * (8/40*20), 1),
          'hours_count_20_q2_m1' => round($working_days_count_q2_m1 * (8/40*20), 1),
          'hours_count_20_q2_m2' => round($working_days_count_q2_m2 * (8/40*20), 1),
          'hours_count_20_q2_m3' => round($working_days_count_q2_m3 * (8/40*20), 1),
          //При 18-годинному робочому тижні
          'hours_count_18_q2' => round($working_days_count_q2 * (8/40*18), 1),
          'hours_count_18_q2_m1' => round($working_days_count_q2_m1 * (8/40*18), 1),
          'hours_count_18_q2_m2' => round($working_days_count_q2_m2 * (8/40*18), 1),
          'hours_count_18_q2_m3' => round($working_days_count_q2_m3 * (8/40*18), 1),
        ],

        'q3' => [
          //Кількість календарних днів
          'days_count_q3' => $days_count_q3,
          'days_count_q3_m1' => $days_count_q3_m1,
          'days_count_q3_m2' => $days_count_q3_m2,
          'days_count_q3_m3' => $days_count_q3_m3,
          //Кількість святкових днів і днів релігійних свят
          'days_count_national_holidays_q3' => $days_count_national_holidays_q3,
          'days_count_national_holidays_q3_m1' => $days_count_national_holidays_q3_m1 . self::parseDaysList($hq3m1),
          'days_count_national_holidays_q3_m2' => $days_count_national_holidays_q3_m2 . self::parseDaysList($hq3m2),
          'days_count_national_holidays_q3_m3' => $days_count_national_holidays_q3_m3 . self::parseDaysList($hq3m3),
          //Кількість вихідних днів
          'days_count_holidays_q3' => $days_count_holidays_q3,
          'days_count_holidays_q3_m1' => $days_count_holidays_q3_m1,
          'days_count_holidays_q3_m2' => $days_count_holidays_q3_m2,
          'days_count_holidays_q3_m3' => $days_count_holidays_q3_m3,
          //Кількість днів, робота в які не проводиться
          'not_working_days_count_q3' => $days_count_q3 - $working_days_count_q3,
          'not_working_days_count_q3_m1' => $days_count_q3_m1 - $working_days_count_q3_m1,
          'not_working_days_count_q3_m2' => $days_count_q3_m2 - $working_days_count_q3_m2,
          'not_working_days_count_q3_m3' => $days_count_q3_m3 - $working_days_count_q3_m3,
          //Кількість робочих днів
          'working_days_count_q3' => $working_days_count_q3,
          'working_days_count_q3_m1' => $working_days_count_q3_m1,
          'working_days_count_q3_m2' => $working_days_count_q3_m2,
          'working_days_count_q3_m3' => $working_days_count_q3_m3,
          //Кількість днів, що передують святковим та неробочим, в які тривалість робочого дня (зміни) при 40-годинному тижні зменшується на одну годину
          'short_days_count_q3' => $short_days_count_q3,
          'short_days_count_q3_m1' => $short_days_count_q3_m1 . self::parseDaysList($short_days_q3_m1),
          'short_days_count_q3_m2' => $short_days_count_q3_m2 . self::parseDaysList($short_days_q3_m2),
          'short_days_count_q3_m3' => $short_days_count_q3_m3 . self::parseDaysList($short_days_q3_m3),
          //При 40-годинному робочому тижні
          'hours_count_40_q3' => $working_days_count_q3 * 8 - $short_days_count_q3,
          'hours_count_40_q3_m1' => $working_days_count_q3_m1 * 8 - $short_days_count_q3_m1,
          'hours_count_40_q3_m2' => $working_days_count_q3_m2 * 8 - $short_days_count_q3_m2,
          'hours_count_40_q3_m3' => $working_days_count_q3_m3 * 8 - $short_days_count_q3_m3,
          //При 39-годинному робочому тижні
          'hours_count_39_q3' => round($working_days_count_q3 * (8/40*39), 1),
          'hours_count_39_q3_m1' => round($working_days_count_q3_m1 * (8/40*39), 1),
          'hours_count_39_q3_m2' => round($working_days_count_q3_m2 * (8/40*39), 1),
          'hours_count_39_q3_m3' => round($working_days_count_q3_m3 * (8/40*39), 1),
          //При 38,5-годинному робочому тижні
          'hours_count_385_q3' => round($working_days_count_q3 * (8/40*38.5), 1),
          'hours_count_385_q3_m1' => round($working_days_count_q3_m1 * (8/40*38.5), 1),
          'hours_count_385_q3_m2' => round($working_days_count_q3_m2 * (8/40*38.5), 1),
          'hours_count_385_q3_m3' => round($working_days_count_q3_m3 * (8/40*38.5), 1),
          //При 36-годинному робочому тижні
          'hours_count_36_q3' => round($working_days_count_q3 * (8/40*36), 1),
          'hours_count_36_q3_m1' => round($working_days_count_q3_m1 * (8/40*36), 1),
          'hours_count_36_q3_m2' => round($working_days_count_q3_m2 * (8/40*36), 1),
          'hours_count_36_q3_m3' => round($working_days_count_q3_m3 * (8/40*36), 1),
          //При 33-годинному робочому тижні
          'hours_count_33_q3' => round($working_days_count_q3 * (8/40*33), 1),
          'hours_count_33_q3_m1' => round($working_days_count_q3_m1 * (8/40*33), 1),
          'hours_count_33_q3_m2' => round($working_days_count_q3_m2 * (8/40*33), 1),
          'hours_count_33_q3_m3' => round($working_days_count_q3_m3 * (8/40*33), 1),
          //При 30-годинному робочому тижні
          'hours_count_30_q3' => round($working_days_count_q3 * (8/40*30), 1),
          'hours_count_30_q3_m1' => round($working_days_count_q3_m1 * (8/40*30), 1),
          'hours_count_30_q3_m2' => round($working_days_count_q3_m2 * (8/40*30), 1),
          'hours_count_30_q3_m3' => round($working_days_count_q3_m3 * (8/40*30), 1),
          //При 25-годинному робочому тижні
          'hours_count_25_q3' => round($working_days_count_q3 * (8/40*25), 1),
          'hours_count_25_q3_m1' => round($working_days_count_q3_m1 * (8/40*25), 1),
          'hours_count_25_q3_m2' => round($working_days_count_q3_m2 * (8/40*25), 1),
          'hours_count_25_q3_m3' => round($working_days_count_q3_m3 * (8/40*25), 1),
          //При 24-годинному робочому тижні
          'hours_count_24_q3' => round($working_days_count_q3 * (8/40*24), 1),
          'hours_count_24_q3_m1' => round($working_days_count_q3_m1 * (8/40*24), 1),
          'hours_count_24_q3_m2' => round($working_days_count_q3_m2 * (8/40*24), 1),
          'hours_count_24_q3_m3' => round($working_days_count_q3_m3 * (8/40*24), 1),
          //При 20-годинному робочому тижні
          'hours_count_20_q3' => round($working_days_count_q3 * (8/40*20), 1),
          'hours_count_20_q3_m1' => round($working_days_count_q3_m1 * (8/40*20), 1),
          'hours_count_20_q3_m2' => round($working_days_count_q3_m2 * (8/40*20), 1),
          'hours_count_20_q3_m3' => round($working_days_count_q3_m3 * (8/40*20), 1),
          //При 18-годинному робочому тижні
          'hours_count_18_q3' => round($working_days_count_q3 * (8/40*18), 1),
          'hours_count_18_q3_m1' => round($working_days_count_q3_m1 * (8/40*18), 1),
          'hours_count_18_q3_m2' => round($working_days_count_q3_m2 * (8/40*18), 1),
          'hours_count_18_q3_m3' => round($working_days_count_q3_m3 * (8/40*18), 1),
        ],

        'q4' => [
          //Кількість календарних днів
          'days_count_q4' => $days_count_q4,
          'days_count_q4_m1' => $days_count_q4_m1,
          'days_count_q4_m2' => $days_count_q4_m2,
          'days_count_q4_m3' => $days_count_q4_m3,
          //Кількість святкових днів і днів релігійних свят
          'days_count_national_holidays_q4' => $days_count_national_holidays_q4,
          'days_count_national_holidays_q4_m1' => $days_count_national_holidays_q4_m1 . self::parseDaysList($hq4m1),
          'days_count_national_holidays_q4_m2' => $days_count_national_holidays_q4_m2 . self::parseDaysList($hq4m2),
          'days_count_national_holidays_q4_m3' => $days_count_national_holidays_q4_m3 . self::parseDaysList($hq4m3),
          //Кількість вихідних днів
          'days_count_holidays_q4' => $days_count_holidays_q4,
          'days_count_holidays_q4_m1' => $days_count_holidays_q4_m1,
          'days_count_holidays_q4_m2' => $days_count_holidays_q4_m2,
          'days_count_holidays_q4_m3' => $days_count_holidays_q4_m3,
          //Кількість днів, робота в які не проводиться
          'not_working_days_count_q4' => $days_count_q4 - $working_days_count_q4,
          'not_working_days_count_q4_m1' => $days_count_q4_m1 - $working_days_count_q4_m1,
          'not_working_days_count_q4_m2' => $days_count_q4_m2 - $working_days_count_q4_m2,
          'not_working_days_count_q4_m3' => $days_count_q4_m3 - $working_days_count_q4_m3,
          //Кількість робочих днів
          'working_days_count_q4' => $working_days_count_q4,
          'working_days_count_q4_m1' => $working_days_count_q4_m1,
          'working_days_count_q4_m2' => $working_days_count_q4_m2,
          'working_days_count_q4_m3' => $working_days_count_q4_m3,
          //Кількість днів, що передують святковим та неробочим, в які тривалість робочого дня (зміни) при 40-годинному тижні зменшується на одну годину
          'short_days_count_q4' => $short_days_count_q4,
          'short_days_count_q4_m1' => $short_days_count_q4_m1 . self::parseDaysList($short_days_q4_m1),
          'short_days_count_q4_m2' => $short_days_count_q4_m2 . self::parseDaysList($short_days_q4_m2),
          'short_days_count_q4_m3' => $short_days_count_q4_m3 . self::parseDaysList($short_days_q4_m3),
          //При 40-годинному робочому тижні
          'hours_count_40_q4' => $working_days_count_q4 * 8 - $short_days_count_q4,
          'hours_count_40_q4_m1' => $working_days_count_q4_m1 * 8 - $short_days_count_q4_m1,
          'hours_count_40_q4_m2' => $working_days_count_q4_m2 * 8 - $short_days_count_q4_m2,
          'hours_count_40_q4_m3' => $working_days_count_q4_m3 * 8 - $short_days_count_q4_m3,
          //При 39-годинному робочому тижні
          'hours_count_39_q4' => round($working_days_count_q4 * (8/40*39), 1),
          'hours_count_39_q4_m1' => round($working_days_count_q4_m1 * (8/40*39), 1),
          'hours_count_39_q4_m2' => round($working_days_count_q4_m2 * (8/40*39), 1),
          'hours_count_39_q4_m3' => round($working_days_count_q4_m3 * (8/40*39), 1),
          //При 38,5-годинному робочому тижні
          'hours_count_385_q4' => round($working_days_count_q4 * (8/40*38.5), 1),
          'hours_count_385_q4_m1' => round($working_days_count_q4_m1 * (8/40*38.5), 1),
          'hours_count_385_q4_m2' => round($working_days_count_q4_m2 * (8/40*38.5), 1),
          'hours_count_385_q4_m3' => round($working_days_count_q4_m3 * (8/40*38.5), 1),
          //При 36-годинному робочому тижні
          'hours_count_36_q4' => round($working_days_count_q4 * (8/40*36), 1),
          'hours_count_36_q4_m1' => round($working_days_count_q4_m1 * (8/40*36), 1),
          'hours_count_36_q4_m2' => round($working_days_count_q4_m2 * (8/40*36), 1),
          'hours_count_36_q4_m3' => round($working_days_count_q4_m3 * (8/40*36), 1),
          //При 33-годинному робочому тижні
          'hours_count_33_q4' => round($working_days_count_q4 * (8/40*33), 1),
          'hours_count_33_q4_m1' => round($working_days_count_q4_m1 * (8/40*33), 1),
          'hours_count_33_q4_m2' => round($working_days_count_q4_m2 * (8/40*33), 1),
          'hours_count_33_q4_m3' => round($working_days_count_q4_m3 * (8/40*33), 1),
          //При 30-годинному робочому тижні
          'hours_count_30_q4' => round($working_days_count_q4 * (8/40*30), 1),
          'hours_count_30_q4_m1' => round($working_days_count_q4_m1 * (8/40*30), 1),
          'hours_count_30_q4_m2' => round($working_days_count_q4_m2 * (8/40*30), 1),
          'hours_count_30_q4_m3' => round($working_days_count_q4_m3 * (8/40*30), 1),
          //При 25-годинному робочому тижні
          'hours_count_25_q4' => round($working_days_count_q4 * (8/40*25), 1),
          'hours_count_25_q4_m1' => round($working_days_count_q4_m1 * (8/40*25), 1),
          'hours_count_25_q4_m2' => round($working_days_count_q4_m2 * (8/40*25), 1),
          'hours_count_25_q4_m3' => round($working_days_count_q4_m3 * (8/40*25), 1),
          //При 24-годинному робочому тижні
          'hours_count_24_q4' => round($working_days_count_q4 * (8/40*24), 1),
          'hours_count_24_q4_m1' => round($working_days_count_q4_m1 * (8/40*24), 1),
          'hours_count_24_q4_m2' => round($working_days_count_q4_m2 * (8/40*24), 1),
          'hours_count_24_q4_m3' => round($working_days_count_q4_m3 * (8/40*24), 1),
          //При 20-годинному робочому тижні
          'hours_count_20_q4' => round($working_days_count_q4 * (8/40*20), 1),
          'hours_count_20_q4_m1' => round($working_days_count_q4_m1 * (8/40*20), 1),
          'hours_count_20_q4_m2' => round($working_days_count_q4_m2 * (8/40*20), 1),
          'hours_count_20_q4_m3' => round($working_days_count_q4_m3 * (8/40*20), 1),
          //При 18-годинному робочому тижні
          'hours_count_18_q4' => round($working_days_count_q4 * (8/40*18), 1),
          'hours_count_18_q4_m1' => round($working_days_count_q4_m1 * (8/40*18), 1),
          'hours_count_18_q4_m2' => round($working_days_count_q4_m2 * (8/40*18), 1),
          'hours_count_18_q4_m3' => round($working_days_count_q4_m3 * (8/40*18), 1),
        ],


      ];

      return (object)[
        'tags' => ['node_list:event'],
        'data' => (object)[
          'events' => $filtered_events,
          'types' => \Drupal::service('entity_field.manager')->getFieldDefinitions('node', 'event')['field_event_type']->getFieldStorageDefinition()->getSetting('allowed_values'),
          'holidays' => $holidays,
          'short_days' => $short_days,
          'working_days' => !empty($working_days) ? $working_days : [],
          'additional_holidays' => !empty($additional_holidays) ? $additional_holidays : [],
          'production_calendar' => $production_calendar
        ]
      ];

    });

    return $data;

  }

  public static function number_of_working_days($from, $to, $holidayDays) {
    $workingDays = [1, 2, 3, 4, 5];
    $from = new \DateTime($from);
    $to = new \DateTime($to);
    $to->modify('+1 day');
    $interval = new \DateInterval('P1D');
    $periods = new \DatePeriod($from, $interval, $to);
    $days = 0;
    foreach ($periods as $period) {
        if (!in_array($period->format('N'), $workingDays)) continue;
        if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
        $days++;
    }
    return $days;
  }

  public static function number_of_holiday_days($from, $to) {
    $holidayDays = [6, 7];
    $from = new \DateTime($from);
    $to = new \DateTime($to);
    $to->modify('+1 day');
    $interval = new \DateInterval('P1D');
    $periods = new \DatePeriod($from, $interval, $to);
    $days = 0;
    foreach ($periods as $period) {
        if (!in_array($period->format('N'), $holidayDays)) continue;
        $days++;
    }
    return $days;
  }

  public static function getHolidays ($params) {

    $year = $params->year;

    $date_start = ($year - 1) . '-01-01';

    $date_end = ($year + 1) . '-01-01';

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $query->addCondition('type', 'event')
    ->addCondition('field_event_date_start', $date_start, '>=')
    ->addCondition('field_event_date_start', $date_end, '<')
    ->addCondition('field_event_type', 'national_holiday')
    ->addCondition('status', 1);
    $query->sort('field_event_date_start', 'ASC');
    $query->range(0, 99999);

    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $nodes = node_load_multiple($nids);

    $data = [];

    foreach ($nodes as $node) {

      if ( !empty($node->field_event_date_start) && !empty($node->field_event_date_start->value)) {
        $data[] = $node->field_event_date_start->value;
      }

    }

    return $data;
  }

  public static function getEvents ($year) {

    $date_start = ($year - 1) . '-01-01';

    $date_end = ($year + 1) . '-12-31';

    $system = get_current_system();
    $systems = [$system->id];
    if ( !empty($system->parent_id) ) {
      $systems[] = $system->parent_id;
    }
    $currentUser = \Drupal::currentUser();
    $account = User::load($currentUser->id());

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $query->addCondition('type', 'event')
      ->addCondition('status', 1);

    $orGroup = $query->createConditionGroup('OR')
      ->addCondition('field_systems', $systems, 'IN')
      ->addCondition('field_event_type', ['national_holiday'], 'IN');

    $query->addConditionGroup($orGroup);

    $andGroup1 = $query->createConditionGroup('AND')
      ->addCondition('field_event_date_start', $date_start, '>=')
      ->addCondition('field_event_date_start', $date_end, '<=');

    $andGroup2 = $query->createConditionGroup('AND')
      ->addCondition('field_event_date_start', $date_start, '<=')
      ->addCondition('field_event_date_end_computed', $date_start, '>=');

    $orGroup = $query->createConditionGroup('OR')
      ->addConditionGroup($andGroup1)
      ->addConditionGroup($andGroup2);

    $query->addConditionGroup($orGroup);

    $orGroup = $query->createConditionGroup('OR')
      ->addCondition('field_event_users', $account->id())
      ->addCondition('field_event_type', ['report', 'event', 'professional_day', 'national_holiday'], 'IN');
    if ( $account->id() != 1 ) {
      $orGroup->addCondition('uid', $account->id());
    }

    if ( !empty($account->field_edrpou->value) ) {
      $orGroup->addCondition('field_event_edrpou', $account->field_edrpou->value);
    }

    $query->addConditionGroup($orGroup);

    $query->sort('field_event_date_start', 'ASC');
    $query->range(0, 99999);

    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };

    return $nids;

  }

  public static function getAdditionalHolidays ($params) {

    $year = $params->year;

    $date_start = ($year - 1) . '-01-01';

    $date_end = ($year + 1) . '-01-01';

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $query->addCondition('type', 'event')
    ->addCondition('field_event_date_start', $date_start, '>=')
    ->addCondition('field_event_date_start', $date_end, '<')
    ->addCondition('field_event_type', 'national_holiday')
    ->addCondition('status', 1);
    $query->sort('field_event_date_start', 'ASC');
    $query->range(0, 99999);

    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $nodes = Node::loadMultiple($nids);

    $data = [];

    foreach ($nodes as $node) {

      if ( !empty($node->field_additional_holiday) && !empty($node->field_additional_holiday->value)) {
        $data[] = $node->field_additional_holiday->value;
      }

    }

    return $data;
  }

  public static function parseDaysList ($dates) {

    $list = '';

    if ( !empty($dates) ) {

      $days = [];

      foreach ( $dates as $date ) {

        $day = substr($date, -2);
        if ( substr($day, 0, 1) == '0' ) {
          $day = substr($day, -1);
        }

        $days[] = $day;

      }

      $list = '(' . implode(',', $days) . ')';

    }

    return $list;

  }

  public static function getHolidaysBetweenDates ($date_start, $date_end ) {

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $query->addCondition('type', 'event')
    ->addCondition('field_event_date_start', $date_start, '>=')
    ->addCondition('field_event_date_start', $date_end, '<')
    ->addCondition('field_event_type', 'national_holiday')
    ->addCondition('status', 1);
    $query->sort('field_event_date_start', 'ASC');
    $query->range(0, 99999);

    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $nodes = Node::loadMultiple($nids);

    $data = [];

    foreach ($nodes as $node) {

      if ( !empty($node->field_event_date_start) && !empty($node->field_event_date_start->value)) {
        $data[] = $node->field_event_date_start->value;
      }

    }

    return $data;
  }

  public static function getAdditionalHolidaysBetweenDates ($date_start, $date_end ) {

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $query->addCondition('type', 'event')
    ->addCondition('field_event_date_start', $date_start, '>=')
    ->addCondition('field_event_date_start', $date_end, '<')
    ->addCondition('field_event_type', 'national_holiday')
    ->addCondition('status', 1);
    $query->sort('field_event_date_start', 'ASC');
    $query->range(0, 99999);

    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $nodes = Node::loadMultiple($nids);

    $data = [];

    foreach ($nodes as $node) {

      if ( !empty($node->field_additional_holiday) && !empty($node->field_additional_holiday->value)) {
        $data[] = $node->field_additional_holiday->value;
      }

    }

    return $data;
  }

  public static function getWorkingdays ($params) {

    $year = $params->year;

    $date_start = ($year - 1) . '-01-01';

    $date_end = ($year + 1) . '-01-01';

    $index = \Drupal\search_api\Entity\Index::load('default');
    $query = $index->query();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')->createInstance('direct');
    $query->setParseMode($parse_mode);

    $query->addCondition('type', 'event')
    ->addCondition('field_event_date_start', $date_start, '>=')
    ->addCondition('field_event_date_start', $date_end, '<')
    ->addCondition('field_event_type', 'national_holiday')
    ->addCondition('status', 1);
    $query->sort('field_event_date_start', 'ASC');
    $query->range(0, 99999);

    $result = $query->execute();
    $items = array_keys($result->getResultItems());
    $nids = [];
    foreach ($items as $item) {
      $nids[] = explode(':uk', explode('entity:node/', $item)[1])[0];
    };
    $nodes = Node::loadMultiple($nids);

    $data = [];

    foreach ($nodes as $node) {

      if ( !empty($node->field_additional_working_day) && !empty($node->field_additional_working_day->value)) {
        $data[] = $node->field_additional_working_day->value;
      }

    }

    return $data;
  }


}
