<?php
/**
 * @file
 * Contains \Drupal\konsultant\SystemConfigController.
 */

namespace Drupal\konsultant;


use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\entityQuery;

class SystemConfigController extends ControllerBase {

  public function content() {

  	return [
  		'#theme' => 'system_config',
      '#form_config' => []
  	];

  }

}
