<?php

namespace Drupal\konsultant\Cache;

use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Defines the MenuStateContext service, for per menu state caching.
 *
 * Cache context ID: 'konsultant_menu_state'.
 */
class MenuStateContext implements CacheContextInterface  {


	/**
	 * Constructor
	 */
	public function __construct()
	{

	}

	/**
	 * {@inheritdoc}
	 */
	public static function getLabel() {

		return t('Menu State');

	}

	/**
	 * {@inheritdoc}
	 */
	public function getContext($parameters = NULL) {
		return !empty($_COOKIE["menu-toggle"]) ? $_COOKIE["menu-toggle"] : 'false';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getCacheableMetadata($parameters = NULL) {

		return new CacheableMetadata();

	}

}
