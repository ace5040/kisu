<?php

namespace Drupal\konsultant\Cache;

use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Defines the MobileCacheContext service, for mobile or desktop caching.
 *
 * Cache context ID: 'mobile_detect'.
 */
class MobileCacheContext implements CacheContextInterface  {


	/**
	 * Constructor
	 */
	public function __construct(\Drupal\konsultant\Detect\MobileDetect $mobileDetector)
	{

		$this->mobileDetector = $mobileDetector;

	}

	/**
	 * {@inheritdoc}
	 */
	public static function getLabel() {

		return t('Mobile Detect');

	}

	/**
	 * {@inheritdoc}
	 */
	public function getContext($parameters = NULL) {

		$context = $this->mobileDetector->isMobile() ? 'mobile' : 'desktop';

		return $context;

	}

	/**
	 * {@inheritdoc}
	 */
	public function getCacheableMetadata($parameters = NULL) {

		return new CacheableMetadata();

	}

}