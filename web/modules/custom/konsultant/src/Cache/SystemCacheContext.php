<?php

namespace Drupal\konsultant\Cache;

use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Defines the SystemCacheContext service, for per system caching.
 *
 * Cache context ID: 'konsultant_system'.
 */
class SystemCacheContext implements CacheContextInterface  {


	/**
	 * Constructor
	 */
	public function __construct()
	{

	}

	/**
	 * {@inheritdoc}
	 */
	public static function getLabel() {

		return t('Current System');

	}

	/**
	 * {@inheritdoc}
	 */
	public function getContext($parameters = NULL) {
		$system = get_current_system();
		return $system->id;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getCacheableMetadata($parameters = NULL) {

		return new CacheableMetadata();

	}

}
