<?php
/**
 * @file
 * Contains \Drupal\konsultant\PagesController.
 */

namespace Drupal\konsultant;


use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\entityQuery;

class PagesController extends ControllerBase {

  public function content() {

  	return [
  		'#theme' => 'node_page',
  	];

  }

}
