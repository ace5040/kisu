<?php

namespace Drupal\konsultant\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\shs\Cache\ShsTermCacheDependency;
/**
 * Cacheable dependency object for term data.
 */
class KonsultantShsTermCacheDependency extends ShsTermCacheDependency {

  /**
   * {@inheritdoc}
   */
  public function __construct($tags = []) {
    $this->contexts = ['languages:language_interface', 'konsultant_system'];
    $this->tags = Cache::mergeTags(['taxonomy_term_values'], $tags);
    $this->maxAge = Cache::PERMANENT;
  }

}
