<?php
/**
 * @file
 * Contains \Drupal\konsultant\Controller\StatsController.
 */

namespace Drupal\konsultant\Controller;


use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Database;

class StatsController extends ControllerBase {

  public function content(Request $request) {

		$period_param = $request->query->get('period');
		$date_from_param = $request->query->get('date_from');
		$date_to_param = $request->query->get('date_to');
		$system_param = $request->query->get('system');
		$period = !empty($period_param) ? $period_param : 'today';
		$date_from = !empty($date_from_param) ? $date_from_param : false;
		$date_to = !empty($date_to_param) ? $date_to_param : false;
		$system = !empty($system_param) ? $system_param : 0;

		if ( !empty($period) ) {
			switch ( $period ) {
				case 'today':
					$date = new \DateTime();
					$date->setTime(0, 0, 0);
					$date_from = $date->getTimestamp();
					$date_to = false;
				break;
				case 'yesterday':
					$date = new \DateTime();
					$interval = new \DateInterval('P1D');
					$date->sub($interval);
					$date->setTime(0, 0, 0);
					$date_from = $date->getTimestamp();
					$date->setTime(23, 59, 59);
					$date_to = $date->getTimestamp();
				break;
				case 'thisWeek':
					$date = new \DateTime();
					$date->setISODate((int)$date->format('o'), (int)$date->format('W'), 1);
					$date->setTime(0, 0, 0);
					$date_from = $date->getTimestamp();
					$date_to = false;
				break;
				case 'lastWeek':
					$date = new \DateTime();
					$date->setISODate((int)$date->format('o'), (int)$date->format('W'), 1);
					$date->setTime(0, 0, 0);
					$date_to = $date->getTimestamp() - 1;
					$interval = new \DateInterval('P7D');
					$date->sub($interval);
					$date_from = $date->getTimestamp();
				break;
				case 'thisMonth':
					$date = new \DateTime('first day of this month');
					$date->setTime(0, 0, 0);
					$date_from = $date->getTimestamp();
					$date_to = false;
				break;
				case 'lastMonth':
					$date = new \DateTime('first day of this month');
					$date->setTime(0, 0, 0);
					$date_to = $date->getTimestamp() - 1;
					$interval = new \DateInterval('P1M');
					$date->sub($interval);
					$date_from = $date->getTimestamp();
				break;
				case 'thisYear':
					$date = new \DateTime('first day of January ' . date('Y'));
					$date->setTime(0, 0, 0);
					$date_from = $date->getTimestamp();
					$date_to = false;
				break;
				case 'lastYear':
					$date = new \DateTime('first day of January ' . date('Y'));
					$date->setTime(0, 0, 0);
					$date_to = $date->getTimestamp() - 1;
					$date = new \DateTime('first day of January ' . (date('Y') - 1));
					$date_from = $date->getTimestamp();
				break;
				case 'allTime':
					$date_from = false;
					$date_to = false;
				break;
				case 'customPeriod':
					$date_from = !empty($date_from) ? \DateTime::createFromFormat('d.m.Y', $date_from)->setTime(0, 0, 0)->getTimestamp() : false;
					$date_to = !empty($date_to) ? \DateTime::createFromFormat('d.m.Y', $date_to)->setTime(23, 59, 59)->getTimestamp() : false;
				break;
			}
		}

		$types = ['new', 'example', 'accountinglog', 'crib', 'instruction', 'interview', 'consultation', 'dkhp', 'event', 'interviewee', 'help', 'system_template', 'template'];
		$roles = ['expert', 'editor', 'administrator', 'root'];
		$role_labels = ['expert' => 'Експерт', 'editor' => 'Редактор', 'administrator' => 'Адміністратор', 'root' => 'Адміністратор Drupal'];

		$connection = Database::getConnection();
		$q = $connection->select('node_revision', 'nr');
		$q->leftJoin('users_field_data', 'ufd', 'ufd.uid=nr.revision_uid');
		$q->leftJoin('user__field_fio', 'uff', 'uff.entity_id=ufd.uid');
		$q->leftJoin('node_field_data', 'nfd', 'nfd.nid=nr.nid');
		$q->leftJoin('user__roles', 'ur', 'ur.entity_id=ufd.uid');
		$q->addField('ufd', 'uid', 'id');
		$q->addField('ufd', 'name', 'name');
		$q->addField('uff', 'field_fio_value', 'fio');
		$q->addField('ur', 'roles_target_id', 'role');
		$q->condition('nfd.type', $types, 'IN');
		if ( !empty($system) ) {
			$q->leftJoin('node__field_systems', 'nfs', 'nfs.entity_id=nr.nid');
			$q->condition('nfs.field_systems_target_id', $system);
		}
		$q->condition('ur.roles_target_id', $roles, 'IN');
		$q->groupBy('ufd.uid');
		$q->groupBy('ufd.name');
		$q->groupBy('uff.field_fio_value');
		$q->groupBy('ur.roles_target_id');
		$q->range(0, 100);
		$q->orderBy('ur.roles_target_id');
		$q->orderBy('uff.field_fio_value');
		$items = $q->execute()->fetchAll();

		$list=[];

		foreach ( $items as $item ) {
			$q = $connection->select('node_field_data', 'nfd');
			$q->condition('nfd.type', $types, 'IN');
			if(!empty($date_from)) {
				$q->condition('nfd.created', $date_from,'>=');
			}
			if(!empty($date_to)) {
				$q->condition('nfd.created', $date_to,'<=');
			}
			$q->condition('nfd.uid', $item->id);
			if ( !empty($system) ) {
				$q->leftJoin('node__field_systems', 'nfs', 'nfs.entity_id=nfd.nid');
				$q->condition('nfs.field_systems_target_id', $system);
			}
			$q->addExpression('COUNT(nfd.nid)', 'node_count');
			$q->addField('nfd', 'type', 'type');
			$q->groupBy('nfd.type');
			$addCounts = $q->execute()->fetchAll();

			$q = $connection->select('node_revision', 'nr');
			$q->leftJoin('node_field_data', 'nfd', 'nfd.nid=nr.nid');
			$q->condition('nfd.type', $types, 'IN');
			$q->condition('nr.revision_uid', $item->id);
			$q->addExpression("COUNT(nr.vid)", "node_count");
			if ( !empty($system) ) {
				$q->leftJoin('node__field_systems', 'nfs', 'nfs.entity_id=nr.nid');
				$q->condition('nfs.field_systems_target_id', $system);
			}
			if(!empty($date_from)) {
				$q->condition('nr.revision_timestamp', $date_from,'>=');
			}
			if(!empty($date_to)) {
				$q->condition('nr.revision_timestamp', $date_to,'<=');
			}
			$q->addExpression("COUNT(nr.vid)", "node_count");
			$q->where('nr.revision_timestamp <> nfd.created');
			$q->addField('nfd', 'type', 'type');
			$q->groupBy('nfd.type');
			$editCounts = $q->execute()->fetchAll();

			$listItem = (object)[];

			foreach ( $types as $type ) {
				$attrName = $type . '_add';
				$listItem->{$attrName} = 0;
				$attrName = $type . '_edit';
				$listItem->{$attrName} = 0;
			}

			foreach ( $addCounts as $addCount ) {
				$attrName = $addCount->type . '_add';
				$listItem->{$attrName} = intval($addCount->node_count);
			}

			foreach ( $editCounts as $editCount ) {
				$attrName = $editCount->type . '_edit';
				// $subAttrName = $editCount->type . '_add';
				// $sub = !empty($listItem->{$subAttrName}) ? $listItem->{$subAttrName} : 0;
				//$listItem->{$attrName} = intval($editCount->node_count) - $sub;
				$listItem->{$attrName} = intval($editCount->node_count);
			}

			$listItem->id = $item->id;
			$listItem->role = $role_labels[$item->role];
			$listItem->name = $item->name === 'admin' ? $item->name : "{$item->fio} ($item->name)";

			$list[] = $listItem;

		}

		$dateF = new \DateTime();
		$dateF = $dateF->setTimestamp($date_from)->format('d.m.Y');
		$dateT = new \DateTime();
		$dateT = $dateT->setTimestamp($date_to)->format('d.m.Y');
		$label = !empty($date_from) ? "з <b>{$dateF}</b>" : 'з самого початку';
		$label .= !empty($date_to) ? " по <b>{$dateT}</b>" : ' по цей час';

		return [
			'#theme' => 'doc_stats',
			'#label' => $label,
			'#list' => $list,
			'#period' => $period,
			'#date_from' => $date_from_param,
			'#date_to' => $date_to_param,
			'#activeSystem' => $system,
			'#systems' => self::getAllSystems()
		];

  }

	private static function getAllSystems () {
		$terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('system', false, null, true);
		$systems = [];
		foreach($terms as $term) {
			$systems[] = (object)[
				'id'=> $term->Id(),
				'name' => $term->getName()
			];
		}
		return $systems;
	}

}
