<?php

/**
 * @file
 * Contains \Drupal\konsultant\Controller\KonsultantController.
 */

namespace Drupal\konsultant\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Symfony\Component\HttpFoundation\Request;

class KonsultantController extends ControllerBase {

  public function userEditAccess($user) {
    $current_user = \Drupal::currentUser();
    $current_user_roles = $current_user->getRoles();
    $current_user_id = $current_user->Id();
    if ( $current_user_id == 1 ) {
      return AccessResult::allowed();
    } else {

      $user = user_load($user);
      $user_id = $user->Id();
      $user_roles = $user->getRoles();

      if ( in_array('administrator', $current_user_roles) ) {
        if ( in_array('editor', $user_roles) || in_array('editor_norm', $user_roles) ||  $current_user_id == $user_id ) {
          return AccessResult::allowed();
        }
      }

      if ( in_array('editor', $current_user_roles) ) {
        if ( in_array('expert', $user_roles)  ||  $current_user_id == $user_id ) {
          return AccessResult::allowed();
        }
      }

    }

    return AccessResult::forbidden();

  }

  public function userViewAccess($user) {
    $current_user = \Drupal::currentUser();
    $current_user_roles = $current_user->getRoles();
    $current_user_id = $current_user->Id();
    if ( $current_user_id == 1 ) {
      return AccessResult::allowed();
    } else {

      $user = user_load($user);
      $user_id = $user->Id();
      $user_roles = $user->getRoles();

      if ( in_array('administrator', $current_user_roles) ) {
        if ( in_array('editor', $user_roles) || in_array('editor_norm', $user_roles) ||  $current_user_id == $user_id ) {
          return AccessResult::allowed();
        }
      }

      if ( in_array('editor', $current_user_roles) ) {
        if ( in_array('expert', $user_roles)  ||  $current_user_id == $user_id ) {
          return AccessResult::allowed();
        }
      }

    }

    return AccessResult::forbidden();
  }

}

