<?php
/**
 * @file
 * Contains \Drupal\konsultant\Controller\NormController.
 */

namespace Drupal\konsultant\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Site\Settings;
use Drupal\konsultant\Controller\HtmltopdfController;

class NormController extends ControllerBase {

  private static $title = 'norm doc';
  private static $normdoc = [];

  public static function content(Request $request) {

    if ( empty(self::$normdoc) ) {
      self::$normdoc = self::getNorm($request->get('id'), $request->get('rev'));
    }

    $normdoc = self::$normdoc;

    if ( empty($normdoc) ) {
      self::$title = $normdoc->title;
      return [
        '#theme' => 'norm',
        '#id' => $normdoc->id,
        '#created' => $normdoc->created,
        '#date_created' => $normdoc->date_created,
        '#date_valid' => $normdoc->date_valid,
        '#date_invalid' => $normdoc->date_invalid,
        '#date_partially_invalid' => $normdoc->date_partially_invalid,
        '#revisions' => $normdoc->revisions,
        '#currentRevision' => $normdoc->currentRevision,
        '#currentRevisionStatus' => $normdoc->currentRevisionStatus,
        '#title' => !empty($normdoc->title) ? $normdoc->title : false,
        '#file' => !empty($normdoc->file) ? $normdoc->file : false,
        '#file_type' => !empty($normdoc->file_type) ? $normdoc->file_type : false,
        '#html' => $normdoc->html
      ];
    } else {
      self::$title = '';
      return [
        '#theme' => 'norm',
        '#id' => 0,
        '#created' => '',
        '#date_created' => '',
        '#date_valid' => '',
        '#date_invalid' => '',
        '#date_partially_invalid' => '',
        '#revisions' => [],
        '#currentRevision' => '',
        '#currentRevisionStatus' => '',
        '#title' => '',
        '#file' => false,
        '#file_type' => false,
        '#html' => ''
      ];
    }
  }

  public static function getTitle(Request $request) {

    if ( self::$title == 'norm doc' ) {
      self::content($request);
    }

    return  self::$title;

  }

  public static function getNorm ($id, $rev) {
    $id = explode('-', $id)[0];
    $params = (object)[
      'id' => $id
    ];
    if ( !empty($rev) ) {
      $params->revision = $rev;
    }

    $normdoc = self::sendApiCommand('getNorm', $params);
    $domain = Settings::get('norm_api_url', '');

    $html = '';
    if ( isset($normdoc->html) ) {
      if ( !empty($normdoc->html) ) {
        $isAnonymous = \Drupal::currentUser()->isAnonymous();
        if ( !$isAnonymous ) {
          $html = str_replace(' src="/sites/', ' src="' . $domain . '/sites/', $html);
          $html = str_replace(' href="/sites/', ' href="' . $domain . '/sites/', $html);
        } else {
          $html = '';
        }
      }
      $normdoc->html = $html;
    } else {
      $normdoc = false;
    }

    return $normdoc;

  }

  public static function pdf (Request $request) {

    $id = \Drupal::routeMatch()->getParameter('id');
    $rev = \Drupal::routeMatch()->getParameter('rev');
    $normdoc = NormController::getNorm($id, $rev);

    $twig = \Drupal::service('twig');
    $template = $twig->loadTemplate('themes/konsultant_v3/templates/printable.html.twig');

    $html = $template->render([]);

    $response = new Response(
      'Service is temporary unavailable',
      Response::HTTP_OK,
      ['content-type' => 'text/html']
    );

    if ( !empty($html) ) {

      $directory = 'public://normpdf/';
      $file_name = substr(livejournal_transliteration_filename($normdoc->data->title), 0, 32) . '-' . $normdoc->data->id . '.pdf';

      if ( !file_exists($directory . $file_name) ) {

        $pdf = HtmltopdfController::convert($html);
        if ( !empty($pdf) ) {
          file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
          file_unmanaged_save_data($pdf, $directory . $file_name , FILE_EXISTS_REPLACE);
        } else {
          return $response;
        }

      }

      $response = new RedirectResponse(file_create_url($directory . $file_name));

    }

    return $response;

  }

  public static function print (Request $request) {

    $twig = \Drupal::service('twig');
    $template = $twig->loadTemplate('themes/konsultant_v3/templates/printable.html.twig');

    $html = $template->render([]);

    $data = $html;
    $response = new Response();
    $response->headers->set('Content-Type', 'text/html');
    $response->setContent($data);
    return $response;

  }

  private static function sendApiCommand ($command, $params) {

    $norm_api_url = Settings::get('norm_api_url', '');
    $user = Settings::get('norm_api_user', '');
    $password = Settings::get('norm_api_pass', '');

    $ch = curl_init();
    $post = [
      'command' => $command,
      'params' => json_encode($params)
    ];
    curl_setopt($ch, CURLOPT_URL, $norm_api_url . '/api');
    curl_setopt($ch, CURLOPT_USERPWD, $user . ":" . $password);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
    $data = curl_exec($ch);
    curl_close($ch);
    if ( !empty($data) ) {
      return json_decode($data)->data;
    } else {
      return (object)[];
    }
  }

}
