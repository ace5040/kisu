<?php
/**
 * @file
 * Contains \Drupal\konsultant\FrontpageController.
 */

namespace Drupal\konsultant;


use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\entityQuery;

class FrontpageController extends ControllerBase {

  public function content() {

  	return [
  		'#theme' => 'front_page',
  	];

  }

  public function fixCompanyField() {

    konsultant_change_text_field_max_length('user', 'field_company', 200);

  	return [
  		'#theme' => 'front_page',
  	];

  }

  public function sendExpertMessage(Request $request) {

    $expert_id = $request->get('expert_id');
    $destination = $request->query->get('destination');

    if ( !empty($expert_id) ) {
      $expert = user_load($expert_id);

      if ( !empty($expert) ) {

        $expert_roles = $expert->getRoles();

        if ( in_array('expert', $expert_roles) ) {

          $user = \Drupal::currentUser();
          $user_roles = $user->getRoles();

          if ( in_array('editor', $user_roles) || in_array('administrator', $user_roles) || $user->id() == 1 ) {

            $system = get_current_system();

            $vars = [];
            $login = '';
            $password = '';
            $fio = '';

            if ( !empty($expert->getUsername()) ) {
              $login = $expert->getUsername();
            }

            if ( !empty($expert->field_password) && !empty($expert->field_password->getValue()) ) {
              $password = $expert->field_password->getValue()[0]['value'];
            }

            if ( !empty($expert->field_fio) && !empty($expert->field_fio->getValue()) ) {
              $fio = $expert->field_fio->getValue()[0]['value'];
            }

            $vars['login'] = $login;
            $vars['password'] = $password;
            $vars['fio'] = $fio;

            $tmpl = get_system_template_email('expert-access-email', $vars);

            if ( !empty( $tmpl->subject ) ) {
              $subject = $tmpl->subject;
            } else {
              $subject = 'Доступ до платформи ' . $system->name;
            }

            konsultant_send_message($expert_id, $tmpl->subject, $tmpl->html, $tmpl);

          }

        }

      }

    }

    if ( empty($destination) ) {
      $destination = '/admin/editor-users';
    }

    $response = new RedirectResponse($destination);

    return $response;

  }

}
