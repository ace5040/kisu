<?php
/**
 * @file
 * Contains \Drupal\syncapi\Controller\SyncAPIController.
 */

namespace Drupal\syncapi\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Drupal\user\Entity\User;
use Drupal\Core\Site\Settings;

class SyncAPIController extends ControllerBase {

  public function main(Request $request) {
    $response = [
      'Результат' => false
    ];

    try {
      if (!empty($request)) {
        $data = $request->getContent();
        //file_put_contents('debug.txt', $data, FILE_APPEND);
        \Drupal::logger('syncapi-raw')->debug($data);
        $data = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
        return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');}, $data);
        //$data = stripcslashes($data);
        //$data = substr($data, 1);
        //$data = substr($data, 0, -1);
        //file_put_contents('parsed.txt', $data, FILE_APPEND);
        \Drupal::logger('syncapi-parsed')->debug($data);
        $data = Json::decode($data);
        \Drupal::logger('syncapi-decoded')->debug('<pre>' . print_r($data, 1) . '</pre>');
        if ( !empty($data) && !empty($data['Функция']) ) {
          $command = $data['Функция'];
          switch ($command) {
            case 'SetUser':
              $response = $this->setUser($data);
            break;
            case 'SetProduct':
              $response = $this->setProduct($data);
            break;
            case 'GetUserAccess':
              $response = $this->getUserAccess($data);
            break;
            case 'Ping':
              $response = [
                'Результат' => true
              ];
            break;
          }
        } else {
          $response = 'empty request';
        }

      }

    } catch (\Exception $exception) {
      \Drupal::logger('syncapi')->error($exception->getMessage());
      return new Response('error' , Response::HTTP_INTERNAL_SERVER_ERROR, ['Content-Type' => 'text/html']);
    }

    return new JsonResponse($response, 200, ['Content-Type'=> 'application/json']);
  }

  private static function createUser($customer, $system) {

    $access_levels = Settings::get('access_levels', []);

    $user_create = User::create();
    $user_create->enforceIsNew();
    $user_create->setPassword($customer['password']);
    $user_create->setEmail( $customer['login'] . '.' . $customer['email'] . '-' . $system->id );
    $user_create->setUsername($customer['login']);
    $user_create->set('field_system', $system->id);
    $user_create->set('field_access_type', $customer['access_type']);
    $user_create->set('field_user_email', $customer['email']);
    $user_create->set('field_fio', $customer['contact_name']);
    $user_create->set('field_company', $customer['client_name']);
    $user_create->set('field_edrpou', $customer['client_edrpo']);
    $user_create->set('field_password', $customer['password']);
    $user_create->set('field_access_id', $customer['id']);
    $user_create->set('field_user_id', $customer['contact_id']);
    $user_create->set('field_company_id', $customer['client_id']);
    $user_create->set('field_access_begin', substr($customer['date_begin'], 0, 10));
    $user_create->set('field_product_id', $customer['product_id']);
    $access_level = ( !empty($customer['product_id']) && !empty($access_levels) && !empty($access_levels[$customer['product_id']]) ) ? $access_levels[$customer['product_id']] : 'start';
    $user_create->set('field_access_level', $access_level);

    $user_create->activate();

    if ( $customer['access_type'] == 'full' ) {
      $user_create->addRole('customer');
      $user_create->set('field_access_end', substr($customer['date_end'], 0, 10));
    } else {
      $user_create->addRole('demo');
      $week = date_create_from_format('Y-m-d', substr($customer['date_begin'], 0, 10));
      date_add($week, date_interval_create_from_date_string("7 days"));
      $week = date('Y-m-d', date_timestamp_get($week));
      $user_create->set('field_access_end', $week);
    }

    $user_create->save();

    \Drupal::logger('syncapi-SetUser created')->debug(dbg($customer));

    return $user_create;

  }

  private static function sendEmail($user_id, $customer, $system) {

    $vars = [];
    $vars['password'] = $customer['password'];
    $vars['login'] = $customer['login'];
    $vars['fio'] = $customer['contact_name'];

    if ( $customer['access_type'] == 'full' ) {
      $tmpl = get_system_template_email('new-user-access-email', $vars);
      $subject = t('Доступ до платформи') . ' ' . $system->name;
    } else {
      $tmpl = get_system_template_email('new-demo-access-email', $vars);
      $subject = t('Демо-доступ до платформи') . ' ' . $system->name;
    }

    if ( !empty( $tmpl->subject ) ) {
      $subject = $tmpl->subject;
    }

    konsultant_send_message($user_id, $subject, $tmpl->html, $tmpl);

  }

  private static function updateUser($found_user, $customer, $system) {

    $access_levels = Settings::get('access_levels', []);

    $found_user->setPassword($customer['password']);
    $found_user->setEmail( $customer['login'] . '.' . $customer['email'] . '-' . $system->id );
    $found_user->setUsername($customer['login']);
    $found_user->set('field_user_email', $customer['email']);
    $found_user->set('field_fio', $customer['contact_name']);
    $found_user->set('field_company', $customer['client_name']);
    $found_user->set('field_edrpou', $customer['client_edrpo']);
    $found_user->set('field_password', $customer['password']);
    $found_user->set('field_access_id', $customer['id']);
    $found_user->set('field_user_id', $customer['contact_id']);
    $found_user->set('field_company_id', $customer['client_id']);
    $found_user->set('field_access_begin', substr($customer['date_begin'], 0, 10));
    $found_user->set('field_access_end', substr($customer['date_end'], 0, 10));
    $access_level = ( !empty($customer['product_id']) && !empty($access_levels) && !empty($access_levels[$customer['product_id']]) ) ? $access_levels[$customer['product_id']] : 'start';
    $found_user->set('field_access_level', $access_level);

    if ( $customer['access_type'] == 'full' && $found_user->hasRole('demo') ) {
      $found_user->removeRole('demo');
      $found_user->addRole('customer');
    }

    if ( $customer['access_type'] == 'demo' && $found_user->hasRole('customer') ) {
      $found_user->addRole('demo');
      $found_user->removeRole('customer');
      $week = date_create_from_format('Y-m-d', substr($customer['date_begin'], 0, 10));
      date_add($week, date_interval_create_from_date_string("7 days"));
      $week = date('Y-m-d', date_timestamp_get($week));
      $found_user->set('field_access_end', $week);
    }

    $found_user->save();

    \Drupal::logger('syncapi-SetUser update')->debug(dbg($customer));

  }

  private function setUser($data) {
    \Drupal::logger('syncapi-setUser data')->debug('<pre>' . print_r($data, 1) . '</pre>');

    $system = get_current_system();

    $result = false;

    $customer = $data['Данные']['data']['refUsers'][0];

    if ( !empty($customer) ) {

      \Drupal::logger('syncapi-SetUser customer')->debug(dbg($customer));

      $found_user_id = self::findUser($customer);

      \Drupal::logger('syncapi-SetUser found id')->debug(dbg($found_user_id));

      $user_email = $customer['email'];

      if ( !empty($user_email) ) {

        if ( empty($found_user_id) ) {
          $user_create = self::createUser($customer, $system);
          $user_id = $user_create->id();
          self::sendEmail($user_id, $customer, $system);
          $result = 'true';
    	  } else {
          $found_user = user_load($found_user_id);
          if  ( !empty($found_user) ) {
            self::updateUser($found_user, $customer, $system);
            $user_id = $found_user->id();
            self::sendEmail($user_id, $customer, $system);
            $result = 'true';
          }
        }
      }

    } else {

      \Drupal::logger('syncapi-SetUser')->debug('empty customer');

    }

	  return [
      'Результат' => $result
    ];

  }


  private function setProduct($data) {
    \Drupal::logger('syncapi-setProduct')->debug('<pre>' . print_r($data, 1) . '</pre>');

    $response = [
      'Результат' => true
    ];

    return $response;
  }


  private function getUserAccess($data) {

    \Drupal::logger('syncapi-getUserAccess')->debug('<pre>' . print_r($data, 1) . '</pre>');

    $result = '';
    $data = $data['Данные']['ID'];
    $system = get_current_system();
    $query = \Drupal::entityQuery('user');
    $query->condition('field_system', $system->id);
    $query->condition('field_access_id', $data);
    $uid = $query->execute();
    $uid = reset($uid);

    if ( !empty($uid) ) {
      $user = user_load($uid);
      $last_access = $user->getLastLoginTime();
      if ( !empty($last_access) ) {
        $date_access = date('d.m.Y H:i:s', $last_access);
        $result = $date_access;
      }
    }

    \Drupal::logger('syncapi-getUserAccess response')->debug('<pre>' . print_r($result, 1) . '</pre>');

    return [
      'Результат' => $result
    ];
  }

  private static function findUser($customer) {
    $system = get_current_system();
    $query = \Drupal::entityQuery('user');
    $query->condition('field_access_id', $customer['id']);
    $uid = $query->execute();
    \Drupal::logger('soapsync-SetUser-edit')->debug(dbg($uid));
    if ( empty($uid) ) {
      $query = \Drupal::entityQuery('user');
      $query->condition('field_company_id', $customer['client_id']);
      $query->condition('field_user_id', $customer['contact_id']);
      $query->condition('field_system', $system->id);
      $uid = $query->execute();
      \Drupal::logger('soapsync-SetUser-update')->debug(dbg($uid));
    }
    return !empty($uid) ? current($uid) : false;
  }

}
